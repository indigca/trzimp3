=Code.RA(
""& VbCrLf &
"--Uncomment the next DECLARE/SET statements for testing the query on Management Studio"& VbCrLf &
"--2013 must be replaced in the whole query by the year of the archive is going to be tested (e.g. 2013)"& VbCrLf &
"DECLARE @Language nvarchar(2);"& VbCrLf &
"SET @Language = 'es';"& VbCrLf &
"DECLARE @Company nvarchar(3);"& VbCrLf &
"SET @Company = '036';"& VbCrLf &
"DECLARE @Starttime datetime;"& VbCrLf &
"SET @Starttime = '1-1-2013';"& VbCrLf &
"DECLARE @Endtime datetime;"& VbCrLf &
"SET @Endtime = (SELECT GETDATE());"& VbCrLf &
"DECLARE @Region nvarchar(2);"& VbCrLf &
"SET @Region = '16';"& VbCrLf &
"DECLARE @validated int;"& VbCrLf &
"SET @validated = 0;"& VbCrLf &
""& VbCrLf &
"/*Outages"& VbCrLf &
"It retrieves info about all the aoutages in the system"& VbCrLf &
"*/"& VbCrLf &
"WITH outages AS ( "& VbCrLf &
"	--First UNION get the outages from MVOutageReport"& VbCrLf &
"	SELECT	dbo.GETNOTNULL(  "& VbCrLf &
" 								dbo.GETCODEINFO('FAULTREASON', FAULTREASON),  "& VbCrLf &
" 								dbo.GETCODEINFO('MAINTOUTAGEREASON', MAINTOUTAGEREASON)  "& VbCrLf &
" 												 ) AS REASON,  "& VbCrLf &
"					NUMBER, STARTINGTIME, ENDINGTIME, TYPE, Duration, 1 AS MV, Cust"& VbCrLf &
"	FROM		MVOutageReport2013 "& VbCrLf &
"	UNION "& VbCrLf &
"	--Second UNION get the outages from FaultReport"& VbCrLf &
"	SELECT	FAULTREASON AS REASON, "& VbCrLf &
"					NUMBER, STARTINGTIME, ENDINGTIME, 0 AS TYPE, Duration, 1 AS MV, Cust "& VbCrLf &
"	FROM		FaultReport2013 "& VbCrLf &
"	UNION "& VbCrLf &
"	--Third UNION get the outages from MaintenanceOutageReport"& VbCrLf &
"	SELECT	MAINTOUTAGEREASON AS REASON,  "& VbCrLf &
"					NUMBER, STARTINGTIME, ENDINGTIME, 1 AS TYPE, DURATION AS Duration, 1 AS MV, CUST AS Cust"& VbCrLf &
"	FROM		MaintenanceOutageReport2013 "& VbCrLf &
"    UNION"& VbCrLf &
"	--Fourth UNION get the outages from LVOutageReport"& VbCrLf &
"	SELECT	dbo.GETCODEINFO('FAULTREASON', LVFaultReason) AS REASON,  "& VbCrLf &
"					NUMBER, STARTINGTIME, ENDINGTIME, TYPE, Duration, 0 AS MV, Cust"& VbCrLf &
"	FROM		LVOutageReport2013"& VbCrLf &
"),"& VbCrLf &
"/*SPLIT_ACOMETIDAS"& VbCrLf &
"Uses the function dbo.SplitStrings to return as many rows as acometidas where affected by the outage. "& VbCrLf &
"It returns results just for outages with acometidas affected"& VbCrLf &
"*/"& VbCrLf &
"SPLIT_ACOMETIDAS AS(SELECT OA.NUMBER, OA.STARTINGTIME, OA.ENDINGTIME, f.Value"& VbCrLf &
"  FROM dbo.OutageAreas2013 AS OA"& VbCrLf &
"	CROSS APPLY dbo.SplitStrings(OA.Borders, '#') AS f"& VbCrLf &
"	WHERE Value <> ''),"& VbCrLf &
"/*CONT_POW_BT"& VbCrLf &
"Sum of contracted Power of all the affected customers on BT Outages*/"& VbCrLf &
"CONT_POW_BT AS (SELECT A.Number, A.STARTINGTIME, A.ENDINGTIME, COUNT(DISTINCT C.CODE) AS CUST, "& VbCrLf &
"		SUM(C.CONTRACTED_POWER) AS CONTRACTED_POW "& VbCrLf &
"	FROM SPLIT_ACOMETIDAS A"& VbCrLf &
"		JOIN CUSTOMER C ON A.Value = C.CUSTOMER_NODE"& VbCrLf &
"	GROUP BY A.NUMBER, A.STARTINGTIME, A.ENDINGTIME"& VbCrLf &
"	--ORDER BY A.NUMBER"& VbCrLf &
"),"& VbCrLf &
"/*CONT_POW_MT"& VbCrLf &
"Sum of contracted Power of all the affected customers on MT Outages*/"& VbCrLf &
"CONT_POW_MT AS (SELECT OA.Number, OA.STARTINGTIME, OA.ENDINGTIME, C.LV_NETWORK, COUNT(C.LV_NETWORK) AS CUST, "& VbCrLf &
"		SUM(C.CONTRACTED_POWER) AS CONTRACTED_POW "& VbCrLf &
"	FROM OutageAreas2013 OA"& VbCrLf &
"	JOIN LVNetworkOutages2013"& VbCrLf &
"		ON OA.ID = LVNetworkOutages2013.OUTAGEAREA"& VbCrLf &
"	JOIN CUSTOMER C ON LVNetworkOutages2013.CODE = C.LV_NETWORK"& VbCrLf &
"	GROUP BY OA.NUMBER, OA.STARTINGTIME, OA.ENDINGTIME, C.LV_NETWORK"& VbCrLf &
"	--ORDER BY A.NUMBER"& VbCrLf &
")"& VbCrLf &
""& VbCrLf &
"SELECT outages.NUMBER, outages.STARTINGTIME, outages.ENDINGTIME, outages.Duration, LVNetworkOutages2013.CODE AS INSTALACION_MT,"& VbCrLf &
"	(CASE WHEN MV = 1 THEN ''"& VbCrLf &
"		  WHEN MV = 0 THEN OutageAreas2013.BORDERS"& VbCrLf &
"	 END) AS INSTALACION_BT, "& VbCrLf &
"	 (CASE"& VbCrLf &
"			WHEN MV = 1 THEN"& VbCrLf &
"				(SELECT TRANSLATION FROM TRANSLATION"& VbCrLf &
"					WHERE TRANSLATION.LANG = @Language AND TRANSLATION.STRING = 'MV')"& VbCrLf &
"			WHEN MV = 0 THEN"& VbCrLf &
"				(SELECT TRANSLATION FROM TRANSLATION"& VbCrLf &
"					WHERE TRANSLATION.LANG = @Language AND TRANSLATION.STRING = 'LV')"& VbCrLf &
"		END) AS NTension,"& VbCrLf &
"	gdlCodeMun.NameMun, gdlCodeProv.NameProv, "& VbCrLf &
"	(CASE WHEN MV_LV_SUBSTATION.PROPIOS = 1 THEN "& VbCrLf &
"				(SELECT TRANSLATION FROM TRANSLATION"& VbCrLf &
"					WHERE TRANSLATION.LANG = @Language AND TRANSLATION.STRING = 'YES')"& VbCrLf &
"			WHEN MV_LV_SUBSTATION.PROPIOS = 0 THEN "& VbCrLf &
"				(SELECT TRANSLATION FROM TRANSLATION"& VbCrLf &
"					WHERE TRANSLATION.LANG = @Language AND TRANSLATION.STRING = 'NO')"& VbCrLf &
"			WHEN MV_LV_SUBSTATION.PROPIOS IS NULL THEN"& VbCrLf &
"				(SELECT TRANSLATION FROM TRANSLATION"& VbCrLf &
"					WHERE TRANSLATION.LANG = @Language AND TRANSLATION.STRING = 'NOT_DEFINED')"& VbCrLf &
"	END) AS EN_PROPIEDAD,"& VbCrLf &
"	COALESCE((CASE WHEN MV = 1 THEN (SELECT CUST FROM CONT_POW_MT C "& VbCrLf &
"				WHERE C.NUMBER = outages.NUMBER AND C.STARTINGTIME = outages.STARTINGTIME AND C.ENDINGTIME = outages.ENDINGTIME"& VbCrLf &
"					AND C.LV_NETWORK = LVNetworkOutages2013.CODE)"& VbCrLf &
"		WHEN MV = 0 THEN (SELECT CUST FROM CONT_POW_BT C "& VbCrLf &
"				WHERE C.NUMBER = outages.NUMBER AND C.STARTINGTIME = outages.STARTINGTIME AND C.ENDINGTIME = outages.ENDINGTIME)"& VbCrLf &
"	END), 0) AS CUST,"& VbCrLf &
"	COALESCE((CASE WHEN MV = 1 THEN (SELECT CONTRACTED_POW FROM CONT_POW_MT C "& VbCrLf &
"				WHERE C.NUMBER = outages.NUMBER AND C.STARTINGTIME = outages.STARTINGTIME AND C.ENDINGTIME = outages.ENDINGTIME"& VbCrLf &
"					AND C.LV_NETWORK = LVNetworkOutages2013.CODE)"& VbCrLf &
"		WHEN MV = 0 THEN (SELECT CONTRACTED_POW FROM CONT_POW_BT C "& VbCrLf &
"				WHERE C.NUMBER = outages.NUMBER AND C.STARTINGTIME = outages.STARTINGTIME AND C.ENDINGTIME = outages.ENDINGTIME)"& VbCrLf &
"	END), 0) AS CONT_POW,"& VbCrLf &
"	TRANSFORMER.SN1 AS INSTALLED_POW"& VbCrLf &
""& VbCrLf &
"   FROM outages "& VbCrLf &
"    JOIN OutageAreas2013"& VbCrLf &
"		ON outages.NUMBER = OutageAreas2013.NUMBER"& VbCrLf &
"	JOIN LVNetworkOutages2013"& VbCrLf &
"		ON OutageAreas2013.ID = LVNetworkOutages2013.OUTAGEAREA"& VbCrLf &
"	JOIN MV_LV_SUBSTATION"& VbCrLf &
"		ON MV_LV_SUBSTATION.CODE = LVNetworkOutages2013.CODE"& VbCrLf &
"	JOIN gdlCodeMun"& VbCrLf &
"		ON MV_LV_SUBSTATION.MUN = gdlCodeMun.IDMun AND MV_LV_SUBSTATION.PROVINCE = gdlCodeMun.IDProv"& VbCrLf &
"	JOIN gdlCodeProv"& VbCrLf &
"		ON gdlCodeMun.IDProv = gdlCodeProv.IDProv"& VbCrLf &
"	LEFT JOIN gdlValidatedReports"& VbCrLf &
"		ON gdlValidatedReports.ID = OutageAreas2013.NUMBER"& VbCrLf &
"	LEFT JOIN TRANSFORMER"& VbCrLf &
"		ON TRANSFORMER.PLACING_SITE = MV_LV_SUBSTATION.CODE"& VbCrLf &
""& VbCrLf &
"	WHERE   OutageAreas2013.STARTINGTIME BETWEEN @Starttime AND @Endtime "& VbCrLf &
"		AND MV_LV_SUBSTATION.COMPANY IN (@Company) "& VbCrLf &
"		AND gdlCodeProv.IDProv IN (@Region) "& VbCrLf &
"		AND @validated = CASE WHEN gdlValidatedReports.id IS NULL THEN 0 ELSE 1 END"& VbCrLf &
""& VbCrLf &
"   ORDER BY NUMBER, STARTINGTIME, ENDINGTIME, INSTALACION_MT"& VbCrLf &
)

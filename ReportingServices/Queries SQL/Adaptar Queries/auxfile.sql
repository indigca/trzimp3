"
"--Uncomment the next DECLARE/SET statements for testing the query on Management Studio
"--2013 must be replaced in the whole query by the year of the archive is going to be tested (e.g. 2013)
"DECLARE @Language nvarchar(2);
"SET @Language = 'es';
"DECLARE @Company nvarchar(3);
"SET @Company = '036';
"DECLARE @Starttime datetime;
"SET @Starttime = '1-1-2013';
"DECLARE @Endtime datetime;
"SET @Endtime = (SELECT GETDATE());
"DECLARE @Region nvarchar(2);
"SET @Region = '16';
"DECLARE @validated int;
"SET @validated = 0;
"
"/*Outages
"It retrieves info about all the aoutages in the system
"*/
"WITH outages AS ( 
"	--First UNION get the outages from MVOutageReport
"	SELECT	dbo.GETNOTNULL(  
" 								dbo.GETCODEINFO('FAULTREASON', FAULTREASON),  
" 								dbo.GETCODEINFO('MAINTOUTAGEREASON', MAINTOUTAGEREASON)  
" 												 ) AS REASON,  
"					NUMBER, STARTINGTIME, ENDINGTIME, TYPE, Duration, 1 AS MV, Cust
"	FROM		MVOutageReport2013 
"	UNION 
"	--Second UNION get the outages from FaultReport
"	SELECT	FAULTREASON AS REASON, 
"					NUMBER, STARTINGTIME, ENDINGTIME, 0 AS TYPE, Duration, 1 AS MV, Cust 
"	FROM		FaultReport2013 
"	UNION 
"	--Third UNION get the outages from MaintenanceOutageReport
"	SELECT	MAINTOUTAGEREASON AS REASON,  
"					NUMBER, STARTINGTIME, ENDINGTIME, 1 AS TYPE, DURATION AS Duration, 1 AS MV, CUST AS Cust
"	FROM		MaintenanceOutageReport2013 
"    UNION
"	--Fourth UNION get the outages from LVOutageReport
"	SELECT	dbo.GETCODEINFO('FAULTREASON', LVFaultReason) AS REASON,  
"					NUMBER, STARTINGTIME, ENDINGTIME, TYPE, Duration, 0 AS MV, Cust
"	FROM		LVOutageReport2013
"),
"/*SPLIT_ACOMETIDAS
"Uses the function dbo.SplitStrings to return as many rows as acometidas where affected by the outage. 
"It returns results just for outages with acometidas affected
"*/
"SPLIT_ACOMETIDAS AS(SELECT OA.NUMBER, OA.STARTINGTIME, OA.ENDINGTIME, f.Value
"  FROM dbo.OutageAreas2013 AS OA
"	CROSS APPLY dbo.SplitStrings(OA.Borders, '#') AS f
"	WHERE Value <> ''),
"/*CONT_POW_BT
"Sum of contracted Power of all the affected customers on BT Outages*/
"CONT_POW_BT AS (SELECT A.Number, A.STARTINGTIME, A.ENDINGTIME, COUNT(DISTINCT C.CODE) AS CUST, 
"		SUM(C.CONTRACTED_POWER) AS CONTRACTED_POW 
"	FROM SPLIT_ACOMETIDAS A
"		JOIN CUSTOMER C ON A.Value = C.CUSTOMER_NODE
"	GROUP BY A.NUMBER, A.STARTINGTIME, A.ENDINGTIME
"	--ORDER BY A.NUMBER
"),
"/*CONT_POW_MT
"Sum of contracted Power of all the affected customers on MT Outages*/
"CONT_POW_MT AS (SELECT OA.Number, OA.STARTINGTIME, OA.ENDINGTIME, C.LV_NETWORK, COUNT(C.LV_NETWORK) AS CUST, 
"		SUM(C.CONTRACTED_POWER) AS CONTRACTED_POW 
"	FROM OutageAreas2013 OA
"	JOIN LVNetworkOutages2013
"		ON OA.ID = LVNetworkOutages2013.OUTAGEAREA
"	JOIN CUSTOMER C ON LVNetworkOutages2013.CODE = C.LV_NETWORK
"	GROUP BY OA.NUMBER, OA.STARTINGTIME, OA.ENDINGTIME, C.LV_NETWORK
"	--ORDER BY A.NUMBER
")
"
"SELECT outages.NUMBER, outages.STARTINGTIME, outages.ENDINGTIME, outages.Duration, LVNetworkOutages2013.CODE AS INSTALACION_MT,
"	(CASE WHEN MV = 1 THEN ''
"		  WHEN MV = 0 THEN OutageAreas2013.BORDERS
"	 END) AS INSTALACION_BT, 
"	 (CASE
"			WHEN MV = 1 THEN
"				(SELECT TRANSLATION FROM TRANSLATION
"					WHERE TRANSLATION.LANG = @Language AND TRANSLATION.STRING = 'MV')
"			WHEN MV = 0 THEN
"				(SELECT TRANSLATION FROM TRANSLATION
"					WHERE TRANSLATION.LANG = @Language AND TRANSLATION.STRING = 'LV')
"		END) AS NTension,
"	gdlCodeMun.NameMun, gdlCodeProv.NameProv, 
"	(CASE WHEN MV_LV_SUBSTATION.PROPIOS = 1 THEN 
"				(SELECT TRANSLATION FROM TRANSLATION
"					WHERE TRANSLATION.LANG = @Language AND TRANSLATION.STRING = 'YES')
"			WHEN MV_LV_SUBSTATION.PROPIOS = 0 THEN 
"				(SELECT TRANSLATION FROM TRANSLATION
"					WHERE TRANSLATION.LANG = @Language AND TRANSLATION.STRING = 'NO')
"			WHEN MV_LV_SUBSTATION.PROPIOS IS NULL THEN
"				(SELECT TRANSLATION FROM TRANSLATION
"					WHERE TRANSLATION.LANG = @Language AND TRANSLATION.STRING = 'NOT_DEFINED')
"	END) AS EN_PROPIEDAD,
"	COALESCE((CASE WHEN MV = 1 THEN (SELECT CUST FROM CONT_POW_MT C 
"				WHERE C.NUMBER = outages.NUMBER AND C.STARTINGTIME = outages.STARTINGTIME AND C.ENDINGTIME = outages.ENDINGTIME
"					AND C.LV_NETWORK = LVNetworkOutages2013.CODE)
"		WHEN MV = 0 THEN (SELECT CUST FROM CONT_POW_BT C 
"				WHERE C.NUMBER = outages.NUMBER AND C.STARTINGTIME = outages.STARTINGTIME AND C.ENDINGTIME = outages.ENDINGTIME)
"	END), 0) AS CUST,
"	COALESCE((CASE WHEN MV = 1 THEN (SELECT CONTRACTED_POW FROM CONT_POW_MT C 
"				WHERE C.NUMBER = outages.NUMBER AND C.STARTINGTIME = outages.STARTINGTIME AND C.ENDINGTIME = outages.ENDINGTIME
"					AND C.LV_NETWORK = LVNetworkOutages2013.CODE)
"		WHEN MV = 0 THEN (SELECT CONTRACTED_POW FROM CONT_POW_BT C 
"				WHERE C.NUMBER = outages.NUMBER AND C.STARTINGTIME = outages.STARTINGTIME AND C.ENDINGTIME = outages.ENDINGTIME)
"	END), 0) AS CONT_POW,
"	TRANSFORMER.SN1 AS INSTALLED_POW
"
"   FROM outages 
"    JOIN OutageAreas2013
"		ON outages.NUMBER = OutageAreas2013.NUMBER
"	JOIN LVNetworkOutages2013
"		ON OutageAreas2013.ID = LVNetworkOutages2013.OUTAGEAREA
"	JOIN MV_LV_SUBSTATION
"		ON MV_LV_SUBSTATION.CODE = LVNetworkOutages2013.CODE
"	JOIN gdlCodeMun
"		ON MV_LV_SUBSTATION.MUN = gdlCodeMun.IDMun AND MV_LV_SUBSTATION.PROVINCE = gdlCodeMun.IDProv
"	JOIN gdlCodeProv
"		ON gdlCodeMun.IDProv = gdlCodeProv.IDProv
"	LEFT JOIN gdlValidatedReports
"		ON gdlValidatedReports.ID = OutageAreas2013.NUMBER
"	LEFT JOIN TRANSFORMER
"		ON TRANSFORMER.PLACING_SITE = MV_LV_SUBSTATION.CODE
"
"	WHERE   OutageAreas2013.STARTINGTIME BETWEEN @Starttime AND @Endtime 
"		AND MV_LV_SUBSTATION.COMPANY IN (@Company) 
"		AND gdlCodeProv.IDProv IN (@Region) 
"		AND @validated = CASE WHEN gdlValidatedReports.id IS NULL THEN 0 ELSE 1 END
"
"   ORDER BY NUMBER, STARTINGTIME, ENDINGTIME, INSTALACION_MT

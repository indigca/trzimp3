$filein = 'infile.sql'
$fileout = 'outfile.sql'
$fileaux = 'auxfile.sql' 

$patron_i = '^'
$replace_i = '"'
$patron_f = '$'
$replace_f = '"& VbCrLf &'

cat $filein | %{$_ -replace $patron_i, $replace_i } > $fileaux
echo '=Code.RA(' > $fileout
cat $fileaux | %{$_ -replace $patron_f, $replace_f } >> $fileout
echo ')' >> $fileout
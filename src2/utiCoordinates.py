# -*- coding: iso-8859-15 -*-
'''
Created on 31/01/2011
Last modification 03/12/2013 by ALP
@author: n1, ALP
'''

from math import sin, cos, tan, pi, sqrt
from pyproj import Proj
from pyproj import transform

#WTF do this function does?
def convertED50 (x50, y50, zone):
    """Converts from UTM coordinates (Zone,E,N) into coordinates used in DMS600 (X,Y)"""
    
    p50 = Proj(proj="utm", ellps="intl", k=0.9996, x_0=500000.0, y_0=0.0 ,
    zone=zone, units="m", towgs84="-125.098545,-76.000054,-156.198703,0.0,0.0,-1.129,8.30463101")
    'WGS84'    
    p89 = Proj(proj="utm", zone=zone, ellps="GRS80", units="m", towgs84="0,0,0")
    
    '''x50,y50=p50(-3.09581,40.79182)
    x89,y89=transform (p50,p89,x50,y50)
    print x50,y50
    print x89,y89'''
    
    x89, y89 = transform(p50, p89, x50, y50)
    lon, lat = p89(x89, y89, inverse=True)
    x89, y89 = getUTMProjection(lat, lon)
    flannaganY89 = -3980000
    flannaganX89 = +30000
#    flannaganY50 = -4000000
#    flannaganX50 = 0
    return int(x89 + flannaganX89), int(y89 + flannaganY89)

def deg2rad(deg):
    """Converts degrees into radians"""
    return (deg/360*2*pi)

#http://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system
def getUTMProjection(lat, lon):
    """converts latitude and longitude coordinates into UTM coordinates (E,N)"""
    e = 0.0818192
    a = 6378137 # meters
    k0 = 0.9996
    N0 = 0
    lat0 = 0
    lon0 = deg2rad(-3.0)
    E0 = 500000
    
    lat = deg2rad(lat)
    lon = deg2rad(lon)

    v = 1/sqrt(1-e**2*sin(lat)*sin(lat))
    A = (lon-lon0)*cos(lat)
    s = (1-e**2/4-3*e**4/64-5*e**6/256)*lat - (3*e**2/8+3*e**4/32+45*e**6/1024)*sin(2*lat)+(15*e**4/256+45*e**6/1024)*sin(4*lat)-35*e**6/3072*sin(6*lat)
    T = tan(lat)*tan(lat)
    C = e**2/(1-e**2)*cos(lat)*cos(lat)
    E = E0 + k0*a*v*(A+(1-T+C)*A**3/6 + (5-18*T+T**2)*A**5/120)
    N = N0+k0*a*(s+v*tan(lat)*(A**2/2+(5-T+9*C+4*C**2)*A**4/24+(61-58*T+T*T)*A**6/720))
    
    return [E, N]

def getCoordinates(nodecode):
    """
    Get coordinates from a nodecode where nodecode follows
    the DMS naming pattern that contains the coordinates
    """
    xg = nodecode[0:3] + nodecode[6:9] + '000'
    yg = nodecode[3:6] + nodecode[9:12] + '000'  
    return int(xg), int(yg)
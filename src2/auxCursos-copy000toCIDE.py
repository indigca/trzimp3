import utiDataBase as ud

"""
Este programa copia una distribuidora de una base de datos a otra base de datos.
Ha sido creado para pasar la distribuidora 000 de los ordenadores de GEDLux (maqina CIDE-DES) a los de CIDE y asi poder usar dicha distribuidora en los cursos.
"""

MINECO = '000'



TABLE="MV_NODE"
WHERE = "WHERE gdlMINECO IS NULL"
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO MV_NODE VALUES ('"+str(entry[0])+"','"+str(entry[1])+"','"+str(entry[2])+"','"+str(entry[3])+"','"+str(entry[4])+"','"+str(entry[5])+"','"+str(entry[6])+"','"+str(entry[7])+"','"+str(entry[8])+"','000')"
    query=query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="MV_SECTIONPOINT"
WHERE = "WHERE gdlMINECO IS NULL"
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"+str(entry[0])+"','"+str(entry[1])+"','"+str(entry[2])+"','"+str(entry[3])+"','"+str(entry[4])+"','"+str(entry[5])+"','"+str(entry[6])+"','000')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')

TABLE="DISCONNECTOR"
WHERE = "WHERE CODE LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    #El insert hacerlo con ud.execute(query,'TRAZA') y poner temporalmente los datos del traza que apunten al DMS de CIDE
    #Los datos reales del Traza guardarlos como seccion [TRAZA_ORIG] o algo asi.
    #construir insert para todo antes de hacer el insert, y probar primero en una base de datos vacia sin distribuidora 000.
    #Para ello borrar la 000 desde TrazaImport (old).

TABLE="CUSTOMER"
WHERE = "WHERE LV_NETWORK LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        if i == 13:
            query = query + "(SELECT(Getdate())),"
        else:
            query = query + "'" + str(entry[i]) + "',"
    query = query + "'" + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')

TABLE="CUSTOMER_NODE_LOAD"
WHERE = "WHERE CUSTOMER_NODE LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')

TABLE="DIAGRAM"
WHERE = "WHERE CODE LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')

#FALTA CAPACITOR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

TABLE="BUSBARNODE"
WHERE = "WHERE CODE LIKE 'C01%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + "000-" +str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')

TABLE="BUSBARPOINT"
WHERE = "WHERE CODE LIKE 'C01%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('000-"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query +str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')

TABLE="CIRCUIT_BREAKER"
WHERE = "WHERE CODE LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="LV_BOX"
WHERE = "WHERE CODE LIKE '000%'"  # AKI HAY MAS COSAS QUE NO COPIO CON CODES RAROS------------------
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="LV_SWITCH"  # AKI HAY MAS COSAS QUE NO COPIO CON CODES RAROS---------------------------
WHERE = "WHERE BOX LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="LV_NODE"
WHERE = "WHERE gdlMINECO IS NULL"  
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + "000')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')


TABLE="LV_SECTION"
WHERE = "WHERE LV_NETWORK LIKE '000%'"  
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="LV_SECTIONPOINT"
WHERE = "WHERE gdlMINECO IS NULL"  
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + "000')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')

TABLE="LV_CUSTOMER_NODE"
WHERE = "WHERE CUSTOMER_NODE LIKE '000%'"  
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        query = query +"'"+ str(entry[i]) + "',"
    query = query + "(SELECT(Getdate())))"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="MV_FEEDER"
WHERE = "WHERE CODE LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')

TABLE="MV_LV_SUBSTATION"
WHERE = "WHERE CODE LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-4):
        query = query + str(entry[i]) + "','"
    query = query + "079','000','28',NULL)"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
   
# FALTA MV_LV_LOAD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

TABLE="MV_SECTION"
WHERE = "WHERE DISTRICT IS NULL" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        if i==14:
            query = query + "000','"
        else:
            query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="MV_SITE"
WHERE = "WHERE gdlMINECO IS NULL"  
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + "000')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')

#FALTA NODE_REGIONS!!!!!!!!!!!!!!!!!!!
#FALTA NODE_REGIONS2!!!!!!!!!!!!!!!!!!!

TABLE="SUBSTATION"
WHERE = "WHERE CODE LIKE '000%'"
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="TRANSFORMER"
WHERE = "WHERE CODE LIKE '000%'"  
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        if i == 20:
            query = query + "(SELECT(Getdate())),"
        else:
            query = query + "'" + str(entry[i]) + "',"
    query = query + "'" + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="TRANSFORMER_NODE"
WHERE = "WHERE MV_LV_SUBSTATION LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="TRANSFORMER_PLACING"
WHERE = "WHERE CODE LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        query = query +"'"+ str(entry[i]) + "',"
    query = query + "(SELECT(Getdate())))"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')


#Falta gdlCTConnections!!!!!!!!!!!!!

TABLE="FEEDINGPOINT"
WHERE = "WHERE CODE LIKE '000%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="SwitchingComponent"
WHERE = "WHERE gdlMINECO IS NULL"
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        if i==8:
            query = query + "NULL,"
        else:
            query = query + "'"+str(entry[i]) + "',"
    query = query + "'000')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
TABLE="PRIMARY_TRANSFORMER"
WHERE = "WHERE CODE LIKE '000%'"  
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,'DMS')
fields = len(result[0])
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        if i == 18:
            query = query + "(SELECT(Getdate())),"
        else:
            query = query + "'" + str(entry[i]) + "',"
    query = query + "'" + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,'TRAZA')
    
print 'FIIIIIIIIIIIIIIIIIIIIIIIIIIIIIINNNNNNNNNNNNNNNNNNNNNNNNNNNNNN'

#FALTA MV_FUSE!!!!
#Falta gdlOPC!!!!!! (Y aqui hay un par de registros)
#Falta gdlLV_NODE_BTVano!!!!!!!
#Falta gdlXYDMS!!!!!!!!!!!!

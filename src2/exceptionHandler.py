# -*- coding: iso-8859-15 -*-
'''
Created on 23/12/2013
@author: ALP
'''
import sys
import traceback
import log
import wx

def handleExceptionError(exception,errorMsg,errorBox,returnValue):
    # Get the traceback object
    tb = sys.exc_info()[2]
    tbinfo = traceback.format_tb(tb)[0] #Takes the last trace, for more trace select [1], [2], ....

    # Concatenate information together concerning the error into a message string
    pymsg = errorMsg + "\nTraceback info:\n" + tbinfo + "Error Info:\n" + str(sys.exc_info()[1]) + "\n"

    # Print/Write in log Python error messages for use in Python / Python Window
    print pymsg
    log.writeLog(pymsg)
    #wx.MessageBox(errorBox,"Error", wx.OK|wx.ICON_ERROR)
    return returnValue

def handleExceptionWarning(exception,errorMsg,returnValue):
    # Get the traceback object
    tb = sys.exc_info()[2]
    tbinfo = traceback.format_tb(tb)[0]

    # Concatenate information together concerning the error into a message string
    pymsg = errorMsg + "\nTraceback info:\n" + tbinfo + "Error Info:\n" + str(sys.exc_info()[1]+"\n")

    #Write in log
    log.writeLog(pymsg)
    return returnValue
    
def handleError(errorMsg,errorBox):
    #Give format to errorMsg for being written in the log
    pymsg = errorMsg+"\n"

    #Write in log
    log.writeLog(pymsg)
    
    #Prompt box
    #wx.MessageBox(errorBox,"Error", wx.OK|wx.ICON_ERROR)

def handleWarning(errorMsg):
    ##Give format to errorMsg for being written in the log
    pymsg = errorMsg+"\n"

    #Write in log
    log.writeLog(pymsg)


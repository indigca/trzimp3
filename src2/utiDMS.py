# -*- coding: iso-8859-15 -*-
'''
Created on 05/12/2013
@author: ALP
'''

import utiDataBase as uDB
import utiGedlux as uG
import log
import wx
import exceptionHandler as eh

def insertTrafoSQL(Ux, code, orderNumber, type, year, sn1, placingSite, nombre, num):
    """Insert needed data to dbo.transformer to add one transformer to DMS"""
    query = "INSERT INTO TRANSFORMER (CODE,PLACING_SITE,ORDERNUMBER,TYPE,YEAR,SN1,U1,U2,U3) \
                 VALUES ('"+str(code)+"','"+str(placingSite)+"','"+str(orderNumber)+"','"+str(type)+ \
                 "','"+str(year)+"','"+str(sn1)+"','"+str(Ux["U1"])+"','"+str(Ux["U2"])+"','"+str(Ux["U3"])+"')"
    query = query.replace("'None'","NULL")
    if uDB.execute(query,'DMS') == -1:
        errorMsg = "ERROR. SQL insert failed"
        errorBox = "Fallo al insertar el transformador n�mero "+str(num)+" de "+str(nombre)
        eh.handleError(errorMsg,errorBox)
        
def insertTrafo(IdTransformador,MINECO,numInCT,xi,yi,code,sitenode,mun_code,prov_code,ctName,propios):
    nodecode = getDMSCode(xi, yi) + "M1"
    codetrafo = IdTransformador
    if numInCT == 1: #First trafo placed in one CT
        placeTrafo(IdTransformador, code, nodecode, sitenode)
        insertMV_Node(nodecode, xi, yi, xi, yi, sitenode, mineco=MINECO,codigoGDL=uG.getGDLCode('T',code,str(numInCT)))
    else:
        placeTrafo(IdTransformador, code[:-1] + str(numInCT), nodecode, nodecode)
        insertMV_Node(nodecode, xi, yi, xi, yi, nodecode, mineco=MINECO,codigoGDL=uG.getGDLCode('T',code,str(numInCT)))
        insertMV_Site(code[:-1] + str(numInCT), 'T', xi, yi, name="", mineco=MINECO)
        insertMV_LV_Substation(code[:-1] + str(numInCT), ctName, mun=mun_code, prov=prov_code, mineco=MINECO, propios=propios )
        
def placeTrafo(IdTransformador, sitename, nodecode, sitenode):#Transformer Placement
    trafoOrderNumber = getTransformerOrderNumber(sitename) + 1 

    query = """UPDATE dbo.TRANSFORMER SET PLACING_SITE='"""+sitename+"""',ORDERNUMBER='"""+str(trafoOrderNumber)+"""'
                WHERE CODE LIKE '"""+IdTransformador+"""'"""
    uDB.execute(query,'DMS')

    query="""insert into dbo.transformer_node (nodecode, sitenode) select nodecode='"""+nodecode+"""'
                , sitenode= '"""+sitenode+"""' where '"""+nodecode+"""' not in (select nodecode from
                 dbo.transformer_node)
                update dbo.transformer_node
                set mv_lv_substation='"""+sitename+"""',ordernumber='"""+str(trafoOrderNumber)+"""', sitenode='"""+sitenode+"""'
                    where nodecode = '""" +nodecode+"""'"""
    query = query.replace("'None'","NULL")
    uDB.execute(query,'DMS')


def getTransformerOrderNumber(sitename):
    query = "Select max(ORDERNUMBER) from dbo.TRANSFORMER where PLACING_SITE like '" + sitename + "'"
    row = uDB.selectOne(query, 'DMS')
    if row[0] >= 0: #Checking for null
        return int(row[0])
    else:
        return 0
    
def getMVSectionID():
    """Get the highest section number that should be used and update the table"""
    query="UPDATE dbo.SECTIONNUMBER SET mv_section_id = mv_section_id + 1 WHERE ID=1"
    uDB.execute(query,'DMS')
    query = "SELECT mv_section_id FROM dbo.SECTIONNUMBER WHERE  ID=1"
    result = uDB.selectOne(query,'DMS')
    return int(result[0])
        
def insertDiagram (code, type, xg, yg, num):
    """Insert needed data to dbo.diagram to add one diagram to DMS"""
    #x1 = int(str(xg).split('.')[0] + "000")
    #y1 = int(str(yg).split('.')[0] + "000")
    x1 = xg
    y1 = yg
    xs = 1000 * (1 + num)
    ys = 4000
    x2 = x1 + xs
    y2 = y1 + ys

    query = """insert into dbo.DIAGRAM (code,type,x1,x2,y1,y2,hide_diagram)
            values ('""" + str(code) + "','" + str(type) + "','" + str(x1) + "','" + str(x2) + "','" + str(y1) + \
            "','" + str(y2) + "','" + str(0) + "')"
    query = query.replace("'None'","NULL")                            
    return uDB.execute(query,'DMS')

def insertMV_LV_Substation(code, ctName, mun=None, prov=None, mineco=None, propios=1):
    """Insert needed data to dbo.MV_LV_SUBSTATION to add one CT to DMS"""
    query = "insert into dbo.MV_LV_SUBSTATION\
                (CODE, MUN, PROVINCE, COMPANY, PROPIOS, NAME)\
                VALUES ('""" + str(code) + "','" + str(mun) + "','" + str(prov) + "','" \
                + str(mineco) + "','" + str(propios) + \
                "','" + str(ctName) + "')"
    query = query.replace("'None'","NULL")
    return uDB.execute(query,'DMS')

def insertMV_Node (code, x, y, xcode, ycode, sitenode, symbolangle=0, hide=0, hide_node=0, mineco=None, desc=None, line=None, codigoGDL=None):
    """Insert entry in MV_Node"""
    #CodigoGDL:
    #related to CTs: 'C'+MV_LV_SUBSTATION.CODE, except exit points
    #CTs exit points: 'E'+celda position(2digits)+'-'+MV_LV_SUBSTATION.CODE+'-'+IdInstLinea
    #Belonging to a line: 'L'+IdInst de la L�nea+'-'+IdATVano del Vano
#    if desc == 'ST':
#        code = utiDMS.getSubstationPoint(cursor, utiDMS.normalize(line)[0:12])
#        sitenode = code #I DIDNT REACH THIS AREA OF CODE
    query="""insert into dbo.mv_node (code,sitenode, gdlMINECO, x, y, xcode, ycode,
                    symbolangle, hide, hide_node,CodigoGDL) 
                select code='""" + str(code) + """', 
                sitenode='""" + str(sitenode) + """', gdlMINECO='""" + str(mineco[:3]) + """',
                x='""" + str(x) + """',y='""" + str(y) + """', 
                xcode='""" + str(xcode) + """',ycode='""" + str(ycode) + """',
                symbolangle='""" + str(symbolangle) + """',
                hide='""" + str(hide) + """',hide_node='""" + str(hide_node) + """',CodigoGDL='""" + str(codigoGDL) + """'"""
#            where code LIKE '""" + str(code) + """'"""
    query = query.replace("'None'","NULL")
    if uDB.execute(query,'DMS') < 0:
        return -1
    
    # fill in region data to ensure the node shows even without electricity
    if mineco != None:
        query = """MERGE NODE_REGIONS USING (SELECT '""" + str(code) + """', '""" + str(mineco) + """') AS src (code, mineco)
                        ON (NODE_REGIONS.NODECODE LIKE src.code)
                        WHEN MATCHED THEN UPDATE SET REGION_CODES = src.mineco
                        WHEN NOT MATCHED THEN INSERT (NODECODE, REGION_CODES)
                        VALUES (src.code, src.mineco);"""
        query = query.replace("'None'","NULL")
    return uDB.execute(query,'DMS')

# def insertLineNode(code,x,y,MINECO):
#     """Insert a MV_Node from a line"""
#     query = "SELECT CODE FROM MV_NODE WHERE X = '" + str(x) + "' AND Y = '" + str(y) + "'"
#     while uDB.selectOne(query,'DMS') != None:
#         #AQUI CREO QUE EN VEZ DE MOVER DIRECTAMENTE EL PUNTOP PODRIA TIRAR DE LO DE FINALIZAR EL DMSCODE CON X1,X2,...
#         x = x + 1000
#         y = y + 1000
#         query = "SELECT CODE FROM MV_NODE WHERE X = '" + str(x) + "' AND Y = '" + str(y) + "'"
#     dmsCode = getDMSCode(x,y) + 'X1'
#     if insertMV_Node (dmsCode, x, y, x, y, dmsCode, mineco=MINECO, codigoGDL=code) < 0:
#         return -1
#     return dmsCode

def insertLineNode(type,code,x,y,MINECO):
    """Insert a MV_Node from a line"""
    endings = [] #endings = '1','2','3','4','5','6','7','8','9','A','B','C',.....
    for i in range(49,58):
        endings.append(chr(i))
    for i in range(65,91):
        endings.append(chr(i))

    dmsCode = getDMSCode(x,y) + type + '1'
    dmsCode = uG.getEmptyCode(dmsCode,'mv_node')

    if insertMV_Node (dmsCode, x, y, x, y, dmsCode, mineco=MINECO, codigoGDL=code) < 0:
        return -1
    return dmsCode
    

def insertMV_Site(sitecode, type, xg, yg, name=None, mineco=None):
    """Insert entry in MV_Site"""
    if type == 'M':
        nodecode = uG.getEmptyCode(getDMSCode(xg, yg) + 'X1', 'mv_site')
    elif type == 'T':
        nodecode = uG.getEmptyCode(getDMSCode(xg, yg) + 'M1', 'mv_site')
#    eif type == 'E':
#         nodecode = utiDMS.getDMSCode(xg, yg) + 'E1'
 #DIDN'T REACH THIS YET

#     if name == None:
#         name = sitecode

    query = "insert into dbo.mv_site (nodecode,type,gdlMINECO) select  nodecode= '""" + str(nodecode) + """',
                type='""" + str(type) + """', gdlMINECO='""" + str(mineco[:3]) + """' 
                where '""" + str(nodecode) + """' not in (select nodecode from dbo.mv_site)
                update dbo.mv_site
                set type='""" + str(type) + """', sitecode='""" + str(sitecode) + """', name='""" + str(name) + """',
                protective_earthing='""" + str(0) + """', system_earthing='""" + str(0) + """', combined_earthing='""" + str(0) + """'
                where nodecode = '""" + str(nodecode) + """'"""
    query = query.replace("'None'","NULL")
    return uDB.execute(query,'DMS')

def insertDisconnector(nodecode, elemCode, code, name):
    """Insert entry in DISCONNECTOR"""
    query = """insert into dbo.disconnector (nodecode,code) select nodecode='""" + nodecode + """', 
            code='""" + elemCode +"""' where '""" + nodecode + """' not in (select nodecode from dbo.disconnector)
            update dbo.disconnector
            set code='""" + elemCode + """',mv_site='""" + name + """', disconnector_type='L',status=1,control=0,
                subsymbolcode=0, direction=' '
            where nodecode = '""" + nodecode + """'"""
    query = query.replace("'None'","NULL")
    return uDB.execute(query,'DMS')
    
    #ESTO ES PARA HACER LA CONEXION OPC!!!!!!!!!!!!!!!!!
    #cursor.execute("insert into gdlOPC values (?,'I',?)", code[0:3], code[3:])
#     cursor.execute("select * from gdlCT where DMS_Name LIKE ?", code[3:-2])
#     codes = cursor.fetchall()
#     if len(codes) == 0 and CTName != None:
#         cursor.execute("insert into gdlCT values (?,?,NULL,?)", code[0:3], code[3:-2], CTName)

def insertFuse (nodecode, elemCode, code):
    """Insert entry in MV_FUSE"""
    query="""insert into dbo.MV_FUSE (nodecode,code) select nodecode='""" + nodecode + """', 
                    code='""" + elemCode + """' where '""" + nodecode + """' not in (select nodecode from dbo.disconnector)
                    update dbo.MV_FUSE
                    set code='""" + elemCode + """',type='DEFAULT_200',status=1,statusctrl=0
                    where nodecode = '""" + nodecode + """'"""
    query = query.replace("'None'","NULL")
    return uDB.execute(query,'DMS')

def insertCTElement(DisyuntorFusible,MINECO,i,code,name,xi,yi,sitenode,GDLCode):
    if DisyuntorFusible == 'Fusible': #Aqu� se podr�an a�adir m�s casos (aparte de Fusible y disconector) cuando se contemplen en populategdlCeldas
        elemCode = getElementCode(MINECO, 'FUS', i, code)
        nodecode = uG.getEmptyCode(getDMSCode(xi, yi) + "O1",'mv_node')
        insertFuse(nodecode, elemCode, code)
    else: #Consideramos seccionador todo lo que no sea fusible (hasta que haga falta meter m�s elementos)
        elemCode = getElementCode(MINECO, 'SCC', i, code)
        nodecode = uG.getEmptyCode(getDMSCode(xi, yi) + "E1",'mv_node')
        insertDisconnector(nodecode, elemCode, code, name)
    insertMV_Node(nodecode, xi, yi, xi, yi, sitenode, mineco=MINECO,codigoGDL=GDLCode)
    return nodecode

def insertLineElement(DisyuntorFusible,MINECO,i,nodecode,name,xi,yi):
    if DisyuntorFusible == 'Fusible': #Aqu� se podr�an a�adir m�s casos (aparte de Fusible y disconector) cuando se contemplen en populategdlCeldas
        elemCode = getElementCode(MINECO, 'FUS', i, MINECO+'L'+nodecode[:-2])
        insertFuse(nodecode, elemCode, nodecode)
    else: #Consideramos seccionador todo lo que no sea fusible (hasta que haga falta meter m�s elementos)
        elemCode = getElementCode(MINECO, 'SCC', i, MINECO+'L'+nodecode[:-2])
        insertDisconnector(nodecode, elemCode, 0, nodecode)
    return nodecode
    
def insertCircuitBreaker(nodecode,code,voltage):
    query = "INSERT INTO CIRCUIT_BREAKER(NODECODE,CODE,STATUS,REMOTE_CONTROL,\
                    NOMINAL_VOLTAGE,TRUCK) VALUES ('"+nodecode+"','"+code+"','1','1','"+str(voltage)+"','0')"
    query = query.replace("'None'","NULL")
    if uDB.execute(query,'DMS') == -1:
        errorMsg = "ERROR. SQL insert failed"
        errorBox = "Fallo al insertar el interruptor " + code
        eh.handleError(errorMsg,errorBox)


def insertMVSection(MV_Section_Id, NODE1, NODE2, CONDUCTOR, nMINECO, VOLTAGE_LEVEL, Xt, Yt, \
                        PARALLEL_NUMBER=1, \
                        LENGTH=0, \
                        SUBSTATION=None, \
                        MANUFACTURE_YEAR=0, \
                        INSPECTION_YEAR=0, \
                        POLING_YEAR=0, \
                        WIRING_YEAR=0, \
                        AREA=None, \
                        OWNER=None, \
                        ANGLE=0, \
                        HIDE=0, \
                        HIDE_SECTION=0,
                        NAME = None):
    if NODE1 not in (None,"") and NODE2 not in (None,""):
        query="""insert into dbo.MV_SECTION (MV_Section_Id, NODE1, NODE2, PARALLEL_NUMBER, VOLTAGE_LEVEL,
                    LENGTH, SUBSTATION, CONDUCTOR, MANUFACTURE_YEAR, INSPECTION_YEAR, POLING_YEAR, WIRING_YEAR,
                    AREA, OWNER, DISTRICT, Xt, Yt, ANGLE, HIDE, HIDE_SECTION,NAME)
                    VALUES ('"""+str(MV_Section_Id)+"""','"""+str(NODE1)+"""','"""+str(NODE2)+"""','"""+str(PARALLEL_NUMBER)+"""','"""+str(VOLTAGE_LEVEL).split('.')[0]+"""',
                    '"""+str(LENGTH)+"""','"""+str(SUBSTATION)+"""','"""+str(CONDUCTOR)+"""','"""+str(MANUFACTURE_YEAR)+"""','"""+str(INSPECTION_YEAR)+"""',
                    '"""+str(POLING_YEAR)+"""','"""+str(WIRING_YEAR)+"""','"""+str(AREA)+"""','"""+str(OWNER)+"""','"""+str(nMINECO)+"""',
                    '"""+str(Xt)+"""','"""+str(Yt)+"""','"""+str(ANGLE)+"""','"""+str(HIDE)+"""','"""+str(HIDE_SECTION)+"""','"""+str(NAME)+"""')"""
        query = query.replace("'None'","NULL")
    else:
        errorMsg = "ERROR. SQL insert MV_Section failed. Invalid section, empty or None nodecodes"
        eh.handleWarning(errorMsg)
        return -1

    if uDB.execute(query,'DMS') < 0:
        errorMsg = "ERROR. SQL insert MV_Section failed. Section may already exists, " + NODE1 + "-" + NODE2
        eh.handleWarning(errorMsg)
        return -1
    return 0  
    
def insertMVSectionPoints(mv_section_id, node1, node2, secc, mineco):
    """Insert points into one Mv_section"""
    #CodigoGDL
    #For CT: 'C' + MV_LV_SUBSTATION.CODE
    #For line: 'L'+IdInst+'-'+IdATVano   (idInst of the upper line)
    
    if node1 == None or node2 == None:
        logging.debug("")
        errorMsg = "ERROR. SQL insert MV_Section_Points failed. Trying to create section with null node, " + node1 + "-" + node2
        eh.handleWarning(errorMsg)
        return -1
    
    nro = 0
    error = 0
    for i in range(0, len(secc)):
        seccN = secc[i]
        nro = nro + 1
        x = seccN["x"]
        y = seccN["y"]
        query= """insert into dbo.MV_SECTIONPOINT (mv_section_id, node1, node2, parallel_number,\
                    number, x, y, gdlMINECO, CodigoGDL)
                values('"""+str(mv_section_id)+"""','"""+str(node1)+"""','"""+str(node2)+"""','1',
                '"""+str(nro)+"""','"""+str(x)+"""','"""+str(y)+"""','"""+mineco[:3]+"""','"""+seccN["gdlCode"]+"""')"""
        query = query.replace("'None'","NULL")
        if uDB.execute(query,'DMS') < 0:
            error = -1
    return error

def insertST(MINECO, NAME, Un):
    """Insert needed data to dbo.SUSBTATION to add one Border Point to DMS"""
    query = "INSERT INTO SUBSTATION(CODE,NAME,Un) VALUES('"+MINECO+NAME+"','"+NAME+"','"+str(Un)+"')"
    query = query.replace("'None'","NULL")
    if uDB.execute(query,'DMS') == -1:
        errorMsg = "ERROR. SQL insert failed"
        errorBox = "Fallo al insertar el punto frontera " + NAME
        eh.handleError(errorMsg,errorBox)
        
def insertSTDiagram(code, type, bpx, bpy):
    """Insert needed data to dbo.diagram to add one border point diagram to DMS"""
    dsX1 = bpx - 2000
    dsY1 = bpy + 1000
    dsX2 = bpx + 2000
    dsY2 = bpy + 11000
    
    query = "INSERT INTO dbo.DIAGRAM (code,type,x1,x2,y1,y2,hide_diagram) \
                VALUES ('"+code+"','"+type+"','"+str(dsX1)+"','"+str(dsX2)+"','"+str(dsY1)+"','"+str(dsY2)+"','0')"
    query = query.replace("'None'","NULL")
    if uDB.execute(query,'DMS') == -1:
        errorMsg = "ERROR. SQL insert failed"
        errorBox = "Fallo al insertar el diagrama del punto frontera " + code
        eh.handleError(errorMsg,errorBox)                                              

def insertPrimaryTrafo(nodecode, code, Sn1, Un1, Un2, MINECO):
    """Insert needed data to dbo.PRIMARY_TRANSFORMER to add one primary trafo of a border point"""
    query = "INSERT INTO PRIMARY_TRANSFORMER(NODECODE,CODE,Sn1,Un1,Un2,\
    SET_VALUE_U2,REGIONCODE) VALUES('"+nodecode+"','"+code+"','"+str(Sn1)+"','"+str(Un1)+"','"+str(Un2)+"','"+str(Un2)+"','"+MINECO+"')"
    query = query.replace("'None'","NULL")
    if uDB.execute(query,'DMS') == -1:
        errorMsg = "ERROR. SQL insert failed"
        errorBox = "Fallo al insertar el trafo primario " + code
        eh.handleError(errorMsg,errorBox)              
        
def insertFeedingPoint(nodecode, code, Un):
    query = "INSERT INTO FEEDINGPOINT(NODECODE,CODE,Un) VALUES('"+nodecode+"','"+code+"','"+str(Un)+"')"
    query = query.replace("'None'","NULL")
    if uDB.execute(query,'DMS') == -1:
        errorMsg = "ERROR. SQL insert failed"
        errorBox = "Fallo al insertar el feeding point en un bp. codigo: " + code
        eh.handleError(errorMsg,errorBox)  
        
def insertBusbarPoint(code,X,Y,number):
    query = "INSERT INTO BUSBARPOINT(CODE,X,Y,NUMBER,HIDE_BUSBAR) VALUES('"+code+"','"+str(X)+"','"+str(Y)+"','"+str(number)+"','0')"
    query = query.replace("'None'","NULL")
    if uDB.execute(query,'DMS') == -1:
        errorMsg = "ERROR. SQL insert failed"
        errorBox = "Fallo al insertar el Busbar point en un bp. codigo: " + code
        eh.handleError(errorMsg,errorBox)  
    
def insertBusbarNode(nodecode,code):
    query = "INSERT INTO BUSBARNODE(NODECODE,CODE) VALUES ('"+nodecode+"','"+code+"')"
    query = query.replace("'None'","NULL")
    if uDB.execute(query,'DMS') == -1:
        errorMsg = "ERROR. SQL insert failed"
        errorBox = "Fallo al insertar el Busbar node en un bp. codigo: " + code
        eh.handleError(errorMsg,errorBox)

def insertMVFeeder(nodecode,code):
    query = "INSERT INTO MV_FEEDER(NODECODE,CODE,PenColorRef,PenWidth)\
                    VALUES ('"+nodecode+"','"+code+"',0,0)"
    query = query.replace("'None'","NULL")
    if uDB.execute(query,'DMS') == -1:
        errorMsg = "ERROR. SQL insert failed"
        errorBox = "Fallo al insertar el MV feeder en un bp. codigo: " + code
        eh.handleError(errorMsg,errorBox)
        
def deleteMV_LVSubs(MINECO,code):
    """Delete MV_LV_SUBSTATIONS from MV_LV_SUBSTATIONS. 
    If code == None delete all MV_LV_SUBSTATIONS of given MINECO. Otherwise delete MV_LV_SUBSTATION of given code.
    """
    query = "DELETE FROM DBO.MV_LV_SUBSTATIONS WHERE CODE LIKE '"
    if code == None:
        query = query + MINECO[:3] + "%'"
    else:
        query = query + str(code) + "'"
    return uDB.execute(query,'DMS')

def deletegdlCeldas(MINECO,code):
    """Delete celdas from dbo.gdlCeldas. 
    If code == None delete all gdlCeldas of given MINECO. Otherwise delete gdlCeldas of given code.
    """
    
    query = "DELETE FROM DBO.gdlCeldas WHERE CodigoCT LIKE '"
    if code == None:
        query = query + MINECO[:3] + "%'"
    else:
        query = query + str(code) + "'"
    return uDB.execute(query,'DMS')

def deleteTransformers(MINECO,placingSite):
    """Delete Tranformers from dbo.transformer. 
    If placingSite == None delete all transformers of given MINECO. Otherwise delete transformer of given placingSite.
    """
    
    query = "DELETE FROM DBO.TRANSFORMER WHERE CODE LIKE '"
    if placingSite == None:
        query = query + MINECO[:3] + "%'"
    else:
        query = query + str(placingSite)[:-1] + "%'"
    return uDB.execute(query,'DMS')
        
def deleteDiagrams(MINECO,code):
    """Delete Diagrams from dbo.diagram. 
    If code == None delete all diagrams of given MINECO. Otherwise delete diagram of given code.
    """
    
    query = "DELETE FROM DBO.DIAGRAM WHERE CODE LIKE '"
    if code == None:
        query = query + MINECO[:3] + "%'"
    else:
        query = query + str(code) + "'"
    return uDB.execute(query,'DMS')

def deleteMV_Node(code):
    query = "DELETE FROM MV_NODE WHERE CodigoGDL = '" + code + "'"
    return uDB.execute(query,'DMS')

def delete_distributor_without_substations(MINECO):
    """Delete all company network elements but substations"""
    
    queries = []
    queries.append("""DELETE FROM MV_NODE WHERE CODE IN (
                SELECT CODE FROM gdlExtension, MV_NODE WHERE gdlMINECO IS NULL AND X BETWEEN X1*1000 AND X2*1000 
                AND Y BETWEEN Y1*1000 AND Y2*1000 AND MINECO LIKE '""" + str(MINECO[:3]) + """%'
                AND SITENODE NOT IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))""")
    queries.append("""DELETE FROM MV_NODE WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'
                    AND SITENODE NOT IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER)""")
    queries.append("""DELETE FROM MV_SECTIONPOINT WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM DISCONNECTOR WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'
                    AND NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))""")
    queries.append("""DELETE FROM CUSTOMER WHERE LV_NETWORK LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM CUSTOMER_NODE_LOAD WHERE CUSTOMER_NODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM DIAGRAM WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'
                    AND CODE NOT IN (SELECT CODE FROM SUBSTATION)""")
    queries.append("""DELETE FROM CAPACITOR WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM CIRCUIT_BREAKER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'
                    AND NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))""")
    queries.append("""DELETE FROM LV_BOX WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM LV_NODE WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM LV_SECTION WHERE LV_NETWORK LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM LV_SECTIONPOINT WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM LV_CUSTOMER_NODE WHERE CUSTOMER_NODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM MV_FEEDER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'
                    AND NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))""")
    queries.append("""DELETE FROM MV_LV_SUBSTATION WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM MV_LV_LOAD WHERE MV_LV_SUBSTATION LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM MV_SECTION WHERE DISTRICT LIKE '""" + str(MINECO[:3]) + """%'
                    AND NODE1 NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER)) AND NODE2 NOT IN 
                    (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))""")
    queries.append("""DELETE FROM MV_SITE WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM NODE_REGIONS WHERE REGION_CODES LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM NODE_REGIONS2 WHERE REGION_CODES LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM TRANSFORMER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM TRANSFORMER_NODE WHERE MV_LV_SUBSTATION LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM TRANSFORMER_PLACING WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM gdlCTConnections WHERE LI_IdInst LIKE '""" + str(MINECO[:3]) + """%'""")
    # delete gdlCTConnections references to non-existent nodes
    queries.append("""DELETE FROM gdlCTConnections WHERE CT_NODECODE NOT IN (SELECT CODE FROM MV_NODE)""")
#    queries.append("DELETE FROM FEEDINGPOINT WHERE CODE LIKE ?",MINECO[0:3] + '%')
    queries.append("""DELETE FROM SwitchingComponent WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM NODE_REGIONS WHERE REGION_CODES LIKE '""" + str(MINECO[:3]) + """%'""")
#    queries.append("DELETE FROM PRIMARY_TRANSFORMER WHERE CODE LIKE ?",MINECO[0:3] + '%')
    queries.append("""DELETE FROM MV_FUSE WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM gdlOPC WHERE MINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM LV_SWITCH WHERE BOX LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM gdlLV_NODE_BTVano WHERE IdBTVano LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM gdlXYDMS WHERE CodigoAcometida LIKE '""" + str(MINECO[:3]) + """%'""")   
#    DELETE SUBSTATIONS FROM BORDER POINTS
    queries.append("""DELETE from SUBSTATION  WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046'""")
    queries.append("""DELETE from PRIMARY_TRANSFORMER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046T'""")
    queries.append("""DELETE from DIAGRAM WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046'""")
    queries.append("""DELETE from FEEDINGPOINT WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046G'""")
    queries.append("""DELETE from BUSBARNODE WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046B'""")
    queries.append("""DELETE from BUSBARPOINT WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046B'""")
    queries.append("""DELETE from CIRCUIT_BREAKER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046I'""")
    queries.append("""DELETE from MV_FEEDER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046F'""")
    queries.append("""DELETE from MV_NODE WHERE CODE IN (
                  SELECT NODE1 from MV_SECTION WHERE SUBSTATION LIKE '""" + str(MINECO[:3]) + """%5046' UNION
                  SELECT NODE2 from MV_SECTION WHERE SUBSTATION LIKE '""" + str(MINECO[:3]) + """%5046')""")
    queries.append("""DELETE from MV_SECTION WHERE SUBSTATION LIKE '""" + str(MINECO[:3]) + """%5046'""")
    queries.append("""DELETE FROM [gdlCeldas] WHERE CodigoCT LIKE '""" + str(MINECO[:3]) + """%'""")   

    for query in queries:
        uDB.execute(query,'DMS')
        
def delete_distributor_with_substations(MINECO):
    """Delete all company network elements but substations"""
    
    queries = []
    queries.append("""DELETE FROM MV_NODE WHERE CODE IN (
                SELECT CODE FROM gdlExtension, MV_NODE WHERE gdlMINECO IS NULL AND X BETWEEN X1*1000 AND X2*1000 
                AND Y BETWEEN Y1*1000 AND Y2*1000 AND MINECO LIKE '""" + str(MINECO[:3]) + """%'
                AND SITENODE NOT IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))""")
    queries.append("""DELETE FROM MV_NODE WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'
                    AND SITENODE NOT IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER)""")
    queries.append("""DELETE FROM MV_SECTIONPOINT WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM DISCONNECTOR WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'
                    AND NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))""")
    queries.append("""DELETE FROM CUSTOMER WHERE LV_NETWORK LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM CUSTOMER_NODE_LOAD WHERE CUSTOMER_NODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM DIAGRAM WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'
                    AND CODE NOT IN (SELECT CODE FROM SUBSTATION)""")
    queries.append("""DELETE FROM CAPACITOR WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM CIRCUIT_BREAKER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'
                    AND NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))""")
    queries.append("""DELETE FROM LV_BOX WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM LV_NODE WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM LV_SECTION WHERE LV_NETWORK LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM LV_SECTIONPOINT WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM LV_CUSTOMER_NODE WHERE CUSTOMER_NODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM MV_FEEDER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'
                    AND NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))""")
    queries.append("""DELETE FROM MV_LV_SUBSTATION WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM MV_LV_LOAD WHERE MV_LV_SUBSTATION LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM MV_SECTION WHERE DISTRICT LIKE '""" + str(MINECO[:3]) + """%'
                    AND NODE1 NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER)) AND NODE2 NOT IN 
                    (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))""")
    queries.append("""DELETE FROM MV_SITE WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM NODE_REGIONS WHERE REGION_CODES LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM NODE_REGIONS2 WHERE REGION_CODES LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM TRANSFORMER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM TRANSFORMER_NODE WHERE MV_LV_SUBSTATION LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM TRANSFORMER_PLACING WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM gdlCTConnections WHERE LI_IdInst LIKE '""" + str(MINECO[:3]) + """%'""")
    # delete gdlCTConnections references to non-existent nodes
    queries.append("""DELETE FROM gdlCTConnections WHERE CT_NODECODE NOT IN (SELECT CODE FROM MV_NODE)""")
    queries.append("DELETE FROM FEEDINGPOINT WHERE CODE LIKE ?",MINECO[0:3] + '%')
    queries.append("""DELETE FROM SwitchingComponent WHERE gdlMINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM NODE_REGIONS WHERE REGION_CODES LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("DELETE FROM PRIMARY_TRANSFORMER WHERE CODE LIKE ?",MINECO[0:3] + '%')
    queries.append("""DELETE FROM MV_FUSE WHERE CODE LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM gdlOPC WHERE MINECO LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM LV_SWITCH WHERE BOX LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM gdlLV_NODE_BTVano WHERE IdBTVano LIKE '""" + str(MINECO[:3]) + """%'""")
    queries.append("""DELETE FROM gdlXYDMS WHERE CodigoAcometida LIKE '""" + str(MINECO[:3]) + """%'""")   
#    DELETE SUBSTATIONS FROM BORDER POINTS
    queries.append("""DELETE from SUBSTATION  WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046'""")
    queries.append("""DELETE from PRIMARY_TRANSFORMER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046T'""")
    queries.append("""DELETE from DIAGRAM WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046'""")
    queries.append("""DELETE from FEEDINGPOINT WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046G'""")
    queries.append("""DELETE from BUSBARNODE WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046B'""")
    queries.append("""DELETE from BUSBARPOINT WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046B'""")
    queries.append("""DELETE from CIRCUIT_BREAKER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046I'""")
    queries.append("""DELETE from MV_FEEDER WHERE CODE LIKE '""" + str(MINECO[:3]) + """%5046F'""")
    queries.append("""DELETE from MV_NODE WHERE CODE IN (
                  SELECT NODE1 from MV_SECTION WHERE SUBSTATION LIKE '""" + str(MINECO[:3]) + """%5046' UNION
                  SELECT NODE2 from MV_SECTION WHERE SUBSTATION LIKE '""" + str(MINECO[:3]) + """%5046')""")
    queries.append("""DELETE from MV_SECTION WHERE SUBSTATION LIKE '""" + str(MINECO[:3]) + """%5046'""")
    queries.append("""DELETE FROM [gdlCeldas] WHERE CodigoCT LIKE '""" + str(MINECO[:3]) + """%'""")   

    for query in queries:
        uDB.execute(query,'DMS')
        
def deleteLines(MINECO,):
    """Delete lines not belonging to MV_LV_SUBSTATIONS"""
    queries = []
    queries.append("""DELETE FROM MV_SECTIONPOINT
                WHERE MV_Section_Id IN (SELECT MV_Section_Id FROM MV_SECTION 
                    WHERE DISTRICT LIKE '""" + MINECO + """'
                    AND SUBSTATION IS NULL)""")
    queries.append("DELETE FROM MV_SECTIONPOINT WHERE CodigoGDL LIKE 'L" + MINECO +"%'")
    queries.append("""DELETE FROM [MV_SECTION] WHERE DISTRICT LIKE '""" + MINECO + """'
                AND SUBSTATION IS NULL""")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'L" + MINECO +"%'")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'D" + MINECO +"%'")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'S" + MINECO +"%'")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'O" + MINECO +"%'")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'J" + MINECO +"%'")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'K" + MINECO +"%'")
    queries.append("DELETE FROM MV_NODE WHERE gdlMINECO = '" + MINECO + "' AND CodigoGDL LIKE 'FXX-" + MINECO +"%'")
    queries.append("DELETE FROM DISCONNECTOR WHERE CODE LIKE '" + MINECO +"L%'")
    queries.append("DELETE FROM MV_FUSE WHERE CODE LIKE '" + MINECO +"L%'")
    for query in queries:
        uDB.execute(query,'DMS')
    
def deleteBorderPoints(MINECO):
    """Delete borderpoints not belonging to MV_LV_SUBSTATIONS"""
    queries = []
    queries.append("DELETE FROM DIAGRAM WHERE [TYPE] = 'A' AND CODE LIKE '"+MINECO+"%'")
    queries.append("DELETE FROM SUBSTATION WHERE CODE LIKE '" + MINECO +"%'")
    queries.append("DELETE FROM PRIMARY_TRANSFORMER WHERE CODE LIKE '" + MINECO +"%'")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'B%' AND gdlMINECO = '" + MINECO +"'")
    queries.append("DELETE FROM FEEDINGPOINT WHERE CODE LIKE '" + MINECO +"%'")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'G%' AND gdlMINECO = '" + MINECO +"'")
    queries.append("DELETE FROM BUSBARPOINT WHERE CODE LIKE '" + MINECO +"%'")
    queries.append("DELETE FROM BUSBARNODE WHERE CODE LIKE '" + MINECO +"%'")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'N%' AND gdlMINECO = '" + MINECO +"'")
    queries.append("DELETE FROM CIRCUIT_BREAKER WHERE CODE LIKE '" + MINECO +"%'")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'C%' AND gdlMINECO = '" + MINECO +"'")
    queries.append("DELETE FROM MV_NODE WHERE CodigoGDL LIKE 'H%' AND gdlMINECO = '" + MINECO +"'")
    queries.append("DELETE FROM MV_FEEDER WHERE CODE LIKE '" + MINECO +"%'")           
    queries.append("DELETE FROM MV_SECTION WHERE NAME LIKE '"+MINECO+"PF-%'")
    for query in queries:
        uDB.execute(query,'DMS')
    
    
def deleteSectionPoint(gdlCode):
    query = "DELETE FROM MV_SECTIONPOINT WHERE CodigoGDL = '" + gdlCode + "'"
    return uDB.execute(query,'DMS')
    
        
def getDMSCode(xg, yg):
    """Create Node code of one node from its coordinates in DMS (x,y)"""
    
    xxg = str(int(xg))
    yyg = str(int(yg))

    while len(xxg)<6:
        xxg = '0' + xxg
    while len(yyg)<6:
        xxg = '0' + xxg   

    nodecode = xxg[0:3] + yyg[0:3] + xxg[3:6] + yyg[3:6]
    return nodecode

def getElementCode(MINECO, elemType, number, CTCode):
    endings = [] #endings is the code to add at the end of disconnector code = ['A', 'B',... 'AA', 'AB', ...]
    for i in range(65,91):
        endings.append(chr(i))
    for i in range(65,91):
        for j in range(65,91):
            endings.append(chr(i)+chr(j))

    if len(CTCode) > 26:
        CTCode = CTCode[:27]
        
    code = CTCode + elemType + str(number)
    
    freeCode = False
    codeTemp = code
    
    if elemType == 'FUS': # SI SE A�ADEN M�S TIPOS DE ELEMENTOS HABR� QUE SE�ALARLO AQU�
        table = 'MV_FUSE'
    else:
        table = 'DISCONNECTOR'
    
    for end in endings:
        query = "Select CODE from dbo." + table + " WHERE CODE LIKE '" + codeTemp +"'"
        result = uDB.selectOne(query,'DMS')
        if result == None:
            return codeTemp
        else:
            codeTemp = code + end

import utiDataBase as ud

"""
Este programa pone el codigo MINECO en los campos necesarios de una red creada a mano para que pueda ser enviada a CIDE.
Hay que tener en cuenta que la base de datos debe tener unica y exclusivamente la red que se desea fijar el MINECO,
ya que si hay mas de una red, su MINECO tambien sera modificado.
"""



MINECO = "180" #Poner como string

dataBase = 'DMS2' #Definida en config.ini

#------------- MT
#BUSBARNODE: CODE DEBE EMPEZAR POR MINECO
#BUSBARPOINT: CODE DEBE EMPEZAR POR MINECO
#CAPACITOR: CODE DEBE EMPEZAR POR MINECO
#CIRCUIT_BREAKER: CODE DEBE EMPEZAR POR MINECO
#DIAGRAM: CODE DEBE EMPEZAR POR MINECO
#DISCONNECTOR: CODE DEBE EMPEZAR POR MINECO
#FEEDINGPOINT: CODE DEBE EMPEZAR POR MINECO
#MV_FEEDER: CODE DEBE EMPEZAR POR MINECO
#MV_FUSE: CODE DEBE EMPEZAR POR MINECO
#PRIMARY_TRANSFORMER: CODE DEBE EMPEZAR POR MINECO
#SUBSTATION: CODE DEBE EMPEZAR POR MINECO
#TRANSFORMER: CODE DEBE EMPEZAR POR MINECO. PLACING_SITE DEBE CORRESPONDER CON EL CODIGO DE LA MV_LV_SUBSTATION DONDE SE ENCUENTRA EL TRAFO.
#TRANSFORMER_NODE: MV_LV_SUBSTATION CORRESPONDE AL SITENODE Y AL NODECODE DEL TRAFO.
#TRANSFORMER_PLACING: CODE DEBE EMPEZAR POR MINECO

table = "MV_NODE"
column = "gdlMINECO"
query = "UPDATE " + table + " SET " + column + " = '" + MINECO + "'"
print query
ud.execute(query,dataBase)

table = "MV_SECTION"
column = "DISTRICT"
query = "UPDATE " + table + " SET " + column + " = '" + MINECO + "'"
print query
ud.execute(query,dataBase)

table = "MV_SECTIONPOINT"
column = "gdlMINECO"
query = "UPDATE " + table + " SET " + column + " = '" + MINECO + "'"
print query
ud.execute(query,dataBase)

table = "MV_SITE" #SITECODE DEBE  CORRESPONDER AL CODIGO DE LA MV_LV_SUBSTATION
column = "gdlMINECO"
query = "UPDATE " + table + " SET " + column + " = '" + MINECO + "'"
print query
ud.execute(query,dataBase)

table = "SwitchingComponent" #SWITCH DEBE CORRESPONDER AL CODIGO DEL ELEMENTO SCC, INT O LO QUE SEA.
column = "gdlMINECO"
query = "UPDATE " + table + " SET " + column + " = '" + MINECO + "'"
print query
ud.execute(query,dataBase)


#------------ BT
#CUSTOMER: CUSTOMER_NODE DEBE EMPEZAR POR MINECO. LV_NETWORK DEBE SER EL CODIGO DE SU MV_LV_SUBSTATION
#CUSTOMER_NODE_LOAD: CUSTOMER_NODE DEBE EMPEZAR POR MINECO.
#LV_CUSTOMER_NODE: CUSTOMER_NODE DEBE EMPEZAR POR MINECO. LV_NETWORK DEBE SER EL CODIGO DE SU MV_LV_SUBSTATION. FEEDER DEBE SER EL CODIGO DE LA SALIDA DE BT. BOX DEBE SER EL CODIGO DE LA CAJA DE BT.
#LV_BOX: CODE DEBE EMPEZAR POR MINECO
#LV_SECTION: LV_NETWORK DEBE SER EL CODIGO DE SU MV_LV_SUBSTATION
#LV_SWITCH: FEEDER DEBE SER EL CODIGO DE LA SALIDA DE BT. BOX DEBE SER EL CODIGO DE LA CAJA DE BT.
#MV_LV_LOAD: MV_LV_SUBSTATION DEBE SER EL CODIGO DE SU MV_LV_SUBSTATION


table = "LV_NODE"
column = "gdlMINECO"
query = "UPDATE " + table + " SET " + column + " = '" + MINECO + "'"
print query
ud.execute(query,dataBase)


table = "LV_SECTIONPOINT"
column = "gdlMINECO"
query = "UPDATE " + table + " SET " + column + " = '" + MINECO + "'"
print query
ud.execute(query,dataBase)

table = "MV_LV_SUBSTATION" #CODE DEBE SER MINECO-codigo
column = "COMPANY"
query = "UPDATE " + table + " SET " + column + " = '" + MINECO + "'"
print query
ud.execute(query,dataBase)


# -*- coding: iso-8859-15 -*-
'''
Created on 30/07/2012
Last Modification on 02/12/2013 by ALP
@author: Eugenio, Marco, ALP
'''
import wx
import os
import mainWindow
import utiGedlux
#
#DESCOMENTAR EN PRODUCCION
#sys.setdefaultencoding('iso-8859-15')


class Main(wx.Frame):
    """Login form window"""
    def __init__(self):
        """Configure shape window and locked parameters"""
        
        #Generates the window
        wx.Frame.__init__(self, None, -1, 'Login TrazaImport', 
                size=(330, 160),style= wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
        panel = wx.Panel(self, -1)
        self.Centre()

        #Set GEDLux logo on the window
        logoGedlux = wx.Image('media/logo_gedlux.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        wx.StaticBitmap(panel, -1, logoGedlux, (30, 5), (logoGedlux.GetWidth(), logoGedlux.GetHeight()))
        
        #Create username field and, it with SO user name and disable the field for being changed
        self.lblUser = wx.StaticText(panel, label="user :",pos=(20,50))
        self.tbUser = wx.TextCtrl(panel, pos=(80,50), size=(140, -1), style = wx.NORMAL)
        self.tbUser.SetLabel(os.environ.get( "USERNAME" ).lower())
        self.tbUser.Disable()
        
        #Create password field
        self.lblPassword = wx.StaticText(panel, label="password :",pos=(20,90))
        self.tbPassword = wx.TextCtrl(panel, pos=(80,90), size=(140, -1), style = wx.TE_PASSWORD)
        
        #Create login button and bind it to the event OnLogin
        self.LoginButton= wx.Button(panel, -1, "Login", pos=(230, 90))
        self.LoginButton.Bind(wx.EVT_BUTTON, self.OnLogin)
        
        #Bind password value to the event OnLogin and set the acction o
        self.tbPassword.Bind(wx.EVT_TEXT_ENTER, self.OnLogin)

        
    def OnLogin(self,event):
        """Describes what happens when pressing 'Login' button"""
        
        #Retrieve OS username
        user = os.environ.get( "USERNAME" ).lower()
        
        #Check password
        passCode = utiGedlux.checkPassword(user,self.tbPassword.Value)
        if passCode == 1:
            self.Close(True)
            mainWindow.openForm()
        elif passCode == -2:
            return
        elif passCode == -3:
            dlg = wx.MessageBox("El usuario actual no est� dado de alta en el sistema", "Sin resultados", wx.OK | wx.ICON_INFORMATION)
        else:
            dlg = wx.MessageBox("password incorrecto", "password incorrecto", wx.OK | wx.ICON_INFORMATION)

        
def start():
    """Shows the login window"""
    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
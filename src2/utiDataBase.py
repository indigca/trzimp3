# -*- coding: iso-8859-15 -*-
'''
Created on 03/12/2013
@author: ALP
'''

import wx
from configReader import ConfigReader as cReader
import pyodbc
import exceptionHandler as eh

def getConnectionSQL (server, uid, pwd, database):
    """Returns a connection to a SQL Server""" 
    
    #Creates waiting window------------------------------------------------------------NEEDS IMPROVEMENT!!!
    #wx.MessageBox("Comunicando con la Base de Datos","Espere")
    #dial = wx.MessageDialog(None, 'Comunicando con la Base de datos...', 'Espere', wx.OK)
    #dial.ShowModal()
    #dial.Hide()
    
    cnx = None
    try:
        cnx = pyodbc.connect('DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + uid + ';PWD=' + pwd + ';autocommit=False')
    except Exception as e:
        errorMsg = 'ERROR. SQL Connection failed \n DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + uid + ';PWD=' + pwd + ';autocommit=False'
        errorBox = "Problemas de conexi�n con la Base de Datos"
        returnValue = -1
        return eh.handleExceptionError(e,errorMsg,errorBox,returnValue)
    
    return cnx

def getCursor(selector, option):
    """Returns a Cursor from a connection to a SQL Server if option=1 returns also the connection as a tuple"""
    
    #Connect to DB depending on selector
    cnx = -1
    if selector == 'DMS':
        address, user, password, table = cReader().getDMSdata()
        if address != -1:
            cnx = getConnectionSQL(address, user, password, table)
    elif selector == 'TRAZA':
        address, user, password, table = cReader().getTRAZAdata()
        if address != -1:
            cnx = getConnectionSQL(address, user, password, table)
    elif selector == 'DMS2':
        address, user, password, table = cReader().getDMS2data()
        if address != -1:
            cnx = getConnectionSQL(address, user, password, table)
    else:
        errorMsg = "ERROR. Data base selector not expected"
        errorBox = "Se est� intentando conectar a una base de datos no contemplada entre las opciones."
        eh.handleError(errorMsg,errorBox)
     
    #Error handling
    if cnx == -1:
        return cnx
        
    if option == 1:
        return cnx.cursor(),cnx
    return cnx.cursor()


def selectOne(query, selector):
    """Execute the query in a database which depends on the selector and returns the first match"""
    
    #Get cursor
    cursor=getCursor(selector,0)
    
    #If there is no errors, execute the query and return the result
    if cursor == -1:
        return -1
    else:
        try:
            cursor.execute(query)
            result = cursor.fetchone()
            #if result == None:
            #    return None
        except Exception as e:
            errorMsg = "ERROR. SQL query failed: \n" + query
            errorBox = "Problemas en la consulta a la base de datos"
            returnValue = -1
            return eh.handleExceptionError(e,errorMsg,errorBox,returnValue)
        return result
            
def selectAll(query, selector):
    """Execute the query in a database which depends on the selector and returns all matching data"""
    
    #Get cursor
    cursor=getCursor(selector,0)
    
    #If there is no errors, execute the query and return the result
    if cursor == -1:
        return -2
    else:
        try:
            cursor.execute(query)
            result = cursor.fetchall()
        except Exception as e:
            errorMsg = "ERROR. SQL query failed \n query: " + query 
            errorBox = "Problemas en la consulta a la base de datos"
            returnValue = -1
            return eh.handleExceptionError(e,errorMsg,errorBox,returnValue)
        return result
    
def execute(query,selector):
    """Execute the query in a database which depends on the selector and commit the data"""
     
    #Get cursor
    cursor,connection=getCursor(selector,1)
    
    #If there is no errors, execute the query and return the result
    if cursor == -1:
        return -2
    else:
        try:
            connection.autocommit = True
            cursor.execute(query)
        except Exception as e:
            errorMsg = "ERROR. SQL execution failed \n query: " + query
            errorBox = "Problemas en la escritura en la base de datos"
            returnValue = -1
            return eh.handleExceptionError(e,errorMsg,errorBox,returnValue)
        return 0
    
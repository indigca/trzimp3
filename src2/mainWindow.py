# -*- coding: iso-8859-15 -*-
'''
Created on 31/01/2011
Last modification 03/12/2013 by ALP
@author: n1, ALP
'''
# import utiDMS
# import pyodbc
# import ConfigParser
# import trazaCreate
# import LVline
# import formMapsMG
# import formMaps
# import datetime
# import markup
# import HTMLCreate
# import cStringIO
# import check0
# import hashlib
# import importExportExcel
# from db import DataBase
# from lib2to3.pytree import convert
#DESCOMENTAR EN PRODUCCION
#sys.setdefaultencoding('iso-8859-15')

import wx
import os
import sys
import utiGedlux
import utiDMS as uD
import trazaImport
import exceptionHandler as eh


class Main(wx.Frame):
    """"Main Traza Import window"""
    def __init__(self):
        """Configure the view of the window"""
        
        #Generates the window
        wx.Frame.__init__(self, None, -1, 'Importacion de Traza', 
                size=(700, 605),style= wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX )
        panel = wx.Panel(self, -1)
        self.Centre()
        
        #Bind close window button to function onCloseMe
        self.Bind(wx.EVT_CLOSE, self.onCloseMe)
        #bind own close button to function onCloseMe
        self.exitButton = wx.Button(panel, -1, "Salir", pos=(300, 540))
        self.exitButton.Bind(wx.EVT_BUTTON, self.onCloseMe)
        
        #CIDE and GEDLux logos are buttons wich leads to its respective web pages
        bmpGdl = wx.Image("media/logo_gedlux.png", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btnLogoGdl = wx.BitmapButton(panel, -1, bmpGdl, pos=(500, 530))
        self.btnLogoGdl.Bind(wx.EVT_BUTTON, self.onGedluxLogo)
        bmpCIDE = wx.Image("media/logo_cide.png", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btnLogoCIDE = wx.BitmapButton(panel, -1, bmpCIDE, pos=(1, 468))
        self.btnLogoCIDE.Bind(wx.EVT_BUTTON, self.onCideLogo)
             
        #List of companies the user can see.  Bind the select action of a company to onSelect function
        self.selectedDistLabel=wx.StaticText(panel,-1,"Seleccione distribuidora",(20,2),size=(140,-1),style=wx.ALIGN_LEFT | wx.ST_NO_AUTORESIZE )
        user = os.environ.get( "USERDOMAIN" ) + "\\" + os.environ.get( "USERNAME" )
        companiesList = utiGedlux.getCompaniesList(user)
        self.companiesListField = wx.ListBox(panel, -1, (20, 20), (250, 80), companiesList, 
                wx.LB_SINGLE)
        self.companiesListField.SetSelection(-1)
        self.companiesListField.Bind(wx.EVT_LISTBOX,self.onSelectCompany)
        
        #Import button. Bind it to onSelectImportAll
        self.importAllButton = wx.Button(panel, -1, "Importar", pos=(307, 40))
        self.importAllButton.Bind(wx.EVT_BUTTON, self.onImportAll) #---------------------UNIMPLEMENTED onImportAll!!!!!!!!!!!!
        
        #CTs list field
        self.ctsText="Listado de CTs"
        self.ctsLabel=wx.StaticText(panel,-1,self.ctsText,pos=(20,110),size=(80,-1))
        self.ctsListField = wx.ListBox(panel, -1, (20, 130), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
        
        #Lines list field
        self.linesText="Listado de Lineas"
        self.linesLabel=wx.StaticText(panel,-1,self.linesText,pos=(350,110),size=(120,-1))
        self.linesListField = wx.ListBox(panel, -1, (350, 130), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)

        #Trafos list field
        self.trafosText="Listado de Transformadores"
        self.trafosLabel=wx.StaticText(panel,-1,self.trafosText,pos=(20,260),size=(140,-1))
        self.trafosListField = wx.ListBox(panel, -1, (20, 280), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
        
        #Customers list button
        self.customersText="Listado de Clientes asociados"
        self.customersLabel=wx.StaticText(panel,-1,self.customersText,pos=(350,260),size=(140,-1))
        self.customersListField = wx.ListBox(panel, -1, (350, 280), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
        
        #Displays selected company
        self.selectedCompany="Ninguna selecci�n"
        self.selectedCompanyLabel=wx.StaticText(panel,-1,self.selectedCompany,(275,20),size=(140,-1),style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE )
        self.MINECO=""
        
        #Displays delete company button
        self.DeleteDistributorButton = wx.Button(panel, -1, "Borrar distribuidora", pos=(565, 35))
        self.DeleteDistributorButton.Bind(wx.EVT_BUTTON, self.onDeleteCompany)
        
        #Hidden Buttons (just for develop mode)
        #Trafos
        self.importTrafosButton= wx.Button(panel, -1, "3. Importar Trafos", pos=(450, 15))
        self.importTrafosButton.Bind(wx.EVT_BUTTON, self.onImportTrafos)
        self.importTrafosButton.Hide()
        #CTs
        self.importCTsButton= wx.Button(panel, -1, "4. Importar CTs", pos=(450, 35))
        self.importCTsButton.Bind(wx.EVT_BUTTON, self.onImportAllCTs) 
        self.importCTsButton.Hide()
        #One CT
        self.importOneCTButton= wx.Button(panel, -1, "Importar CT seleccionado", pos=(170, 105))
        self.importOneCTButton.Bind(wx.EVT_BUTTON, self.onImportOneCT)
        self.importOneCTButton.Hide()
        #Lines
        self.importLinesButton= wx.Button(panel, -1, "5. Importar Lineas", pos=(450, 55))
        self.importLinesButton.Bind(wx.EVT_BUTTON, self.onImportAllLines) #-----------------------WORKING ON IT!
        self.importLinesButton.Hide()
        #OneLine
        self.importOneLineButton= wx.Button(panel, -1, "Importar Linea seleccionada", pos=(515, 105))
        self.importOneLineButton.Bind(wx.EVT_BUTTON, self.onImportOneLine) #-----------------------WORKING ON IT!
        self.importOneLineButton.Hide()
        #Customers
        self.importCustomersButton= wx.Button(panel, -1, "2. Importar Clientes", pos=(450, 75))
        self.importCustomersButton.Bind(wx.EVT_BUTTON, self.onSelectCustomersButton)#-----------------------NOT IMPLEMENTED OnSelectCustomersButton!!!!!
        self.importCustomersButton.Hide()
        #Delete all companies
        self.DeleteAllCompaniesButton = wx.Button(panel, -1, "Borrar todas distrib.", pos=(565, 65)) #-----------------------NOT IMPLEMENTED onDeleteAllCompanies!!!!!
        self.DeleteAllCompaniesButton.Bind(wx.EVT_BUTTON, self.onDeleteAllCompanies)
        self.DeleteAllCompaniesButton.Hide()
        #Displays delete Lines
        self.DeleteLinesButton = wx.Button(panel, -1, "Borrar Lineas", pos=(565, 5))
        self.DeleteLinesButton.Bind(wx.EVT_BUTTON, self.onDeleteLines)
        self.DeleteLinesButton.Hide()
        #Hidden buttons are shown if develop mode is true
        if utiGedlux.isDevelopMode():
            self.importCTsButton.Show()
            self.importLinesButton.Show()
            self.importTrafosButton.Show()
            self.importCustomersButton.Show()
            self.importOneCTButton.Show()
            self.importOneLineButton.Show()
            self.DeleteAllCompaniesButton.Show()
            self.DeleteLinesButton.Show()



        """
        'Change Password Button'
        bmpCambiarContrasenha = wx.Image("CambiarContrasenha.png", wx.BITMAP_TYPE_ANY).Rescale(56.5,48.5).ConvertToBitmap()
        self.btnCngContras = wx.BitmapButton(panel, -1, bmpCambiarContrasenha, pos=(605, 425))
        self.btnCngContras.Bind(wx.EVT_BUTTON, self.OpenChgPwdFrame)
        


        
        'Import Maps Button'
        self.importMapsButton = wx.Button(panel, -1, "Importar Mapas", pos=(580, 5))
        self.importMapsButton.Bind(wx.EVT_BUTTON, self.OnSelectMapsButton)
        
        
        

#        if userid == "":

#        if userid == "":
#            self.SubestacionCheckbox = wx.CheckBox(panel, -1, "Borrar subestaci�n", pos=(525, 95))
 
        
        'Informe TrazaImport'
        self.LogButton = wx.Button(panel, -1, "Informe TrazaImport", pos=(410, 425))
        self.LogButton.Bind(wx.EVT_BUTTON, self.Log)
        
        'Infome'
        self.InformeTrazaCheckButton = wx.Button(panel, -1, "Informe TrazaCheck", pos=(285, 425))
        self.InformeTrazaCheckButton.Bind(wx.EVT_BUTTON, self.InformeTrazaCheck)
        
        
        'TrazaCheck'
#        self.TrazaCheckButton = wx.Button(panel, -1, "TrazaCheck", pos=(200, 425))
#        self.TrazaCheckButton.Bind(wx.EVT_BUTTON, self.TrazaCheck)
        print self.InformeTrazaCheckButton.GetForegroundColour()
        print self.InformeTrazaCheckButton.GetBackgroundColour()
        
        'TrazaCheck TODAS LA LISTA DE DISTRIBUIDORAS'
        if DataBase.DevelopmentMode == 'YES' :
            self.TrazaCheckALLButton = wx.Button(panel, -1, "TrazaCheck-ALL", pos=(100, 425))
            self.TrazaCheckALLButton.Bind(wx.EVT_BUTTON, self.TrazaCheckALL)
        
        self.ImportMTCheckbox = wx.CheckBox(panel, -1, "Media Tensi�n", pos=(307, 70))
        self.ImportMTCheckbox.Value = True
        
        if isAvanzado:
            self.ImportBTCheckbox = wx.CheckBox(panel, -1, "Baja tensi�n", pos=(307, 85))
        else:
            self.ImportBTCheckbox = wx.CheckBox(panel, -1, "Clientes", pos=(307, 85))
            
        if DataBase.DevelopmentMode == 'YES' :
            'Orden de celdas'
#            self.lbBackup = wx.StaticText(panel, label="Archivo de orden de celdas: ", pos=(50,455))
#            self.tbBackup = wx.TextCtrl(panel, size=(200, -1), pos=(200,455))
#            self.btnFile = wx.Button(panel, label="Seleccionar archivo", pos=(410,455))
#            self.btnFile.Bind(wx.EVT_BUTTON, self.onOpenFile)
            self.btnOrdenCeldas = wx.Button(panel, label="Orden Celdas", pos=(200,425))
            self.btnOrdenCeldas.Bind(wx.EVT_BUTTON, self.onOrdenCeldas)
        
        'Logo'        
#        logoCide = wx.Image('logo_cide2.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
#        wx.StaticBitmap(panel, -1, logoCide, (-10, 490), (logoCide.GetWidth(), logoCide.GetHeight()))

#        logoGedlux = wx.Image('logo_gedlux.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
#        wx.StaticBitmap(panel, -1, logoGedlux, (500, 530), (logoGedlux.GetWidth(), logoGedlux.GetHeight()))


        
        'flecha'        
        #self.panelFlecha = wx.Panel(self)
        flecha = wx.Image('flecha.png', wx.BITMAP_TYPE_ANY).Rescale(50,50).ConvertToBitmap()
        
        self.flechaStaticBitmap=wx.StaticBitmap(panel, -1, flecha, (320, 450), (flecha.GetWidth(), flecha.GetHeight()))
        self.flechaStaticBitmap.Hide()
        

        
        'Import Low Voltage'
#        self.importMapsButton = wx.Button(panel, -1, "6. Importar Baja Tension", pos=(465, 300))
#        self.importMapsButton.Bind(wx.EVT_BUTTON, self.OnSelectLVButton)
        
        
#        self.gauge = wx.Gauge(panel, -1, 50, size=(500, 25),pos=(100, 540))
#        
#    def updateGauge(self, value):
#        self.gauge.SetValue(value)
        """
        
            
            
        

        #--------------------------------------------------------------

    def onCloseMe(self,event):
        """Promts a second window to warn you about you are exiting the program"""
        dlg = wx.MessageDialog(self,
                "�Est� seguro de que quiere salir de TrazaImport?",
                "Mensaje de confirmaci�n", wx.OK|wx.CANCEL|wx.ICON_QUESTION)
        result = dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_OK:
            sys.exit()
        
    def onSelectCompany(self,event):
        """Fills the field of CTs, lines, trafos and customers of a company when it is selected"""

        #Get company name and MINECO code
        try:
            self.selectedCompany=self.companiesListField.GetString(self.companiesListField.GetSelection())
        except Exception as e:
            errorMsg = "WARNING. Selection was not performed correctly"
            return eh.handleExceptionWarning(e,errorMsg,-1)
        self.MINECO=self.selectedCompany[:3]
        self.selectedCompanyLabel.SetLabel(self.selectedCompany)

        #Fill CTs list field
        self.ctsList = utiGedlux.populateCTsListField(self.MINECO)
        self.ctsListField.Clear()
        self.ctsListField.InsertItems(self.ctsList,0)
        self.ctsLabel.SetLabel(self.ctsText+' ('+str(len(self.ctsList))+')')
        
        #Fill lines list field
        self.linesList = utiGedlux.populateLinesListField(self.MINECO)
        self.linesListField.Clear()
        self.linesListField.InsertItems(self.linesList,0)
        self.linesLabel.SetLabel(self.linesText+' ('+str(len(self.linesList))+')')   
        
        #Fill trafos list field
        self.trafosList = utiGedlux.populateTrafosListField(self.MINECO)
        self.trafosListField.Clear()
        self.trafosListField.InsertItems(self.trafosList,0)
        self.trafosLabel.SetLabel(self.trafosText+' ('+str(len(self.trafosList))+')')      

        #Fill customers list field
        customersListTop100 = utiGedlux.populateCustomersListField(self.MINECO)
        self.customersListField.Clear()
        self.customersListField.InsertItems(customersListTop100,0)
        length = len(customersListTop100)
        if length < 100:
            self.customersLabel.SetLabel(self.customersText+' ('+str(length)+')') 
        else:
            self.customersLabel.SetLabel(self.customersText+' (100 primeros)') 
            
    def onGedluxLogo(self,event):
        """Open iexplorer on GEDLux web page"""
        os.system("start iexplore.exe www.gedlux.es")
                   
    def onCideLogo(self,event):
        """Open iexplorer on CIDE web page"""
        os.system("start iexplore.exe www.cide.net")

    def onImportTrafos(self,event):
        """Import all LV trafos from a company and store them on WAREHOUSE-1"""
        #Check if there is a company selected
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
            return
        
        #Check if there are trafos already imported
        if utiGedlux.areThereTrafos(self.MINECO[:3]) == True:
            dlg = wx.MessageBox('Transformadores ya importados', 'Transformadores ya importados', 
                wx.OK | wx.ICON_INFORMATION)
            return
        
        #If all is OK, import trafos
        trazaImport.importTrafos(self.MINECO[:3])
        dlg = wx.MessageBox('Trafos importados', 'Trafos importados', 
        wx.OK | wx.ICON_INFORMATION)


    def onImportOneCT(self,event):
        """Import selected CT"""
        if self.MINECO == "":
             dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
             wx.OK | wx.ICON_INFORMATION)
             
        elif self.ctsListField.GetSelection() < 0:
            dlg = wx.MessageBox('Debe seleccionar un CT', 'Seleccione un CT', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            CTName = self.ctsListField.GetStringSelection()
            if utiGedlux.isCT(self.MINECO[:3],ctName=CTName) == True:
                dlg = wx.MessageBox('CT ya importado', 'CT ya importado', 
                        wx.OK | wx.ICON_INFORMATION)
            else:
                if utiGedlux.areThereTrafos(self.MINECO[:3]) == False:
                    trazaImport.importTrafos(self.MINECO[:3],ctName=CTName)
                trazaImport.importCTs(self.MINECO[:3],ctName=CTName)
                dlg = wx.MessageBox('CT Importado', 'CT Importado', 
                    wx.OK | wx.ICON_INFORMATION)
                
    def onDeleteCompany(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            dlg = wx.MessageBox('�Est� seguro de que quiere eliminar los datos de la distribuidora seleccionada?', 'Eliminar', 
            wx.YES_NO | wx.ICON_INFORMATION)
            if dlg == 2:
                self.importAllButton.Disable()
                self.DeleteDistributorButton.Disable()
                self.Refresh()                
                uD.delete_distributor_without_substations(self.MINECO)
                eh.handleWarning("Datos de la distribuidora "+self.MINECO+" eliminados")
                dlg = wx.MessageBox('Datos de la distribuidora eliminados', 'Datos de la distribuidora eliminados', 
                                    wx.OK | wx.ICON_INFORMATION)
                self.importAllButton.Enable()
                self.DeleteDistributorButton.Enable()
                
    def onDeleteLines(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:              
            uD.deleteLines(self.MINECO)
            dlg = wx.MessageBox('Lineas eliminadas', 'Lineas eliminadas', 
                                wx.OK | wx.ICON_INFORMATION)



    def onImportAllCTs(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            if utiGedlux.isCT(self.MINECO[:3]) == True:
                dlg = wx.MessageBox('Centros de transformaci�n ya importados', 'Centros de transformaci�n ya importados', 
                    wx.OK | wx.ICON_INFORMATION)
                return                
            trazaImport.importCTs(self.MINECO[0:3])
            dlg = wx.MessageBox('Centros importados', 'Centros importados', 
                    wx.OK | wx.ICON_INFORMATION)
            
            
    def onImportOneLine(self,event): #--------------------------------IM WORKING ON IT!!!!!!!!!
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        elif self.linesListField.GetSelection() < 0:
            dlg = wx.MessageBox('Debe seleccionar una l�nea', 'Seleccione una l�nea', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            lineName = self.linesListField.GetStringSelection()
            if utiGedlux.isLine(self.MINECO[:3],lineName=lineName) == True:
                dlg = wx.MessageBox('L�nea ya importada', 'L�nea ya importada', 
                        wx.OK | wx.ICON_INFORMATION)
            else:
                trazaImport.importLines(self.MINECO[:3],lineName=lineName)
                dlg = wx.MessageBox('Linea importada', 'Linea importada', 
                                    wx.OK | wx.ICON_INFORMATION)
                
    def onImportAllLines(self,event): #--------------------------------IM WORKING ON IT!!!!!!!!!
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            if utiGedlux.isLine(self.MINECO[:3]) == True:
                dlg = wx.MessageBox('L�neas ya importadas', 'L�neas ya importadas', 
                        wx.OK | wx.ICON_INFORMATION)
                return
            else:
                trazaImport.importLines(self.MINECO[:3])
                dlg = wx.MessageBox('Lineas importadas', 'Lineas importadas', 
                                    wx.OK | wx.ICON_INFORMATION)
                


    #----------------------------------------------------------------------BELOW ARE NOT YET ADDED FUNCTIONS!!!!

        #AKI ESTOY: YA HE HECHO LA IMPORTACION DE LOS TRAFOS, AHORA DEBERIA PASAR A IMPORTAR ONECT PARA
        #LUEGO IMPORTAR ALL CTs
 






    






    def onOpenFile(self, event):
        wildcard = "zip file (*.zip)|*.zip"
        """
        Create and show the Open FileDialog
        """

        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultFile="",
            defaultDir="C:\Backups",
#            wildcard=wildcard,
            style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR
            )
        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPaths()
            for path in paths:
                self.tbBackup.SetValue(path)
        dlg.Destroy()
        
    def Log(self,event):
        print DataBase.log
        os.system("start iexplore.exe "+DataBase.log)
        
    def InformeTrazaCheck(self,event):
        global fallos
        
        if self.MINECO!="":
            if fallos is None:
                self.TrazaCheck(event)
                
            os.system("start iexplore.exe "+DataBase.Check+self.MINECO[0:3]+"/chequeo/")
            
            if fallos>0 and DataBase.SugerenciaErrores == 'YES' :
                dlg = wx.MessageBox('�Quieres consultar la guia de TrazaCheck para saber como solucionar los problemas de importaci�n?','Se han encontrado errores!', wx.YES_NO | wx.ICON_QUESTION)
                if dlg==wx.YES:
                    print 'YES!'
                    os.system("start iexplore.exe "+DataBase.Check+"/SolucionTrazaCheck.html")
                else:
                    print 'NO!'
        else:
            dlg = wx.MessageBox('Seleccione una distribuidora', 'Seleccione una distribuidora', 
                                            wx.OK | wx.ICON_INFORMATION)
    def TrazaCheck(self,event):
        
        if self.MINECO!="":
            self.InformeTrazaCheckButton.SetBackgroundColour((240, 240, 240,255)) #Default Grey
            self.InformeTrazaCheckButton.SetForegroundColour((0,0,0,255))
            self.flechaStaticBitmap.Hide()
            
            global fallos
            fallos=check0.check(self.MINECO[0:3])
            
            
            if fallos>0:
                mensaje='NO SE PUEDE IMPORTAR TODAV�A, consulte el Informe de datos de traza creado, se han encontrado '+str(fallos)+' errores!'
            else:
                mensaje='Informe de datos de traza creado, no se han encontrado errores, puede proceder con la importaci�n.'
                
            dlg = wx.MessageBox(mensaje, 'Operacion Finalizada', 
                                            wx.OK | wx.ICON_INFORMATION)
        else:
            dlg = wx.MessageBox('Seleccione una distribuidora', 'Seleccione una distribuidora', 
                                            wx.OK | wx.ICON_INFORMATION)        
            
    def TrazaCheckALL(self,event):
         global MINECOList
         
         for distribuidora in MINECOList:
             print distribuidora
             check0.check(distribuidora)
         
         print 'TrazaCheck ALL REALIZADO CORRECTAMENTE'
        
        
    def OnChgPwd(self,event):
        if len(self.tbPassword.GetLabel()) >= 4 and len(self.tbRepPassword.GetLabel()) >= 4:
            print 'contrase�a de por lo menos 4 caracteres'
            if self.tbPassword.GetLabel() == self.tbRepPassword.GetLabel():
                self.ChgPwdInBD(self.tbPassword.GetLabel())
            else:
                print 'distintos'
                wx.MessageBox("Las contrase�as introducidas NO COINCIDEN", "Error", wx.OK | wx.ICON_WARNING)
                self.tbPassword.SetLabel("")
                self.tbRepPassword.SetLabel("")
        else:
             wx.MessageBox("La contrase�a tiene que estar compuesta por lo menos de 4 caracteres", "Error", wx.OK | wx.ICON_WARNING)
             self.tbPassword.SetLabel("")
             self.tbRepPassword.SetLabel("")
             
    def ChgPwdInBD(self,password):
        print 'cambia la contrase�a en la Base de Datos'
        'Connect to DMS600'
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        userid = os.environ.get( "USERNAME" ).lower()
#        #BORRAR EN PRODUCCION
        userid = 'gedlux'
        
        password=self.tbPassword.GetLabel()
        user_reverse = userid[::-1]
        md5introducido = hashlib.md5(password+ user_reverse).hexdigest()
        sentencia = "update gdlTrazaImportUser set password= '%s' where usuario='%s'" % (md5introducido,userid)
        print sentencia
        try:
            cursorD.execute(sentencia)
        except:
            return
        self.cnxnD.commit()
        
        if self.isPasswordEqualTo(md5introducido)== True:
            print 'contrase�a igual'
            dialog= wx.MessageBox("La contrase�a ha sido modificada con exito!", "Contrase�a modificada", wx.OK | wx.ICON_INFORMATION)
        else:
            print 'contrase�a distinta'
            dialog= wx.MessageBox("No ha sido posible modificar la contrase�a", "ERROR", wx.OK | wx.ICON_ERROR)
            
        self.frame.Close()
    
    def isPasswordEqualTo(self,md5introducido):
        'Connect to DMS600'
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        userid = os.environ.get( "USERNAME" ).lower()
#       BORRAR EN PRODUCCION
#        userid = 'gedlux'
        
        sentencia = "SELECT password FROM gdlTrazaImportUser where usuario= '%s'" % userid
        print sentencia
        try:
            cursorD.execute(sentencia)
            row=str(cursorD.fetchone())
        except:
            return
        self.cnxnD.commit()
        
        print row
        print row[3:-4]
        
        if row[3:-4]==md5introducido:
            return True
        else:
            return False
        
       
    
    def OpenChgPwdFrame(self,event):
        print 'cambia contrase�a'
        
        self.frame = wx.Frame(None, -1, 'Cambiar contrase�a')
        self.frame.SetSize((325,150))
        self.frame.SetBackgroundColour((240,240,240)) 
        self.lblPassword = wx.StaticText(self.frame, label="Contrase�a nueva :",pos=(55,25))
        self.tbPassword = wx.TextCtrl(self.frame, pos=(160,20), size=(130, -1), style = wx.TE_PASSWORD)
        self.tbPassword.SetMaxLength(20)
        
        self.lblRepPassword = wx.StaticText(self.frame, label="Confirma Contrase�a nueva :",pos=(10,55))
        self.tbRepPassword = wx.TextCtrl(self.frame, pos=(160,50), size=(130, -1), style = wx.TE_PASSWORD)
        self.tbRepPassword.SetMaxLength(20)
        
        self.AceptarButton= wx.Button(self.frame, -1, "Aceptar", pos=(215, 80))
        self.AceptarButton.Bind(wx.EVT_BUTTON, self.OnChgPwd)
        
        self.frame.Centre()
        self.frame.Show()
        
        
 

        
        
    def onSelectMapsButton (self,event):
        formMaps.open()
    
        
    def onSelectLVButton (self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM [dbo].[LV_SECTION] \
                              WHERE LV_NETWORK LIKE ?", self.MINECO[0:3] + '%')
            linelvNumber = cursorD.fetchone()
            if linelvNumber == None:
                if isAvanzado:
                    WriteLog("Importacion lineas BT  iniciada")
                else:
                    WriteLog("Importacion clientes (Basico) iniciada")
                    
                LVline.Import(self.MINECO[0:4])
                if isAvanzado:
                    WriteLog("Importacion lineas BT  finalizada")
                else:
                    WriteLog("Importacion clientes (Basico) finalizada")
            else:
                if isAvanzado:
                    dlg = wx.MessageBox('Baja tensi�n ya importada', 'Baja tensi�n ya importada', wx.OK | wx.ICON_INFORMATION)
                else:
                    dlg = wx.MessageBox('Clientes (Basico) ya importados', 'Clientes ya importada', wx.OK | wx.ICON_INFORMATION)
        


    
#    def OnSelectDevelopModeCheckbox(self,event):
#        if self.DevelopModeCheckbox.IsChecked():
#            self.importCTsButton.Show()
#            self.importLinesButton.Show()
#            self.importTrafosButton.Show()
#            self.importCustomersButton.Show()
#        else:
#            self.importCTsButton.Hide()
#            self.importLinesButton.Hide()
#            self.importTrafosButton.Hide()
#            self.importCustomersButton.Hide()
            

    

        



    def onSelectCustomersButton(self,event):
        cursorD = self.cnxnD.cursor()
        cursorD.execute("SELECT *\
                      FROM [dbo].[CUSTOMER]\
                      where LV_NETWORK like ?",self.MINECO[0:3] + '%')
        customersNumber = cursorD.fetchone()
        if customersNumber == None:
            if self.MINECO == "":
                dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
                wx.OK | wx.ICON_INFORMATION)
            else:
                WriteLog(self.CTsList.GetStringSelection())
                WriteLog(self.MINECO)
        #        try:
                WriteLog("Importacion clientes iniciada")
                trazaCreate.customer(self.cnxnT, self.MINECO)
                WriteLog("Importacion clientes finalizada")
                WriteHTML()
        #        except Exception:
        #            print Exception.message
        else:
            dlg = wx.MessageBox('Clientes ya importados', 'Clientes ya importados', 
                wx.OK | wx.ICON_INFORMATION)
        


        
    def onDeleteAllCompanies(self,event):
        dlg = wx.MessageBox('�Est� seguro de que quiere eliminar todos los datos de todas las distribuidoras?', 'Eliminar', 
        wx.YES_NO | wx.ICON_INFORMATION)
        
        if dlg == 2:

            WriteLog("Eliminando datos de todas las distribuidoras")
            #if self.SubestacionCheckbox:
            trazaCreate.delete_all_Companies()
            #else:
            #   trazaCreate.delete_all_Companies_without_substations()
            WriteLog("Datos de todas las distribuidoras eliminados")
            WriteHTML()
            dlg = wx.MessageBox('Datos de las distribuidoras eliminados', 'Datos de las distribuidoras eliminados', 
                                    wx.OK | wx.ICON_INFORMATION)
        
    def onImportAll(self,event):
        incidenciaImportacion=False
        
        print str(self.MINECO.replace("A", ""))
        print str(self.MINECO.replace("B", ""))
        
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            self.MINECO=self.MINECO.replace("A", "")
            self.MINECO=self.MINECO.replace("B", "")
            if  uD.getValidacionTrazaCheck(self.MINECO)==False  and DataBase.SugerenciaErrores == 'YES' :
                wx.MessageBox( 'Antes de importar tiene que ejecutar TrazaCheck', 'Aviso',wx.OK | wx.ICON_INFORMATION)
                self.InformeTrazaCheckButton.SetBackgroundColour((255,0,0)) #RED
                self.flechaStaticBitmap.Show()
                return
            
            if self.ImportMTCheckbox.Value == False and self.ImportBTCheckbox.Value == False:
                self.ImportMTCheckbox.SetValue(True)
                       
            self.ImportAllButton.Disable()
            self.DeleteDistributorButton.Disable()
            
            if self.ImportMTCheckbox.Value == True:
                cursorD = self.cnxnD.cursor()
                cursorD.execute("SELECT *\
                                  FROM TRANSFORMER \
                                  where CODE like ?", self.MINECO[0:3] + '%')
                transformerNumber = cursorD.fetchone()
                if transformerNumber == None:
                    uD.setValidacionTrazaCheck(self.MINECO.replace("A", ""),False)
                    WriteLog("Importando Transformadores")
                    trazaCreate.trafo(DataBase.TRAZATable,DataBase.DMSTable, self.MINECO)
                    WriteLog("Importando Centros de transformacion")
                    trazaCreate.CT(self.cnxnT,self.CTsList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importando Lineas de Alta y Media Tension")            
                    trazaCreate.line(self.cnxnT,self.LinesList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importando borderPoints")                    
                    trazaCreate.borderPoints(self.MINECO[0:3])
                    WriteLog("Importacion finalizada")
                    WriteHTML()
                    
#                    dlg = wx.MessageBox('Importaci�n realizada', 'Importaci�n realizada', 
#                                            wx.OK | wx.ICON_INFORMATION)
                else:
                    dlg = wx.MessageBox('Debe eliminar primero la media tensi�n para importarla de nuevo', 'MT ya importada', 
                                    wx.OK | wx.ICON_INFORMATION)
                    incidenciaImportacion=True
            if self.ImportBTCheckbox.Value == True:
                cursorD = self.cnxnD.cursor()
                cursorD.execute("SELECT *\
                                  FROM TRANSFORMER \
                                  where CODE like ?", self.MINECO[0:3] + '%')
                transformerNumber = cursorD.fetchone()
                if transformerNumber != None:
                    cursorD.execute("SELECT *\
                                      FROM CUSTOMER \
                                      where LV_NETWORK like ?", self.MINECO[0:3] + '%')
                    customerNumber = cursorD.fetchone()
                    if customerNumber == None:
                        WriteLog("Importando Clientes")
                        trazaCreate.customer(self.cnxnT, self.MINECO)
                        if isAvanzado:
                            WriteLog("Importando Lineas de Baja Tension")
                        LVline.Import(self.MINECO[0:4])
                        WriteLog("Importacion finalizada")
                        WriteHTML()
                    else:
                        if isAvanzado:
                            dlg = wx.MessageBox('Red de BT ya importada', 'BT ya importada',  wx.OK | wx.ICON_INFORMATION)
                        else:
                            dlg = wx.MessageBox('Red de Clientes ya importada', 'Clientes ya importada',  wx.OK | wx.ICON_INFORMATION)
                            
                        incidenciaImportacion=True
#                    dlg = wx.MessageBox('Importaci�n realizada', 'Importaci�n realizada', 
#                                            wx.OK | wx.ICON_INFORMATION)
                else:
                    dlg = wx.MessageBox('Debe importar primero la media tensi�n', 'MT no importada', 
                                    wx.OK | wx.ICON_INFORMATION)
                    incidenciaImportacion=True
            trazaCreate.separarLineas()
            
            if incidenciaImportacion!=True:
                dlg = wx.MessageBox('Importaci�n finalizada', 'Importaci�n finalizada', wx.OK | wx.ICON_INFORMATION)
            
            self.ImportAllButton.Enable()
            self.DeleteDistributorButton.Enable()
        
        
    def onOrdenCeldas(self, event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
                wx.OK | wx.ICON_INFORMATION)
        else:
            importExportExcel.openForm(self.MINECO)

#            trazaCreate.ordenCeldas(self.tbBackup.Value, self.MINECO)
#            dlg = wx.MessageBox('Finalizado', 'Orden de celdas actualizado', 
#                                                wx.OK | wx.ICON_INFORMATION)
        
            
#---------------------------------------------------------BELOW IS JUST FOR TESTING   
if __name__ == '__main__':
    app = wx.PySimpleApp()
    
    s = 'hola como estas'
    l = s.replace(' ','')
    print l
    
    #uD.deleteLines('180')
#     uD.deleteBorderPoints('180')
#     print "bp's deleted"
#     trazaImport.importBorderPoints('180')
#     print 'BP imported'

#     uD.delete_distributor_without_substations('180')
#     uD.delete_distributor_without_substations('101')
#     uD.delete_distributor_without_substations('133')
#     uD.delete_distributor_without_substations('044')
#     uD.delete_distributor_without_substations('124')
#     uD.deleteBorderPoints('180')
    
#     MINECO = '101'
#     uD.delete_distributor_with_substations(MINECO)
#     print 'company deleted'
#     trazaImport.importTrafos(MINECO)
#     print 'trafos imported'
#     trazaImport.importCTs(MINECO)
#     print 'CTs imported'
#     trazaImport.importLines(MINECO)#,lineName='SUBDER A R ALONSO_25-04')
#     print 'Lines Imported'
#     trazaImport.importBorderPoints(MINECO)
#     print 'BP imported'

    import utiDataBase as uDB
    dbSrc = 'DMS'
    dbDst = 'DMS2' 
    query = "SELECT * FROM MapAdjustment WHERE NAME LIKE '180%'"
    result = uDB.selectAll(query,dbSrc)
    try:
        fields = len(result[0])
    except:
        fields = 0
    print 'Field in Maps',fields
    cont = 0
    for entry in result:
        cont += 1
        query = "INSERT INTO MapAdjustment VALUES ('"
        for i in range(fields-1):
            query = query + str(entry[i]) + "','"
        query = query + str(entry[fields-1]) + "')"
        query = query.replace("'None'", "NULL")
        print query
        uDB.execute(query,dbDst)
    print 'cont final',cont
    print 'Field in Maps',fields





#     print'FINNNN, falta separate'
#     import utiGedlux as uG
#     uG.separateSectionPoints('101')
#     uG.separateLineNodes('101')
#     print'FINNNN del tooo'

    lines = []
    lines.append('Hola')
    lines.append('Adios')
    if 'Hola' in lines:
        print 'YES'

    #Main().Show()
    #app.MainLoop()
#---------------------------------------------------------ABOVE IS JUST FOR TESTING
    
def openForm():
    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
 

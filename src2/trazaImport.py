# -*- coding: iso-8859-15 -*-
'''
Created on 05/12/2013
@author: ALP
'''
import log
import utiGedlux as uG
import utiCoordinates as uC
import utiDMS as uD
import utiDataBase as uDB
import wx
import exceptionHandler as eh

def importTrafos(MINECO,ctName=None):
    """Import all trafos from Traza and store them on WAREHOUSE-1"""
    log.writeLog("Import trafos for MINECO " + str(MINECO) + " selected. Importing started...")
    PrgDialog = wx.ProgressDialog("Importando", "Importando Transformadores...", maximum=1000, style=wx.PD_AUTO_HIDE)
    PrgDialog.SetSize((400, 110))
    
    #Get the desired values of the list of trafos. More values can be taken.
    list = uG.getTrafosList(MINECO,ctName=ctName)
    i = 0
    for trafo in list:
        Ux = uG.getTrafoVLevels(trafo.Descripcion)
        code = trafo.IdTransformador[:32]
        orderNumber = 0                 #Transformer not placed
        type = trafo.Modelo
        if type != None:
            type = type[:15]
        year = trafo.AnhoFabricacion
        sn1 = trafo.Potencia
        placingSite = 'WAREHOUSE1'
        name = trafo.Nombre
        num = trafo.NumeroTransformador
        
        if uD.insertTrafoSQL(Ux, code, orderNumber, type, year, sn1, placingSite,name,num) == -1:
            errorMsg = "ERROR. SQL insert failed: "+srt(name)+"-"+str(num)
            errorBox = "Problemas al importar el transformador n�mero "+str(num)+" de "+srt(name)
            eh.handleError(errorMsg,errorBox)
        
        i += 1
        PrgDialog.Update(float(i) / float(len(list)) * 1000.0)
        PrgDialog.SetLabel("Faltan "+str(len(list)-i)+" transformadores...")
    PrgDialog.Destroy()
    log.writeLog("Trafos importation finished for MINECO: " + MINECO)
        
def importCTs(MINECO,ctName=None):
    """Import from Traza the CT with given name. If Name == None Import all CTs of given MINECO"""
    log.writeLog("Importing CTs for MINECO " + str(MINECO) + ". Importing started...")
    ProgDialog = wx.ProgressDialog("Importando", "Importando centros de transformaci�n...", maximum=1000, style=wx.PD_AUTO_HIDE)
    ProgDialog.SetSize((400, 110))
    prog = 0
    
    oldPopulates = ['180']
    
    if MINECO in oldPopulates:
        uG.oldPopulate(MINECO)
    
    CTs = uG.getCTsList(MINECO,ctName=ctName)
    
    for CT in CTs:
        print CT
        vLevel = uG.getCTVoltageLevel(CT.Nombre,MINECO)
        
        #In order to being able to have more than one trafo in a CT, each trafo is considered as ione CT itself
        #so we add + '1'  to the CT code to differentiate the first trafo added from the following ones (if there are any)
        code = CT.Codigo + '1' 
        name = CT.Nombre
        
        if MINECO not in oldPopulates:
            uG.populategdlCeldas(MINECO,ctName=name)
        
        #In case traza code does not have the used format we format it
        code = uG.formatCTCode(code,MINECO)
        celdas = uG.getOrderedCeldas(code)
        
        usedCeldas = (1,2,5) #For now, we only draw this kind of celdas
        
        num = len(celdas)
        if num == 0:
            """Modified code to import even CTs without celdas. Old code commented below."""
            uG.insertgdlCeldas(uG.getCeldasFromInsAndOuts(name,MINECO),MINECO)  
            celdas = uG.getOrderedCeldas(code)
            print name + ' no devuelve celdas!!'
#             uD.deleteTransformers(MINECO,code)
#             uD.deletegdlCeldas(MINECO,code)
#             errorMsg = "ERROR. Import CT "+name+".No celdas returned after getOrderedCeldas. len(celdas)"
#             errorBox = "Fallo al importar el centro "+name+". El centro no se importar�."
#             eh.handleError(errorMsg,errorBox)
#             prog += 1
#             continue
        num = len(celdas)
        if num == 0:
            print name + ' no se importar�'
            continue
        
        #Prepare DMS coordinates from one celda
        xg, yg = uC.convertED50(celdas[0].X, celdas[0].Y, uG.getCompanyZone(MINECO))
        xg = int(str(xg).split('.')[0] + "000") #WHY????
        yg = int(str(yg).split('.')[0] + "000") #WHY????

        # municipality code must be padded with zeros
        mun_code = uG.pad(CT.CodigoPeco, 3)
        prov_code = uG.pad(CT.PROV, 2)
        
        #Diagram
        if uD.insertDiagram(code, 'M', xg, yg, num) < 0:
            uD.deleteTransformers(MINECO,code)
            uD.deletegdlCeldas(MINECO,code)
            errorMsg = "ERROR. Import CT "+name+". Couldn't insert diagram " + str(code) + "."
            errorBox = "Fallo al importar el centro "+name+". El centro no se importar�."
            eh.handleError(errorMsg,errorBox)
            prog += 1
            continue
        
        #MV_LV_substation
        if uD.insertMV_LV_Substation(code, name,
                             mun = mun_code,
                             prov = prov_code,
                             mineco = MINECO,
                             propios = int(CT.Particular)^1 ) < 0:
            uD.deletegdlCeldas(MINECO,code)
            uD.deleteTransformers(MINECO,code)
            uD.deleteDiagrams(MINECO,code)
            errorMsg = "ERROR. Import CT "+name+". Couldn't insert MV_LV_SUBSTATION " + str(code) + "."
            errorBox = "Fallo al importar el centro "+name+". El centro no se importar�."
            eh.handleError(errorMsg,errorBox)
            prog += 1
            continue
        
        #SiteNode
        sitenode = uG.getEmptyCode(uD.getDMSCode(xg,yg) + "X1", 'mv_node')
        uD.insertMV_Node(sitenode, xg, yg, xg, yg, sitenode, mineco=MINECO,codigoGDL=uG.getGDLCode('CT',code))
        
        #MV_Site
        uD.insertMV_Site(code, 'M', xg, yg, name=name, mineco=MINECO)    

        
        #Insert Elements in CT
        i = 0 #order counting
        t = 0 #trafos counting
        prevChar = "X" #Initializing variable for creating mv_sections
        prevX = xg
        prevY = yg
        for celda in celdas:
            i += 1
            
            celda.Posicion = i
            typeCelda =  celda.IdTipoCelda
            
            #Insert nodes in busbar
            if typeCelda != 5 and typeCelda in usedCeldas: #WHY NOT IN THE FIRST CELDA for the nodes in busbar?? 
                currentX = xg + 1000 * i
                currentY = yg + 1000
                currentChar = "X"
                currentNodecode = uG.getEmptyCode(uD.getDMSCode(currentX, currentY) + "X1", 'mv_node')
                uD.insertMV_Node(currentNodecode, currentX, currentY, currentX, currentY, sitenode, mineco=MINECO,codigoGDL=uG.getGDLCode('C',code))
            
            #Insert celda's elements                
            if typeCelda == 1: #Celda de linea
                #Insert element itself
                x1 = xg + 1000 * i
                y1 = yg + 2000
                node1 = uD.insertCTElement(celda.DisyuntorFusible,MINECO,i,code,name,x1,y1,sitenode,uG.getGDLCode('CTelem',code,str(i)))
                #Insert exit node
                x2 = x1 
                y2 = yg + 4000
                node2 = uG.getEmptyCode(uD.getDMSCode(x2, y2) + "X1", 'mv_node')
                pos = uG.formatPosition(i)
                uD.insertMV_Node(node2, x2, y2, x2, y2, sitenode, mineco=MINECO,codigoGDL=uG.getGDLCode('E',pos,celda.CodigoCT,str(celda.IdInstLinea)))
                #Insert conection element-exit
                mvSectionId = uD.getMVSectionID()
                uD.insertMVSection(mvSectionId, node1, node2, 'BUSBAR', MINECO, vLevel, x1, y1, SUBSTATION=code, HIDE=1, NAME=name)
            elif typeCelda == 2: #Celda de protecci�n
                #Insert element itself
                x1 = xg + 1000 * i
                y1 = yg + 2000
                node1 = uD.insertCTElement(celda.DisyuntorFusible,MINECO,i,code,name,x1,y1,sitenode,uG.getGDLCode('CTelem',code,str(i)))
                #Insert trafo
                t += 1
                x2 = xg + 1000 * i
                y2 = yg + 3000
                uD.insertTrafo(celda.IdTransformador[:32],MINECO,t,x2,y2,code,sitenode,mun_code,prov_code,name,int(CT.Particular)^1)
                node2 = uD.getDMSCode(x2, y2) + "M1"
                #Insert conection element-trafo
                mvSectionId = uD.getMVSectionID()
                uD.insertMVSection(mvSectionId, node1, node2, 'BUSBAR', MINECO, vLevel, x1, y1, SUBSTATION=code, HIDE=1, NAME=name)
            elif typeCelda == 3: #Remonte
                i -= 1
                pass #Qu� hacer con esto?
            elif typeCelda == 4: #Protecci�n General
                i -= 1
                pass #Qu� hacer con esto?
            elif typeCelda == 5: #Seccionamiento de barras (NO EST� PROBADO QUE SE DIBUJE BIEN!! habr�a que buscar una distribuidora con una celda de este tipo e importarla)
                currentChar = "E"
                currentX = xg + 1000 * i
                currentY = yg + 1000
                currentNodecode = uD.insertCTElement(celda.DisyuntorFusible,MINECO,i,code,name,currentX,currentY,sitenode,uG.getGDLCode('CTelem',code,str(i)))
            elif typeCelda == 6: #Medida
                i -= 1
                pass #Metemos medidas?
                
            #Insert conections element-lowerbusbar
            if typeCelda != 5 and typeCelda in usedCeldas:
                mvSectionId = uD.getMVSectionID()
                uD.insertMVSection(mvSectionId, node1, currentNodecode, 'BUSBAR', MINECO, vLevel, x1, y1, SUBSTATION=code, HIDE=1, NAME=name)
   
            #insert mv_sections of lower busbar
            if i > 1 and typeCelda in usedCeldas:  
                node1 = uD.getDMSCode(prevX, prevY) + prevChar + currentNodecode[-1]
                node2 = uD.getDMSCode(currentX, currentY) + currentChar + currentNodecode[-1]
                mvSectionId = uD.getMVSectionID()
                uD.insertMVSection(mvSectionId, node1, node2, 'BUSBAR', MINECO, vLevel, prevX, prevY, SUBSTATION=code, HIDE=1, NAME=name)
                #uD.insertMVSectionPoints(mvSectionId, node1, node2,[{'x':xg + 1000, 'y':yg + 1000, 'numSecc':1}], MINECO, 'C'+code)
            
            #Update celda position in gdlCeldas
            if typeCelda in usedCeldas:
                uG.setCeldaPosition(celda,i)
               
            prevChar = currentChar
            prevX = currentX
            prevY = currentY
            prevNodecode = currentNodecode
                     
        prog += 1
        ProgDialog.Update(float(prog) / float(len(CTs)) * 1000.0)
        ProgDialog.SetLabel("Faltan "+str(len(CTs)-prog)+" CTs...")
            
    ProgDialog.Update(1000.0)
    ProgDialog.SetLabel("Faltan "+str(len(CTs)-prog)+" CTs...")
    log.writeLog("CTs Importation finished for MINECO: " + MINECO)


def importLines(MINECO,lineName=None,importedLines = None):
    """Import from Traza the line with given name. If Name == None Import all liness of given MINECO"""
    log.writeLog("Importing lines for MINECO " + str(MINECO) + ". Importing started...")
    ProgDialog = wx.ProgressDialog("Importando", "Importando l�neas de media tensi�n...", maximum=1000, style=wx.PD_AUTO_HIDE)
    ProgDialog.SetSize((400, 110))
    prog = 0
    
    lines = uG.getLinesList(MINECO,lineName=lineName)
    
    if importedLines == None:
        importedLines = []

    for line in lines:
        print line.Nombre
        if line.Nombre in importedLines:
            prog += 1
            continue #line has already been imported
        importedLines.append(line.Nombre)
        
        #get Origin point
        origin = uG.getLineOrigin(line.IdTipoLinea,line.IdTipoOrigen,line.IdOrigenPunto,line.Nombre,line.IdOrigenLinea)        
        if origin < 0: #Si no se encuentra el origen la linea no se importa, aunque puedne tomarse otras medidas si se desea.
            errorMsg = "ERROR. Import Line "+line.Nombre+". Couldn't get origin point."
            errorBox = "Fallo al importar la l�nea "+line.Nombre+". La l�nea no se importar� correctamente."
            eh.handleError(errorMsg,errorBox)
            origin = [['Z']]
        
        sections = uG.getLineSections(MINECO,lineName=line.Nombre)
        if len(sections) == 0:
            print line.Nombre + ' no tiene secciones!!!'
            continue  
        
        #CHECK ORIGIN OF LINE AND GET THE NODECODE WHERE IT IS GOING TO START
        nodeCode = -1
        if origin[0][0] == 'E':
            nodeCode, xgStart, ygStart = uG.getCeldaToConnect(origin[0],origin[1])
        elif origin[0][0] == 'L':
            upperLine = uG.getUpperLine(MINECO=MINECO,lineName=line.Nombre)
            if upperLine > 0:
                importedLines = importUpperLines(MINECO,upperLine,importedLines)
            nodeCode, xgStart, ygStart = uG.getNodeToConnect(origin)
            derProtection = uG.getProtectionInDerivation(MINECO,line.IdOrigenPunto,line.Nombre)
            if derProtection > 0:
                nodeCode, xgStart, ygStart = uG.getStartNodeWithProtection(origin,nodeCode, xgStart, ygStart,derProtection,sections[0],line)
        elif origin[0][0] == 'F':
            nodeCode, xgStart, ygStart = origin[1], origin[2][0], origin[2][1]
        elif origin[0][0] == 'Z':
            xgStart, ygStart = uC.convertED50(sections[0].X, sections[0].Y, uG.getCompanyZone(MINECO))
            nodeCode = uD.getDMSCode(xgStart, ygStart) + 'X1'
                
        if nodeCode < 0:
            """Modified code to import erronous lines. Old code commented below"""
            xgStart, ygStart = uC.convertED50(sections[0].X, sections[0].Y, uG.getCompanyZone(MINECO))
            nodeCode = uD.getDMSCode(xgStart, ygStart) + 'X1'
            print line.Nombre + ' no se importar� bien!!'
        
        startCode = nodeCode
        sectionPoints = []
        conductorPrev = sections[0].Conductor
        xPrev = xgStart
        yPrev = ygStart
        
        circuitNum = 1
        long = 0
        
        for section in sections:
            
            if origin[0][0] == 'Z':
                origin[0][0] = 0
                continue
            #print str(section.CodApoyo) + '/-----/' + str(section.Designacion)
            changeSection = 0
            #Prepare DMS coordinates of the section
            xg, yg = uC.convertED50(section.X, section.Y, uG.getCompanyZone(MINECO))
            xg = int(str(xg).split('.')[0] + "000") #WHY????
            yg = int(str(yg).split('.')[0] + "000") #WHY????
            print section.ApoyoSecuencial, section.Designacion   
            
            long += uG.calcLong(xPrev, yPrev, xg, yg)
            
            if section.Conductor != conductorPrev:
                changeSection = 1
                
            protection = uG.getRelayProtection(section.IdATVano)
            if protection > 0:
                changeSection = 1
            #print protection
            
            if changeSection == 1:
                if protection > 0:
                    if protection == 'Fusible':
                        ending = 'O1'
                    else:
                        ending = 'E1'
                    endCode = uD.getDMSCode(xg,yg) + ending
                    endCode = uG.drawLine(startCode,xgStart,ygStart,endCode,xg,yg,conductorPrev,sectionPoints,MINECO,line.Nombre, line.IdOrigenLinea, section.IdInst,section.IdATVano,str(int(line.Voltaje)),length=long)
                    uD.insertLineElement(protection,MINECO,circuitNum,endCode,line.Nombre,xg,yg)
                else:  
                    endCode = uD.getDMSCode(xg,yg) + 'X1'
                    endCode = uG.drawLine(startCode,xgStart,ygStart,endCode,xg,yg,conductorPrev,sectionPoints,MINECO,line.Nombre, line.IdOrigenLinea, section.IdInst,section.IdATVano,str(int(line.Voltaje)),length=long)
                startCode = endCode
                conductorPrev = section.Conductor
                sectionPoints = []
                xgStart, ygStart = xg, yg
                long = 0
            else:
                sectionPoints.append({'x':xg, 'y':yg, 'gdlCode':uG.getGDLCode('L',section.IdInst,section.IdATVano)})
        
            xPrev, yPrev = xg, yg
                
        if len(sectionPoints)>0 : #CONEXION FIN DE LINEAAAA!!!!!!!!!!
            endCode = uD.getDMSCode(xg,yg) + 'X1'
            long += uG.calcLong(int(str(xgStart)[:6]), int(str(ygStart)[:6]), int(str(xg)[:6]), int(str(yg)[:6]))
            uG.drawLine(startCode,xgStart,ygStart,endCode,xg,yg,conductorPrev,sectionPoints,MINECO,line.Nombre, line.IdOrigenLinea,section.IdInst,section.IdATVano,str(int(line.Voltaje)),length=long)
              

            
        
#         
#                 
#         for cont in range (1,len(sections)-1):
#             if sections[cont].Conductor != conductorPrev:
#                 pass
#             elif hasProtection(sections[cont]) == True:
#                 pass
#             elif getIntoCT(sections[cont]) == True:
#                 pass
#             elif hasDerivacion(Section[cont]) == True:
#                 pass
#             else:
#                 sectionPoints.append({'x':xg, 'y':yg})
             
        
#         
#         xs, ys = uC.convertED50(origin[1], origin[2], uG.getCompanyZone(MINECO))
#         xs = int(str(xs).split('.')[0] + "000") #WHY????
#         ys = int(str(ys).split('.')[0] + "000") #WHY????
#         
#         nodeCode = uG.getLineDMSCode(origin[0],xs,ys,MINECO)
#         if nodeCode < 0:
#             continue
#         
#         print nodeCode
#         
#         
#         
#         i = 0
#         if nodeCode == uD.getDMSCode(sections[0].X,sections[0].Y):
#             print 'SI'
#             i += 1
#         else:
#             print 'NO'
#             
#         print 'start!!!'
#             
#         for cont in range (i,len(sections)):
#             print section.ApoyoSecuencial
        

        #RECUERDA A�ADIR A CADA COSA QUE INSERTES UNA QUERY PARA BORRARLO EN DELETE LINES.
   
                     
        prog += 1
        ProgDialog.Update(float(prog) / float(len(lines)) * 1000.0)
        ProgDialog.SetLabel("Faltan "+str(len(lines)-prog)+" l�neas...")
         
    uG.separateSectionPoints(MINECO)
    uG.separateLineNodes(MINECO)
    ProgDialog.Update(1000)
    ProgDialog.SetLabel("Faltan "+str(len(lines)-prog)+" l�neas...")       
    log.writeLog("Lines importation finished for MINECO: " + MINECO)
    
def importUpperLines(MINECO,lineName,importedLines):
    """Imports all necesary lines  for caller line to have an origin point"""
    if lineName in importedLines:
        return importedLines
    upperLine = uG.getUpperLine(MINECO = MINECO,lineName = lineName)
    if upperLine > 0:
        importedLines = importUpperLines(MINECO,upperLine,importedLines)
    importLines(MINECO,lineName,importedLines=importedLines)
    return importedLines

def importBorderPoints(MINECO,pointName = None):
    """Imports from Traza border points and substations"""
    log.writeLog("Importing border points for MINECO " + str(MINECO) + ". Importing started...")
    ProgDialog = wx.ProgressDialog("Importando", "Importando puntos frontera...", maximum=1000, style=wx.PD_AUTO_HIDE)
    ProgDialog.SetSize((400, 110))
    prog = 0
     
    bps = uG.getBPList(MINECO,bpName=pointName)
     
    HighVoltageLevel = 80
     
    for bp in bps:
        print bp.Nombre
        MediumVoltageLevel = bp.TensionSubestacion
         
        #Prepare DMS coordinates of the bp
        bpx, bpy = uC.convertED50(bp.X, bp.Y, uG.getCompanyZone(MINECO))
        bpx = int(str(bpx).split('.')[0] + "000") #WHY????
        bpy = int(str(bpy).split('.')[0] + "000") #WHY????
        
        feededNodes = uG.getNodesFeededByBP(bp.IdNodo)

        #bpCode = uG.formatCTCode(bp.Nombre.replace(' ',''),MINECO)
     
        if bp.Subestacion_o_PE != 'Generador':
            
            #insert ST and diagram
            uD.insertST(MINECO, bp.Nombre,HighVoltageLevel)
            uD.insertSTDiagram(MINECO+bp.Nombre,'A',bpx,bpy)
             
            # Insert Primary Transformer
            ptX = bpx
            ptY = bpy + 8000
            ptnodecode = uG.getEmptyCode(uD.getDMSCode(ptX, ptY) + 'P1','mv_node')
            ptcode = MINECO + bp.Nombre + 'T'
            Sn1 = 1
            Un1 = HighVoltageLevel
            Un2 = MediumVoltageLevel
            uD.insertPrimaryTrafo(ptnodecode, ptcode, Sn1, Un1, Un2, MINECO)
            uD.insertMV_Node(ptnodecode, ptX, ptY, ptX, ptY, ptnodecode, mineco=MINECO,codigoGDL=uG.getGDLCode('ST',bp.Nombre,bp.IdInst))
             
            # Insert Feeding Point
            fpX = bpx
            fpY = bpy + 10000 
            fpnodecode = uG.getEmptyCode(uD.getDMSCode(fpX, fpY) + 'G1','mv_node')
            fpcode = MINECO + bp.Nombre + 'G'
            Un = HighVoltageLevel
            uD.insertFeedingPoint(fpnodecode, fpcode, Un)
            uD.insertMV_Node(fpnodecode, fpX, fpY, fpX, fpY, ptnodecode, mineco=MINECO,codigoGDL=uG.getGDLCode('Gen',bp.Nombre,bp.IdNodo,bp.IdInst))
             
            # Insert Busbar Points
            bpcode = MINECO + bp.Nombre + 'B'
            # Busbar Point 1
            bpX1 = bpx - 1000
            bpY1 = bpy + 6000    
            uD.insertBusbarPoint(bpcode, bpX1, bpY1, 1)         
             
            # Busbar Point 2
            bpX2 = bpx + 1000
            bpY2 = bpy + 6000 
            uD.insertBusbarPoint(bpcode, bpX2, bpY2, 2)
 
            # Insert Busbar Node
            bnX = bpx
            bnY = bpy + 6000 
            bnnodecode = uG.getEmptyCode(uD.getDMSCode(bnX, bnY) + 'A1','mv_node')
            uD.insertBusbarNode(bnnodecode, bpcode)
            uD.insertMV_Node(bnnodecode, bnX, bnY, bnX, bnY, ptnodecode, mineco=MINECO, codigoGDL=uG.getGDLCode('BBN',bp.Nombre,bp.IdInst,bnnodecode))
 
            # Insert Circuit Breaker
            cbX = bpx
            cbY = bpy + 4000 
            cbnodecode = uG.getEmptyCode(uD.getDMSCode(cbX, cbY) + 'K1','mv_node')
            cbcode = MINECO + bp.Nombre + 'I'
            uD.insertCircuitBreaker(cbnodecode,cbcode,MediumVoltageLevel)
            uD.insertMV_Node(cbnodecode, cbX, cbY, cbX, cbY, ptnodecode, mineco=MINECO, codigoGDL=uG.getGDLCode('CBBP',bp.Nombre,bp.IdInst,bnnodecode))
 
            # Insert Feeder
            fX = bpx
            fY = bpy + 2000 
            fnodecode = uG.getEmptyCode(uD.getDMSCode(fX, fY) + 'S1','mv_node')
            fcode = MINECO + bp.Nombre + 'F'
            uD.insertMVFeeder(fnodecode, fcode)
            uD.insertMV_Node(fnodecode, fX, fY, fX, fY, ptnodecode, mineco=MINECO, codigoGDL=uG.getGDLCode('STF',bp.Nombre,bp.IdNodo,bp.IdInst))
             
            # Insert Sections
            # Transformer - Feeding Point
            mvSectionId = uD.getMVSectionID()
            uD.insertMVSection(mvSectionId, ptnodecode, fpnodecode, 'BUSBAR', MINECO, HighVoltageLevel, ptX, ptY, SUBSTATION=MINECO + bp.Nombre, HIDE=1, NAME=MINECO+"PF-"+bp.Nombre)
            # Transformer - Busbar Node
            mvSectionId = uD.getMVSectionID()
            uD.insertMVSection(mvSectionId, ptnodecode, bnnodecode, 'BUSBAR', MINECO, MediumVoltageLevel, ptX, ptY, SUBSTATION=MINECO + bp.Nombre, HIDE=1, NAME=MINECO+"PF-"+bp.Nombre)
            # Busbar Node - Circuit Breaker
            mvSectionId = uD.getMVSectionID()
            uD.insertMVSection(mvSectionId, bnnodecode, cbnodecode, 'BUSBAR', MINECO, MediumVoltageLevel, bnX, bnY, SUBSTATION=MINECO + bp.Nombre, HIDE=1, NAME=MINECO+"PF-"+bp.Nombre)
            # Circuit Breaker - Feeder
            mvSectionId = uD.getMVSectionID()
            uD.insertMVSection(mvSectionId, cbnodecode, fnodecode, 'BUSBAR', MINECO, MediumVoltageLevel, cbX, cbY, SUBSTATION=MINECO + bp.Nombre, HIDE=1, NAME=MINECO+"PF-"+bp.Nombre)
            # Feeder - Border Point
            if feededNodes == 0:
                code = uG.getEmptyCode(uD.getDMSCode(bpx,bpy) + "X1", 'mv_node')
                uD.insertMV_Node(code, bpx, bpy, bpx, bpy, code, mineco=MINECO,codigoGDL=uG.getGDLCode('F','XX',bp.IdNodo,bp.IdInst))
                mvSectionId = uD.getMVSectionID()
                uD.insertMVSection(mvSectionId, fnodecode,code, 'BUSBAR', MINECO, MediumVoltageLevel, fX, fY, SUBSTATION=MINECO + bp.Nombre, HIDE=1, NAME=MINECO+'PF-'+bp.Nombre)
            else:
                for node in feededNodes: 
                    mvSectionId = uD.getMVSectionID()
                    uD.insertMVSection(mvSectionId, fnodecode,node.CODE, 'BUSBAR', MINECO, MediumVoltageLevel, fX, fY, SUBSTATION=MINECO + bp.Nombre, HIDE=1, NAME=MINECO+'PF-'+bp.Nombre)
        
        else:
            # Insert Feeding Point
            fpX = bpx
            fpY = bpy + 10000 
            fpnodecode = uG.getEmptyCode(uD.getDMSCode(fpX, fpY) + 'G1','mv_node')
            fpcode = MINECO + bp.Nombre + 'G'
            Un = HighVoltageLevel
            uD.insertFeedingPoint(fpnodecode, fpcode, Un)
            uD.insertMV_Node(fpnodecode, fpX, fpY, fpX, fpY, fpnodecode, mineco=MINECO,codigoGDL=uG.getGDLCode('Gen',bp.Nombre,bp.IdNodo,bp.IdInst))

            # Transformer - Feeding Point
            if feededNodes == 0:
                code = uG.getEmptyCode(uD.getDMSCode(bpx,bpy) + "X1", 'mv_node')
                uD.insertMV_Node(code, bpx, bpy, bpx, bpy, code, mineco=MINECO,codigoGDL=uG.getGDLCode('F','XX',bp.IdNodo,bp.IdInst))
                mvSectionId = uD.getMVSectionID()
                uD.insertMVSection(mvSectionId, code, fpnodecode, 'BUSBAR', MINECO, HighVoltageLevel, fpX, fpY, SUBSTATION=MINECO + bp.Nombre, HIDE=1, NAME=MINECO+'PF-'+bp.Nombre)
            else:
                for node in feededNodes: 
                     mvSectionId = uD.getMVSectionID()
                     uD.insertMVSection(mvSectionId, node.CODE, fpnodecode, 'BUSBAR', MINECO, HighVoltageLevel, fpX, fpY, SUBSTATION=MINECO + bp.Nombre, HIDE=1, NAME=MINECO+'PF-'+bp.Nombre)
 
           
        prog += 1
        ProgDialog.Update(float(prog) / float(len(bps)) * 1000.0)
        ProgDialog.SetLabel("Faltan "+str(len(bps)-prog)+" puntos frontera...")
        
    ProgDialog.Update(1000)
    ProgDialog.SetLabel("Faltan "+str(len(bps)-prog)+" puntos frontera...")       
    log.writeLog("Border points importation finished for MINECO: " + MINECO)
 
    
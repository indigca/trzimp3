import utiDataBase as ud

"""
Este programa copia una distribuidora de una base de datos a otra base de datos.
Ha sido creado para pasar la distribuidora 000 de los ordenadores de GEDLux (maqina CIDE-DES) a los de CIDE y asi poder usar dicha distribuidora en los cursos.
Hay que senalar que las configuraciones indicadas en el documento 'Alta de socio en sistema' deben estar 
realizadas antes de ejecutar este programa y realizar el traspaso.
"""

MINECO = '180'  #ponerlo como string
dbOrigen = 'DMS'
dbDestino = 'DMS2'

 
mvSections = {}
 
def getMVSectionID(id = None):
    """Get the highest section number that should be used and update the table"""
    if id == None:
        query="UPDATE dbo.SECTIONNUMBER SET mv_section_id = mv_section_id + 1 WHERE ID=1"
        ud.execute(query,dbDestino)
        query = "SELECT mv_section_id FROM dbo.SECTIONNUMBER WHERE ID=1"
        result = ud.selectOne(query,dbDestino)
        mvSections[str(entry[i])] = str(result.mv_section_id)
        return str(result.mv_section_id)
    else:
        return mvSections[str(id)]
     
 
TABLE="MV_NODE"
WHERE = "WHERE gdlMINECO = '" + MINECO +"'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO MV_NODE VALUES ('"+str(entry.CODE)+"','"+str(entry.X)+"','"+str(entry.Y)+"','"+str(entry.XCODE)+"','"+str(entry.YCODE)+"','"+str(entry.SITENODE)+"','"+str(entry.SymbolAngle)+"','"+str(entry.HIDE)+"','"+str(entry.HIDE_NODE)+"','"+MINECO+"')"
    query=query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
#El insert hacerlo con ud.execute(query,dbDestino) y poner temporalmente los datos del DMS2 que apunten al DMS de CIDE
#Los datos reales del DMS2 guardarlos como seccion [DMS2_ORIG] o algo asi.
#construir insert para todo antes de hacer el insert, y probar primero en una base de datos vacia sin distribuidora 000.
#Para ello borrar la 000 desde DMS2Import (old).
 
TABLE="MV_SECTION"
WHERE = "WHERE DISTRICT = '" + MINECO +"'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-2):
        if i==0:
            id = getMVSectionID()
            query = query + str(id)+ "','"
        else:
            query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-2]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino) 
     
TABLE="MV_SECTIONPOINT"
WHERE = "WHERE gdlMINECO = '" + MINECO +"'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    id = getMVSectionID(id=entry.MV_Section_Id)
    query = "INSERT INTO "+TABLE+" VALUES ('"+str(id)+"','"+str(entry.NODE1)+"','"+str(entry.NODE2)+"','"+str(entry.PARALLEL_NUMBER)+"','"+str(entry.NUMBER)+"','"+str(entry.X)+"','"+str(entry.Y)+"','"+MINECO+"')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="DISCONNECTOR"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="CUSTOMER"
WHERE = "WHERE LV_NETWORK LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        if i == 13:
            query = query + "(SELECT(Getdate())),"
        else:
            query = query + "'" + str(entry[i]) + "',"
    query = query + "'" + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="CUSTOMER_NODE_LOAD"
WHERE = "WHERE CUSTOMER_NODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="DIAGRAM"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="CAPACITOR"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
 
TABLE="BUSBARNODE"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="BUSBARPOINT"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query +str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="CIRCUIT_BREAKER"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
TABLE="LV_BOX"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'"  # AKI HAY MAS COSAS QUE NO COPIO CON CODES RAROS------------------
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
TABLE="LV_SWITCH"
WHERE = "WHERE BOX LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
TABLE="LV_NODE"
WHERE = "WHERE gdlMINECO = '"+MINECO+"'"  
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + MINECO + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="LV_SECTION"
WHERE = "WHERE LV_NETWORK LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="LV_SECTIONPOINT"
WHERE = "WHERE gdlMINECO = '"+MINECO+"'"  
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + MINECO + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="LV_CUSTOMER_NODE"
WHERE = "WHERE CUSTOMER_NODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        query = query +"'"+ str(entry[i]) + "',"
    query = query + "(SELECT(Getdate())))"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
TABLE="MV_FEEDER"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="MV_LV_SUBSTATION"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-2):
        query = query + str(entry[i]) + "','"
    query = query + "None')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
    
TABLE="MV_LV_LOAD"
WHERE = "WHERE MV_LV_SUBSTATION LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="MV_FUSE"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
 
TABLE="MV_SITE"
WHERE = "WHERE gdlMINECO = '"+MINECO+"'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + MINECO + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
 
TABLE="NODE_REGIONS"
WHERE = "WHERE REGION_CODES = '"+MINECO+"'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + MINECO + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
 
TABLE="SUBSTATION"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
TABLE="TRANSFORMER"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        if i == 20:
            query = query + "(SELECT(Getdate())),"
        else:
            query = query + "'" + str(entry[i]) + "',"
    query = query + "'" + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
TABLE="TRANSFORMER_NODE"
WHERE = "WHERE MV_LV_SUBSTATION LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
TABLE="TRANSFORMER_PLACING"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        query = query +"'"+ str(entry[i]) + "',"
    query = query + "(SELECT(Getdate())))"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)

 
TABLE="FEEDINGPOINT"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
TABLE="SwitchingComponent"
WHERE = "WHERE Switch LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        if i==8:
            query = query + "NULL,"
        else:
            query = query + "'"+str(entry[i]) + "',"
    query = query + "'" +MINECO+"')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
TABLE="PRIMARY_TRANSFORMER"
WHERE = "WHERE CODE LIKE '"+MINECO+"%'" 
query="SELECT * FROM " + TABLE + " " + WHERE
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO "+TABLE+" VALUES ("
    for i in range(fields-1):
        if i == 18:
            query = query + "(SELECT(Getdate())),"
        else:
            query = query + "'" + str(entry[i]) + "',"
    query = query + "'" + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
     
print 'FIIIIIIIIIIIIIIIIIIIIIIIIIIIIIINNNNNNNNNNNNNNNNNNNNNNNNNNNNNN'
 
"""
MAPS
"""

query = "SELECT * FROM MapAdjustment WHERE NAME LIKE '"+MINECO+"%'"
result = ud.selectAll(query,dbOrigen)
try:
    fields = len(result[0])
except:
    fields = 0
print fields
for entry in result:
    query = "INSERT INTO MapAdjustment VALUES ('"
    for i in range(fields-1):
        query = query + str(entry[i]) + "','"
    query = query + str(entry[fields-1]) + "')"
    query = query.replace("'None'", "NULL")
    print query
    ud.execute(query,dbDestino)
    
    
print 'THE END!!!!!!!!!!!!'


#FALTA NODE_REGIONS2!!!!!!!!!!!!!!!!!!!
#Falta gdlCTConnections!!!!!!!!!!!!!
#Falta gdlOPC!!!!!! (Y aqui hay un par de registros)
#Falta gdlLV_NODE_BTVano!!!!!!!
#Falta gdlXYDMS!!!!!!!!!!!!

# -*- coding: iso-8859-15 -*-
'''
Created on 03/12/2013
@author: ALP
'''
from configReader import ConfigReader as cReader
import utiDataBase
import hashlib
import exceptionHandler as eh
import utiDMS
import utiCoordinates as uC
from math import atan2, sqrt

def checkPassword(username,password):
    """Check if given user has given password"""
    #Get user password (md5)
    query = "SELECT password FROM gdlTrazaImportUser WHERE usuario = '%s'" % username
    realMD5=utiDataBase.selectOne(query,'DMS')
    #return error -2: The error has been already handled
    if realMD5 < 0:
        return -2
    #return error -3: No results of given username
    elif realMD5 == None:
        return -3
        
    #Calculate md5 from given user and  typed password
    userReverse = username[::-1]
    typedMD5 = hashlib.md5(password + userReverse).hexdigest()
        
    #Check password
    if realMD5[0] == typedMD5:
        return 1
    else:
        return -1
    
def pad(string, length, padChar = '0'):
    """
    Pads a string with the specified character to
    be len characters long. Longer than that, and 
    it will be cut out.
    """
    if string == None:
        return padChar * length
    elif len(str(string)) > length:
        return str(string)[0:length]
    else:
        return (length - len(str(string))) * padChar + str(string)

def getEmptyCode(code, table):
    endings = [] #endings = '1','2','3','4','5','6','7','8','9','A','B','C',.....
    for i in range(49,58):
        endings.append(chr(i))
    for i in range(65,91):
        endings.append(chr(i))
        
    if table == 'mv_node':
        column = 'code'
    elif table == 'mv_site':
        column = 'nodecode'
    
    query = "SELECT * \
                FROM " + table + " \
                WHERE " + column + " LIKE '" + code +"'"
    i = 0
    while utiDataBase.selectOne(query, 'DMS') != None:
        i += 1
        code = code[:-1] + endings[i]
        query = "SELECT * \
                FROM " + table + " \
                WHERE " + column + " LIKE '" + code +"'"
    return code

def getCompaniesList(username):
    """Returns the list of companies a user can see"""
    query="SELECT L.Name, L.ID ,L.DMS FROM gdlMineco L\
                 WHERE L.ID IN (SELECT COMPANY FROM gdlReportCompany WHERE USERID LIKE '%s')" % username          
    queryResult = utiDataBase.selectAll(query,'DMS')      
    if queryResult < 0:
        return []
    list = []
    for row in queryResult:
        list.append(row.ID+row.DMS+"-"+row.Name)
    return list
    
def populateCTsListField(MINECO): 
    """Returns the list of CTs from a given MINECO  just for show"""
    query ="SELECT Distinct Ins.Nombre \
                 FROM Instalaciones Ins,  CT CT   \
                 WHERE  Ins.IdInst=CT.IdInst and Ins.IdInst LIKE '" + str(MINECO) +"%'"
    queryResult = utiDataBase.selectAll(query, 'TRAZA')
    if queryResult < 0:
        return []
    list = []
    for row in queryResult:
        list.append(row.Nombre)
    return list

def populateLinesListField(MINECO): 
    """Returns the list of lines from a given MINECO  just for show"""
    query ="SELECT Distinct Ins.Nombre \
            FROM Instalaciones Ins,  ATVano ATV \
            WHERE  Ins.IdInst=ATV.IdInst and Ins.IdInst LIKE'" + str(MINECO) +"%'"
    queryResult = utiDataBase.selectAll(query, 'TRAZA')
    if queryResult < 0:
        return []
    list = []
    for row in queryResult:
        list.append(row.Nombre)
    return list
    
def populateTrafosListField(MINECO):
    """Returns the list of LV transformers from a given MINECO  just for show"""
    query ="SELECT Ins.Nombre, Trf.Potencia, Trf.NumeroTransformador \
            FROM CTTransformadores Trf, Instalaciones Ins \
            WHERE Trf.idInst=Ins.IdInst and Ins.IdInst LIKE '" + str(MINECO) +"%'"
    queryResult = utiDataBase.selectAll(query, 'TRAZA')
    if queryResult < 0:
        return []
    list = []
    for row in queryResult:
        list.append(row.Nombre+"-"+str(row.NumeroTransformador)+"-"+str(row.Potencia))
    return list

def populateCustomersListField(MINECO): 
    """Returns the list of first 100 LV customers from a given MINECO just for show"""
    query ="SELECT TOP 100 Cli.CUPS, Ins.Nombre, Trf.NumeroTransformador \
            FROM Clientes Cli, CTTransformadores Trf, Instalaciones Ins, CT CT \
            WHERE Ins.IdInst LIKE '"+ str(MINECO) +"%' AND \
                Ins.IdInst = Cli.IdInst and Cli.IdInst = CT.IdInst and Cli.IdTransformador=Trf.IdTransformador \
            ORDER BY Ins.Nombre" 
    queryResult = utiDataBase.selectAll(query, 'TRAZA')
    if queryResult < 0:
        return []
    list = []
    for row in queryResult:
        list.append(row.Nombre+"-"+str(row.NumeroTransformador)+"-"+row.CUPS)
    return list

def populategdlCeldas(MINECO,ctName=None): #Hay que mejorar como saca el tipo de protecci�n (seccionardo  o fusible) de cada celda.
    """Fill table gdlCeldas. It works different depending on what kind of CT is"""
    cts = getTipoYPropiedadCT(MINECO,ctName=ctName)
    error = 0
    
    for ct in cts:
        name = ct[0]
        type = ct[1]
        particular = ct[2]
            
        if type == 'Sobre apoyo':
            if particular == True:
                error = insertgdlCeldas(getCeldasFromInsAndOuts(name,MINECO),MINECO)
            else:
                error = insertgdlCeldas(getCeldasFromInsAndOuts(name,MINECO),MINECO)
        elif type == 'En p�rtico':
            if particular == True:
                error = insertgdlCeldas(getCeldasFromInsAndOuts(name,MINECO),MINECO)
            else:
                error = insertgdlCeldas(getCeldasFromInsAndOuts(name,MINECO),MINECO)
        elif type == 'Caseta':
            if particular == True:
                error = insertgdlCeldas(getCeldasFromInsAndOuts(name,MINECO),MINECO)
            else:
                error = insertgdlCeldas(getCeldasFromCTCeldas(name,MINECO),MINECO)
        elif type == 'Subterr�neo':
            if particular == True:
                error = insertgdlCeldas(getCeldasFromInsAndOuts(name,MINECO),MINECO)
            else:
                error = insertgdlCeldas(getCeldasFromCTCeldas(name,MINECO),MINECO)
        elif type == 'Interior':
            if particular == True:
                error = insertgdlCeldas(getCeldasFromInsAndOuts(name,MINECO),MINECO)                
            else:
                error = insertgdlCeldas(getCeldasFromCTCeldas(name,MINECO),MINECO)
#         if isCelda(MINCEO, ctName) == False:
#             print ctName + ' no devuelve celdas!!!'
#             error = insertgdlCeldas(getCeldasFromInsAndOuts(name,MINECO),MINECO)
        if error != 0:
            errorMsg = "ERROR. populate gdlCeldas on CT: " + name 
            errorBox = "Problemas al obtener las celdas o posiciones del centro " + name
            eh.handleError(errorMsg,errorBox)

                 
#El campo disyuntor fusible s�lo diferencia entre Fusible y resto, ser�a bueno que aceptase m�s opciones de tipo de aparamenta.
#Ese mismo campo podr� ser buscado en otras tablas del traza (apoyos, elementos lineas, etc. si se requiere, y ser� creada una nueva query
# del tipo getCeldasFromLokesea
def getCeldasFromCTCeldas(name, MINECO):
    """Get CT positions form table CTCeldas"""
    query = """SELECT InsCT.Codigo CodigoCT, ISNULL(InsLinea.Codigo, '1') AS CodigoLinea, CT.X, CT.Y, InsCT.Nombre, InsLinea.IdInst IdInstLinea,
                 CTCeldas.IdTipoCelda, InsCT.IdInst, CTCeldas.IdTransformador, DisyuntorFusible, 
                 CTCeldas.Modelo, NULL AS Transformador, NULL as Posicion
                 FROM CTCeldas
                     LEFT JOIN CTDerivaciones 
                         ON CTCeldas.IdDerivacion = CTDerivaciones.IdDerivacion 
                     LEFT JOIN CTAlimentaciones 
                         ON CTCeldas.IdAlimentacion = CTAlimentaciones.IdAlimentacion 
                     LEFT JOIN CT 
                         ON CTCeldas.IdInst = CT.IdInst 
                     LEFT JOIN Instalaciones InsCT 
                         ON CTCeldas.IdInst = InsCT.IdInst 
                     LEFT JOIN Instalaciones InsLinea 
                         ON (CTDerivaciones.IdInstDerivaciones = InsLinea.IdInst AND CTAlimentaciones.IdInstAlimentaciones IS NULL) 
                         OR (CTAlimentaciones.IdInstAlimentaciones = InsLinea.IdInst AND CTDerivaciones.IdInstDerivaciones IS NULL) 
                 WHERE CT.Alta = 1 AND CTCeldas.Alta = 1 AND CTCeldas.IdTipoCelda <> 2 AND CTCeldas.IdInst LIKE '""" + str(MINECO) + """%' """
    if name != None:
        query = query + """ AND InsCT.Nombre = '""" + name + """' """
    query = query + """ UNION ALL
                 --Transformers
                 SELECT    Instalaciones.Codigo CodigoCT,
                         CTTransformadores.NumeroTransformador AS CodigoLinea,
                         CT.X,
                         CT.Y,
                         Instalaciones.Nombre,
                         NULL AS IdInstLinea,
                         IdTipoCelda,
                         Instalaciones.IdInst,
                         CTTransformadores.IdTransformador,
                         DisyuntorFusible,
                         NULL AS Modelo,
                         CTTransformadores.NumeroTransformador AS Transformador, 
                         NULL as Posicion
                 FROM    CT
                         JOIN Instalaciones
                             ON CT.IdInst=Instalaciones.IdInst
                         JOIN CTTransformadores 
                             ON CTTransformadores.IdInst = Instalaciones.IdInst
                         JOIN CTCeldas
                             ON CTCeldas.IdTransformador = CTTransformadores.IdTransformador
                 WHERE CT.Alta = 1 AND CTCeldas.Alta = 1 AND Instalaciones.IdInst LIKE '""" + str(MINECO) + """%' """
    if name != None:
        query = query + """ AND Instalaciones.Nombre = '""" + name + """' """
    return utiDataBase.selectAll(query, 'TRAZA') 

#El campo disyuntor fusible s�lo diferencia entre Fusible y resto, ser�a bueno que aceptase m�s opciones de tipo de aparamenta.
#Ese mismo campo podr� ser buscado en otras tablas del traza (apoyos, elementos lineas, etc. si se requiere, y ser� creada una nueva query
# del tipo getCeldasFromLokesea
def getCeldasFromInsAndOuts(name, MINECO):
    """Get CT positions from tables Alimentaciones and Derivaciones"""
    query="""SELECT  Instalaciones.Codigo AS CodigoCT,
                         ISNULL(Ins_ali.Codigo, '1') AS CodigoLinea,
                         CT.X,
                         CT.Y,
                         Instalaciones.Nombre,
                         CTAlimentaciones.IdInstAlimentaciones AS IdInstLinea,
                         '1' AS IdTipoCelda,
                         Instalaciones.IdInst,
                         NULL AS IdTransformador,
                         1 AS DisyuntorFusible,
                         NULL AS Modelo,
                         NULL AS Transformador, 
                         NULL as Posicion
                 FROM    CT
                         JOIN Instalaciones
                             ON CT.IdInst = Instalaciones.IdInst
                         JOIN CTAlimentaciones
                             ON CTAlimentaciones.IdInst = Instalaciones.IdInst
                         JOIN Instalaciones Ins_ali
                             ON Ins_ali.IdInst = CTAlimentaciones.IdInstAlimentaciones
                 WHERE CT.Alta = 1 AND Instalaciones.IdInst LIKE '""" + str(MINECO) + """%' """
    if name != None:
        query = query + """ AND Instalaciones.Nombre = '""" + name + """' """
        
    query = query + """ UNION ALL
                 -- derivaciones, or outgoing lines
                 SELECT    Instalaciones.Codigo AS CodigoCT,
                         ISNULL(Ins_der.Codigo, '1') AS CodigoLinea,
                         CT.X,
                         CT.Y,
                         Instalaciones.Nombre,
                         CTDerivaciones.IdInstDerivaciones AS IdInstLinea,
                         '1' AS IdTipoCelda,
                         Instalaciones.IdInst,
                         NULL AS IdTransformador,
                         1 AS DisyuntorFusible,
                         NULL AS Modelo,
                         NULL AS Transformador, 
                         NULL as Posicion
                 FROM    CT
                         JOIN Instalaciones
                             ON CT.IdInst = Instalaciones.IdInst
                         JOIN CTDerivaciones
                             ON CTDerivaciones.IdInst = Instalaciones.IdInst
                         JOIN Instalaciones AS Ins_der
                             ON Ins_der.IdInst = CTDerivaciones.IdInstDerivaciones
                 WHERE CT.Alta = 1 AND Instalaciones.IdInst LIKE '""" + str(MINECO) + """%' """
    if name != None:
        query = query + """ AND Instalaciones.Nombre = '""" + name + """' """
    query = query + """ UNION ALL
                 --Transformers
                 SELECT    Instalaciones.Codigo CodigoCT,
                         CTTransformadores.NumeroTransformador AS CodigoLinea,
                         CT.X,
                         CT.Y,
                         Instalaciones.Nombre,
                         NULL AS IdInstLinea,
                         '2' AS IdTipoCelda,
                         Instalaciones.IdInst,
                         CTTransformadores.IdTransformador,
                         1 AS DisyuntorFusible,
                         NULL AS Modelo,
                         CTTransformadores.NumeroTransformador AS Transformador, 
                         NULL as Posicion
                 FROM    CT
                         JOIN Instalaciones
                             ON CT.IdInst=Instalaciones.IdInst
                         JOIN CTTransformadores 
                             ON CTTransformadores.IdInst = Instalaciones.IdInst
                 WHERE CT.Alta = 1 AND Instalaciones.IdInst LIKE '""" + str(MINECO) + """%' """
    if name != None:
        query = query + """ AND Instalaciones.Nombre = '""" + name + """' """
    return utiDataBase.selectAll(query, 'TRAZA')
    
    
def getTipoYPropiedadCT(MINECO,ctName=None):
    """Return a list of CTs with type and ownership"""
    query = "SELECT Instalaciones.Nombre, CTTipo.Descripcion, CT.Particular FROM CT \
            LEFT JOIN CTTipo \
                ON CT.IdTipoCT = CTTipo.IdTipoCT\
            LEFT JOIN Instalaciones \
                ON CT.IdInst = Instalaciones.IdInst \
            WHERE Instalaciones.IdInst LIKE '" + str(MINECO) + "%' \
                AND CT.Alta = 1 "
    if ctName == None:
        return utiDataBase.selectAll(query, 'TRAZA')
    else:
        query = query + " AND Instalaciones.Nombre = '" + ctName + "'"
        return utiDataBase.selectAll(query, 'TRAZA')
    
def oldPopulate(MINECO):
    query = """DECLARE @wildcard nvarchar(4);
                SET @wildcard = '"""+str(MINECO)+"""%';
                
                /* SUBQUERY ct_no_celda
                Tabla con IdInst de los CTs sin celdas
                */
                WITH ct_no_celda AS 
                (
                -- if we join to this subquery the listing should only contains CTs
                -- that have no celdas
                    SELECT CT.IdInst
                    FROM CT
                        LEFT JOIN CTCeldas
                            ON CT.IdInst = CTCeldas.IdInst
                    WHERE CT.IdInst LIKE @wildcard AND CTCeldas.IdInst IS NULL
                )
                -- the first query gets the data from CTCeldas, which is where
                -- we assume things are correct. For those that have no rows in
                -- CTCeldas we fetch the data by deducing from the number of 
                -- lines connected etc
                
                /*PRIMER UNION
                
                */
                
                SELECT InsCT.Codigo CodigoCT, ISNULL(InsLinea.Codigo, '1') AS CodigoLinea, CT.X, CT.Y, InsCT.Nombre, InsLinea.IdInst IdInstLinea,
                CTCeldas.IdTipoCelda, InsCT.IdInst, CTCeldas.IdTransformador,
                CASE WHEN DisyuntorFusible = 'Fusible' THEN 1 ELSE NULL END AS DisyuntorFusible, --OJO!!!!! ESTO ESTA BIEN?? NO ENTIENDO POR QUE SOLO DA OPCION DE FUSIBLE O NADA, CUANDO LAS OPCIONES VAN MUCHO MAS ALLA. INCLUSO HAY DOS TIPOS DE FUSIBLE. ESTO SE VE EN [traza].[dbo].[TipoManiobraProteccion]
                --CTCeldas.DisyuntorFusible, esto es lo que pondria yo en su lugar, pero ojo! esto seria variable varchar y el codigo anterior usaba int.
                CTCeldas.Modelo, NULL AS Transformador, NULL as Posicion
                FROM CTCeldas
                    LEFT JOIN CTDerivaciones 
                        ON CTCeldas.IdDerivacion = CTDerivaciones.IdDerivacion
                    LEFT JOIN CTAlimentaciones
                        ON CTCeldas.IdAlimentacion = CTAlimentaciones.IdAlimentacion
                    LEFT JOIN CT
                        ON CTCeldas.IdInst = CT.IdInst
                    LEFT JOIN Instalaciones InsCT
                        ON CTCeldas.IdInst = InsCT.IdInst
                    LEFT JOIN Instalaciones InsLinea
                        ON (CTDerivaciones.IdInstDerivaciones = InsLinea.IdInst AND CTAlimentaciones.IdInstAlimentaciones IS NULL)
                        OR (CTAlimentaciones.IdInstAlimentaciones = InsLinea.IdInst AND CTDerivaciones.IdInstDerivaciones IS NULL)
                WHERE CTCeldas.IdTipoCelda = 1 AND CTCeldas.Alta = 1 AND CTCeldas.IdInst LIKE @wildcard
                        
                /*SEGUNDO UNION
                
                */
                UNION ALL
                -- alimentationes (incoming lines)
                SELECT  Instalaciones.Codigo AS CodigoCT,
                        ISNULL(Ins_ali.Codigo, '1') AS CodigoLinea,
                        CT.X,
                        CT.Y,
                        Instalaciones.Nombre,
                        CTAlimentaciones.IdInstAlimentaciones AS IdInstLinea,
                        '1' AS IdTipoCelda,
                        Instalaciones.IdInst,
                        NULL AS IdTransformador,
                        1 AS DisyuntorFusible,
                        NULL AS Modelo,
                        NULL AS Transformador, NULL as Posicion
                FROM    CT
                        JOIN ct_no_celda
                            ON ct_no_celda.IdInst = CT.IdInst
                        JOIN Instalaciones
                            ON CT.IdInst = Instalaciones.IdInst
                        JOIN CTAlimentaciones
                            ON CTAlimentaciones.IdInst = Instalaciones.IdInst 
                        JOIN Instalaciones Ins_ali
                            ON Ins_ali.IdInst = CTAlimentaciones.IdInstAlimentaciones
                WHERE Instalaciones.IdInst LIKE @wildcard
                
                /*TERCER UNION
                
                */
                
                UNION ALL
                -- derivaciones, or outgoing lines
                SELECT    Instalaciones.Codigo AS CodigoCT,
                        ISNULL(Ins_der.Codigo, '1') AS CodigoLinea,
                        CT.X,
                        CT.Y,
                        Instalaciones.Nombre,
                        CTDerivaciones.IdInstDerivaciones AS IdInstLinea,
                        '1' AS IdTipoCelda,
                        Instalaciones.IdInst,
                        NULL AS IdTransformador,
                        1 AS DisyuntorFusible,
                        NULL AS Modelo,
                        NULL AS Transformador, NULL as Posicion
                FROM    CT
                        JOIN ct_no_celda
                            ON ct_no_celda.IdInst = CT.IdInst
                        JOIN Instalaciones
                            ON CT.IdInst = Instalaciones.IdInst
                        LEFT JOIN CTDerivaciones
                            ON CTDerivaciones.IdInst = Instalaciones.IdInst
                        JOIN AT
                            ON AT.IdInst = CTDerivaciones.IdInstDerivaciones
                            -- usually derivaciones is ok, sometimes this row does
                            -- not exist and we have to actually see if there is a
                            -- line that starts from our CT
                            OR (AT.IdOrigenPunto = CT.IdInst AND IdTipoOrigen = 4)
                        JOIN Instalaciones AS Ins_der
                            ON Ins_der.IdInst = AT.IdInst
                WHERE    Instalaciones.IdInst LIKE @wildcard
                
                /*CUARTO UNION
                
                */
                
                UNION ALL
                -- transformer celdas
                SELECT    Instalaciones.Codigo CodigoCT,
                        CTTransformadores.NumeroTransformador AS CodigoLinea,
                        CT.X,
                        CT.Y,
                        Instalaciones.Nombre,
                        NULL AS IdInstLinea,
                        '2' AS IdTipoCelda,
                        Instalaciones.IdInst,
                        CTTransformadores.IdTransformador,
                        NULL AS DisyuntorFusible,
                        NULL AS Modelo,
                        CTTransformadores.NumeroTransformador AS Transformador, NULL as Posicion
                FROM    CT
                        JOIN Instalaciones
                            ON CT.IdInst=Instalaciones.IdInst
                        JOIN CTTransformadores 
                            ON CTTransformadores.IdInst = Instalaciones.IdInst
                WHERE    Instalaciones.IdInst LIKE @wildcard
                
                 """
    result = utiDataBase.selectAll(query,'TRAZA')
    return insertgdlCeldas(result,MINECO)

def getTrafosList(MINECO,ctName=None):
    """If name = None, returns a list of trafos of given MINECO with the necessary info to be imported into DMS.
    If name != None return the same info about one trafo."""
    query = "SELECT T.Descripcion, CT.IdTransformador, CT.Potencia, CT.IdFabricante, \
                 CT.AnhoFabricacion, CT.Modelo, Ins.Nombre, CT.NumeroTransformador \
            FROM CTTransformadores CT, CTTensiones T, Instalaciones Ins \
            WHERE CT.idInst=Ins.IdInst and T.IdTension=CT.IdTension  and Ins.IdInst LIKE '" + str(MINECO) +"%'"
    if ctName != None:
        query = query + " and Ins.Nombre = '" + ctName +"'"
    return utiDataBase.selectAll(query, 'TRAZA')

def getTrafoVLevels(vDescription):
    """From a trafo description (i.e 20000/400/230) makes a dictionary filling U1, U2 and U3 voltages."""
    vLevels = vDescription.split("/")
    Ux = []
    for voltage in vLevels:
        Ux.append(int(voltage.replace(".", "")))
    
    if Ux.__len__()==2:
        Ux.append(int(0))
           
    Ux.sort(reverse=True)
        
    dict = {"U1":Ux[0], "U2":Ux[1], "U3":Ux[2]}
    return dict

def getCTsList(MINECO,ctName=None):
    """Return the data necesary to insert one or several CTs into DMS"""
    query="""SELECT  Distinct Ins.Nombre,
                    lp.CodigoPeco AS PROV,
                    lm.CodigoPeco,
                    Ins.Codigo,
                    CT.Particular
             FROM Instalaciones Ins JOIN CT ON Ins.IdInst=CT.IdInst 
                 LEFT JOIN LocMunicipios lm ON lm.IdMunicipio = Ins.IdMunicipio
                 LEFT JOIN LocProvincias lp ON lp.IdProvincia = Ins.IdProvincia  
             WHERE  Borrada = 0 AND CT.Alta = 1 AND Ins.IdInst LIKE '"""+ MINECO[:3] + """%'"""
    if ctName != None:
        query = query + " AND Ins.Nombre = '" +str(ctName)+ "'"
    return utiDataBase.selectAll(query,'TRAZA')

def getCTVoltageLevel(CTName,MINECO):
    """
    Get the DMS CODEINFO CODE for the voltage level of the busbars inside
    CT given in parameter CTName.
    """
    
    query="""select top 1 ATTensionNominal.Valor from CTAlimentaciones, AT, ATTensionNominal, Instalaciones
            where CTAlimentaciones.IdInstAlimentaciones LIKE AT.IdInst and ATTensionNominal.IdTensionNominal LIKE AT.IdTensionNominal 
            and Instalaciones.IdInst LIKE CTAlimentaciones.IdInst and Instalaciones.Nombre LIKE '"""+CTName+"""' 
            and Instalaciones.IdInst LIKE '""" + str(MINECO) + """%'
            union
            select top 1 ATTensionNominal.Valor from CTDerivaciones, AT, ATTensionNominal, Instalaciones
            where CTDerivaciones.IdInstDerivaciones LIKE AT.IdInst and ATTensionNominal.IdTensionNominal LIKE AT.IdTensionNominal 
            and Instalaciones.IdInst LIKE CTDerivaciones.IdInst and Instalaciones.Nombre LIKE '"""+CTName+"""' 
            and Instalaciones.IdInst LIKE '""" + str(MINECO) + """%'"""
    vLevel = utiDataBase.selectOne(query,'TRAZA')
    if vLevel == None:
        # didn't find voltage level, getting the voltage level from *any* trafo we can find
        query="""select ATTensionNominal.Valor from CTAlimentaciones, AT, ATTensionNominal, Instalaciones
                where CTAlimentaciones.IdInstAlimentaciones = AT.IdInst and ATTensionNominal.IdTensionNominal = AT.IdTensionNominal 
                and Instalaciones.IdInst = CTAlimentaciones.IdInst
                union
                select ATTensionNominal.Valor from CTDerivaciones, AT, ATTensionNominal, Instalaciones
                where CTDerivaciones.IdInstDerivaciones = AT.IdInst and ATTensionNominal.IdTensionNominal = AT.IdTensionNominal 
                and Instalaciones.IdInst = CTDerivaciones.IdInst"""
        vLevel = utiDataBase.selectOne(query,'TRAZA')
        errorMsg = "Warning! Cannot find voltage level for CT " + CTName + ", using default (" + str(vLevel[0]) + ")"
        eh.handleWarning(errorMsg)
    vLevel = vLevel[0]
    
    query = "SELECT CODE FROM CODEINFO WHERE INFOTYPE = 'VOLTAGE_LEVEL' AND INFO = '"+str(vLevel)+" kV'"
    query = query.replace('.0', '')

    vLevel = utiDataBase.selectOne(query,'DMS')
    
    if vLevel is None:
        errorMsg = "Warning! Cannot find CODE in CODEINFO for voltage level of CT " + CTName + ", set to 0"
        eh.handleWarning(errorMsg)
        vLevel=0   
    else:
        vLevel = vLevel[0]

    
    return vLevel

def getLinesList(MINECO, lineName=None): #Implementar. Seg�n necesite a�adir algo para insertar una l�nea, pongo que esta query recupere ese dato. (A lo mejor no todos, pero los m�s relevantes y que luego solo tenga que volver al Traza para tal vez los sectionpoints o cosas as�.
    query ="""SELECT Ins.IdInst, AT.IdTipoLinea, AT.IdOrigenLinea, AT.IdTipoOrigen,
                AT.IdOrigenPunto, ATTensionNominal.Valor AS Voltaje, AT.Alta, Ins.Nombre, Ins.Codigo
              FROM [traza].[dbo].[AT] 
              JOIN Instalaciones Ins ON AT.IdInst = Ins.IdInst
              LEFT JOIN ATTensionNominal ON AT.IdTensionNominal=ATTensionNominal.IdTensionNominal
              WHERE AT.Alta = 1 AND Ins.IdInst LIKE '""" + str(MINECO) +"%' "
            
#     """********** APA�O CHACON, YA QUE TIENE MAL CLASIFICADAS MUCHAS DERIVACIONES Y LAS PONE COMO SUBRDERIVACIONES **********"""
#     query ="""SELECT Ins.IdInst,CASE WHEN Ins.Nombre LIKE 'D%' THEN 2 
#                         WHEN Ins.Nombre = 'L.CARPA.2' THEN 1 
#                         ELSE AT.IdTipoLinea END AS IdTipoLinea , AT.IdOrigenLinea, AT.IdTipoOrigen,
#                 AT.IdOrigenPunto, ATTensionNominal.Valor AS Voltaje, AT.Alta, Ins.Nombre, Ins.Codigo
#               FROM [traza].[dbo].[AT] 
#               JOIN Instalaciones Ins ON AT.IdInst = Ins.IdInst
#               LEFT JOIN ATTensionNominal ON AT.IdTensionNominal=ATTensionNominal.IdTensionNominal
#               WHERE AT.Alta = 1 AND Ins.IdInst LIKE '""" + str(MINECO) +"%' "
#     """********** FIN APA�O CHACON **********"""
    
    #TESTING
    #query = query + " AND AT.IdTipoLinea IN (1) " 
            
    if lineName != None:
        query = query + " AND Ins.Nombre = '" + lineName + "'"
    query = query + " ORDER BY IdTipoLinea ASC"
    return utiDataBase.selectAll(query,'TRAZA')

def getLineOrigin(lineType,originType,originPoint,lineName,IdOrigenLinea):
    """Get the starting point of a line it returns: CodigoGDL,X,Y"""
#     if lineName == 'LP CDC VILLAMA�AN-CDC FRESNO_10-00':
#          return getLineOriginFromPF(IdOrigenLinea,lineName)
#     if lineName == 'L.AGIP' and IdOrigenLinea[:3] == '180':
#         return getLineOriginFromPF(IdOrigenLinea,lineName)
    if str(lineType) == '1': # Linea Principal     HAY QUE PONER STR!!!!!!!
        if str(originType) == '1': #Punto de entronque
            return getLineOriginFromPF(IdOrigenLinea,lineName) #EN OBRAS
        elif str(originType) == '2': #Subestaci�n
            return getLineOriginFromPF(IdOrigenLinea,lineName) #EN OBRAS
        elif str(originType) == '4': #Centro de transformaci�n
            return getLineOriginFromCT(originPoint,lineName)

    elif str(lineType) in ('2','3'): # Derivacion y subderivacion
        if str(originType) == '4': ##Centro de transformaci�n
            return getLineOriginFromCT(originPoint,lineName)
        elif str(originType) == '5': #Apoyo
            return getLineOriginFromRelay(originPoint,lineName)
        
def getLineOriginFromCT(originPoint,lineName):
    if originPoint == None:
        errorMsg = "ERROR. Line " +lineName+": doesn't has defined an OriginPoint." 
        errorBox = "Problemas al obtener el punto de origen de la l�nea " + lineName + ". No definido."
        eh.handleError(errorMsg,errorBox)
        return -1
    query = """SELECT Lins.IdInst AS lineID, CTIns.Codigo 
            FROM Instalaciones LIns
                JOIN AT
                    ON LIns.IdInst = AT.IdInst
                JOIN Instalaciones CTIns
                    ON AT.IdOrigenPunto = CTIns.IdInst
             WHERE LIns.Nombre ='""" + lineName + "' AND LIns.IdInst LIKE '" + originPoint[:3] + "%' AND CTIns.IdInst = '" + originPoint + "'"
    result = utiDataBase.selectOne(query,'TRAZA')
    if result == None:
        errorMsg = "ERROR. Line +" +lineName+": Has defined an OriginPoint that doesn't exists." 
        errorBox = "Problemas al obtener el punto de origen de la l�nea " + lineName + ". No existe"
        eh.handleError(errorMsg,errorBox)
        return -1
    code = formatCTCode(result.Codigo+'1', originPoint[:3])
    exitCode = 'EXX' + '-' + code + '-' + result.lineID
    return exitCode,result.lineID,0


def getLineOriginFromRelay(originPoint,lineName):
    if originPoint == None:
        errorMsg = "ERROR. Line +" +lineName+": doesn't has defined an OriginPoint." 
        errorBox = "Problemas al obtener el punto de origen de la l�nea " + lineName + ". No definido."
        eh.handleError(errorMsg,errorBox)
        return -1
    query = "SELECT IdInst, IdATVano, X, Y FROM ATVano WHERE IdATVano = '" + originPoint +"'"
    result = utiDataBase.selectOne(query,'TRAZA')
    if result == None:
        errorMsg = "ERROR. Line +" +lineName+": Has defined an OriginPoint that doesn't exists." 
        errorBox = "Problemas al obtener el punto de origen de la l�nea " + lineName + ". No existe"
        eh.handleError(errorMsg,errorBox)
        return -1
    relayCode= getGDLCode('L',result.IdInst,result.IdATVano)
    return relayCode,result.X,result.Y

def getLineOriginFromPF(IdOrigenLinea,lineName): 
    if IdOrigenLinea == None:
        errorMsg = "ERROR. Line +" +lineName+": doesn't has defined an OriginPoint." 
        errorBox = "Problemas al obtener el punto de origen de la l�nea " + lineName + ". No definido."
        eh.handleError(errorMsg,errorBox)
        return -1
    query = "SELECT Nodos.IdNodo,Nodos.X,Nodos.Y, Instalaciones.IdInst FROM Nodos \
            JOIN Instalaciones ON Nombre='" + lineName + "' AND IdInst LIKE '" + IdOrigenLinea[:3] + "%'\
             WHERE IdNodo ='" + IdOrigenLinea +"'"
    result = utiDataBase.selectOne(query,'TRAZA')
    if result == None:
        """NEW QUERY FOR ST WITHOUT NODES DEFINED. NOT SURE IF IT IS CORRECT WAY OF DOING THIS. PREV CODE HAS THE ERROR HANDLING JUST RIGHT HERE, NOT INSIDE ANOTHER IF AFTER THE NEW QUERY."""
        print lineName + ' no tiene nodo de origen!!'
        query = "SELECT ST.X,ST.Y, Instalaciones.IdInst, Instalaciones.IdInst AS IdNodo FROM Instalaciones \
                JOIN AT ON AT.IdInst = Instalaciones.IdInst \
                JOIN ST ON ST.IdInst = AT.IdOrigenLinea \
                WHERE Instalaciones.Nombre='" + lineName + "' AND Instalaciones.IdInst LIKE '" + IdOrigenLinea[:3] + "%'"
        result = utiDataBase.selectOne(query,'TRAZA')
        if result == None:
            errorMsg = "ERROR. Line +" +lineName+": Has defined an OriginPoint that doesn't exists." 
            errorBox = "Problemas al obtener el punto de origen de la l�nea " + lineName + ". No existe"
            eh.handleError(errorMsg,errorBox)
            return -1
    
    feederCode=getGDLCode('Feed','XX',result.IdNodo,result.IdInst)
    xg, yg = uC.convertED50(result.X,result.Y, getCompanyZone(result.IdNodo[:3]))
    xg = int(str(xg).split('.')[0] + "000")
    yg = int(str(yg).split('.')[0] + "000")
    dmsCode = utiDMS.insertLineNode('X',feederCode,xg,yg,result.IdNodo[:3])
    if dmsCode < 0:
        errorMsg = "ERROR. Line +" +lineName+": Error while inserting new node as line origin." 
        errorBox = "Problemas al obtener el punto de origen de la l�nea " + lineName + ". No se puedo crear."
        eh.handleError(errorMsg,errorBox)
        return -1
    
    return feederCode,dmsCode,(xg,yg)

def getBPList(MINECO, bpName=None):
    query = """SELECT Instalaciones.Nombre Nombre, Instalaciones.IdInst, Nodos.IdNodo, Nodos.X, Nodos.Y, Nodos.TensionSubestacion, Nodos.Subestacion_o_PE 
                FROM Nodos, Instalaciones
                WHERE Nodos.IdNodo = Instalaciones.IdInst AND IdNodo LIKE '""" +str(MINECO) + """%'"""
                
    if bpName != None:
        query = query + """ AND Instalaciones.Nombre = '"""+ bpName + """'"""
        
    return utiDataBase.selectAll(query,'TRAZA')
        
        


def getOrderedCeldas(code):
    query = """SELECT  gdlCeldas.CodigoCT,
                gdlCeldas.CodigoLinea,
                gdlCeldas.X,
                gdlCeldas.Y,
                gdlCeldas.Nombre,
                gdlCeldas.IdInst,
                gdlCeldas.IdInstLinea,
                gdlCeldas.IdTipoCelda,
                gdlCeldas.IdTransformador,
                gdlCeldas.DisyuntorFusible,
                gdlCeldas.Modelo,
                gdlCeldas.Transformador,
                CASE WHEN gdlOrdenCeldas.Posicion IS NULL THEN gdlCeldas.Posicion
                    ELSE gdlOrdenCeldas.Posicion END AS Posicion
        FROM gdlCeldas
            LEFT JOIN gdlOrdenCeldas
                ON gdlCeldas.CodigoCT = gdlOrdenCeldas.CodigoCT
                AND (gdlCeldas.Transformador = gdlOrdenCeldas.Transformador
                        OR
                     gdlCeldas.CodigoLinea = gdlOrdenCeldas.CodigoLinea)
        where gdlCeldas.CodigoCT = '""" + str(code) + """'
        order by gdlOrdenCeldas.Posicion"""
    return utiDataBase.selectAll(query,'DMS')

def getLineSections(MINECO,lineName):
    """Returns a list of line sections of given line"""
    query = """SELECT  Ins.IdInst, Ins.IdTipoInst, Ins.Nombre, Ins.Codigo, ATV.IdATVano,
                ATV.Designacion, ATV.Subterraneo, ATV.IdCircuitoAT, Con.Descripcion AS Conductor, 
                ATV.IdInstCTNoSobre, ATV.IdInstCTSobre, ATV.CodApoyo, ATV.IdInstPFNoSobre,
                ATV.IdFuncion, ATV.X, ATV.Y, ATV.ApoyoSecuencial, ATV.Alta
            FROM Instalaciones Ins
                JOIN ATVano ATV ON Ins.IdInst = ATV.IdInst
                JOIN VATVanoTiposConductores Con ON Con.IdTipoConductor = ATV.IdTipoConductor 
            WHERE Ins.IdInst LIKE '""" + str(MINECO[0:3]) + """%' --AND ATV.IdConductorFase IS NULL
                AND Ins.Nombre LIKE '""" + str(lineName) + """'
            ORDER BY ApoyoSecuencial"""
    return utiDataBase.selectAll(query,'TRAZA')

def drawLine(startCode,xgStart,ygStart,endCode,xg,yg,conductor,sectionPoints,MINECO,lineName, IdLineOrigin, IdLinea,IdVano,voltage, length = 0):
    """Draw a Line in DMS"""
    type = endCode[-2]
    if type == 'X':
        gdlCode = getGDLCode('L',IdLinea,IdVano)
    elif type == 'E':
        gdlCode = getGDLCode('Scc',IdLinea,IdVano,endCode)
    elif type == 'O':
        gdlCode = getGDLCode('Fus',IdLinea,IdVano,endCode)
    if isNode(endCode) != True:
        endCode = utiDMS.insertLineNode(type,gdlCode,xg,yg,MINECO)
    else:
        if nodeBelongToLine(endCode,IdLinea) == False & nodeBelongToLine(endCode,IdLineOrigin) == False:
            endCode = utiDMS.insertLineNode(type,gdlCode,xg,yg,MINECO)
    
    mv_SectionID = utiDMS.getMVSectionID()
    utiDMS.insertMVSection(mv_SectionID, startCode, endCode, conductor, MINECO, voltage, xgStart, ygStart, LENGTH = length, NAME = lineName)
    utiDMS.insertMVSectionPoints(mv_SectionID, startCode, endCode, sectionPoints, MINECO)
    
    return endCode

def getGDLCode(type,var1=None,var2=None,var3=None):   
    """Return the GDLCode depending on object type"""
    if type == 'L': #relay of a line: 'L'+IdInst de la L�nea+'-'+IdATVano del Vano
        return 'L'+var1+'-'+var2
    if type == 'D': #Relay that has been moved beacuse it has a derivation and a protection: 'D'+IdInst de la L�nea+'-'+IdATVano del Vano
        return 'D'+var1+'-'+var2
    if type == 'CT': #related to CTs: 'C'+MV_LV_SUBSTATION.CODE, except exit points
        return 'C'+var1
    if type == 'CTelem': #CTs elements: 'P'+MV_LV_SUBSTATION.CODE+-+PosNumber
        return 'P'+var1+'-'+var2
    if type == 'E': #CTs exit points: 'E'+celda position(2digits)+'-'+MV_LV_SUBSTATION.CODE+'-'+IdInstLinea
        return 'E'+var1+'-'+var2+'-'+var3
    if type == 'Feed': #Primary Substation Feeder or PF Feeder: 'F'+celda position(2digits)+'-'+result.IdNodo+'-'+result.IdInst
        return 'F'+var1+'-'+var2+'-'+var3
    if type == 'Scc': #Seccionador in line: 'S'+IdInstLinea+'-'+IdATVano
        return 'S'+var1+'-'+var2
    if type == 'SccD': #Seccionador in line when its the protection from a starting derivation: 'J'+IdInstLinea+'-'+IdATVano
        return 'J'+var1+'-'+var2
    if type == 'FusD':
        return 'K'+var1+'-'+var2 #Seccionador in line when its the protection from a starting derivation: 'K'+IdInstLinea+'-'+IdATVano
    if type == 'Fus':  #Fuse in line: 'O'+IdInstLinea+'-'+IdATVano
        return 'O'+var1+'-'+var2
    if type == 'T': #Transformer: 'T'+CTCode+'-'numTrafo
        return 'T'+var1+'-'+var2
    if type == 'ST': #Substation or BorderPoint Diagram: 'B'+bp.Name+'-'+bp.IdNodo
        return 'B'+var1+'-'+var2
    if type == 'STF': #Substation or BorderPoint feeder: 'B'+bp.Nombre+'-'+bp.IdNodo+'-'+bp.IdInst
        return 'H'+var1+'-'+var2
    if type == 'BBN':  #Substation or BorderPoint Busbar Node: 'N'+bp.Name+'-'+bp.IdNodo+'-'+nodecode
        return 'N'+var1+'-'+var2+'-'+var3
    if type == 'CBBP':  #Substation or BorderPoint Circuit breaker: 'C'+bp.Name+'-'+bp.IdNodo+'-'+nodecode
        return 'C'+var1+'-'+var2+'-'+var3 
    if type == 'Gen':  #Substation or BorderPoint Feeding Point: 'C'+bp.Name+'-'+bp.IdNodo+'-'+bp.IdInst
        return 'G'+var1+'-'+var2+'-'+var3 
    errorMsg = "ERROR. GEDLCode couldn't have been created. Type '"+type+"' not found." 
    errorBox = "Problema inesperado. No se puydo crear el c�digo identificativo GDLCode. Tipo: " + type
    eh.handleError(errorMsg,errorBox)
    return -1
    
    
def getCompanyZone(MINECO):
    """Return the UTM zone of given company"""
    query = "select zone from gdlMINECO WHERE ID LIKE '" + str(MINECO[0:3])+"'"
    result = utiDataBase.selectOne(query,'DMS')
    if result == None:
        pymsg = "Warning! getCompanyZone not found results in gdlMINECO. Using zone 29 for company " + str(MINECO[:3])
        return 29
    return result[0]

def getLineDMSCode(code,X,Y,MINECO):
    """Returns the DMSCode where the node should be inserted.
    Checks if there is another node in that point belonging to a different line not to overlap the nodes."""
    if code[0] == 'E':
        return -6 # EN OBRAS
    elif code[0] == 'L': 
        if isInNodes(code) == True:
            query = "SELECT CODE FROM MV_NODE WHERE CodigoGDL = '" + code + "'"
            return utiDataBase.selectOne(query,'DMS')[0]
        elif isInPoints(code)== True:
            utiDMS.deleteMV_Node(code)
            utiDMS.insertLineNode(code,X,Y,MINCEO)
        else:
            errorMsg = "ERROR. Point with code " +code+" not found. Needed for making conection." 
            errorBox = "Problemas al buscar el punto de conexi�n " + code + ". No se puedo encontrar."
            eh.handleError(errorMsg,errorBox)
            return -1
    errorMsg = "ERROR. PrepareNode got an incorrect code: " + code 
    errorBox = "Error inesperado. C�digo de vano No v�lido: " + code
    eh.handleError(errorMsg,errorBox)
    return -1

def getDrawingDirection(IdLinea, IdATVano, mode, avoidAxis = None):
    """Calculates the direction in which the line drawing advances (just 4 cardinal points accuracy).
    mode determines if it takes the prev vano to calculate direction (mode='P') or the next 'N' or the origin vano
    belonging to origin line 'O'. avoidAxis prevents the result to be contained in selectesd axis ('V' or 'H')"""
    if mode == 'P':
        query = """WITH Vano AS (SELECT ApoyoSecuencial AS N1, X AS X1, Y AS Y1
              FROM [traza].[dbo].[ATVano] WHERE IdATVano = '"""+IdATVano+"""'),
            PrevVano AS (SELECT ApoyoSecuencial AS N0, X AS X0, Y AS Y0
                FROM ATVano WHERE IdInst = '"""+IdLinea+"""' AND ApoyoSecuencial = (SELECT N1
              FROM Vano) -1) 
            SELECT N1, X1, Y1, N0, X0, Y0
            FROM Vano, PrevVano"""
    elif mode == 'N':
        query = """WITH Vano AS (SELECT ApoyoSecuencial AS N0, X AS X0, Y AS Y0
              FROM [traza].[dbo].[ATVano] WHERE IdATVano = '"""+IdATVano+"""'),
            NextVano AS (SELECT ApoyoSecuencial AS N1, X AS X1, Y AS Y1
                FROM ATVano WHERE IdInst = '"""+IdLinea+"""' AND ApoyoSecuencial = (SELECT N0
              FROM Vano) + 1) 
            SELECT N1, X1, Y1, N0, X0, Y0
            FROM Vano, NextVano"""
    elif mode == 'O':
        query = """WITH Vano AS (SELECT ApoyoSecuencial AS N0, X AS X0, Y AS Y0
              FROM [traza].[dbo].[ATVano] WHERE IdATVano = '"""+IdATVano+"""'),
            NextVano AS (SELECT TOP 1 ApoyoSecuencial AS N1, X AS X1, Y AS Y1
                FROM ATVano WHERE IdInst = '"""+IdLinea+"""'
                ORDER BY ApoyoSecuencial ASC)
            SELECT N1, X1, Y1, N0, X0, Y0
            FROM Vano, NextVano
            """
    result = utiDataBase.selectOne(query,'TRAZA')
    
    if result < 0:
        return result
    Vx = int(result.X1)-int(result.X0)
    Vy = int(result.Y1)-int(result.Y0)
    rad = atan2(Vy,Vx)
    
    if avoidAxis == None:
        if rad < 0.78 and rad > -0.78:
            return 'r' #right
        if rad >= 0.78 and rad < 2.35:
            return 'u' #up
        if rad <= -0.78 and rad > -2.35:
            return 'd' #down
        return 'l' #left
    if avoidAxis == 'V':
        if rad > 1.57 or rad < -1.57:
            return 'l'
        return 'r'
    if avoidAxis == 'H':
        if rad > 0:
            return 'u'
        return 'd'
    
    

def setCeldaPosition(celda,i):
    """Update position in gdlCeldas"""
    query = "UPDATE t SET t.Posicion = '" + str(i) + "'\
            FROM (SELECT Posicion ,ROW_NUMBER() OVER (ORDER BY Posicion) AS Num FROM gdlCeldas \
                  WHERE CodigoCT = '" + str(celda.CodigoCT) + "' AND IdTipoCelda = '" + str(celda.IdTipoCelda) +"'"
    if celda.CodigoLinea == None:
        query = query + "  AND CodigoLinea IS NULL "
    else:
        query = query + "AND CodigoLinea = '" + str(celda.CodigoLinea) + "' "
        
    if celda.IdInstLinea == None:
        query = query + "  AND IdInstLinea IS NULL "
    else:
        query = query + " AND IdInstLinea = '" + str(celda.IdInstLinea) + "'"
        
    if celda.IdInst == None:
        query = query + "  AND IdInst IS NULL "
    else:
        query = query + " AND IdInst = '" + str(celda.IdInst) + "'"
        
    query = query + ") t WHERE Num = 1"
        
    if utiDataBase.execute(query,'DMS') < 0:
        errorMsg = "ERROR. Insertando posici�n: " + celda.CodigoCT +", psocion " + str(i) + "."
        errorBox = "Error inesperado. No se pudo insertar la posicion de la celda " + str(i) \
            + " del centro " + celda.Nombre + "." 
        eh.handleError(errorMsg,errorBox)
        return -1
    return 0

def getCeldaToConnect(gdlCode,lineID):
    """Returns the nodecode to conect with given origin point."""
    query = "SELECT CODE, X, Y FROM MV_NODE \
            WHERE CodigoGDL LIKE 'E%" + gdlCode[3:] + "' AND gdlMINECO LIKE '" + lineID[:3] + "'"
    result = utiDataBase.selectAll(query,'DMS')
    if result < 0:
        errorMsg = "ERROR. getting celda to connect. GDLCode: " + gdlCode +". SQL Error."
        errorBox = "Error inesperado. Fallo en la consulta al localizar una celda libre para la l�nea " + str(lineID) + "." 
        eh.handleError(errorMsg,errorBox)
        return -1, -1, -1
    if len(result) == 1:
        return result[0].CODE, result[0].X, result[0].Y
    if len(result) == 0:
        errorMsg = "ERROR. getting celda to connect. GDLCode " + gdlCode +" doesn't exist."
        errorBox = "Error inesperado. No se encontr� la celda donde comienza la l�nea " + str(lineID) + "." 
        eh.handleError(errorMsg,errorBox)
        return -1, -1, -1
    if len(result) > 1:
        for code in result:
            if isSection(code.CODE) == True:
                continue
            else:
                return code.CODE, code.X, code.Y
        errorMsg = "ERROR. getting celda to connect. GDLCode: " + gdlCode +". Empty celda not found."
        errorBox = "Error inesperado. No se encontr� una celda libre para la l�nea " + str(lineID) + "." 
        eh.handleError(errorMsg,errorBox)
        
def getNodeToConnect(origin): #FALTA METER VER SI EL NODO DONDE CONECTA TIENE DERIVACION Y/O PROTECCION
    """Returns the nodecode to conect with given origin point."""
    IdATVano = str(origin[0].split('-')[1])
    protection = getRelayProtection(IdATVano)
    if protection > 0:
        c,x,y = getNodeToConnectInRelayWithProtection(origin,protection)
        return c,x,y
    lineID = str(origin[0].split('-')[0][1:])
    query= "SELECT CODE, X, Y FROM MV_NODE WHERE CodigoGDL = '" + origin[0] + "'"
    result = utiDataBase.selectOne(query,'DMS')
    if result == -1:
        errorMsg = "ERROR. getting node to connect. GDLCode: " + origin[0] +". SQL Error."
        errorBox = "Error inesperado. Fallo en la consulta al localizar un nodo libre para la l�nea " + str(lineID) + "." 
        eh.handleError(errorMsg,errorBox)
        return -1, -1, -1
    if result != None:
        return result.CODE, result.X, result.Y
    query= "SELECT CodigoGDL, gdlMINECO, X, Y, MV_Section_Id, NUMBER FROM MV_SECTIONPOINT WHERE CodigoGDL = '" + origin[0] + "'"
    result = utiDataBase.selectOne(query,'DMS')
    if result == -1:
        errorMsg = "ERROR. getting node to connect. GDLCode: " + origin[0] +". SQL Error."
        errorBox = "Error inesperado. Fallo en la consulta al localizar un nodo libre para la l�nea " + str(lineID) + "." 
        eh.handleError(errorMsg,errorBox)
        return -1, -1, -1
    if result != None:
        utiDMS.deleteSectionPoint(result.CodigoGDL)
        dmsCode = utiDMS.insertLineNode ('X',result.CodigoGDL, result.X, result.Y, result.gdlMINECO)
        if dmsCode < 0:
            errorMsg = "ERROR. creating node to connect. GDLCode: " + origin[0] +". SQL Error."
            errorBox = "Error inesperado. Fallo en la creaci�n de un nodo para la l�nea " + lineID + "." 
            eh.handleError(errorMsg,errorBox)
            return -1, -1, -1
        
        splitSection(dmsCode,result.MV_Section_Id,result.CodigoGDL,RelayNum = result.NUMBER)
        x,y = uC.getCoordinates(dmsCode)
        return dmsCode, x, y
    errorMsg = "ERROR. Node to connect not found. GDLCode: " + origin[0] +". SQL Error."
    errorBox = "Error inesperado. Fallo al localizar un nodo de inicio para la l�nea " + lineID + "." 
    eh.handleError(errorMsg,errorBox)
    return -1,-1,-1

def getNodeToConnectInRelayWithProtection(origin,protection):
    """When a line starts in a relay with a protection, the line cannot start in the protection itself and 
        therefore the line origin should be moved a bit"""        
    #If the new node has already been created, return its data
    if isNode('0','D'+origin[0][1:]):
        query = "SELECT CODE, X, Y FROM MV_NODE WHERE CodigoGDL = 'D"++origin[0][1:]+"'"
        if result == -1:
            errorMsg = "ERROR. getting node to connect. GDLCode: " + origin[0] +". SQL Error."
            errorBox = "Error inesperado. Fallo en la consulta al localizar un nodo libre para la l�nea " + str(lineID) + "." 
            eh.handleError(errorMsg,errorBox)
            return -1, -1, -1
        return result.CODE, result.X, result.Y
    
    #If not, get data from parameters
    if protection == 'Fusible':
        GDLCode = 'O' + origin[0][1:]
    else:
        GDLCode = 'S' + origin[0][1:]
        
    lineID = origin[0][1:].split('-')[0]
    relayID = origin[0][1:].split('-')[1]
        
    
    query = """SELECT CODE, X, Y, MV_Section_Id
            FROM MV_NODE LEFT JOIN MV_SECTION 
                ON MV_SECTION.NODE1 = MV_NODE.CODE
            WHERE MV_NODE.CodigoGDL = '"""+GDLCode+"""'"""    
    result = utiDataBase.selectOne(query,'DMS')
    
   

    if result == -1:
        errorMsg = "ERROR. getting node to connect. GDLCode: " + origin[0] +". SQL Error."
        errorBox = "Error inesperado. Fallo en la consulta al localizar un nodo libre para la l�nea " + str(lineID) + "." 
        eh.handleError(errorMsg,errorBox)
        return -1, -1, -1
    if result != None:
        X = result.X
        Y = result.Y

        
        if result.MV_Section_Id == None:
            return result.CODE, X, Y
        
        dir = getDrawingDirection(lineID,relayID,'P')
        if dir == 'u':
            Y = Y + 1000
        elif dir == 'd':
            Y = Y - 1000
        elif dir == 'r':
            X = X + 1000
        else:
            X = X - 1000
        
        dmsCode = utiDMS.insertLineNode ('X','D'+GDLCode[1:], X, Y, origin[0][1:4])
        if dmsCode < 0:
            errorMsg = "ERROR. creating node to connect. GDLCode: " + 'D'+GDLCode[1:] +". SQL Error."
            errorBox = "Error inesperado. Fallo en la creaci�n de un nodo para la l�nea " + lineID + "." 
            eh.handleError(errorMsg,errorBox)
            return -1, -1, -1
        
        

        splitSection(dmsCode,result.MV_Section_Id,'D'+GDLCode[1:])
        return dmsCode, X, Y
        
        #dependiendo de la direccion de avance de dibujo de la linea mover las coords 1000 a dcha,izq,arr o ab.
        #En ese nuevo punto hacer un split line y meter el branchin point donde enganchar� la nueva l�nea.
        #devolver el c�digo de ese branching point (habr� que definirlo para estos casos).
        
    errorMsg = "ERROR. Node to connect not found. GDLCode: " + protection + " " + origin[0][1:] +". SQL Error."
    errorBox = "Error inesperado. Fallo al localizar un nodo de inicio para la l�nea " + lineID + "." 
    eh.handleError(errorMsg,errorBox)
    return -1,-1,-1      
    
def getNodesFeededByBP(idNodo):
    """Returns all nodes which are feeded by given bp"""
    query = "SELECT CODE FROM MV_NODE WHERE CodigoGDL LIKE 'FXX-" + idNodo +"-%'"
    result = utiDataBase.selectAll(query,'DMS')
    if result == None:
        return 0
    return result 


def getUpperLine(MINECO = None,lineName = None, IdInst = None):
    """Returns the upper line name if given line has upper line. Otherwise it returns 0.
    Search par�meters are MINECO and Line Name (both) or IdInst"""
    query = """SELECT Nombre,IdInst FROM Instalaciones 
            WHERE IdInst = (SELECT AT.IdOrigenLinea
            FROM AT
            JOIN Instalaciones Ins ON AT.IdInst = Ins.IdInst
            WHERE AT.Alta = 1 AND AT.IdTipoOrigen = 5 AND"""
    if lineName != None and MINECO != None:
        query = query + """ Ins.IdInst LIKE '"""+MINECO+"""%' AND          
                        Ins.Nombre = '"""+lineName+"""' )"""
    elif IdInst != None:
        query = query + """ Ins.IdInst = '"""+IdInst+"""')"""
    else:
        return -1
    
    result = utiDataBase.selectOne(query,'TRAZA')
    if result < 0:
        return result
    if result == None:
        return 0
    if lineName != None and MINECO != None:
        return result.Nombre
    return result.IdInst

        
def isSection(code):
    """Check if there is a MV_SECTION passing trough given node"""
    query = "SELECT MV_Section_Id FROM MV_SECTION WHERE SUBSTATION IS NULL AND (NODE1 = '" +\
                    code + "' OR NODE2 = '" + code + "')"
    result = utiDataBase.selectOne(query,'DMS')
    if result < 0:
        return result
    if result == None:
        return False
    return True

def isNode(code, GDLCode = None):
    """Check if there is a MV_NODE with given node"""
    query = "SELECT CODE FROM MV_NODE WHERE CODE = '" + code + "'"
    if GDLCode != None:
        query = "SELECT CodigoGDL FROM MV_NODE WHERE CODE = '" + GDLCode + "'" 
    result = utiDataBase.selectOne(query,'DMS')
    if result < 0:
        return result
    if result == None:
        return False
    return True

def getRelayProtection(idATVano):
    query = """    SELECT PROTECTION FROM (SELECT    CASE
                -- Fuse
                WHEN ATVM.IdATTipoManiobraProteccion IN (2,3,6)
                        OR ATVM.IdATTipoManiobraProteccion IN (2,3,6)
                        THEN 'Fusible'
                -- Disconnector
                WHEN ATVM.IdATTipoManiobraProteccion IN (4,5,6,8,13)
                        OR ATVM.IdATTipoManiobraProteccion IN (4,5,6,8,13)
                        THEN 'Seccionador'
                ELSE NULL
            END AS PROTECTION 
    FROM ATVano ATV
        JOIN ATVanoManiobraProteccion ATVM
            ON ATVM.IdATVano = ATV.IdATVano

    WHERE ATV.IdATVano = '"""+idATVano+"""') AS t
    WHERE t.PROTECTION IS NOT NULL"""
    result = utiDataBase.selectOne(query,'TRAZA')
    if result == None:
        return 0
    elif result < 0:
        return -1
    return result.PROTECTION


def getProtectionInDerivation(MINECO,IdOrigenPunto,lineName):
    """If given line starts in a relay and in that relay is defined a derivation with its own protection, 
    it returns the type of protection if the name of derivation matches the given one. Otherwise returns 0"""

    query= """SELECT  CASE
                -- Fuse
                WHEN ATVDM.IdATTipoManiobraProteccion IN (2,3,6)
                        OR ATVDM.IdATTipoManiobraProteccion IN (2,3,6)
                        THEN 'Fusible'
                -- Disconnector
                WHEN ATVDM.IdATTipoManiobraProteccion IN (4,5,6,8,13)
                        OR ATVDM.IdATTipoManiobraProteccion IN (4,5,6,8,13)
                        THEN 'Seccionador'
                ELSE NULL
            END AS PROTECTION  
            FROM ATVanoDerivacionesManiobras ATVDM 
                JOIN ATVanoDerivaciones ATVD 
                    ON ATVDM.IdDerivacion = ATVD.IdDerivacion
                JOIN Instalaciones Ins 
                    ON Ins.IdInst = ATVD.IdInstDerivada
            WHERE  ATVD.IdATVano = '"""+IdOrigenPunto+"""' AND Ins.Nombre = '"""+lineName+"""'"""

#OLD QUERY, IT SEEMS TO FAIL
#     query = """SELECT PROTECTION FROM (SELECT    CASE
#                 -- Fuse
#                 WHEN ATVDM.IdATTipoManiobraProteccion IN (2,3,6)
#                         OR ATVDM.IdATTipoManiobraProteccion IN (2,3,6)
#                         THEN 'Fusible'
#                 -- Disconnector
#                 WHEN ATVDM.IdATTipoManiobraProteccion IN (4,5,6,8,13)
#                         OR ATVDM.IdATTipoManiobraProteccion IN (4,5,6,8,13)
#                         THEN 'Seccionador'
#                 ELSE NULL
#             END AS PROTECTION 
#     FROM ATVano ATV
#         JOIN ATVanoDerivaciones ATVD
#             ON ATV.IdATVano = ATVD.IdATVano
#         JOIN ATVanoDerivacionesManiobras ATVDM
#             ON ATVDM.IdDerivacionManiobra = ATVD.IdDerivacion
#         JOIN Instalaciones Ins
#             ON Ins.IdInst = ATVD.IdInstDerivada
#     WHERE ATV.IdATVano = '"""+IdOrigenPunto+"""' AND Ins.Nombre = '"""+lineName+"""') AS t
#     WHERE t.PROTECTION IS NOT NULL"""
    result = utiDataBase.selectOne(query,'TRAZA')
    if result == None:
        return 0
    elif result < 0:
        return -1
    return result.PROTECTION

def getStartNodeWithProtection(origin, nodeCode, xgStart, ygStart,protection,secc,line):
    """Returns new start point of a line which starts with a protection, after having created the protection"""
    lineID = origin[0][1:].split('-')[0]
    relayID = origin[0][1:].split('-')[1]
    MINECO = origin[0][1:4]
    
    upperDir = getDrawingDirection(lineID,relayID,'P')

    if upperDir == 'l' or upperDir == 'r':
        dir = getDrawingDirection(line.IdInst,relayID,'O',avoidAxis='H')
    elif upperDir == 'u' or upperDir == 'd':
        dir = getDrawingDirection(line.IdInst,relayID,'O',avoidAxis='V')
    else:
        dir = 'r' #default direction in case of unexpected errors
        errorMsg = "WARNING. Default direction has been taken when getting starting node with protection in the derivation for line " + line.Nombre
        eh.handleWarning(errorMsg)

    X=xgStart
    Y=ygStart
    
    if dir == 'u':
        Y = Y + 1000
    elif dir == 'd':
        Y = Y - 1000
    elif dir == 'r':
        X = X + 1000
    else:
        X = X - 1000
        
    if protection == 'Fusible':
        type = 'O'
        gdlCode=getGDLCode('FusD',lineID,relayID)
    else:
        type = 'E'
        gdlCode=getGDLCode('SccD',lineID,relayID)
        
    protDMSCode = utiDMS.insertLineNode(type,gdlCode,X,Y,MINECO)
    circ=1
    utiDMS.insertLineElement(protection,MINECO,circ,protDMSCode,'',X,Y)

    utiDMS.insertMVSection(utiDMS.getMVSectionID(), nodeCode, protDMSCode, secc.Conductor, MINECO, str(int(line.Voltaje)), xgStart, ygStart, NAME = line.Nombre) 

    return protDMSCode, X, Y
    

def insertgdlCeldas(celdas,MINECO):
    """Insert a list of celdas in gdlCeldas"""
    if celdas == None:
        return -1
    error = False
    for c in celdas:
        #In order to being able to have more than one trafo in a CT, each trafo is considered as one CT itself
        #so we add + '1'  to the CT code to differentiate the first trafo added from the following ones (if there are any). 
        #Celdas are placed in the first CT aded, so its CTCode is +'1'
        c.CodigoCT = formatCTCode(c.CodigoCT,MINECO) + '1'  
        query = """INSERT INTO gdlCeldas VALUES(
                '"""+str(c.CodigoCT)+"""'
                ,'"""+str(c.CodigoLinea[:10])+"""'
                ,'"""+str(int(c.X))+"""'
                ,'"""+str(int(c.Y))+"""'
                ,'"""+str(c.Nombre)+"""'
                ,'"""+str(c.IdInstLinea)+"""'
                ,'"""+str(c.IdTipoCelda)+"""'
                ,'"""+str(c.IdInst)+"""'
                ,'"""+str(c.IdTransformador)+"""'
                ,'"""+str(c.DisyuntorFusible)+"""'
                ,'"""+str(c.Modelo)+"""'
                ,'"""+str(c.Transformador)+"""'
                ,'"""+str(c.Posicion)+"""')"""      
        query = query.replace("'None'","NULL")
        if utiDataBase.execute(query,'DMS') < 0:
            error = True
    if error == True:
        return -1
    return 0

def isDevelopMode():
    """Check if develop mode is on"""
    return cReader().getDevelopMode()

# def isCelda(MINCEO, ctName):
#     """Check if there are celdas of given CT"""
#     query = "SELECT * \
#                 FROM gdlCeldas \
#                 WHERE CODE LIKE '" + str(MINECO)+"%'"
#     if ctName != None:
#         query = query + " AND NAME LIKE '" + ctName + "'"
#     queryResult = utiDataBase.selectOne(query, 'DMS')
#     if queryResult == None:
#         return False
#     return True

def isCT(MINECO,ctName=None):
    """Check if named CT is already imported"""
    query = "SELECT * \
                FROM MV_LV_SUBSTATION \
                WHERE CODE LIKE '" + str(MINECO)+"%'"
    if ctName != None:
        query = query + " AND NAME LIKE '" + ctName + "'"
    queryResult = utiDataBase.selectOne(query, 'DMS')
    if queryResult == None:
        return False
    return True

def isLine(MINECO,lineName=None):
    """Check if named line is already imported"""
    query = "SELECT * \
                FROM MV_SECTION \
                WHERE DISTRICT LIKE '" + str(MINECO)+"%' AND SUBSTATION IS NULL"
    if lineName != None:
        query = query + " AND NAME LIKE '" + lineName + "'"
    queryResult = utiDataBase.selectOne(query, 'DMS')
    if queryResult == None:
        return False
    return True
                

def areThereTrafos(MINECO):
    """Check if there are trafos of given company already imported"""
    query="SELECT * \
            FROM TRANSFORMER \
            WHERE CODE LIKE '" + str(MINECO) +"%'"
    queryResult = utiDataBase.selectOne(query, 'DMS')
    if queryResult == None:
        return False
    return True

def splitSection(dmsCode,MV_Section_Id,CodigoGDL,RelayNum = 0):
    """Split a line in 2. If relay Num isn't given, is assumed that no Relays are placed between the splitting point and the begining of the line"""
    error = 0
    #Collect needed info
    query = "SELECT NODE1, NODE2, VOLTAGE_LEVEL, CONDUCTOR, NAME, DISTRICT FROM MV_SECTION WHERE MV_Section_Id = '"+str(MV_Section_Id)+"'"
    result = utiDataBase.selectOne(query,'DMS')
    if result == None:
        errorMsg = "ERROR. Splitting line. MV_Section_Id: "+MV_Section_Id+".GDLCode: "+CodigoGDL+". SQL Error."
        errorBox = "Error inesperado. Fallo al separar las secciones de la l�nea. Error de SQL. La importaci�n puede no ser correcta." 
        eh.handleError(errorMsg,errorBox)
        return -1
    
    #FIRST SECTION
    #Change ending point of first section points and calculate new distance
    query = "SELECT CodigoGDL, X, Y FROM MV_SECTIONPOINT WHERE MV_Section_Id = '"+str(MV_Section_Id)+"' AND NUMBER < '"+str(RelayNum)+"' ORDER BY NUMBER"
    resultPoints = utiDataBase.selectAll(query,'DMS')
    if resultPoints == None:
        errorMsg = "ERROR. Splitting line. MV_Section_Id: "+str(MV_Section_Id)+".GDLCode: "+CodigoGDL+". SQL Error."
        errorBox = "Error inesperado. Fallo al separar las secciones de la l�nea. Error de SQL. La importaci�n puede no ser correcta." 
        eh.handleError(errorMsg,errorBox)
        return -1

    query = ''
    long = 0

    x0, y0 = uC.getCoordinates(result.NODE1)
    x0 = int(str(x0)[:6])
    y0 = int(str(y0)[:6])

    for point in resultPoints:
        query = query + """UPDATE MV_SECTIONPOINT SET NODE2 = '"""+dmsCode+"""' WHERE CodigoGDL = '"""+point.CodigoGDL+"""'
        """
        x1,y1 = point.X,point.Y
        long += calcLong(x0, y0, x1, y1)
        x0,y0 = x1,y1
    if utiDataBase.execute(query,'DMS') < 0:
        error = 1
        
    #last section length
    x1,y1 = uC.getCoordinates(dmsCode)
    long += calcLong(x0, y0, x1, y1)
    
    #Change ending point of first section and updating new distance
    query = "UPDATE MV_SECTION SET NODE2 = '" + dmsCode + "', LENGTH='"+str(long)+"' WHERE MV_Section_Id = '" +  str(MV_Section_Id) + "'"
    if utiDataBase.execute(query,'DMS') < 0:
        error = 1
        
    #SECOND SECTION
    #Change sectionpoints of old section to second section and calculate new distance
    mvSectionID = utiDMS.getMVSectionID()
    query = "SELECT CodigoGDL, X, Y FROM MV_SECTIONPOINT WHERE MV_Section_Id = '"+str(MV_Section_Id)+"' AND NUMBER > '"+str(RelayNum)+"' ORDER BY NUMBER"
    resultPoints = utiDataBase.selectAll(query,'DMS')
    if resultPoints == None:
        errorMsg = "ERROR. Splitting line. MV_Section_Id: "+str(MV_Section_Id)+".GDLCode: "+CodigoGDL+". SQL Error."
        errorBox = "Error inesperado. Fallo al separar las secciones de la l�nea. Error de SQL. La importaci�n puede no ser correcta." 
        eh.handleError(errorMsg,errorBox)
        return -1
    
    query = ''
    i = 0
    long = 0

    x0, y0 = uC.getCoordinates(dmsCode)
    x0 = int(str(x0)[:6])
    y0 = int(str(y0)[:6])

    for point in resultPoints:
        i += 1
        query = query + """UPDATE MV_SECTIONPOINT SET NUMBER = '"""+str(i)+"""', NODE1 = '"""+dmsCode+"""',
        MV_Section_Id = '"""+str(mvSectionID)+"""' WHERE CodigoGDL = '"""+point.CodigoGDL+"""'
        """
        x1,y1 = point.X,point.Y
        long += calcLong(x0, y0, x1, y1)
        x0,y0 = x1,y1
    if utiDataBase.execute(query,'DMS') < 0:
        error = 1
    
    #last conection
    x1, y1 = uC.getCoordinates(result.NODE2)
    x1 = int(str(x0)[:6])
    y1 = int(str(y0)[:6])
    long += calcLong(x0, y0, x1, y1)
    
    #Adding second section with new distance
    Xt,Yt = uC.getCoordinates(dmsCode)
    if utiDMS.insertMVSection(mvSectionID, dmsCode, result.NODE2, result.CONDUCTOR, result.DISTRICT, result.VOLTAGE_LEVEL, Xt, Yt, LENGTH = str(long), NAME = result.NAME) < 0:
        error = 1
        
    #Checking for errors
    if error == 1:
        errorMsg = "ERROR. Splitting line. MV_Section_Id: "+MV_Section_Id+".GDLCode: "+CodigoGDL+". SQL Error."
        errorBox = "Error inesperado. Fallo al separar las secciones de la l�nea. Error de SQL. La importaci�n puede no ser correcta." 
        eh.handleError(errorMsg,errorBox)
        return -1
    return 0
    
 

def nodeBelongToLine(endCode,IdLinea):
    """Check if given code belongs to given line"""
    query = "SELECT CODE,CodigoGDL FROM MV_NODE WHERE CODE = '" + endCode + "'"
    result = utiDataBase.selectOne(query,'DMS')
    if result == None:
        return -1
    elif result.CodigoGDL == None:
        return False
    else:
        idLineaFound = result.CodigoGDL.split('-')[0][1:]
        if idLineaFound == IdLinea:
            return True
    return False
        
def formatCTCode(code,MINECO):
    """In case traza code does not have the used format we format it"""
    if len(code) < 3 or code[:3] != MINECO:
        code = MINECO + '-' + code
    elif code[3] != '-':
        code = MINECO + '-' + code[3:]
    return code

def formatPosition(i):
    pos = str(i)
    if len(pos) < 2:
        pos = '0' + pos
    return pos

def separateSectionPoints(MINECO):
    query = "WHILE EXISTS (\
                            SELECT TOP 1 sp1.MV_Section_Id\
                            FROM MV_SECTIONPOINT AS sp1 JOIN MV_SECTIONPOINT AS sp2\
                                            ON sp1.X = sp2.X AND\
                                                 sp1.Y = sp2.Y AND\
                                                 sp1.MV_Section_Id < sp2.MV_Section_Id\
                            WHERE sp1.gdlMINECO = '" + MINECO + "'\
                            )\
                    BEGIN\
                        UPDATE MV_SECTIONPOINT SET \
                        X = X+1000\
                        WHERE MV_Section_Id IN\
                        (\
                        SELECT TOP 1 sp1.MV_Section_Id\
                        FROM MV_SECTIONPOINT AS sp1 JOIN MV_SECTIONPOINT AS sp2\
                                        ON sp1.X = sp2.X AND\
                                             sp1.Y = sp2.Y AND\
                                             sp1.MV_Section_Id < sp2.MV_Section_Id\
                        )\
                    END"
    return utiDataBase.execute(query,'DMS')

def separateLineNodes(MINECO):
    query = """    WHILE EXISTS ( SELECT TOP 1 n1.CODE
                            FROM MV_NODE AS n1 JOIN MV_NODE AS n2
                                            ON n1.X = n2.X AND
                                                 n1.Y = n2.Y AND
                                                 RIGHT(n1.CODE,1) < RIGHT(n2.CODE,1)
                            WHERE n1.gdlMINECO = '101' AND n1.CodigoGDL NOT LIKE 'C%' AND n2.CodigoGDL NOT LIKE 'C%'
                            )
                    BEGIN
                        UPDATE MV_NODE SET 
                        X = X+1000
                        WHERE CODE IN
                        (
                        SELECT TOP 1 n1.CODE
                        FROM MV_NODE AS n1 JOIN MV_NODE AS n2
                                        ON n1.X = n2.X AND
                                             n1.Y = n2.Y AND
                                             RIGHT(n1.CODE,1) < RIGHT(n2.CODE,1)
                        WHERE n1.gdlMINECO = '101' AND n1.CodigoGDL NOT LIKE 'C%' AND n1.CodigoGDL NOT LIKE 'P%'
                                                AND n2.CodigoGDL NOT LIKE 'C%' AND n2.CodigoGDL NOT LIKE 'P%'
                        )
                    END"""
    return utiDataBase.execute(query,'DMS')
         

def calcLong(x0, y0, x1, y1): 
    """Returns the distance between two given points. Coordinates of given point must be in DMS format"""
    x0 = int(str(x0)[:6])
    x1 = int(str(x1)[:6])
    y0 = int(str(y0)[:6])
    y1 = int(str(y1)[:6])
            
    return int(sqrt(((x1-x0)**2)+((y1-y0)**2)))

 

# 
# from db import DataBase
# import utiDMS
# import logging
# log = logging.getLogger('utiGedLux')
# 
# def getSUBDInterna():
#     return sentenciaSelectBDInterna("SELECT ID FROM internaMINECO where isSU='1'")
# 
# def selectTrazaDms(sentencia,BD):
#    TRAZA='T'
#    DMS='D'
#    INTERNA='I'
#    
#    try:
#      print str(BD)
#      print str(DMS)
#      print str(BD==DMS)
#      if BD==DMS:
#          cnxn=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
#      elif BD==TRAZA:
#          cnxn=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable,DataBase.TRAZAUser,DataBase.TRAZAPassword)
#      elif BD==INTERNA:
#          cnxn=utiDMS.connectSQL("192.168.1.15\DMSEXPRESS", "DBINTERNAMG", "sa", "D11382r")
#      else:
#          raise 
#      
#      cursor = cnxn.cursor()
#      cursor.execute(sentencia)
#      if "select" in sentencia.lower():
#      #if sentencia.lower().startswith('select'):
#          rows=cursor.fetchall()
#          return rows
#      else:
#          cnxn.commit()
#          return 'commit'
#      
#    except:
#      log.info("Error en selectDMS")
#      return None
# 
# def sentenciaDeleteDms(sentencia):
#      return selectTrazaDms(sentencia,'D')
#      
# def sentenciaInsertDms(sentencia):
#      return selectTrazaDms(sentencia,'D')
#      
# def sentenciaDeleteBDInterna(sentencia):
#      return selectTrazaDms(sentencia,'I')
#      
# def sentenciaInsertBDInterna(sentencia):
#      return selectTrazaDms(sentencia,'I')
#     
# def sentenciaSelectBDInterna(sentencia):
#      return selectTrazaDms(sentencia,'I')
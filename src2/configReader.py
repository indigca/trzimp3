# -*- coding: iso-8859-15 -*-
'''
Created on 03/12/2013
@author: ALP
'''

import ConfigParser
import base64
import os
import exceptionHandler as eh

class ConfigReader:
    """To retrieve all fields from the config file"""

    def __init__(self):
        """Set the config parameters on the object"""
        #Create the parser
        self.cParser = ConfigParser.RawConfigParser()
        if os.path.exists('config.ini'):
            self.cParser.read('config.ini')
        else:
            errorMsg = "ERROR. Config file not found"
            errorBox = "No se encontr� el fichero de configuraci�n config.ini"
            eh.handleError(errorMsg,errorBox)
            self.cParser = -1
    
    def getDMSdata(self):
        """Returns the configuration parameters for connecting to the DMS database in SQL
        (Returned Values: Address, User, Password, Table"""
        if self.cParser == -1:
            return -1,-1,-1,-1
        try:
            address = self.cParser.get('DMS_DATABASE','Address')
            user = self.cParser.get('DMS_DATABASE','User')
            password = base64.b64decode(self.cParser.get('DMS_DATABASE','Password'))
            table = self.cParser.get('DMS_DATABASE','DMSTable')
            return address, user, password, table
        except Exception as e:
            errorMsg="ERROR. Error in config file definition (DMS)"
            errorBox="El fichero de configuraci�n no est� definido correctamente en la secci�n del DMS"
            returnValue = -1,-1,-1,-1
            return eh.handleException(e,errorMsg,errorBox,returnValue)


    def getTRAZAdata(self):
        """Returns the configuration parameters for connecting to the Traza database in SQL
        (Returned Values: Address, User, Password, Table"""
        if self.cParser == -1:
            return -1,-1,-1,-1
        try:
            if self.getDevelopMode():
                address = self.cParser.get('TRAZA_DATABASE','AddressDevelop')
            else:
                address = self.cParser.get('TRAZA_DATABASE','Address')
            user = self.cParser.get('TRAZA_DATABASE','User')
            password = base64.b64decode(self.cParser.get('TRAZA_DATABASE','Password'))
            table = self.cParser.get('TRAZA_DATABASE','TRAZATable')
            return address, user, password, table
        except Exception as e:
            errorMsg="ERROR. Error in config file definition (Traza)"    
            errorBox="El fichero de configuraci�n no est� definido correctamente en la secci�n del Traza"
            returnValue = -1,-1,-1,-1
            return eh.handleException(e,errorMsg,errorBox,returnValue)
        
    def getDMS2data(self):
        """Returns the configuration parameters for connecting to another DMS database in SQL.
        (Returned Values: Address, User, Password, Table"""
        if self.cParser == -1:
            return -1,-1,-1,-1
        try:
            address = self.cParser.get('DMS2_DATABASE','Address')
            user = self.cParser.get('DMS2_DATABASE','User')
            password = base64.b64decode(self.cParser.get('DMS2_DATABASE','Password'))
            table = self.cParser.get('DMS2_DATABASE','DMSTable')
            return address, user, password, table
        except Exception as e:
            errorMsg="ERROR. Error in config file definition (DMS2)"
            errorBox="El fichero de configuraci�n no est� definido correctamente en la secci�n del DMS2"
            returnValue = -1,-1,-1,-1
            return eh.handleException(e,errorMsg,errorBox,returnValue)
        
    def getPICdata(self):
        """Returns the configuration parameters for connecting to the PIC database in SQL
        (Returned Values: Address, User, Password, Table"""
        if self.cParser == -1:
            return -1,-1,-1,-1
        try:
            address = self.cParser.get('PIC_DATABASE','Address')
            user = self.cParser.get('PIC_DATABASE','User')
            password = base64.b64decode(self.cParser.get('PIC_DATABASE','Password'))
            table = self.cParser.get('PIC_DATABASE','PICTable')
            return address, user, password, table
        except Exception as e:
            errorMsg = "ERROR. Error in config file definition (PIC)" 
            errorBox = "El fichero de configuraci�n no est� definido correctamente en la secci�n del PIC"
            returnValue = -1,-1,-1,-1
            return eh.handleException(e,errorMsg,errorBox,returnValue)
    
    def getDevelopMode(self):
        """Returns the state of the Develope Mode checker (true or false)"""
        check = False
        try:
            if self.cParser.get('DVLOP','Check') == 'YES':
                check = True
        except Exception as e:
            errorMsg = "WARNING. No develop mode found in config file."
            eh.handleWarning(e,errorMsg)
        return check
        
#     DevelopmentMode=config.get('DVLOP','Check')
#   
#     if DevelopmentMode == 'YES' :
#         TRAZAAddress = config.get('TRAZA_DATABASE','AddressDevelop')
#         Check = config.get('TRAZA','CheckDevelop')
#     else:
#         
#         Check = config.get('TRAZA','Check')

#     
#     DMSLocation = config.get('DMS','DMSLocation')
#     if DevelopmentMode == 'YES' :
#         log = config.get('DMS','logDevelop')
#     else: 
#         log = config.get('DMS','log')
#     Zone = config.get('TRAZA','Zone')
#     Webserver = config.get('WEBSERVER','Path')
#     LogEnabled = config.get('DVLOP','LogEnabled')
#     ImportBlock=config.get('DVLOP','ImportBlock')
#     SugerenciaErrores=config.get('DVLOP','Sugerencias')
#    
    
    
'''
Created on 03/12/2013

@author: ALP
'''
import logging
import logging.handlers

#Log file name
logFileName = 'TrazaImport.log'

#Create logger with desired output level
log = logging.getLogger('MiLogger')
global log
logging.basicConfig(level=logging.DEBUG, filename=logFileName, format="%(asctime)s - %(message)s")

def writeLog(text):
    """Append information on the log"""
    log.info(text)
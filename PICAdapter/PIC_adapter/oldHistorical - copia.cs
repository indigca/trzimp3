﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DMSUtils;
using System.Xml.Serialization;
using System.Data.Odbc;
using System.IO;
using System.Security.Cryptography;
using System.Diagnostics;


namespace PIC_Adapter
{
    class Historical
    {
        PICAdapterSettings settings;
        public Historical(string settingsfile)
        {
            XmlSerializer ser = new XmlSerializer(typeof(PICAdapterSettings));
            FileStream str = File.OpenRead(settingsfile);
            settings = (PICAdapterSettings)ser.Deserialize(str);
            str.Close();
            
        }
        public void calculate()
        {
            OdbcConnection cn_riku;
            OdbcConnection cn_pic;
            OdbcConnection cn_hist;
            OdbcConnection cn_hist2;
            OdbcCommand cmd;
            string SQLSentence;   

            // Lectura Tabla riku
            SQLSentence = "SELECT * FROM SCADA_CalidadIndividual";
            cn_riku = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_riku.Open();
            cmd = new OdbcCommand(SQLSentence, cn_riku);
            OdbcDataReader readerRiku = cmd.ExecuteReader();

            // Lectura Tabla historico
            SQLSentence = "SELECT * FROM Historico";
            cn_hist = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_hist.Open();
            cmd = new OdbcCommand(SQLSentence, cn_hist);
            OdbcDataReader readerHist = cmd.ExecuteReader();

            // DELETED
            cn_pic = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_pic.Open();
            //SQLSentence = "DELETE FROM PIC";
            //cmd = new OdbcCommand(SQLSentence, cn_pic);
            //cmd.ExecuteNonQuery();
            //cn_pic.Close();
            cn_hist2 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_hist2.Open();
            deleted(readerHist, cn_riku, cn_pic, cn_hist2);
            updated(readerRiku, cn_riku, cn_pic, cn_hist2);

        }

        public void deleted(OdbcDataReader readerHist, OdbcConnection cn_riku, OdbcConnection cn_pic, OdbcConnection cn_hist)
        {
            while (readerHist.Read())
            {  
                bool isDeleted = true;
                OdbcCommand cmd;
                string SQLSentence;
                SQLSentence = "SELECT * FROM SCADA_CalidadIndividual";
                cn_riku = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                cn_riku.Open();
                cmd = new OdbcCommand(SQLSentence, cn_riku);
                OdbcDataReader readerRiku = cmd.ExecuteReader();
                if (readerHist.GetInt64(10) == 3)
                {
                    isDeleted = false;
                }
                else
                {
                    while (readerRiku.Read())
                    {
                        String CUPSriku = readerRiku.GetString(1);
                        String CUPShist = readerHist.GetString(1);
                        Guid IDriku = readerRiku.GetGuid(3);
                        Guid IDhist = readerHist.GetGuid(3);

                        if (CUPSriku == CUPShist &
                            IDriku == IDhist)
                        {
                            isDeleted = false;
                            break;
                        }
                    }
                }
                if (isDeleted)
                {
                    InsertInto(readerHist, cn_pic, "PIC_CalidadIndividual", "3");
                    Update(readerHist, cn_hist, "Historico", "3");
                }
            }
        }

        public void updated(OdbcDataReader readerRiku, OdbcConnection cn_riku, OdbcConnection cn_pic, OdbcConnection cn_hist)
        {
            OdbcCommand cmd;
            string SQLSentence;
            
            SQLSentence = "SELECT * FROM Historico";
            cn_hist = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_hist.Open();
            cmd = new OdbcCommand(SQLSentence, cn_hist);
            OdbcDataReader readerHist = cmd.ExecuteReader();
            while (readerRiku.Read())
            {                
                bool isUpdated = false;
                bool isInserted = true;
                if (!readerHist.HasRows)
                {
                    InsertInto(readerRiku, cn_pic, "PIC_CalidadIndividual", "1");
                    InsertInto(readerRiku, cn_hist, "Historico", "1");
                }
                else
                {
                    SQLSentence = "SELECT * FROM Historico";
                    cn_hist = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    cn_hist.Open();
                    OdbcConnection cn_hist2;
                    cn_hist2 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    cn_hist2.Open();
                    OdbcConnection cn_hist3;
                    cn_hist3 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    cn_hist3.Open();
                    OdbcConnection cn_hist4;
                    cn_hist4 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    cn_hist4.Open();
                    OdbcConnection cn_hist5;
                    cn_hist5 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    cn_hist5.Open();
                    cmd = new OdbcCommand(SQLSentence, cn_hist);
                    OdbcDataReader readerHist2 = cmd.ExecuteReader();
                    Guid IdModificada = new Guid();
                    while (readerHist2.Read())
                    {
                        
                        String CUPSriku = readerRiku.GetString(1);
                        String CUPShist = readerHist2.GetString(1);
                        Guid IDriku = readerRiku.GetGuid(3);
                        Guid IDhist = readerHist2.GetGuid(3);
                        Guid IDhist2;
                        DateTime Dateriku = readerRiku.GetDateTime(4);
                        DateTime Datehist = readerHist2.GetDateTime(4);
                        Int32 Duracionriku = readerRiku.GetInt32(5);
                        Int32 Duracionhist = readerHist2.GetInt32(5);
                        Int64 Tipohist = readerHist2.GetInt64(10);

                        if (CUPSriku == CUPShist &
                            IDriku != IDhist &
                            Dateriku == Datehist &
                            Tipohist == 3)
                        {
                            string SQLSentence2 = "SELECT * FROM Historico where SCCI_CUPS = '" + CUPShist + "' AND SCCI_FechaInterrupcion = '" + Datehist + "' AND SCCI_Tipo = 2";
                            cmd = new OdbcCommand(SQLSentence2, cn_hist4);
                            OdbcDataReader readerHist3 = cmd.ExecuteReader();
                            if (!readerHist3.HasRows)
                            {
                                string SQLSentence3 = "select TOP 1 SCCI_IdInterrupcion from Historico where SCCI_CUPS = '" + CUPShist + "' ORDER BY SCCI_IdCalidadIndividual DESC";
                                
                                cmd = new OdbcCommand(SQLSentence3, cn_hist5);
                                OdbcDataReader readerHist5 = cmd.ExecuteReader();
                                if (readerHist5.HasRows)
                                {
                                    IDhist2 = readerHist5.GetGuid(0);
                                    IdModificada = IDhist2;
                                }
                                isUpdated = true;
                                isInserted = false;
                                break;
                            }
                        }
                        if (CUPSriku == CUPShist &
                            IDriku == IDhist)
                        {
                            isInserted = false;
                        }
                    }

                    if (isUpdated)
                    {
                        InsertInto(readerRiku, cn_pic, "PIC_CalidadIndividual", "2", IdModificada);
                        InsertInto(readerRiku, cn_hist2, "Historico", "2",IdModificada);
                    }
                    if (isInserted)
                    {
                        InsertInto(readerRiku, cn_pic, "PIC_CalidadIndividual", "1");
                        InsertInto(readerRiku, cn_hist3, "Historico", "1");
                    }
                }
            }
        }

        public void InsertInto(OdbcDataReader reader, OdbcConnection cn, string Table, string Tipo, Guid IdModificada = new Guid())
        {
            OdbcCommand cmd;
            string SQLSentence;
            SQLSentence = "Insert into " + Table + " (SCCI_CUPS,SCCI_MinEco,SCCI_IdInterrupcion,SCCI_FechaInterrupcion,SCCI_Duracion,SCCI_Origen,SCCI_Zona,SCCI_Provincia,SCCI_Municipio,SCCI_Tipo";
            if (IdModificada != new Guid())
                SQLSentence = SQLSentence + ",SCCI_IdModificada";

            SQLSentence = SQLSentence + ") Values (";
            for (int i = 1; i < 10; i++)
            {
                try
                {
                    string Value;
                    switch (i)
                    {
                        case 3:
                            Value = reader.GetGuid(i).ToString();
                            break;
                        case 4:
                            Value = reader.GetDateTime(i).ToString();
                            break;
                        case 5:
                            Value = reader.GetInt32(i).ToString();
                            break;
                        default:
                            Value = reader.GetString(i);
                            break;
                    }
                    SQLSentence = SQLSentence + "'" + Value + "'" + ',';
                }
                catch (Exception)
                {
                    SQLSentence = SQLSentence + "NULL" + ',';
                }                        
            }
            SQLSentence = SQLSentence + "'" + Tipo.ToString() + "'" + ',';
            if (IdModificada != new Guid())
            SQLSentence = SQLSentence + "'" + IdModificada.ToString() + "'" + ',';
            SQLSentence = SQLSentence.Substring(0, SQLSentence.Length - 1) +')';
            cmd = new OdbcCommand(SQLSentence, cn);
            cmd.ExecuteNonQuery();
        }

        public void Update(OdbcDataReader reader, OdbcConnection cn, string Table, string Tipo)
        {
            OdbcCommand cmd;
            string SQLSentence;
            string CUPS = reader.GetString(1);
            Int32 Duracion = reader.GetInt32(5);
            DateTime Fecha = reader.GetDateTime(4);
            SQLSentence = "UPDATE " + Table + " SET SCCI_Duracion = '" + Duracion + "' , SCCI_Tipo = " + Tipo + " WHERE SCCI_CUPS = '" + CUPS + "' AND SCCI_FechaInterrupcion = '" + Fecha + "'";
            cmd = new OdbcCommand(SQLSentence, cn);
            cmd.ExecuteNonQuery();
        }
    }
}
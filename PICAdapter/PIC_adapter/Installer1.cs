﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace PIC_Adapter
{
    [RunInstaller(true)]
    public partial class Installer1 : System.Configuration.Install.Installer
    {
        public Installer1()
        {
            InitializeComponent();
        }

        public override void Install(IDictionary savedState)
        {
            base.Install(savedState);

            if (!System.Diagnostics.EventLog.SourceExists("PIC_adapter"))
            {

                System.Diagnostics.EventLog.CreateEventSource("PIC_adapter", "DMS600");

            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DMSUtils;
using System.Xml.Serialization;
using System.Data.Odbc;
using System.IO;
using System.Security.Cryptography;
using System.Diagnostics;


namespace PIC_Adapter
{
    class Historical
    {
        PICAdapterSettings settings;
        public Historical(string settingsfile)
        {
            XmlSerializer ser = new XmlSerializer(typeof(PICAdapterSettings));
            FileStream str = File.OpenRead(settingsfile);
            settings = (PICAdapterSettings)ser.Deserialize(str);
            str.Close();
            
        }
        public void calculate()
        {


            OdbcConnection cn_dms;
            OdbcConnection cn_tempact;
            OdbcConnection cn_tempant;
            OdbcCommand cmd;
            string SQLSentence;   

            // Lectura Tabla SCADA
            SQLSentence = "SELECT * FROM SCADA_CalidadIndividual";
            cn_dms = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_dms.Open();
            cmd = new OdbcCommand(SQLSentence, cn_dms);
            OdbcDataReader readerSCADA = cmd.ExecuteReader();

            // Lectura Tabla Temp_Anterior
            SQLSentence = "SELECT * FROM Temp_Anterior";
            cn_tempant = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_tempant.Open();
            cmd = new OdbcCommand(SQLSentence, cn_tempant);
            OdbcDataReader readerTempAnt = cmd.ExecuteReader();
            cn_tempact = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_tempact.Open();


            // Escritura Tabla SCADA_CalidadIndividual2
            if (!readerTempAnt.HasRows)
            {
                while (readerSCADA.Read())
                {
                    SQLSentence = "Insert into SCADA_CalidadIndividual2 (SCCI_CUPS,SCCI_MinEco,SCCI_IdInterrupcion,SCCI_FechaInterrupcion,SCCI_Duracion,SCCI_Origen,SCCI_Zona,SCCI_Provincia,SCCI_Municipio,SCCI_Tipo) Values (";
                    for (int i = 1; i < readerSCADA.FieldCount; i++)
                    {
                        try
                        {
                            string Value;
                            if (i==4)
                            {
                                Value = readerSCADA.GetString(i).Replace("-","");
                            }
                            else
                            {
                                Value = readerSCADA.GetString(i);
                            }
                            SQLSentence = SQLSentence + "'" + Value + "'" + ',';
                        }
                        catch (Exception)
                        {
                            SQLSentence = SQLSentence + "NULL" + ',';
                        }
                        
                    }
                    SQLSentence = SQLSentence.Substring(0, SQLSentence.Length - 1) +')';
                    cmd = new OdbcCommand(SQLSentence, cn_tempact);
                    cmd.ExecuteNonQuery();
                    
                }
            }
            else
            {
                while (readerSCADA.Read())
                {
                    bool isPreviouslyInserted = false;
                    while (readerTempAnt.Read())
                    {
                        //SQLSentence = "SELECT * FROM Temp_Anterior WHERE SCCI_CUPS = " + readerSCADA.GetString(1) + " AND SCCI_IdInterrupcion =" + readerSCADA.GetString(3);
                        if (readerSCADA.GetString(1) == readerTempAnt.GetString(1) & readerSCADA.GetString(3) == readerTempAnt.GetString(3))
                        {
                            isPreviouslyInserted = true;
                // MODIFIED
                            if (readerSCADA.GetString(5) != readerTempAnt.GetString(5))
                            {
                                SQLSentence = "UPDATE SCADA_CalidadIndividual2 SET SCCI_Duracion = '" + readerSCADA.GetString(5) + "' , SCCI_Tipo = '2' WHERE SCCI_CUPS = '" + readerSCADA.GetString(1) + "' AND SCCI_IdInterrupcion = '" + readerSCADA.GetString(3) + "'";
                                cmd = new OdbcCommand(SQLSentence, cn_tempact);
                                cmd.ExecuteNonQuery();
                            }
                            break;
                        }                        
                    }

                //INSERTED
                    if (!isPreviouslyInserted)
                    {
                        SQLSentence = "Insert into SCADA_CalidadIndividual2 (SCCI_CUPS,SCCI_MinEco,SCCI_IdInterrupcion,SCCI_FechaInterrupcion,SCCI_Duracion,SCCI_Origen,SCCI_Zona,SCCI_Provincia,SCCI_Municipio,SCCI_Tipo) Values (";
                        for (int i = 1; i < readerSCADA.FieldCount; i++)
                        {
                            try
                            {
                                string Value;
                                if (i == 4)
                                {
                                    Value = readerSCADA.GetString(i).Replace("-", "");
                                }
                                else
                                {
                                    Value = readerSCADA.GetString(i);
                                }
                                SQLSentence = SQLSentence + "'" + Value + "'" + ',';
                            }
                            catch (Exception)
                            {
                                SQLSentence = SQLSentence + "NULL" + ',';
                            }
                        
                        }
                        SQLSentence = SQLSentence.Substring(0, SQLSentence.Length - 1) +')';
                        cmd = new OdbcCommand(SQLSentence, cn_tempact);
                        cmd.ExecuteNonQuery();
                    }

                }
            // DELETED
                while (readerTempAnt.Read())
                {
                    bool isDeleted = true;
                    while (readerSCADA.Read())
                    {
                        if (readerSCADA.GetString(1) == readerTempAnt.GetString(1) & readerSCADA.GetString(3) == readerTempAnt.GetString(3))
                        {
                            isDeleted = false;
                        }
                    }
                    if (isDeleted)
                    {
                        SQLSentence = "UPDATE SCADA_CalidadIndividual2 SET SCCI_Tipo = '3' WHERE SCCI_CUPS = '" + readerTempAnt.GetString(1) + "' AND SCCI_IdInterrupcion = '" + readerTempAnt.GetString(3) + "'";
                        cmd = new OdbcCommand(SQLSentence, cn_tempact);
                        cmd.ExecuteNonQuery();
                    }
                }
            };

            readerSCADA.Close();
            readerTempAnt.Close();

            // Escritura en tabla Temp_Anterior

            cn_tempant = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_tempant.Open();

            SQLSentence = "DELETE FROM Temp_Anterior";
            cmd = new OdbcCommand(SQLSentence, cn_tempact);
            cmd.ExecuteNonQuery();

            SQLSentence = "SELECT * FROM SCADA_CalidadIndividual2";
            cmd = new OdbcCommand(SQLSentence, cn_tempact);
            OdbcDataReader readerTempAct = cmd.ExecuteReader();

            while (readerTempAct.Read())
            {
                SQLSentence = "Insert into Temp_Anterior (SCCI_CUPS,SCCI_MinEco,SCCI_IdInterrupcion,SCCI_FechaInterrupcion,SCCI_Duracion,SCCI_Origen,SCCI_Zona,SCCI_Provincia,SCCI_Municipio,SCCI_Tipo) Values (";
                for (int i = 1; i < readerTempAct.FieldCount; i++)
                {
                    try
                    {
                        string Value;
                        if (i == 4)
                        {
                            Value = readerTempAct.GetString(i).Replace("-", "");
                        }
                        else
                        {
                            Value = readerTempAct.GetString(i);
                        }
                        SQLSentence = SQLSentence + "'" + Value + "'" + ',';
                    }
                    catch (InvalidCastException)
                    {
                        SQLSentence = SQLSentence + "NULL" + ',';
                    }

                }
                SQLSentence = SQLSentence.Substring(0, SQLSentence.Length - 1) + ')';
                cmd = new OdbcCommand(SQLSentence, cn_tempant);
                cmd.ExecuteNonQuery();

            }
            readerTempAct.Close();

            cn_dms.Close();
            cn_tempact.Close();


        }
    }
}

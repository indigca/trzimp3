﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DMSUtils;
using System.Xml.Serialization;
using System.Data.Odbc;
using System.IO;
using System.Security.Cryptography;
using System.Diagnostics;

namespace PIC_Adapter
{
    class PICAdapter
    {
        DbClient picdb;
        DbClient dmsdb;
        DbClient networkdb;

        PICAdapterSettings settings;

        Dictionary<CodeType, List<String>> undefinedCodes;
        Dictionary<String, DbClient> zonal_mdb;
        const String archivePlaceholder = "$$ARCH$$";

        public PICAdapter(string settingsfile)
        {
            //Gurda en settings los ajustes del xml pasado como string (en ppo PIC_Settings.xml)
            XmlSerializer ser = new XmlSerializer(typeof(PICAdapterSettings));
            FileStream str = File.OpenRead(settingsfile);
            settings = (PICAdapterSettings)ser.Deserialize(str);
            str.Close();
            
            picdb = OpenPICDb();
            dmsdb = new DbClient(DbType.DMS);
            networkdb = new DbClient(DbType.NETWORK);
            

            undefinedCodes = new Dictionary<CodeType, List<string>>();

            zonal_mdb = new Dictionary<string, DbClient>();

            prevMonth = DateTime.Now.Month - 1;
            prevYear = DateTime.Now.Year;
            if (prevMonth == 0)
            {
                prevMonth = 12;
                prevYear -= 1;
            }
        }

        private DbClient OpenMinEcoDb(String mineco)
        {
            System.IO.Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            if (!File.Exists("CalidadZonal.mdb"))
            {
                LogFile.Log("Empty quality file CalidadZonal.mdb " +
                            "does not exist! Will not export quality data",
                            EventLogEntryType.Warning);

                return null;
            }
            else
            {
                if (GDLAux.DevelopMode)
                Console.WriteLine("Estoy leyendo la BD");

                String new_file = "CalidadZonal_" + (DateTime.Now.Year - 1).ToString() +
                                  "_" + mineco + ".mdb";

                String driver = "Microsoft Access Driver (*.mdb, *.accdb)";
                if (settings.PICDatabaseSettings.AccessDatabaseDriver != null)
                {
                    driver = settings.PICDatabaseSettings.AccessDatabaseDriver.Trim();
                }

                if (File.Exists(new_file))
                {
                    File.Delete(new_file);
                }

                File.Copy("CalidadZonal.mdb", new_file);

                OdbcConnection conn = new OdbcConnection("Driver={" + driver + "};Dbq="
                                      + AppDomain.CurrentDomain.BaseDirectory + "\\" + new_file + ";");
                conn.Open();
                return new DbClient(conn);
            }
        }

        int prevMonth;
        int prevYear;

        private DbClient OpenPICDb()
        {
            if (GDLAux.DevelopMode)
            Console.WriteLine(settings.PICDatabaseSettings.ConnectionString.Substring(0,135)+"OCULTADA");
            
            OdbcConnection conn = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            conn.Open();

            if (GDLAux.DevelopMode)
            {
                Console.WriteLine("Hash= " + conn.GetHashCode().ToString());
                Console.WriteLine("State= " + conn.State.ToString());
            }

            DbClient ret = new DbClient(conn);
            return ret;
        }

        private void CheckArchiveDefinition()
        {
            if (settings.QualitySettings == null)
            {
                throw new ArgumentNullException("Setting file does not include <QualitySettings> " +
                                                "definitions! Skipping quality data export.");
            }

            if (settings.QualitySettings.DMSArchive == null)
            {
                throw new ArgumentNullException("Setting file does not include <DmsArchive> " +
                                                "definitions! Skipping quality data export.");
            }

            foreach (String arch in settings.QualitySettings.DMSArchive)
            {
                if (arch.Contains(' ') || arch.Contains('\n') || arch.Contains(';'))
                {
                    throw new ArgumentException("Forbidden character in <DmsArchive> parameter in " +
                                                "settings file: \"" + arch + "\"");
                }

                QueryParameter[] parms = { new QueryParameter(arch) };
                OdbcDataReader r = dmsdb.MakeReader("SELECT NAME FROM FaultArchive WHERE NAME = ?", parms);
                GDLAux.escribeEnTxt("SELECT NAME FROM FaultArchive WHERE NAME = "+parms.ToString());
                if (!r.Read())
                {
                    throw new ArgumentException("Incorrect <DmsArchive> parameter in settings file! " +
                                                "Please check that the archive \"" + arch + "\" exists.");
                }

                r.Close();
            }
        }

        private List<CalidadIndividual> GetIndividualQualities()
        {
            bool warnedOfCodeType = false;
            List<CalidadIndividual> q_ind = new List<CalidadIndividual>();

            MD5 md5 = MD5.Create();

            foreach (String arch in settings.QualitySettings.DMSArchive)
            {
                String q = PICAdapterQueries.CalidadIndividual.Replace(archivePlaceholder, arch);
                OdbcDataReader r = dmsdb.MakeReader(q);

                while (r.Read())
                {
                    CalidadIndividual ci = new CalidadIndividual();

                    if (r.IsDBNull(0))
                    {
                        // no customer node here, have to skip
                        continue;
                    }

                    ci.Cust = r.GetString(0);

                    if (!r.IsDBNull(1))
                    {
                        ci.MinEco = r.GetString(1);
                    }

                    // make a unique identifier using the MD5 hash of
                    // archive_name-outage_number
                    String id = arch + "-" + r.GetString(2);
                    byte[] idb = Encoding.UTF8.GetBytes(id);
                    ci.IdInterrupcion = new Guid(md5.ComputeHash(idb));

                    if (!r.IsDBNull(3))
                    {
                        ci.FechaInterrupcion = r.GetDateTime(3);
                    }

                    if (!r.IsDBNull(4))
                    {
                        ci.Duracion = (int)Math.Round(r.GetDouble(4));
                    }

                    if (!r.IsDBNull(5))
                    {
                        ci.Origen = CodeDms2Pic(CodeType.OutageReason, r.GetString(5));
                    }

                    if (!r.IsDBNull(6))
                    {
                        try
                        {
                            ci.Zona = Int32.Parse(CodeDms2Pic(CodeType.ZoneType, r.GetString(6)));
                        }
                        catch (FormatException)
                        {
                            ci.Zona = 0;

                            if (!warnedOfCodeType)
                            {
                                LogFile.Log("PIC ZoneType code '"
                                    + r.GetString(6)
                                    + "' is invalid. Must be a number.",
                                    EventLogEntryType.Warning);

                                warnedOfCodeType = true;
                            }
                        }
                    }

                    if (!r.IsDBNull(7))
                    {
                        ci.Provincia = r.GetString(7);
                    }

                    if (!r.IsDBNull(8))
                    {
                        // drop control digit if present
                        ci.Municipio = r.GetString(8).Substring(0,3);
                    }

                    if (!r.IsDBNull(9) && !r.IsDBNull(10))
                    {
                        // this is an lv report, so we must only add the customer nodes which
                        // had the outage
                        String affectedNodes = r.GetString(9);

                        String custNode = r.GetString(10);

                        if (affectedNodes.Split('#').Contains(custNode))
                        {
                            q_ind.Add(ci);
                        }
                    }
                    else
                    {
                        q_ind.Add(ci);
                    }
                }
            }

            return q_ind;
        }

        private String CodeDms2Pic(CodeType t, string c)
        {
            if (c == null)
            {
                return null;
            }

            foreach (CodeMapping m in settings.QualitySettings.CodeMapping)
            {
                if (m.CodeType == t && m.DMScode.Equals(c))
                {
                    return m.PICcode;
                }
            }

            if (undefinedCodes == null)
            {
                undefinedCodes = new Dictionary<CodeType, List<string>>();
            }

            if (!undefinedCodes.ContainsKey(t))
            {
                undefinedCodes.Add(t, new List<string>());
            }

            if (!undefinedCodes[t].Contains(c))
            {
                LogFile.Log("Undefined DMS " + t.ToString() + " code \"" +
                            c + "\". Please add it to PIC_Settings.xml",
                            EventLogEntryType.Warning);

                // to log only once
                undefinedCodes[t].Add(c);
            }
            return null;
        }

        private Municipality ReadMunicipality(OdbcDataReader r)
        {
            Municipality mun = new Municipality();
            if (r.IsDBNull(0) || r.IsDBNull(1) || r.IsDBNull(2))
            {
                LogFile.Log("Error: some CTs do not have Municipality, Province and Company defined!",
                            EventLogEntryType.Warning);
            }
            mun.MunicipalityCode = r.GetString(0);
            mun.Province = r.GetString(1);
            mun.MinEco = r.GetString(2);

            if (!r.IsDBNull(3))
            {
                // zone can be left undefined
                mun.Zona = r.GetString(3);
            }

            return mun;
        }

        private Dictionary<Municipality, CalidadZonal> GetZonalQualities()
        {
            // zone qualities
            CalidadZonal cz; // temp variable
            // initialize data structure
            Dictionary<Municipality, CalidadZonal> qualities = new Dictionary<Municipality, CalidadZonal>();
            OdbcDataReader r = dmsdb.MakeReader(PICAdapterQueries.ContractedPower);
            GDLAux.escribeEnTxt("PICAdapterQueries.ContractedPower");
            // contracted power
            while (r.Read())
            {
                Municipality mun = ReadMunicipality(r);

                cz = new CalidadZonal();
                cz.SumPotContratadasXMunicipio = (int)Math.Round(r.GetDouble(4));
                qualities.Add(mun, cz);
            }
            r.Close();

            // installed power
            r = dmsdb.MakeReader(PICAdapterQueries.InstalledPower);
            while (r.Read())
            {
                Municipality mun = ReadMunicipality(r);
                cz = qualities[mun];
                cz.PotencialInstalada = (int)Math.Round(r.GetDouble(4));
                qualities[mun] = cz;
            }
            r.Close();

            // LV cust count
            r = dmsdb.MakeReader(PICAdapterQueries.LvCustomerCount);
            while (r.Read())
            {
                Municipality mun = ReadMunicipality(r);
                cz = qualities[mun];
                cz.NumSuministroXMunicipio_BT = r.GetInt32(4);
                qualities[mun] = cz;
            }
            r.Close();

            // MV cust count
            r = dmsdb.MakeReader(PICAdapterQueries.MvCustomerCount);
            while (r.Read())
            {
                Municipality mun = ReadMunicipality(r);
                cz = qualities[mun];
                cz.NumSuministroXMunicipio_MT = r.GetInt32(4);
                qualities[mun] = cz;
            }
            r.Close();

            // get outage durations for each type
            // set time scale
            DateTime startPrev = new DateTime(DateTime.Now.Year - 1, 1, 1, 0, 0, 0);
            DateTime endPrev = new DateTime(DateTime.Now.Year, 1,
                                                 1, 0, 0, 0) - TimeSpan.FromMilliseconds(1);

            // Generation | Unforeseen
            foreach (KeyValuePair<Municipality, QualityPair> cz_this
                        in GetZonalQuality(startPrev, endPrev, "I1", 6))
            {
                cz = qualities[cz_this.Key];
                cz.ImprevistasGeneracion = cz_this.Value;
                qualities[cz_this.Key] = cz;
            }

            // Transmission | Unforeseen
            foreach (KeyValuePair<Municipality, QualityPair> cz_this
                        in GetZonalQuality(startPrev, endPrev, "I2", 6))
            {
                cz = qualities[cz_this.Key];
                cz.ImprevistasTransporte = cz_this.Value;
                qualities[cz_this.Key] = cz;
            }

            // 3rd parties | Unforeseen
            foreach (KeyValuePair<Municipality, QualityPair> cz_this
                        in GetZonalQuality(startPrev, endPrev, "I3", 6))
            {
                cz = qualities[cz_this.Key];
                cz.ImprevistasTerceros = cz_this.Value;
                qualities[cz_this.Key] = cz;
            }

            // Force majeure | Unforeseen
            foreach (KeyValuePair<Municipality, QualityPair> cz_this
                        in GetZonalQuality(startPrev, endPrev, "I4", 6))
            {
                cz = qualities[cz_this.Key];
                cz.ImprevistasFuerzaMayor = cz_this.Value;
                qualities[cz_this.Key] = cz;
            }

            // Particular | Unforeseen
            foreach (KeyValuePair<Municipality, QualityPair> cz_this
                        in GetZonalQuality(startPrev, endPrev, "I5", 6))
            {
                cz = qualities[cz_this.Key];
                cz.ImprevistasPropias = cz_this.Value; // propias == particular?
                qualities[cz_this.Key] = cz;
            }

            // programmed
            // Transmission | Programmed
            foreach (KeyValuePair<Municipality, QualityPair> cz_this
                        in GetZonalQuality(startPrev, endPrev, "P1", 7))
            {
                cz = qualities[cz_this.Key];
                cz.ProgramadasTransporte = cz_this.Value;
                qualities[cz_this.Key] = cz;
            }

            // Distribution | Programmed
            foreach (KeyValuePair<Municipality, QualityPair> cz_this
                        in GetZonalQuality(startPrev, endPrev, "P2", 7))
            {
                cz = qualities[cz_this.Key];
                cz.ProgramadasDistribucion = cz_this.Value;
                qualities[cz_this.Key] = cz;
            }

            return qualities;
        }

        private Dictionary<Municipality, QualityPair> GetZonalQuality(
                DateTime start, DateTime end, String reason, int type)
        {
            Dictionary<Municipality, QualityPair> ret = new Dictionary<Municipality, QualityPair>();
            QueryParameter[] parms = { new QueryParameter(start),
                                       new QueryParameter(end),
                                       new QueryParameter(reason),
                                       new QueryParameter(type) };

            foreach (String arch in settings.QualitySettings.DMSArchive)
            {
                String q = PICAdapterQueries.CalidadZonal.Replace(archivePlaceholder, arch);
                OdbcDataReader r = dmsdb.MakeReader(q, parms);

                while (r.Read())
                {
                    Municipality mun = ReadMunicipality(r);
                    QualityPair pair = new QualityPair();

                    pair.Horas = r.GetDouble(4);
                    pair.Num = r.GetDouble(5);

                    if (!ret.ContainsKey(mun))
                    {
                        ret.Add(mun, pair);
                    }
                    else
                    {
                        QualityPair pairold = ret[mun];
                        pairold.Horas += pair.Horas;
                        pairold.Num += pair.Num;
                        ret[mun] = pairold;
                    }
                }
            }

            return ret;
        }

        public void QualityExport()
        {
            // run Calidad Zonal & Calidad Individual on dmsdb
            // first check that archive names are correct
            CheckArchiveDefinition();

            // run quality queries and store data to PIC
            try
            {
                List <CalidadIndividual> ind = GetIndividualQualities();
                PutIndividualQualities(ind);
            }
            catch (Exception e)
            {
                LogFile.Log("Error exporting individual quality data: " + e.Message,
                            EventLogEntryType.Warning);
            }

            // create access database for government reports
            try
            {
                Dictionary<Municipality, CalidadZonal> zon = GetZonalQualities();

                foreach (KeyValuePair<Municipality, CalidadZonal> z in zon)
                {
                    DbClient mdb = null;

                    if (zonal_mdb.ContainsKey(z.Key.MinEco))
                    {
                        mdb = zonal_mdb[z.Key.MinEco];
                    }
                    else
                    {
                        mdb = OpenMinEcoDb(z.Key.MinEco);
                        zonal_mdb.Add(z.Key.MinEco, mdb);
                    }

                    int INE = GetIne(mdb, z.Key.Province, z.Key.MunicipalityCode);
                    if (INE == 0)
                    {
                        continue;
                    }

                    QueryParameter[] p = { new QueryParameter(INE, "Código INE"),
                                           new QueryParameter(z.Key.Zona, "Zona"),
                                           new QueryParameter(z.Value.NumSuministroXMunicipio_AT +
                                                              z.Value.NumSuministroXMunicipio_BT +
                                                              z.Value.NumSuministroXMunicipio_MT, "NºSuministros"),
                                           new QueryParameter(z.Value.PotencialInstalada, "Potencia Instalada (MVA)"),
                                           new QueryParameter(z.Value.ProgramadasTransporte.Horas, "Horas Programadas Transporte"),
                                           new QueryParameter(z.Value.ProgramadasDistribucion.Horas, "Horas Programadas Distribución"),
                                           new QueryParameter(z.Value.ImprevistasGeneracion.Horas, "Horas Imprevistas Generación"),
                                           new QueryParameter(z.Value.ImprevistasTransporte.Horas, "Horas Imprevistas Transporte"),
                                           new QueryParameter(z.Value.ImprevistasTerceros.Horas, "Horas Imprevistas Terceros"),
                                           new QueryParameter(z.Value.ImprevistasFuerzaMayor.Horas, "Horas Imprevistas Fuerza Mayor"),
                                           new QueryParameter(z.Value.ImprevistasPropias.Horas, "Horas Imprevistas Propias"),
                                           new QueryParameter(z.Value.ProgramadasTransporte.Num, "Número Programadas Transporte"),
                                           new QueryParameter(z.Value.ProgramadasDistribucion.Num, "Número Programadas Distribución"),
                                           new QueryParameter(z.Value.ImprevistasGeneracion.Num, "Número Imprevistas Generación"),
                                           new QueryParameter(z.Value.ImprevistasTransporte.Num, "Número Imprevistas Transporte"),
                                           new QueryParameter(z.Value.ImprevistasTerceros.Num, "Número Imprevistas Terceros"),
                                           new QueryParameter(z.Value.ImprevistasFuerzaMayor.Num, "Número Imprevistas Fuerza Mayor"),
                                           new QueryParameter(z.Value.ImprevistasPropias.Num, "Número Imprevistas Propias"),
                                           new QueryParameter(z.Value.ImprevistasBT.Num, "Número Imprevistas BT"),
                                           new QueryParameter(z.Value.ProgramadasBT.Num, "Número ProgramadasBT") };

                    mdb.Insert("Calidad", p);
                }
            }
            catch (Exception e)
            {
                LogFile.Log("Error exporting zone quality data: " + e.Message,
                            EventLogEntryType.Warning);
            }
        }

        private int GetIne(DbClient mdb, String provincia, String municipio)
        {
            String[] p = { provincia, municipio.Substring(0, 3) };
            OdbcDataReader r = mdb.MakeReader("SELECT [Código INE] FROM MUNICIPIOS " +
                                              "WHERE PROVINCIA = ? AND MUNICIPIO = ?", p);
            GDLAux.escribeEnTxt("SELECT [Código INE] FROM MUNICIPIOS " +
                                              "WHERE PROVINCIA = "+p.ToString()+" AND MUNICIPIO = "+p.ToString());
            if (r.Read())
            {
                int ret = r.GetInt32(0);
                r.Close();
                return ret;
            }
            else
            {
                LogFile.Log("Cannot get INE code from mdb file for " + provincia + "/" + municipio,
                            EventLogEntryType.Warning);
                return 0;
            }
        }

        private void PutIndividualQualities(List<CalidadIndividual> ind)
        {
            // make it empty, as we create all faults again
            picdb.Exec("TRUNCATE TABLE SCADA_CalidadIndividual");

            foreach (CalidadIndividual ci in ind)
            {      
                try
                {
                    String cups = ci.Cust;
                    if (ci.Cust.Length > 22)
                    {
                        cups = ci.Cust.Substring(0, 22);
                    }
                    QueryParameter[] p = { new QueryParameter(cups, "SCCI_CUPS"),
                                           new QueryParameter(ci.MinEco, "SCCI_MinEco"),
                                           new QueryParameter(ci.IdInterrupcion, "SCCI_IdInterrupcion"),
                                           new QueryParameter(ci.FechaInterrupcion, "SCCI_FechaInterrupcion"),
                                           new QueryParameter(ci.Duracion, "SCCI_Duracion"),
                                           new QueryParameter(ci.Origen, "SCCI_Origen"),
                                           new QueryParameter(ci.Zona, "SCCI_Zona"),
                                           new QueryParameter(ci.Provincia, "SCCI_Provincia"),
                                           new QueryParameter(ci.Municipio, "SCCI_Municipio"),
                                           new QueryParameter(1, "SCCI_Tipo")
                                           // WARNING! Tipo field is not yet handled correctly
                                      };
                    picdb.Insert("SCADA_CalidadIndividual", p);
                }
                catch (Exception e)
                {
                    LogFile.Log("Error exporting outage " + ci.IdInterrupcion
                                + " for CUPS '" + ci.Cust + "'"
                                + ": " + Environment.NewLine + e.ToString(),
                                EventLogEntryType.Warning);
                }
            }
        }

        public void CustomerImport()
        {
            // load the new contracted powers from PIC

            // this SQL is defined in GetCustomerData.sql, which is further made into a
            // const string in the class PICAdapterQueries by Sql2Cs.exe
            OdbcDataReader r = picdb.MakeReader(PICAdapterQueries.GetCustomerData);
            GDLAux.escribeEnTxt("PICAdapterQueries.GetCustomerData");
            Dictionary<String, double> powers = new Dictionary<string, double>();

            while (r.Read())
            {
                powers.Add(r.GetString(0), r.GetDouble(1));
            }
            r.Close();

            int n_updates = 0;
            int n_fails = 0;
            // update the customers in DMS database
            foreach (KeyValuePair<String, double> p in powers)
            {
                // check if customer exists, for logging
                r = dmsdb.MakeReader("SELECT CUSTOMER_NODE FROM CUSTOMER WHERE CODE = ?",
                                      p.Key);
                if (!r.Read())
                {
                    LogFile.Log("Customer " + p.Key + " missing from DMS600",
                                System.Diagnostics.EventLogEntryType.Warning);
                    n_fails++;
                }
                else
                {
                    if (r.IsDBNull(0))
                    {
                        LogFile.Log("Customer " + p.Key + " not connected to a usage point",
                                    EventLogEntryType.Warning);
                    }
                    n_updates++;
                }
                r.Close();


                // add a row or update
                // prepare parameters
                QueryParameter[] cust_code = { new QueryParameter(p.Key, "CODE") };
                QueryParameter[] values = { new QueryParameter(p.Value, "CONTRACTED_POWER"),
                                           new QueryParameter(DateTime.Now, "UPDATE_DATE") };

                dmsdb.Upsert("CUSTOMER", cust_code, values);
            }

            // log the success            
            if (n_updates > 0)
            {
                LogFile.Log("Successfully updated " + n_updates.ToString() + " customers.",
                            EventLogEntryType.Warning);
            }
            else
            {
                LogFile.Log("No customers updated.",
                            EventLogEntryType.Information);
            }

            if (n_fails > 0)
            {
                LogFile.Log(n_fails.ToString() + " customer records missing from DMS600.",
                            System.Diagnostics.EventLogEntryType.Warning);
            }
        }
    }
}

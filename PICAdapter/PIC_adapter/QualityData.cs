﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PIC_Adapter
{
    struct CalidadIndividual
    {
        public String Cust;
        public String MinEco;
        public Guid IdInterrupcion;
        public DateTime FechaInterrupcion;
        public int Duracion;
        public String Origen;
        public int? Zona;
        public String Provincia;
        public String Municipio;
    }

    struct QualityPair
    {
        public double Num;
        public double Horas;
    }

    // the PIC table should include column "company"
    struct CalidadZonal
    {
        public int NumSuministroXMunicipio_BT;
        public int NumSuministroXMunicipio_MT;
        public int NumSuministroXMunicipio_AT;

        public int SumPotContratadasXMunicipio;
        public int PotencialInstalada;
        
        // programmed
        public QualityPair ProgramadasTransporte;
        public QualityPair ProgramadasDistribucion;
        
        // unforeseen
        public QualityPair ImprevistasGeneracion;
        public QualityPair ImprevistasTransporte;
        public QualityPair ImprevistasTerceros;
        public QualityPair ImprevistasFuerzaMayor;
        public QualityPair ImprevistasPropias;
        
        // LV
        public QualityPair ImprevistasBT;
        public QualityPair ProgramadasBT;

        // time
        public int AnioPeriodoCalculado;
        public int MesPeriodoCalculado;
    }

    class Municipality : IEquatable<Municipality>
    {
        public Municipality(String municipality, String province, String minEco, String zona)
        {
            MunicipalityCode = municipality;
            Province = province;
            MinEco = minEco;
            Zona = zona;
        }

        public Municipality()
        {
        }
        
        public String MunicipalityCode;
        public String Province;
        public String MinEco;
        public String Zona;

        public bool Equals(Municipality other)
        {
            if (other.MunicipalityCode.Equals(MunicipalityCode) && other.Province.Equals(Province) &&
                other.MinEco.Equals(MinEco) &&
                ((Zona == null && other.Zona == null) || ((Zona != null && other.Zona != null) && Zona.Equals(other.Zona))))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return (MunicipalityCode + Province + MinEco + Zona).GetHashCode();
        }
    }
}

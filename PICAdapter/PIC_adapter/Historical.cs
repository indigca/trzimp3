﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DMSUtils;
using System.Xml.Serialization;
using System.Data.Odbc;
using System.IO;
using System.Security.Cryptography;
using System.Diagnostics;


namespace PIC_Adapter
{
    class Historical
    {
        
        PICAdapterSettings settings;
        public Historical(string settingsfile)
        {
            XmlSerializer ser = new XmlSerializer(typeof(PICAdapterSettings));
            FileStream str = File.OpenRead(settingsfile);
            settings = (PICAdapterSettings)ser.Deserialize(str);
            str.Close();
            
        }
        public void calculate()
        {
            OdbcConnection cn_riku1;
            OdbcConnection cn_riku2;
            //OdbcConnection cn_pic;
            OdbcConnection cn_hist1;
            OdbcConnection cn_hist2;
            OdbcCommand cmd;
            string SQLSentence;   

            // Lectura Tabla riku
            SQLSentence = "SELECT * FROM SCADA_CalidadIndividual";
            GDLAux.escribeEnTxt(SQLSentence);
            cn_riku1 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_riku1.Open();
            cmd = new OdbcCommand(SQLSentence, cn_riku1);
            OdbcDataReader readerRiku1 = cmd.ExecuteReader();

            // Lectura Tabla historico
            SQLSentence = "SELECT * FROM Historico";
            GDLAux.escribeEnTxt(SQLSentence);
            cn_hist1 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_hist1.Open();
            cmd = new OdbcCommand(SQLSentence, cn_hist1);
            OdbcDataReader readerHist1 = cmd.ExecuteReader();

            // DELETED
            //cn_pic = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            //cn_pic.Open();
            //SQLSentence = "DELETE FROM PIC";
            //cmd = new OdbcCommand(SQLSentence, cn_pic);
            //cmd.ExecuteNonQuery();
            //cn_pic.Close();
            //cn_hist2 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            //cn_hist2.Open();
            updated(readerHist1, readerRiku1);
            cn_riku1.Close();
            cn_hist1.Close();

            // Lectura Tabla riku
            SQLSentence = "SELECT * FROM SCADA_CalidadIndividual";
            GDLAux.escribeEnTxt(SQLSentence);
            cn_riku2 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_riku2.Open();
            cmd = new OdbcCommand(SQLSentence, cn_riku2);
            OdbcDataReader readerRiku2 = cmd.ExecuteReader();

            // Lectura Tabla historico
            SQLSentence = "SELECT * FROM Historico";
            GDLAux.escribeEnTxt(SQLSentence);
            cn_hist2 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn_hist2.Open();
            cmd = new OdbcCommand(SQLSentence, cn_hist2);
            OdbcDataReader readerHist2 = cmd.ExecuteReader();
            deleted(readerHist2, readerRiku2);
            cn_riku2.Close();
            cn_hist2.Close();

        }


        public void deleted(OdbcDataReader readerHist, OdbcDataReader readerRiku)
        {
            while (readerHist.Read())
            {
                String CUPShist = readerHist.GetString(1);
                Guid IDhist = readerHist.GetGuid(3);
                DateTime Datehist = readerHist.GetDateTime(4);
                Int32 Duracionhist = readerHist.GetInt32(5);
                Int32 Tipohist = readerHist.GetInt32(10);
                
                Guid IdModificada = new Guid();
                try
                {
                    Guid IdModificadaHist = readerHist.GetGuid(11);
                    IdModificada = IdModificadaHist;
                }
                catch (Exception)
                {
                    
                }

                string SQLSentence = "SELECT top 1 * FROM SCADA_CalidadIndividual where SCCI_CUPS = '" + CUPShist + "' AND SCCI_IdInterrupcion ='" + IDhist + "'";
                GDLAux.escribeEnTxt(SQLSentence);
                if (GDLAux.DevelopMode)
                Console.WriteLine(SQLSentence);
                OdbcConnection cn = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                cn.Open();
                OdbcCommand cmd = new OdbcCommand(SQLSentence, cn);
                OdbcDataReader reader = cmd.ExecuteReader();
                if (!reader.HasRows)
                {

                    if (Tipohist != 3)
                    {
                        Update(readerHist, "Historico", "3");
                        InsertInto(readerHist, "PIC_CalidadIndividual", "3", IdModificada);                                                                     
                    }
                }
                cn.Close();
            }
        }

        public void deleted2(OdbcDataReader readerHist, OdbcDataReader readerRiku)
        {
            while (readerHist.Read())
            {  
                bool isDeleted = true;
                //OdbcCommand cmd;
                //string SQLSentence;
                //SQLSentence = "SELECT * FROM SCADA_CalidadIndividual";
                //cn_riku = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                //cn_riku.Open();
                //cmd = new OdbcCommand(SQLSentence, cn_riku);
                //OdbcDataReader readerRiku = cmd.ExecuteReader();
                if (readerHist.GetInt64(10) == 3)
                {
                    isDeleted = false;
                }
                else
                {
                    while (readerRiku.Read())
                    {
                        String CUPSriku = readerRiku.GetString(1);
                        String CUPShist = readerHist.GetString(1);
                        Guid IDriku = readerRiku.GetGuid(3);
                        Guid IDhist = readerHist.GetGuid(3);

                        if (CUPSriku == CUPShist &
                            IDriku == IDhist)
                        {
                            isDeleted = false;
                            break;
                        }
                    }
                }
                if (isDeleted)
                {
                    InsertInto(readerHist, "PIC_CalidadIndividual", "3");
                    Update(readerHist, "Historico", "3");
                }
            }
        }

        public void updated(OdbcDataReader readerHist, OdbcDataReader readerRiku)
        {
            while (readerRiku.Read())
            {
                String CUPSriku = readerRiku.GetString(1);
                GDLAux.escribeEnTxt("readerRiku.GetGuid(3)");
                Guid IDriku = readerRiku.GetGuid(3);
                DateTime Dateriku = readerRiku.GetDateTime(4);
                Int32 Duracionriku = readerRiku.GetInt32(5);
                //si en el histórico no hay registros
                if (!readerHist.HasRows)
                {
                    InsertInto(readerRiku, "PIC_CalidadIndividual", "1");
                    InsertInto(readerRiku, "Historico", "1");
                }
                //si en el histórico sí hay registros
                else
                {

                    string SQLSentence = "SELECT top 1 * FROM Historico where SCCI_CUPS = '" + CUPSriku + "' AND SCCI_FechaInterrupcion = '" + Dateriku + "' order by SCCI_IdCalidadIndividual DESC";
                    GDLAux.escribeEnTxt(SQLSentence);
                    OdbcConnection cn = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    cn.Open();
                    OdbcCommand cmd = new OdbcCommand(SQLSentence, cn);
                    OdbcDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                    {
                        // No hay interrupciones iguales
                        InsertInto(readerRiku, "PIC_CalidadIndividual", "1");
                        InsertInto(readerRiku, "Historico", "1");
                    }
                    else
                    {
                        String CUPShist = reader.GetString(1);
                        GDLAux.escribeEnTxt(CUPShist);
                        Guid IDhist = reader.GetGuid(3);                        
                        DateTime Datehist = reader.GetDateTime(4);
                        Int32 Duracionhist = reader.GetInt32(5);
                        if (IDriku != IDhist)
                        {
                            // Hay interrupciones iguales
                            Int64 Tipohist = 0;
                            Guid IdModificada = new Guid();
                            while (reader.Read())
                            {
                                Tipohist = reader.GetInt64(10);
                                IdModificada = reader.GetGuid(3);
                            
                                if (Tipohist == 3)
                                {
                                    // La última interrupción igual fue eliminada
                                    InsertInto(readerRiku, "PIC_CalidadIndividual", "1");
                                    InsertInto(readerRiku, "Historico", "1");
                                }
                                else
                                {
                                    // La última interrupción igual no fue eliminada
                                    if (CUPShist != CUPSriku | Datehist != Dateriku | Duracionhist != Duracionriku)
                                    InsertInto(readerRiku, "PIC_CalidadIndividual", "2", IdModificada);
                                    InsertInto(readerRiku, "Historico", "2", IdModificada);
                                    Update(reader, "Historico", "3");
                                }
                            }
                        }
                    }
                    cn.Close();
                }

            }
        }

        public void updated2(OdbcDataReader readerHist, OdbcDataReader readerRiku)
        {
            //OdbcCommand cmd;
            //string SQLSentence;
            
            //SQLSentence = "SELECT * FROM Historico";
            //cn_hist = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            //cn_hist.Open();
            //cmd = new OdbcCommand(SQLSentence, cn_hist);
            //OdbcDataReader readerHist = cmd.ExecuteReader();
            while (readerRiku.Read())
            {                
                bool isUpdated = false;
                bool isInserted = true;
                if (!readerHist.HasRows)
                {
                    InsertInto(readerRiku,"PIC_CalidadIndividual", "1");
                    InsertInto(readerRiku, "Historico", "1");
                }
                else
                {
                    //SQLSentence = "SELECT * FROM Historico";
                    //cn_hist = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    //cn_hist.Open();
                    //OdbcConnection cn_hist2;
                    //cn_hist2 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    //cn_hist2.Open();
                    //OdbcConnection cn_hist3;
                    //cn_hist3 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    //cn_hist3.Open();
                    //OdbcConnection cn_hist4;
                    //cn_hist4 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    //cn_hist4.Open();
                    //OdbcConnection cn_hist5;
                    //cn_hist5 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                    //cn_hist5.Open();
                    //cmd = new OdbcCommand(SQLSentence, cn_hist);
                    //OdbcDataReader readerHist2 = cmd.ExecuteReader();
                    Guid IdModificada = new Guid();
                    while (readerHist.Read())
                    {
                        
                        String CUPSriku = readerRiku.GetString(1);
                        String CUPShist = readerHist.GetString(1);
                        Guid IDriku = readerRiku.GetGuid(3);
                        Guid IDhist = readerHist.GetGuid(3);
                        Guid IDhist2;
                        DateTime Dateriku = readerRiku.GetDateTime(4);
                        DateTime Datehist = readerHist.GetDateTime(4);
                        Int32 Duracionriku = readerRiku.GetInt32(5);
                        Int32 Duracionhist = readerHist.GetInt32(5);
                        Int64 Tipohist = readerHist.GetInt64(10);

                        if (CUPSriku == CUPShist &
                            IDriku != IDhist &
                            Dateriku == Datehist &
                            Tipohist == 3)
                        {
                            OdbcConnection cn;
                            OdbcCommand cmd;
                            cn = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                            cn.Open();
                            string SQLSentence;
                            SQLSentence = "SELECT * FROM Historico where SCCI_CUPS = '" + CUPShist + "' AND SCCI_FechaInterrupcion = '" + Datehist + "' AND SCCI_Tipo = 2";
                            GDLAux.escribeEnTxt(SQLSentence);
                            cmd = new OdbcCommand(SQLSentence, cn);
                            OdbcDataReader readerHist2 = cmd.ExecuteReader();
                            if (!readerHist2.HasRows)
                            {
                                SQLSentence = "select TOP 1 SCCI_IdInterrupcion from Historico where SCCI_CUPS = '" + CUPShist + "' ORDER BY SCCI_IdCalidadIndividual DESC";
                                GDLAux.escribeEnTxt(SQLSentence);
                                OdbcConnection cn2;
                                cn2 = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
                                cn2.Open();
                                cmd = new OdbcCommand(SQLSentence, cn2);
                                OdbcDataReader readerHist3 = cmd.ExecuteReader();
                                if (readerHist3.HasRows)
                                {
                                    IDhist2 = readerHist3.GetGuid(0);
                                    IdModificada = IDhist2;
                                }
                                isUpdated = true;
                                isInserted = false;
                                cn2.Close();
                                break;                                
                            }
                            cn.Close();
                            
                        }
                        if (CUPSriku == CUPShist &
                            IDriku == IDhist)
                        {
                            isInserted = false;
                        }
                    }

                    if (isUpdated)
                    {
                        InsertInto(readerRiku, "PIC_CalidadIndividual", "2", IdModificada);
                        InsertInto(readerRiku, "Historico", "2",IdModificada);
                    }
                    if (isInserted)
                    {
                        InsertInto(readerRiku, "PIC_CalidadIndividual", "1");
                        InsertInto(readerRiku, "Historico", "1");
                    }
                }
            }
        }

        public void InsertInto(OdbcDataReader reader, string Table, string Tipo, Guid IdModificada = new Guid())
        {

            OdbcConnection cn;
            OdbcCommand cmd;

            // Lectura Tabla riku
            
            cn = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn.Open();

            string SQLSentence;
            SQLSentence = "Insert into " + Table + " (SCCI_CUPS,SCCI_MinEco,SCCI_IdInterrupcion,SCCI_FechaInterrupcion,SCCI_Duracion,SCCI_Origen,SCCI_Zona,SCCI_Provincia,SCCI_Municipio,SCCI_Tipo";

            if (IdModificada != new Guid() && Table == "Historico")
                SQLSentence = SQLSentence + ",SCCI_IdModificada";

            SQLSentence = SQLSentence + ") Values (";
            for (int i = 1; i < 10; i++)
            {
                try
                {
                    string Value;
                    switch (i)
                    {
                        case 3:
                            if (Table == "PIC_CalidadIndividual" && Tipo != "1" && IdModificada != new Guid())
                            {
                                Value = IdModificadaFromIdInterrupcion2(IdModificada, reader.GetString(1)).ToString();
                            }
                            else
                            {
                                Value = reader.GetGuid(i).ToString();
                            }                            
                            break;
                        case 4:
                            Value = reader.GetDateTime(i).ToString();
                            break;
                        case 5:
                            Value = reader.GetInt32(i).ToString();
                            break;
                        default:
                            Value = reader.GetString(i);
                            break;
                    }
                    SQLSentence = SQLSentence + "'" + Value + "'" + ',';
                }
                catch (Exception)
                {
                    SQLSentence = SQLSentence + "NULL" + ',';
                }                        
            }
            SQLSentence = SQLSentence + "'" + Tipo.ToString() + "'" + ',';
            if (IdModificada != new Guid() && Table == "Historico")
                SQLSentence = SQLSentence + "'" + IdModificada.ToString() + "'" + ',';
            SQLSentence = SQLSentence.Substring(0, SQLSentence.Length - 1) +')';
            GDLAux.escribeEnTxt(SQLSentence);
            cmd = new OdbcCommand(SQLSentence, cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void Update(OdbcDataReader reader, string Table, string Tipo)
        {
            OdbcCommand cmd;
            OdbcConnection cn;
            cn = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn.Open();
            string SQLSentence;
            string CUPS = reader.GetString(1);
            Int32 Duracion = reader.GetInt32(5);
            DateTime Fecha = reader.GetDateTime(4);
            Guid ID = reader.GetGuid(3);
            SQLSentence = "UPDATE " + Table + " SET SCCI_Duracion = '" + Duracion + "' , SCCI_Tipo = " + Tipo + " WHERE SCCI_CUPS = '" + CUPS + "' AND SCCI_FechaInterrupcion = '" + Fecha + "' AND SCCI_IdInterrupcion = '" + ID + "'";
            GDLAux.escribeEnTxt(SQLSentence);
            cmd = new OdbcCommand(SQLSentence, cn);
            cmd.ExecuteNonQuery();
            cn.Close();

        }

        public Guid IdModificadaFromIdInterrupcion(Guid IdInterrupcion, String CUPS)
        {
            OdbcCommand cmd;
            OdbcConnection cn;
            Guid IdModificada = new Guid();
            cn = new OdbcConnection(settings.PICDatabaseSettings.ConnectionString);
            cn.Open();
            string SQLSentence;
            SQLSentence = "SELECT H1.SCCI_IdModificada FROM Historico AS H1 JOIN Historico AS H2 ON H1.SCCI_IdInterrupcion = '" + IdInterrupcion.ToString() + "' WHERE H1.SCCI_CUPS = '"+CUPS+"' GROUP BY H1.SCCI_IdModificada";
            GDLAux.escribeEnTxt(SQLSentence);
            cmd = new OdbcCommand(SQLSentence, cn);
            OdbcDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                try
                {
                    IdModificada = reader.GetGuid(0);
                }
                catch (Exception)
                {
                    IdModificada = new Guid();
                }
            }
            cn.Close();
            return IdModificada;
        }

        public Guid IdModificadaFromIdInterrupcion2(Guid IdInterrupcion, String CUPS)
        {
            Guid IdModificada = new Guid();
            while (IdInterrupcion != new Guid())
            {
                IdModificada = IdModificadaFromIdInterrupcion(IdInterrupcion, CUPS);
                if (IdModificada == new Guid()) { break; }
                else { IdInterrupcion = IdModificada; }
            }
            return IdInterrupcion;
        }
    }
}
﻿SELECT gdlMvCustLocation.MUNI,
gdlMvCustLocation.PROVINCE,
gdlMvCustLocation.UTILITY,
gdlMunType.TYPE,
COUNT(DISTINCT CUSTOMER.CODE) AS MV
FROM gdlMvCustLocation JOIN CUSTOMER
ON CUSTOMER.CUSTOMER_NODE = gdlMvCustLocation.CUSTOMER_NODE
LEFT JOIN gdlMunType ON (
gdlMvCustLocation.UTILITY = gdlMunType.UTILITY
AND gdlMvCustLocation.MUNI = gdlMunType.MUNI
AND gdlMvCustLocation.PROVINCE = gdlMunType.PROVINCE )
GROUP BY gdlMvCustLocation.MUNI,
gdlMvCustLocation.PROVINCE,
gdlMvCustLocation.UTILITY,
gdlMunType.TYPE
// Generated using SQL2Cs.exe
// DO NOT MODIFY

// 12/7/2011 12:24:07 PM

using System;

namespace PIC_Adapter
{
	static class PICAdapterQueries
	{
		public const String CalidadIndividual =
			"SELECT	DISTINCT\n" +
			"CUSTOMER.CODE,\n" +
			"MV_LV_SUBSTATION.COMPANY AS MinEco,\n" +
			"FaultReport$$ARCH$$.NUMBER AS IdInterrupcion,\n" +
			"OutageAreas$$ARCH$$.STARTINGTIME AS FechaInterrupcion,\n" +
			"dbo.DDIFFHOUR(OutageAreas$$ARCH$$.STARTINGTIME, OutageAreas$$ARCH$$.ENDINGTIME) * 60 AS Duracion,\n" +
			"FAULTREASON,\n" +
			"gdlMunType.TYPE,\n" +
			"MV_LV_SUBSTATION.PROVINCE,\n" +
			"MV_LV_SUBSTATION.MUN,\n" +
			"NULL AS cust_nodes,\n" +
			"CUSTOMER.CUSTOMER_NODE\n" +
			"FROM FaultReport$$ARCH$$\n" +
			"JOIN OutageAreas$$ARCH$$ ON FaultReport$$ARCH$$.NUMBER = OutageAreas$$ARCH$$.NUMBER\n" +
			"JOIN LVNetworkOutages$$ARCH$$ ON OutageAreas$$ARCH$$.ID = LVNetworkOutages$$ARCH$$.OUTAGEAREA\n" +
			"JOIN CUSTOMER ON LVNetworkOutages$$ARCH$$.CODE = CUSTOMER.LV_NETWORK\n" +
			"LEFT JOIN MV_LV_SUBSTATION ON LVNetworkOutages$$ARCH$$.CODE = MV_LV_SUBSTATION.CODE\n" +
			"LEFT JOIN gdlMunType ON (MV_LV_SUBSTATION.COMPANY = gdlMunType.UTILITY AND\n" +
			"MV_LV_SUBSTATION.MUN = gdlMunType.MUNI AND\n" +
			"MV_LV_SUBSTATION.PROVINCE = gdlMunType.PROVINCE)\n" +
			"WHERE dbo.DDIFFHOUR(OutageAreas$$ARCH$$.STARTINGTIME, OutageAreas$$ARCH$$.ENDINGTIME) * 60\n" +
			"> 3.0\n" +
			"UNION\n" +
			"SELECT DISTINCT\n" +
			"CUSTOMER.CODE,\n" +
			"MV_LV_SUBSTATION.COMPANY AS MinEco,\n" +
			"MVOutageReport$$ARCH$$.NUMBER AS IdInterrupcion,\n" +
			"OutageAreas$$ARCH$$.STARTINGTIME AS FechaInterrupcion,\n" +
			"dbo.DDIFFHOUR(OutageAreas$$ARCH$$.STARTINGTIME, OutageAreas$$ARCH$$.ENDINGTIME) * 60 AS Duracion,\n" +
			"FAULTREASON,\n" +
			"gdlMunType.TYPE,\n" +
			"MV_LV_SUBSTATION.PROVINCE,\n" +
			"MV_LV_SUBSTATION.MUN,\n" +
			"NULL AS cust_nodes,\n" +
			"CUSTOMER.CUSTOMER_NODE\n" +
			"FROM MVOutageReport$$ARCH$$\n" +
			"JOIN OutageAreas$$ARCH$$ ON MVOutageReport$$ARCH$$.NUMBER = OutageAreas$$ARCH$$.NUMBER\n" +
			"JOIN LVNetworkOutages$$ARCH$$ ON OutageAreas$$ARCH$$.ID = LVNetworkOutages$$ARCH$$.OUTAGEAREA\n" +
			"JOIN CUSTOMER ON LVNetworkOutages$$ARCH$$.CODE = CUSTOMER.LV_NETWORK\n" +
			"LEFT JOIN MV_LV_SUBSTATION ON LVNetworkOutages$$ARCH$$.CODE = MV_LV_SUBSTATION.CODE\n" +
			"LEFT JOIN gdlMunType ON (MV_LV_SUBSTATION.COMPANY = gdlMunType.UTILITY AND\n" +
			"MV_LV_SUBSTATION.MUN = gdlMunType.MUNI AND\n" +
			"MV_LV_SUBSTATION.PROVINCE = gdlMunType.PROVINCE)\n" +
			"WHERE dbo.DDIFFHOUR(OutageAreas$$ARCH$$.STARTINGTIME, OutageAreas$$ARCH$$.ENDINGTIME) * 60\n" +
			"> 3.0\n" +
			"UNION\n" +
			"SELECT DISTINCT\n" +
			"CUSTOMER.CODE,\n" +
			"MV_LV_SUBSTATION.COMPANY AS MinEco,\n" +
			"LVOutageReport$$ARCH$$.NUMBER AS IdInterrupcion,\n" +
			"OutageAreas$$ARCH$$.STARTINGTIME AS FechaInterrupcion,\n" +
			"dbo.DDIFFHOUR(OutageAreas$$ARCH$$.STARTINGTIME, OutageAreas$$ARCH$$.ENDINGTIME) * 60 AS Duracion,\n" +
			"LVFAULTREASON,\n" +
			"gdlMunType.TYPE,\n" +
			"MV_LV_SUBSTATION.PROVINCE,\n" +
			"MV_LV_SUBSTATION.MUN,\n" +
			"BORDERS AS cust_nodes,\n" +
			"CUSTOMER.CUSTOMER_NODE\n" +
			"FROM LVOutageReport$$ARCH$$\n" +
			"JOIN OutageAreas$$ARCH$$ ON LVOutageReport$$ARCH$$.NUMBER = OutageAreas$$ARCH$$.NUMBER\n" +
			"JOIN LVNetworkOutages$$ARCH$$ ON OutageAreas$$ARCH$$.ID = LVNetworkOutages$$ARCH$$.OUTAGEAREA\n" +
			"JOIN CUSTOMER ON LVNetworkOutages$$ARCH$$.CODE = CUSTOMER.LV_NETWORK\n" +
			"LEFT JOIN MV_LV_SUBSTATION ON LVNetworkOutages$$ARCH$$.CODE = MV_LV_SUBSTATION.CODE\n" +
			"LEFT JOIN gdlMunType ON (MV_LV_SUBSTATION.COMPANY = gdlMunType.UTILITY AND\n" +
			"MV_LV_SUBSTATION.MUN = gdlMunType.MUNI AND\n" +
			"MV_LV_SUBSTATION.PROVINCE = gdlMunType.PROVINCE)\n" +
			"WHERE dbo.DDIFFHOUR(OutageAreas$$ARCH$$.STARTINGTIME, OutageAreas$$ARCH$$.ENDINGTIME) * 60\n" +
			"> 3.0";

		public const String CalidadZonal =
			"WITH outages AS (\n" +
			"SELECT	dbo.GETNOTNULL( FAULTREASON, MAINTOUTAGEREASON) AS REASON,\n" +
			"NUMBER, STARTINGTIME, ENDINGTIME, TYPE, 1 AS MV\n" +
			"FROM		MVOutageReport$$ARCH$$\n" +
			"UNION\n" +
			"SELECT	FAULTREASON AS REASON, NUMBER, STARTINGTIME, ENDINGTIME, 6 AS TYPE, 1 AS MV\n" +
			"FROM		FaultReport$$ARCH$$\n" +
			"UNION\n" +
			"SELECT	MAINTOUTAGEREASON AS REASON, NUMBER, STARTINGTIME, ENDINGTIME, 7 AS TYPE, 1 AS MV\n" +
			"FROM		MaintenanceOutageReport$$ARCH$$\n" +
			")\n" +
			"SELECT	MUN,\n" +
			"MV_LV_SUBSTATION.PROVINCE,\n" +
			"MV_LV_SUBSTATION.COMPANY,\n" +
			"gdlMunType.TYPE,\n" +
			"SUM(CUSTOMER.CONTRACTED_POWER\n" +
			"* dbo.DDIFFHOUR(OutageAreas$$ARCH$$.STARTINGTIME, OutageAreas$$ARCH$$.ENDINGTIME))\n" +
			"/ (SELECT [POWER] FROM MUNI_POWER_SP\n" +
			"WHERE MUNI = MUN\n" +
			"AND PROVINCE = MV_LV_SUBSTATION.PROVINCE\n" +
			"AND UTILITY = MV_LV_SUBSTATION.COMPANY)\n" +
			"AS horas_p,\n" +
			"SUM(CUSTOMER.CONTRACTED_POWER)\n" +
			"/ (SELECT [POWER] FROM MUNI_POWER_SP\n" +
			"WHERE MUNI = MUN\n" +
			"AND PROVINCE = MV_LV_SUBSTATION.PROVINCE\n" +
			"AND UTILITY = MV_LV_SUBSTATION.COMPANY) AS numero_p\n" +
			"FROM	outages\n" +
			"JOIN OutageAreas$$ARCH$$\n" +
			"ON outages.NUMBER = OutageAreas$$ARCH$$.NUMBER\n" +
			"JOIN LVNetworkOutages$$ARCH$$\n" +
			"ON OutageAreas$$ARCH$$.ID = LVNetworkOutages$$ARCH$$.OUTAGEAREA\n" +
			"JOIN MV_LV_SUBSTATION\n" +
			"ON LVNetworkOutages$$ARCH$$.CODE = MV_LV_SUBSTATION.CODE\n" +
			"LEFT JOIN gdlMunType\n" +
			"ON (MV_LV_SUBSTATION.MUN = gdlMunType.MUNI\n" +
			"AND MV_LV_SUBSTATION.PROVINCE = gdlMunType.PROVINCE\n" +
			"AND gdlMunType.UTILITY = MV_LV_SUBSTATION.COMPANY)\n" +
			"LEFT JOIN CUSTOMER\n" +
			"ON MV_LV_SUBSTATION.CODE = CUSTOMER.LV_NETWORK\n" +
			"WHERE	outages.STARTINGTIME BETWEEN ? AND ?\n" +
			"AND dbo.DDIFFHOUR(outages.STARTINGTIME, outages.ENDINGTIME) > 0.05\n" +
			"AND REASON = ?\n" +
			"AND outages.TYPE = ?\n" +
			"GROUP BY	MUN, MV_LV_SUBSTATION.PROVINCE,\n" +
			"MV_LV_SUBSTATION.COMPANY,\n" +
			"gdlMunType.TYPE";

		public const String ContractedPower =
			"SELECT MV_LV_SUBSTATION.MUN,\n" +
			"MV_LV_SUBSTATION.PROVINCE,\n" +
			"COMPANY,\n" +
			"gdlMunType.TYPE,\n" +
			"SUM(CONTRACTED_POWER) AS p\n" +
			"FROM CUSTOMER\n" +
			"JOIN MV_LV_SUBSTATION ON CUSTOMER.LV_NETWORK = MV_LV_SUBSTATION.CODE\n" +
			"LEFT JOIN gdlMunType ON (\n" +
			"MV_LV_SUBSTATION.COMPANY = gdlMunType.UTILITY\n" +
			"AND MV_LV_SUBSTATION.MUN = gdlMunType.MUNI\n" +
			"AND MV_LV_SUBSTATION.PROVINCE = gdlMunType.PROVINCE )\n" +
			"GROUP BY MV_LV_SUBSTATION.MUN, MV_LV_SUBSTATION.PROVINCE,\n" +
			"MV_LV_SUBSTATION.COMPANY, gdlMunType.TYPE";

		public const String GetCustomerData =
			"SELECT	SCPD_CUPS,\n" +
			"SCPD_PotenciaContrataActual\n" +
			"FROM INFO_PIC_Distribuidores";

		public const String InstalledPower =
			"SELECT MUN, MV_LV_SUBSTATION.PROVINCE,  COMPANY, gdlMunType.TYPE, SUM(TRANSFORMER.SN1)\n" +
			"FROM MV_LV_SUBSTATION\n" +
			"JOIN TRANSFORMER ON MV_LV_SUBSTATION.CODE = TRANSFORMER.PLACING_SITE\n" +
			"LEFT JOIN gdlMunType ON (\n" +
			"MV_LV_SUBSTATION.COMPANY = gdlMunType.UTILITY\n" +
			"AND MV_LV_SUBSTATION.MUN = gdlMunType.MUNI\n" +
			"AND MV_LV_SUBSTATION.PROVINCE = gdlMunType.PROVINCE )\n" +
			"GROUP BY MV_LV_SUBSTATION.MUN, MV_LV_SUBSTATION.PROVINCE, COMPANY, gdlMunType.TYPE";

		public const String LvCustomerCount =
			"SELECT MUN,\n" +
			"MV_LV_SUBSTATION.PROVINCE,\n" +
			"COMPANY,\n" +
			"gdlMunType.TYPE,\n" +
			"COUNT(DISTINCT CUSTOMER.CODE) AS LV\n" +
			"FROM MV_LV_SUBSTATION\n" +
			"JOIN CUSTOMER ON MV_LV_SUBSTATION.CODE = CUSTOMER.LV_NETWORK\n" +
			"LEFT JOIN gdlMunType ON (\n" +
			"MV_LV_SUBSTATION.COMPANY = gdlMunType.UTILITY\n" +
			"AND MV_LV_SUBSTATION.MUN = gdlMunType.MUNI\n" +
			"AND MV_LV_SUBSTATION.PROVINCE = gdlMunType.PROVINCE )\n" +
			"GROUP BY MV_LV_SUBSTATION.MUN, MV_LV_SUBSTATION.PROVINCE,\n" +
			"COMPANY, gdlMunType.TYPE";

		public const String MvCustomerCount =
			"SELECT gdlMvCustLocation.MUNI,\n" +
			"gdlMvCustLocation.PROVINCE,\n" +
			"gdlMvCustLocation.UTILITY,\n" +
			"gdlMunType.TYPE,\n" +
			"COUNT(DISTINCT CUSTOMER.CODE) AS MV\n" +
			"FROM gdlMvCustLocation JOIN CUSTOMER\n" +
			"ON CUSTOMER.CUSTOMER_NODE = gdlMvCustLocation.CUSTOMER_NODE\n" +
			"LEFT JOIN gdlMunType ON (\n" +
			"gdlMvCustLocation.UTILITY = gdlMunType.UTILITY\n" +
			"AND gdlMvCustLocation.MUNI = gdlMunType.MUNI\n" +
			"AND gdlMvCustLocation.PROVINCE = gdlMunType.PROVINCE )\n" +
			"GROUP BY gdlMvCustLocation.MUNI,\n" +
			"gdlMvCustLocation.PROVINCE,\n" +
			"gdlMvCustLocation.UTILITY,\n" +
			"gdlMunType.TYPE";
	}
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PIC_Adapter
{
    static class GDLAux
    {

        private static bool developMode = true;

        public static bool DevelopMode
        {
            get
            {
                return developMode;
            }
        }

        public static void escribeEnTxt(string lines)
        {
            // Compose a string that consists of three lines.
            //string lines = "First line.\r\nSecond line.\r\nThird line.";
            // Write the string to a file.
            StreamWriter file = File.AppendText("test.txt");
            //System.IO.StreamWriter file = new System.IO.StreamWriter("test.txt");
            file.WriteLine(lines);
            file.WriteLine("--------------------------------------");
            file.Close();
        }

    }
}

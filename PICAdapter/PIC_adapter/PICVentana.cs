﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DMSUtils;
using System.Diagnostics;
using System.Xml.Serialization;
using System.IO;
using System.Data.Odbc;

namespace PIC_Adapter
{
    public partial class PICVentana : Form
    {
        PICAdapter adapter = null;
        private Timer timer;
        private int segundos = 3;

        public PICVentana()
        {
            InitializeComponent();

            timer = new Timer();
            timer.Interval = 1000;
            timer.Tick += new EventHandler(this.tm_Tick);

        }


        private void PICVentana_Load(object sender, EventArgs e)
        {
            lblPrincipal.Text = "Arrancando PIC...";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("www.gedlux.es");
        }

        public void ejecutaPIC()
        {
            try
            {
                adapter = new PICAdapter("PIC_Settings.xml");
            }
            catch (Exception e)
            {
                LogFile.Log("Error initializing PIC connection: " + e.Message,
                            EventLogEntryType.Error);

                if (GDLAux.DevelopMode)
                {
                    Console.WriteLine(e.Message.ToString());
                    Console.WriteLine(e.Source.ToString());
                    Console.WriteLine("Stacktrace: " + (e.StackTrace).ToString());
                    Console.ReadLine();
                }

                return;
            }

            if (GDLAux.DevelopMode)
            {
                //Console.WriteLine("PAUSA");
               // Console.ReadLine();
            }

            try
            {
                adapter.QualityExport();
            }
            catch (Exception e)
            {
                LogFile.Log("Error exporting outages: " + e.Message,
                            EventLogEntryType.Error);
                if (GDLAux.DevelopMode)
                Console.WriteLine(e.Message.ToString());
                

            }

            Historical hist = new Historical("PIC_Settings.xml");
            hist.calculate();
            if (GDLAux.DevelopMode)


            Console.WriteLine();
            Console.WriteLine("-----> PROCESO TERMINADO");
            Console.WriteLine();
            
        }

        private void PICVentana_Activated(object sender, EventArgs e)
        {
            lblPrincipal.Text = "Inicializando PIC...";
            this.Refresh();
            lblPrincipal.Text = "En proceso...";
            this.Refresh();
            ejecutaPIC();
            lblPrincipal.Text = "COMPLETADO!";
            this.Refresh();
            timer.Start();
           
        }

        private void tm_Tick(object sender, EventArgs e)
        {
            if (segundos > 0)
                this.Text = "CERRANDO PIC -> " + segundos--.ToString();
            else
                this.Close();
        }

    }
}

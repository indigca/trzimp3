﻿This project uses
	-DMSUtils.dll, from ABB
	-Sql2Cs code generator, from ABB
	-xsd.exe code generator, part of visual studio tools

When running, DMSUtils.dll is needed on the same folder or in system folder.

When modifying SQL scripts, Sql2Cs is used to convert them into C# strings.
The command line is
  SQL2Cs PICAdapterQueries PIC_Adapter -p "$(ProjectDir)SQL\\" "$(ProjectDir)SQL\*.sql"
and is also added as a pre-build operation, so compiling needs SQL2Cs to exist in path.

When modifying xsd files, the associated classes must be created with xsd.exe using
comman line
  xsd /namespace:PIC_Adapter /classes PIC_Adapter_Settings.xsd



The SCADA table look like this:
CREATE TABLE [dbo].[SCADA_CalidadIndividual](
    [SCCI_IdCalidadIndividual] [int] IDENTITY(1,1) NOT NULL,
    [SCCI_CUPS] [varchar](22) NOT NULL,
    [SCCI_MinEco] [varchar](3) NOT NULL,
    [SCCI_IdInterrupcion] [uniqueidentifier] NULL,
    [SCCI_FechaInterrupcion] [smalldatetime] NULL,
    [SCCI_Duracion] [int] NULL,
    [SCCI_Origen] [char](1) NULL,
    [SCCI_Zona] [int] NULL,
    [SCCI_Provincia] [varchar](2) NULL,
    [SCCI_Municipio] [varchar](3) NULL,
    [SCCI_Tipo] [int] NOT NULL,
 CONSTRAINT [PK_SCADA_TB2] PRIMARY KEY CLUSTERED
(
    [SCCI_IdCalidadIndividual] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF,
	   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON,
	   ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

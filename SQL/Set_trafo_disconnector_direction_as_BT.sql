-- set direction to 'BT' for each disconnector
-- directly connected to a transformer
UPDATE DISCONNECTOR
SET DIRECTION = 'BT'
WHERE NODECODE IN
(
SELECT NODE1
FROM MV_SECTION
WHERE NODE2 LIKE '%M%'
)

-- add a placeholder when direction not set: otherwise code is repeated
UPDATE DISCONNECTOR SET DIRECTION = '-' WHERE DIRECTION IS NULL
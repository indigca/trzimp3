-- Selects MV_SECTIONs where start and end node are on same position but not connected
SELECT	*
FROM		MV_SECTION AS s1 JOIN MV_NODE AS m1 ON s1.NODE1 = m1.CODE OR s1.NODE2 = m1.CODE,
				MV_SECTION AS s2 JOIN MV_NODE AS m2 ON s2.NODE1 = m2.CODE OR s2.NODE2 = m2.CODE
WHERE		m1.X = m2.X AND m1.Y = m2.Y AND m1.CODE <> m2.CODE


-- Offsets all sectionpoints that overlap with a sectionpoint of another section
-- by 0,5 m on the X-direction
WHILE EXISTS (
		SELECT TOP 1 sp1.MV_Section_Id
		FROM MV_SECTIONPOINT AS sp1 JOIN MV_SECTIONPOINT AS sp2
						ON sp1.X = sp2.X AND
							 sp1.Y = sp2.Y AND
							 sp1.MV_Section_Id < sp2.MV_Section_Id
		)
BEGIN
	UPDATE MV_SECTIONPOINT SET 
	X = X+1000
	WHERE MV_Section_Id IN
	(
	SELECT TOP 1 sp1.MV_Section_Id
	FROM MV_SECTIONPOINT AS sp1 JOIN MV_SECTIONPOINT AS sp2
					ON sp1.X = sp2.X AND
						 sp1.Y = sp2.Y AND
						 sp1.MV_Section_Id < sp2.MV_Section_Id
	)
END
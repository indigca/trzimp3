USE [SCADA]
GO

/****** Object:  Table [dbo].[INFO_PIC_DISTRIBUIDORES]    Script Date: 09/20/2011 10:01:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[INFO_PIC_DISTRIBUIDORES](
	[SCPD_IdInfoPicDistribuidores] [int] IDENTITY(1,1) NOT NULL,
	[SCPD_IdDistribuidor] [int] NOT NULL,
	[SCPD_Cups] [nvarchar](32) NOT NULL,
	[SCPD_MinEco] [nvarchar](3) NOT NULL,
	[SCPD_PotenciaContrataActual] [real] NOT NULL,
	[SCPD_PotenciaContrataAnterior] [real] NULL,
	[SCPD_FechaCambioPotencia] [smalldatetime] NULL,
	[SCPD_NivelTensionActual] [int] NOT NULL,
	[SCPD_NivelTensionAnterior] [int] NULL,
	[SCPD_FechaCambioNivelTension] [smalldatetime] NULL,
	[SCPD_FechaInsercion] [smalldatetime] NOT NULL,
	[SCPD_Accion] [int] NOT NULL,
 CONSTRAINT [PK_SCADA_TB1] PRIMARY KEY CLUSTERED 
(
	[SCPD_IdInfoPicDistribuidores] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



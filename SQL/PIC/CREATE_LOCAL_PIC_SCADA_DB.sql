USE [SCADA]
GO

/****** Object:  Table [dbo].[INFO_PIC_DISTRIBUIDORES]    Script Date: 09/05/2011 12:19:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[INFO_PIC_DISTRIBUIDORES](
	[SCPD_IdInfoPicDistribuidores] [int] IDENTITY(1,1) NOT NULL,
	[SCPD_IdDistribuidor] [int] NOT NULL,
	[SCPD_Cups] [nvarchar](22) NOT NULL,
	[SCPD_MinEco] [nvarchar](3) NOT NULL,
	[SCPD_PotenciaContrataActual] [real] NOT NULL,
	[SCPD_PotenciaContrataAnterior] [real] NULL,
	[SCPD_FechaCambioPotencia] [smalldatetime] NULL,
	[SCPD_NivelTensionActual] [int] NOT NULL,
	[SCPD_NivelTensionAnterior] [int] NULL,
	[SCPD_FechaCambioNivelTension] [smalldatetime] NULL,
	[SCPD_FechaInsercion] [smalldatetime] NOT NULL,
	[SCPD_Accion] [int] NOT NULL,
 CONSTRAINT [PK_SCADA_TB1] PRIMARY KEY CLUSTERED 
(
	[SCPD_IdInfoPicDistribuidores] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[SCADA_CalidadIndividual](
	[SCCI_IdCalidadIndividual] [int] IDENTITY(1,1) NOT NULL,
	[SCCI_IdDistribuidor] [int] NOT NULL,
	[SCCI_Cups] [nvarchar](22) NOT NULL,
	[SCCI_MinEco] [nvarchar](3) NOT NULL,
	[SCCI_AnioInicioCalculado] [int] NULL,
	[SCCI_MesInicioCalculado] [int] NULL,
	[SCCI_AnioFinCalculado] [int] NULL,
	[SCCI_MesFinCalculado] [int] NULL,
	[SCCI_Niepi] [int] NULL,
	[SCCI_Tiepi] [int] NULL,
	[SCCI_Zona] [int] NULL,
	[SCCI_Municipio] [int] NULL,
 CONSTRAINT [PK_SCADA_TB2] PRIMARY KEY CLUSTERED 
(
	[SCCI_IdCalidadIndividual] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[SCADA_CalidadZonal](
	[SCCZ_IdCalidadZonal] [int] IDENTITY(1,1) NOT NULL,
	[SCCZ_IdDistribuidor] [int] NOT NULL,
	[SCCZ_Municipio] [int] NOT NULL,
	[SCCZ_Provincia] [int] NOT NULL,
	[SCCZ_Zona] [int] NOT NULL,
	[SCCZ_NumSuministroXMunicipio] [int] NOT NULL,
	[SCCZ_SumPotContratadasXMunicipio] [int] NOT NULL,
	[SCCZ_PotenciaInstalada] [real] NOT NULL,
	[SCCZ_HorasProgramadasTransporte] [int] NOT NULL,
	[SCCZ_HorasProgramadasDistribucion] [int] NOT NULL,
	[SCCZ_HorasImprevistasGeneracion] [int] NOT NULL,
	[SCCZ_HorasImprevistasTransporte] [int] NOT NULL,
	[SCCZ_HorasImprevistasTerceros] [int] NOT NULL,
	[SCCZ_HorasImprevistasFuerzaMayor] [int] NOT NULL,
	[SCCZ_HorasImprevistasPropias] [int] NOT NULL,
	[SCCZ_NumProgramadasTransporte] [int] NOT NULL,
	[SCCZ_NumProgramadasDistribucion] [int] NOT NULL,
	[SCCZ_NumImprevistasGeneracion] [int] NOT NULL,
	[SCCZ_NumImprevistasTransporte] [int] NOT NULL,
	[SCCZ_NumImprevistasTerceros] [int] NOT NULL,
	[SCCZ_NumImprevistasFuerzaMayor] [int] NOT NULL,
	[SCCZ_NumImprevistasPropias] [int] NOT NULL,
	[SCCZ_NumImprevistasBT] [int] NOT NULL,
	[SCCZ_NumProgramadasBT] [int] NOT NULL,
	[SCCZ_MinEco] [nvarchar](3) NOT NULL,
	[SCCZ_AnioPeriodoCalculado] [int] NOT NULL,
	[SCCZ_MesPeriodoCalculado] [int] NOT NULL,
 CONSTRAINT [PK_SCADA_TB4] PRIMARY KEY CLUSTERED 
(
	[SCCZ_IdCalidadZonal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



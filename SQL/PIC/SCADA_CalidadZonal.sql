USE [SCADA]
GO

/****** Object:  Table [dbo].[SCADA_CalidadZonal]    Script Date: 09/20/2011 10:02:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SCADA_CalidadZonal](
	[SCCZ_IdCalidadZonal] [int] IDENTITY(1,1) NOT NULL,
	[SCCZ_IdDistribuidor] [int] NOT NULL,
	[SCCZ_Municipio] [int] NOT NULL,
	[SCCZ_Provincia] [int] NOT NULL,
	[SCCZ_Zona] [int] NULL,
	[SCCZ_NumSuministroXMunicipio] [int] NOT NULL,
	[SCCZ_SumPotContratadasXMunicipio] [int] NOT NULL,
	[SCCZ_PotenciaInstalada] [real] NOT NULL,
	[SCCZ_HorasProgramadasTransporte] [int] NOT NULL,
	[SCCZ_HorasProgramadasDistribucion] [int] NOT NULL,
	[SCCZ_HorasImprevistasGeneracion] [int] NOT NULL,
	[SCCZ_HorasImprevistasTransporte] [int] NOT NULL,
	[SCCZ_HorasImprevistasTerceros] [int] NOT NULL,
	[SCCZ_HorasImprevistasFuerzaMayor] [int] NOT NULL,
	[SCCZ_HorasImprevistasPropias] [int] NOT NULL,
	[SCCZ_NumProgramadasTransporte] [int] NOT NULL,
	[SCCZ_NumProgramadasDistribucion] [int] NOT NULL,
	[SCCZ_NumImprevistasGeneracion] [int] NOT NULL,
	[SCCZ_NumImprevistasTransporte] [int] NOT NULL,
	[SCCZ_NumImprevistasTerceros] [int] NOT NULL,
	[SCCZ_NumImprevistasFuerzaMayor] [int] NOT NULL,
	[SCCZ_NumImprevistasPropias] [int] NOT NULL,
	[SCCZ_NumImprevistasBT] [int] NOT NULL,
	[SCCZ_NumProgramadasBT] [int] NOT NULL,
	[SCCZ_MinEco] [nvarchar](3) NOT NULL,
	[SCCZ_AnioPeriodoCalculado] [int] NOT NULL,
	[SCCZ_MesPeriodoCalculado] [int] NOT NULL,
 CONSTRAINT [PK_SCADA_TB4] PRIMARY KEY CLUSTERED 
(
	[SCCZ_IdCalidadZonal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



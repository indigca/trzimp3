-- TODO alter table ST to contain correct coords before running

--INSERT INTO DMS.dbo.MV_NODE
SELECT	DMS.dbo.MAKE_NODECODE(XDMS, YDMS, 'P', NumeroTransformador) AS CODE,
				XDMS*1000,
				YDMS*1000,
				XDMS*1000 + 100000 AS XCODE,
				YDMS*1000 + 100000 AS YCODE,
				DMS.dbo.MAKE_NODECODE(XDMS, YDMS, 'P', 1) AS SITENODE,
				0 AS SymbolAngle,
				0 AS HIDE,
				0 AS HIDE_NODE
FROM		STTransformadores
				JOIN STTiposFabricantes ON STTransformadores.IdFabricante = STTiposFabricantes.IdFabricante
				JOIN Instalaciones ON STTransformadores.IdInst = Instalaciones.IdInst
				JOIN ST ON ST.IdInst = STTransformadores.IdInst
WHERE		TensionBT > 10
				
				
--INSERT INTO DMS.dbo.PRIMARY_TRANSFORMER
SELECT	DMS.dbo.MAKE_NODECODE(XDMS, YDMS, 'P', NumeroTransformador) AS NODECODE,
		(SELECT TOP 1 Siglas FROM DatosDistribuidoras) +'_' + Nombre + '_TF' + NumeroTransformador AS CODE, -- TODO
		NULL AS SCADACODE,
		Potencia AS Sn1, -- sometimes kW, othertimes MW
		NULL AS Sn2, 
		NULL AS Sn3,
		NULL AS UK12,
		NULL AS Z01,
		NULL AS THREE_WINDING,
		NULL AS PK12,
		NULL AS P0,
		NULL AS TYPE,
		DMS.dbo.GET_MANUFACTURER(STTiposFabricantes.Descripcion) AS MANUFACTURER,
		NULL AS YEAR,
		TensionAT AS Un1,
		TensionBT AS Un2,
		NULL AS Un3,
		NULL AS MASS,
		NULL AS INSTALLATION_DATE,
		NULL AS REMARKS,
		NULL AS UP_STEP_COUNT,
		NULL AS DONT_STEP_COUNT,
		NULL AS STEP_PERCENT,
		TensionBT AS SET_VALUE_U2,
		NULL AS EARTHED_1,
		NULL AS EARTHED_2,
		NULL AS EARTHED_3,
		NULL AS EARTHING_RESISTANCE_1,
		NULL AS EARTHING_REACTANCE_1,
		NULL AS EARTHING_RESISTANCE_2,
		NULL AS EARTHING_REACTANCE_2,
		NULL AS EARTHING_RESISTANCE_3,
		NULL AS EARTHING_REACTANCE_3,
		(SELECT TOP 1 Siglas FROM DatosDistribuidoras) AS REGIONCODE,
		CodigoPosicion AS NAME,
		0 AS PenColorRefU1,
		0 AS PenColorRefU2,
		NULL AS PenColorRefU3,
		0 AS PenWidthU1,
		0 AS PenWidthU2,
		NULL AS PenWidthU3,
		NULL AS PhasorGroup_CodeU1,
		NULL AS PhasorGroup_CodeU2,
		NULL AS PhasorGroup_CodeU3,
		NULL AS LINE_SECTION_1,
		NULL AS LINE_SECTION_2,  -- TODO must fill!
		NULL AS LINE_SECTION_3,
		NULL AS UK13,
		NULL AS UK23,
		NULL AS PK13,
		NULL AS PK23,
		NULL AS COUPLING_1,
		NULL AS COUPLING_2,
		NULL AS COUPLING_3,
		NULL AS CLOCK_2,
		NULL AS CLOCK_3,
		NULL AS Z02,
		NULL AS Z03,
		NULL AS SET_VALUE_U3
FROM STTransformadores
			JOIN STTiposFabricantes ON STTransformadores.IdFabricante = STTiposFabricantes.IdFabricante
			JOIN Instalaciones ON STTransformadores.IdInst = Instalaciones.IdInst
			JOIN ST ON ST.IdInst = STTransformadores.IdInst
WHERE	TensionBT > 10



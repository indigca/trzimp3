-- CUSTOMER_NODE_LOAD table is only used if Velander factors are used
-- If curves, then ENERGY1 + ENERGY2

-- If curves are used, cos phi comes from the associated curve (in dataMallit.txt)

-- Additionally data needed in
--	* LV_BOX
--	* MV_LV_SUBSTATION
--	* LV_SWITCH (for the fuse)
--  * LV_NODE

--ALTER TABLE CUSTOMER
--ADD CONTRACTED_POWER float

--INSERT INTO DMS.dbo.CUSTOMER
SELECT	Clientes.CUPS AS CODE,
		Clientes.Nombre AS NAME,
		NULL AS CGOUP,  -- Clientes.Poliza ??
		Tarifa AS TARIFF,
		NULL AS CURVE, -- From Poliza ??
		Clientes.EnergiaConsumida AS ENERGY1,
		0 AS ENERGY2, -- secondary energy, such as 'night' energy sometimes used for billing
			-- Energies only used with load curves, not with Velander
		PotenciaMaxDemandada AS REAL_PEAK,
		NULL AS REACTIVE_PEAK,
		NULL AS PHASECOUNT,
		NULL AS FUSE, -- I think this is just the fuse rated amperage
		BTVano.CodigoAcometida AS CUSTOMER_NODE,
		Instalaciones.Codigo + CTTransformadores.NumeroTransformador AS LV_NETWORK,
		GETDATE() AS UPDATE_DATE,
		NULL AS PHASE,
		Clientes.PotenciaContratada AS CONTRACTED_POWER
FROM	Clientes
		LEFT JOIN ClientesAcometidas ON Clientes.CUPS = ClientesAcometidas.CUPS
		LEFT JOIN BTVano ON ClientesAcometidas.IdBTVano = BTVano.IdBTVano
		LEFT JOIN Instalaciones ON Clientes.IdInst = Instalaciones.IdInst
		LEFT JOIN CTTransformadores ON Clientes.IdTransformador = CTTransformadores.IdTransformador

select * from Clientes
select * from clientes

--INSERT INTO DMS.dbo.LV_CUSTOMER_NODE
SELECT DISTINCT DMSB.dbo.MAKE_NODECODE(X,Y, '$', r) AS NODECODE,
		CodigoAcometida AS CUSTOMER_NODE,
		NULL AS FUSE,
		0 AS MV,
		SUBSTRING(DireccionAcometida, 0, 40) AS NAME,
		1 AS IMPORTANCE,
		Instalaciones.Codigo + CTTransformadores.NumeroTransformador AS LV_NETWORK,
		NULL AS FEEDER,	
		NULL AS BOX, -- Refers to LV_BOX
		NULL AS SWITCH, -- Refers to LV_SWITCH
		GETDATE() AS CHANGETIME
FROM	BTVano
			JOIN BT ON BTVano.IdInst = BT.IdInst
			JOIN Instalaciones ON BT.IdInstCT = Instalaciones.IdInst
			JOIN (
					SELECT	ROW_NUMBER() OVER (PARTITION BY X ORDER BY CodigoAcometida) AS r,
								CodigoAcometida AS ca
					FROM		BTVano WHERE CodigoAcometida IS NOT NULL AND CodigoAcometida NOT LIKE '045-'
			      ) AS row_n ON BTVano.CodigoAcometida = row_n.ca
			JOIN CTTransformadores ON BT.IdTransformador = CTTransformadores.IdTransformador
WHERE 	BTVano.CodigoAcometida IS NOT NULL


--INSERT INTO DMS.dbo.CUSTOMER_NODE_LOAD
SELECT	BTVano.CodigoAcometida AS CUSTOMER_NODE,
		ROW_NUMBER() OVER
			(PARTITION BY BTVano.CodigoAcometida ORDER BY Nombre)
			AS NUMBER,
		PotenciaContratada AS P,
		(SELECT 
			CASE
				WHEN (Clientes.EnergiaConsumida < 0.1 OR EnergiaReactiva < 0.1)
					THEN 1.0
				ELSE Clientes.EnergiaConsumida / 
						SQRT((CAST(Clientes.EnergiaConsumida AS bigint) * 
							  CAST(Clientes.EnergiaConsumida AS bigint) +
							  CAST(Clientes.EnergiaReactiva AS bigint) * 
							  CAST(Clientes.EnergiaReactiva AS bigint)))
			END
		) AS COSFII,
		Clientes.EnergiaConsumida AS ENERGY,
		NULL AS COMMENT,
		NULL AS PHASE -- TODO: where can we get the phase?
FROM	Clientes
			JOIN ClientesAcometidas
				ON Clientes.CUPS = ClientesAcometidas.CUPS
			LEFT JOIN BTVano
				ON ClientesAcometidas.IdBTVano = BTVano.IdBTVano
WHERE	BTVano.CodigoAcometida IS NOT NULL AND BTVano.CodigoAcometida <> (Clientes.IdDistribuidora + '-')



--INSERT INTO DMS.dbo.LV_NODE
SELECT	NODECODE, XDMS*1000 AS X,
		YDMS*1000 AS Y,
		XDMS*1000 AS XCODE,
		YDMS*1000 AS YCODE,
		NODECODE AS SITENODE,
		0 AS SymbolAngle
FROM		DMS.dbo.LV_CUSTOMER_NODE
				JOIN BTVano ON LV_CUSTOMER_NODE.CUSTOMER_NODE LIKE BTVano.CodigoAcometida

--INSERT INTO DMS.dbo.MV_LV_LOAD
SELECT	LV_NETWORK,
		ROW_NUMBER() OVER (PARTITION BY LV_NETWORK ORDER BY ENERGY) AS NUMBER,
		P,
		COSFII,
		ENERGY,
		COMMENT
FROM DMS.dbo.LV_CUSTOMER_NODE
		JOIN DMS.dbo.CUSTOMER_NODE_LOAD
			ON CUSTOMER_NODE_LOAD.CUSTOMER_NODE = LV_CUSTOMER_NODE.CUSTOMER_NODE
WHERE LV_NETWORK LIKE ((SELECT TOP 1 Siglas FROM DatosDistribuidoras) + '%')


select * from CUSTOMER where code = 'ES0175000000000002PT'

SELECT    Clientes.CUPS AS CODE,
Clientes.Nombre AS NAME,
NULL AS CGROUP, 
Tarifa AS TARIFF,
NULL AS CURVE, 
Clientes.EnergiaConsumida AS ENERGY1,
0 AS ENERGY2, 
PotenciaMaxDemandada AS REAL_PEAK,
NULL AS REACTIVE_PEAK,
NULL AS PHASECOUNT,
NULL AS FUSE, 
BTVano.CodigoAcometida AS CUSTOMER_NODE,
Instalaciones.Codigo + CTTransformadores.NumeroTransformador AS LV_NETWORK,
GETDATE() AS UPDATE_DATE,
NULL AS PHASE,
Clientes.PotenciaContratada AS CONTRACTED_POWER
FROM    Clientes
LEFT JOIN ClientesAcometidas ON Clientes.CUPS = ClientesAcometidas.CUPS
LEFT JOIN BTVano ON ClientesAcometidas.IdBTVano = BTVano.IdBTVano
LEFT JOIN Instalaciones ON Clientes.IdInst = Instalaciones.IdInst
LEFT JOIN CTTransformadores ON Clientes.IdTransformador = CTTransformadores.IdTransformador
where Clientes.CUPS = 'ES0175000000000002PT'

select SUM(P) from mv_lv_load where MV_LV_SUBSTATION = '045-C2069'


delete from customer

select LV_NETWORK AS MV_LV_SUBSTATION, CONTRACTED_POWER AS P, 1 AS COSFII, ENERGY1 AS ENERGY 
from customer WHERE customer_node is null and (ENERGY1 != 0 OR CONTRACTED_POWER != 0)
select * from CUSTOMER
select * from MV_LV_LOAD
select sum(p) from MV_LV_LOAD where MV_LV_SUBSTATION = '045-I10361'
INSERT INTO MV_LV_LOAD VALUES ('045-C20481',1,75,1,200000,null)
select * from LV_CUSTOMER_NODE where CUSTOMER_NODE = '045-0120107'
select * from TRANSFORMER
select * from TRANSFORMER_NODE
SELECT * FROM MV_NODE
SELECT * FROM MV_LV_SUBSTATION
SELECT * FROM MV_SITE
SELECT * FROM MV_SECTION where NODE1 = '610701999214E1' or NODE2 = '610701999214E1' order by MV_Section_Id
delete FROM MV_SECTION where MV_Section_Id = '65122'
select * from CUSTOMER_NODE_LOAD where CUSTOMER_NODE = '045-0120107'


select * from LV_SECTION where LV_NETWORK LIKE '045-c20012'

select * from LV_BOX
select * from LV_SWITCH
select * from LV_NODE
select * from MV_FUSE
SELECT * FROM TRANSFORMER
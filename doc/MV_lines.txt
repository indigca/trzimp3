Import lines of high and medium voltage from Traza to DMS
The first thing we do is get all the Traza lines
Then we cross all lines

The lines are imported in order of rank and origin.
As for the rank, we get the ATTipoLinea Table, containing the following information:
IdTipoLinea	Descripcion
1	Línea Principal
2	Derivación
3	Subderivación

By the origin, we get the table ATTipoOrigen:
IdTipoOrigen	Descripcion
1	Punto de entronque
2	Subestación
4	Centro de transformación
5	Apoyo

It is important to follow this order of import to avoid duplicates of nodes
With this factors, program cross all nodes of each line that will meet on the above mentioned conditions.

Now we have all the nodes of a line

Once we have all nodes of the line we see the beginning and end of the line to see 
if it has a CT associated, two or none

A preliminary step to draw the line to see where the lines cross to draw nodes there.

We will create a collection that will be ordered every Traza nodes, 
which indicate the code with the coordinates and a value between 0 and 3 depending on the type of node:
0: Fuse
1: Disconnect
2: A cross between lines
3: Common Node

It is recalled that the first three cases should draw a node in DMS, 
and therefore the node will serve as a line sectioning, creating multiple sections per line.

Once we have created this collection we have to change the coordinates of the nodes beginning 
or end of the line with CT associated

With the collection and use modified utiDMS.DrawLine function we draw the line with the coordinates 
of the nodes in the correct CT and intermediate nodes.
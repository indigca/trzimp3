CREATE VIEW duplicatePoles
AS
(SELECT c1.X AS X,c1.Y AS Y,c1.IdInst AS IdInst1,c1.ApoyoSecuencial,c2.IdInst AS IdInst2
FROM dbo.ATVano c1, dbo.ATVano c2
WHERE c1.IdInst <> c2.IdInst and c1.X=c2.X and c1.Y=c2.Y);

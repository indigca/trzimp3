--------------------------------------------------
-- Create Table dbo.gdlCodeMun
--------------------------------------------------
Create table dbo.gdlCodeMun (
    IDProv                         NVARCHAR(2)                    not null,
    IDMun                          NVARCHAR(4)                    PRIMARY KEY,
    NameMun                        NVARCHAR(50)                   not null) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlCodeProv
--------------------------------------------------
Create table dbo.gdlCodeProv (
    IDProv                         NVARCHAR(2)                    PRIMARY KEY,
    NameProv                       NVARCHAR(32)                   null) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlMINECO
--------------------------------------------------
Create table dbo.gdlMINECO (
    ID                             NVARCHAR(3)                    PRIMARY KEY,
    Name                           NVARCHAR(64)                   not null,
    Region                         NVARCHAR(32)                   not null,
    DMS                            NVARCHAR(1)                    null,
    City                           VARCHAR(32)                    null,
    Zone                           NVARCHAR(2)                    null) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlMunType
--------------------------------------------------
Create table dbo.gdlMunType (
    UTILITY                        NVARCHAR(3)                    PRIMARY KEY,
    MUNI                           NVARCHAR(4)                    not null,
    TYPE                           NVARCHAR(1)                    null,
    PROVINCE                       NVARCHAR(2)                    not null) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlReportCompany
--------------------------------------------------
Create table dbo.gdlReportCompany (
	NUMBERID					   int IDENTITY (1, 1)              PRIMARY KEY,
    USERID                         NVARCHAR(4000)                   not null,
    COMPANY                        NVARCHAR(3)                    not null) 
on [PRIMARY] ;


CREATE TABLE [dbo].[gdlMvCustLocation](
	[CUSTOMER_NODE] [nvarchar](32) PRIMARY KEY,
	[UTILITY] [nvarchar](3) NOT NULL,
	[MUNI] [nvarchar](4) NOT NULL,
	[PROVINCE] [nvarchar](2) NOT NULL
)

CREATE TABLE [dbo].[gdlCTConnections](
	[CT_NODECODE] [nvarchar](14) NOT NULL,
	[LI_IdInst] [nvarchar](50) NULL
)

CREATE TABLE [dbo].[gdlXYDMS](
	[CodigoAcometida] [nvarchar](50) NOT NULL,
	[XDMS] [int] NULL,
	[YDMS] [int] NULL
)	
CREATE TABLE [dbo].[gdlOPC](
	[MINECO] [varchar](3) NULL,
	[TYPE] [varchar](2) NULL,
	[OPC_CODE] [varchar](30) NOT NULL
)
CREATE TABLE [dbo].[gdlCT](
	[MINECO] [varchar](3) NULL,
	[DMS_Name] [varchar](50) NULL,
	[SCADA_Name] [varchar](50) NULL,
	[CT_Name] [varchar](50) NULL
) 



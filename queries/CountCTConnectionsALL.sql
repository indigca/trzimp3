---- Returns all the connections for the CTs
CREATE VIEW b_vw
(Total,CTNombre) 
as
SELECT sum(Conexiones) Total ,CTNombre from ( Select count(CTD.IdInst) as Conexiones ,Inst.Nombre as CTNombre from dbo.CTDerivaciones CTD, dbo.Instalaciones Inst where CTD.IdInst = Inst.IdInst group by Inst.Nombre, CTD.IdInst
                        union
						Select count(CTD.IdInst) as Conexiones ,Inst.Nombre as CTNombre from dbo.CTAlimentaciones CTD, dbo.Instalaciones Inst where CTD.IdInst = Inst.IdInst group by Inst.Nombre, CTD.IdInst)as a group by a.CTNombre 







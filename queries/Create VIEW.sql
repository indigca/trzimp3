DROP table TEST33;
CREATE VIEW TEST33
AS
SELECT  Ins.Nombre , ATV.X , ATV.Y, ATV.ApoyoSecuencial,
         ATV.IdTipoConductor
FROM     ATVano ATV, Instalaciones Ins
WHERE       ATV.IdInst = Ins.IdInst
    AND Ins.Nombre = 'Circunvalación Este'
ORDER BY ATV.ApoyoSecuencial;

SELECT T33.Nombre , T33.X , T33.Y, T33.ApoyoSecuencial, TCond.Descripcion
FROM TEST33 T33, ATVanoTiposConductores TCond
WHERE T33.IdTipoConductor=TCond.IdTipoConductor
ORDER BY ATV.ApoyoSecuencial


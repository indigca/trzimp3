DROP duplicatePoles
CREATE VIEW duplicatePoles 
AS
Select c1.X,c1.Y,c1.IdInst,c1.ApoyoSecuencial,c2.IdInst
from ATVano c1, ATVano c2
where c1.IdInst <> c2.IdInst and c1.X=c2.X and c1.Y=c2.Y
order by c1.idInst,c1.ApoyoSecuencial;
update duplicatePoles
set c1.X=c1.X+1
where c1.IdInst='045ARB00000001' and c1.ApoyoSecuencial=2;
COMMIT





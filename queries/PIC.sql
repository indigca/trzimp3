SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SCADA_CalidadIndividual](
	[SCCI_IdCalidadIndividual] [int] IDENTITY(1,1) NOT NULL,
	[SCCI_CUPS] [varchar](32) NOT NULL,
	[SCCI_MinEco] [varchar](3) NOT NULL,
	[SCCI_IdInterrupcion] [uniqueidentifier] NULL,
	[SCCI_FechaInterrupcion] [smalldatetime] NULL,
	[SCCI_Duracion] [int] NULL,
	[SCCI_Origen] [char](1) NULL,
	[SCCI_Zona] [int] NULL,
	[SCCI_Provincia] [varchar](2) NULL,
	[SCCI_Municipio] [varchar](3) NULL,
	[SCCI_Tipo] [int] NULL,
 CONSTRAINT [PK_SCADA_TB3] PRIMARY KEY CLUSTERED 
(
	[SCCI_IdCalidadIndividual] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SCADA_CalidadIndividual2](
	[SCCI_IdCalidadIndividual] [int] IDENTITY(1,1) NOT NULL,
	[SCCI_CUPS] [varchar](32) NOT NULL,
	[SCCI_MinEco] [varchar](3) NOT NULL,
	[SCCI_IdInterrupcion] [uniqueidentifier] NULL,
	[SCCI_FechaInterrupcion] [smalldatetime] NULL,
	[SCCI_Duracion] [int] NULL,
	[SCCI_Origen] [char](1) NULL,
	[SCCI_Zona] [int] NULL,
	[SCCI_Provincia] [varchar](2) NULL,
	[SCCI_Municipio] [varchar](3) NULL,
	[SCCI_Tipo] [int] NULL,
 CONSTRAINT [PK_SCADA_TB5] PRIMARY KEY CLUSTERED 
(
	[SCCI_IdCalidadIndividual] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Temp_Anterior](
	[SCCI_IdCalidadIndividual] [int] IDENTITY(1,1) NOT NULL,
	[SCCI_CUPS] [varchar](32) NOT NULL,
	[SCCI_MinEco] [varchar](3) NOT NULL,
	[SCCI_IdInterrupcion] [uniqueidentifier] NULL,
	[SCCI_FechaInterrupcion] [smalldatetime] NULL,
	[SCCI_Duracion] [int] NULL,
	[SCCI_Origen] [char](1) NULL,
	[SCCI_Zona] [int] NULL,
	[SCCI_Provincia] [varchar](2) NULL,
	[SCCI_Municipio] [varchar](3) NULL,
	[SCCI_Tipo] [int] NULL,
 CONSTRAINT [PK_SCADA_TB2] PRIMARY KEY CLUSTERED 
(
	[SCCI_IdCalidadIndividual] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


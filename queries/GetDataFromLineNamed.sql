SELECT   Ins.Nombre , ATV.X , ATV.Y, ATV.ApoyoSecuencial, ATV.CompartidaCodigo,ATV.Subterraneo, Cond.Descripcion
FROM     ATVano ATV, Instalaciones Ins,ATVanoTiposConductores Cond
WHERE       ATV.IdInst = Ins.IdInst
	AND Ins.Nombre = 'Circunvalación Este'
    AND ATV.IdTipoConductor=Cond.IdTipoConductor
ORDER BY ATV.ApoyoSecuencial;




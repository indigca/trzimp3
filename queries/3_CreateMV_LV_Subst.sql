--------------------------------------------------
-- Create Table dbo.MV_LV_SUBSTATION
--------------------------------------------------
Create table dbo.MV_LV_SUBSTATION (
    CODE                           NVARCHAR(32)                   not null,
    CONSTRUCTION_TYPE              NVARCHAR(3)                    null,
    TYPE                           NVARCHAR(10)                   null,
    OWNER                          NVARCHAR(40)                   null,
    INSTALLATION_DATE              datetime                       null,
    INSPECTION_YEAR                DECIMAL(9)                     null,
    INSPECTOR                      NVARCHAR(4)                    null,
    CHANGE_DATE                    datetime                       null,
    CHANGE_REASON                  NVARCHAR(3)                    null,
    MOUNTING                       NVARCHAR(3)                    null,
    OVERVOLTAGE_PROTECTION         NVARCHAR(3)                    null,
    OVERVOLTAGE_PROTECTION_SITE    NVARCHAR(3)                    null,
    GAP_TYPE                       NVARCHAR(3)                    null,
    GAP_SITE                       NVARCHAR(3)                    null,
    GAP                            DECIMAL(9)                     null,
    UNSUPPLIED                     DECIMAL(9) Default (0)         null,
    SUBSYMBOLCODE                  DECIMAL(9) Default (0)         not null,
    LV_NETWORK_PROT                DECIMAL(9)                     null,
    MUN                            NVARCHAR(4)                    null,
    COMPANY                        NVARCHAR(3)                    null,
    PROVINCE                       NVARCHAR(3)                    null)  ;
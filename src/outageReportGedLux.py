'''
Created on 15/07/2013

@author: Marco Guida
'''
import wx
from db import DataBase
import TrazaImport
import utiDMS
import os
import hashlib
from ImageEnhance import Color
from wx._gdi import Colour
import string
import random





class Main(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, 'Outage Report Gedlux', 
                size=(590, 490),style= wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
        self.panel = wx.Panel(self, -1)
        logoGedlux = wx.Image('logo_gedlux.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.userid = os.environ.get( "USERNAME" ).lower()
        wx.StaticBitmap(self.panel, -1, logoGedlux, (380, 10), (logoGedlux.GetWidth(), logoGedlux.GetHeight()))
       
        self.Centre()
        
        self.clientes=0
        
        'Connect to DMS600'
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        self.cursorD = self.cnxnD.cursor()
        
        self.lblDistribuidora = wx.StaticText(self.panel, label="Distribuidora :",pos=(5,5))
        self.tbDistribuidora = wx.TextCtrl(self.panel, pos=(85,3), size=(-1, -1))
        self.tbDistribuidora.Disable()
        
        self.lblCT = wx.StaticText(self.panel, label="CT :",pos=(5,25))
        self.tbCT = wx.TextCtrl(self.panel, pos=(30,23), size=(155, -1))
        self.tbCT.SetLabel("Nombre no encontrado")
        self.tbCT.Disable()
        
        self.feederList=wx.StaticText(self.panel,-1,"Seleccione un Feeder",(5, 45))
        
        self.lblNumclientes = wx.StaticText(self.panel, label="Numero clientes : ",pos=(427,60))
        self.lblPotContratada = wx.StaticText(self.panel, label="Potencia contratada (suma) :",pos=(370,80))
        #self.lblNDE = wx.StaticText(self.panel, label="NDE :",pos=(483,100))
        
# COMENTAR EN PRODUCCION
        #self.userid = 'user054'
        
        self.userid=self.userid[-3:] 
        if DataBase.DevelopmentMode == 'YES' :
            print self.userid
        
        if not self.CompruebaSiExisteDistribuidora(self.userid):
            wx.MessageBox("Usuario incorrecto","Error", wx.OK | wx.ICON_ERROR)
            self.Close()
            return
        'Get List of feeders part 1 of 2'
        self.cursorD.execute("SELECT LVOutageReport2013.NUMBER\
                        ,LVOutageReport2013.Region\
                        ,LVOutageReport2013.VALIDATED\
                        ,OutageAreas2013.FEEDINGPOINT\
                         FROM LVOutageReport2013 inner join OutageAreas2013\
                         ON LVOutageReport2013.NUMBER=OutageAreas2013.NUMBER where LVOutageReport2013.Region = ?",self.userid)
        rows=self.cursorD.fetchall()
        
        if DataBase.DevelopmentMode == 'YES' :
            print 'registros BD: '+str(rows.__len__())
        if rows.__len__()==0:
            wx.MessageBox("No hay incidencias","Aviso", wx.OK | wx.ICON_EXCLAMATION)
            self.Close()
        
        self.sampleList=[]
        validacion=None
        feedingPoint=''
        
        for row in rows:
            if row.NUMBER == None:
                row.NUMBER = ""
            self.tbDistribuidora.SetLabel(str(row.Region))
            validacion=row.VALIDATED
            feedingPoint=row.FEEDINGPOINT
        #self.tbDistribuidora.SetBackgroundColour((255,0,0)) #RED
        
        'List of feeders'  
#        if rows.__len__()>0:
#            ultimoElemento= self.sampleList[self.sampleList.__len__()-1]
        
        if rows.__len__()>0 and validacion :
            wx.MessageBox("La  ultima incidencia introducida ya ha sido validada","Aviso", wx.OK | wx.ICON_QUESTION)
            self.Close()
            
            
        'Get List of feeders part 2 of 2'
        self.cursorD.execute("SELECT FEEDER,NAME FROM LV_SWITCH where FEEDER like ?",feedingPoint+ '%')
        rows=self.cursorD.fetchall()
        

        
        for row in rows:
            
            if row.FEEDER[-2:]=='IN':
                self.tbCT.SetLabel(row.NAME)
                if DataBase.DevelopmentMode == 'YES' :
                    print row.NAME
            else:
                self.sampleList.append(row.FEEDER)
       
        self.sociosList = wx.ListBox(self.panel, -1, (5, 60), (150, 400), self.sampleList, wx.LB_SINGLE)
        self.sociosList.Bind(wx.EVT_LISTBOX,self.OnSelect)       
        
        self.customerNodeList=[]       
        self.customerNode = wx.ListBox(self.panel, -1, (160, 60), (200, 400), self.customerNodeList, wx.LB_SINGLE)
        self.customerNode.Bind(wx.EVT_LISTBOX,self.OnSelectCustomerNode)


        self.aceptarButton = wx.Button(self.panel, -1, "Aceptar", pos=(505, 435))
        self.aceptarButton.Bind(wx.EVT_BUTTON, self.OnPressAceptarButton)
        self.aceptarButton.Disable()
     

         
        
    def OnSelect(self,event):
        if DataBase.DevelopmentMode == 'YES' :
            print self.sampleList[self.sociosList.GetSelection()]
        feederSeleccionado=self.sampleList[self.sociosList.GetSelection()]
        self.aceptarButton.Disable()
       
        try:
            self.cursorD.execute("SELECT CUSTOMER_NODE,NAME,LV_NETWORK,FEEDER\
                              FROM LV_CUSTOMER_NODE where FEEDER = ?",str(feederSeleccionado))
        except:
                wx.MessageBox("Problemas de conexion con la base de datos","Error", wx.OK|wx.ICON_ERROR)
        
        rows=self.cursorD.fetchall()
        
        self.customerNodeList=[]
        self.customerNodeListNodeCode=[]
        
        for row in rows:
            self.customerNodeList.append(row.NAME)
            self.customerNodeListNodeCode.append(row.CUSTOMER_NODE)
            
        self.customerNode.Destroy()
        self.customerNode = wx.ListBox(self.panel, -1, (160, 60), (200, 400), self.customerNodeList, wx.LB_MULTIPLE)
        self.customerNode.Bind(wx.EVT_LISTBOX,self.OnSelectCustomerNode)
        
        self.lblNumclientes.SetLabel("Numero clientes : ")
        self.lblPotContratada.SetLabel("Potencia contratada (suma) : ")

#        self.customerNode.Bind(wx.EVT_LISTBOX,self.OnSelectCustomerNode)
           
    def OnSelectCustomerNode(self,event):
           rows=self.customerNode.GetSelections()
           if DataBase.DevelopmentMode == 'YES' :
               print str(rows)
           if str(rows) == "()":
               self.aceptarButton.Disable()
           else:
               self.aceptarButton.Enable()
           
           self.clientes=0
           potTotal=0.0
           
           'Cantidad clientes'
           for row in rows:
               #print self.customerNodeList[row]
               #print self.customerNodeListNodeCode[row]
               try:
                   self.cursorD.execute("SELECT COUNT(*) as numElementos FROM CUSTOMER where CUSTOMER_NODE =? ",str(self.customerNodeListNodeCode[row]))
               except:
                   wx.MessageBox("Problemas de conexion con la base de datos","Error", wx.OK|wx.ICON_ERROR)
               cantidadclientes= self.cursorD.fetchone()
               #print prueba.numElementos
               self.clientes+=cantidadclientes.numElementos
               if DataBase.DevelopmentMode == 'YES' :
                   print self.clientes
               
               'Suma potencia contratada'
               self.cursorD.execute("SELECT SUM(CONTRACTED_POWER) AS PT FROM CUSTOMER where CUSTOMER_NODE =?",str(self.customerNodeListNodeCode[row]))
               potTotalContrat= self.cursorD.fetchone()
               
               if not potTotalContrat.PT is None:
                   potTotal+=float(potTotalContrat.PT)
               if DataBase.DevelopmentMode == 'YES' :
                   print potTotal
               
               
           self.lblNumclientes.SetLabel("Numero clientes : "+str(self.clientes))
           self.lblPotContratada.SetLabel("Potencia contratada (suma) : "+str(potTotal))



               
           
    def OnPressAceptarButton(self,event):
        if DataBase.DevelopmentMode == 'YES' :
            print self.customerNode.GetSelections()
        isSuccessOutageAreas2013=False
        isSuccessLVOutageReport2013=False
        
        if DataBase.DevelopmentMode == 'YES' :
            print str(self.userid)
        
        try:
            self.cursorD.execute("SELECT ID,DURATION FROM OutageAreas2013 where FEEDINGPOINT like ? ",str(self.userid)+'%')
        except:
            wx.MessageBox("Problemas de conexion con la base de datos","Error", wx.OK|wx.ICON_ERROR)
        rows=self.cursorD.fetchall()
        
        idOutageAreas2013=''
        duration=''
        
        for row in rows:
            duration=str(row.DURATION)
            idOutageAreas2013=str(row.ID)
        if DataBase.DevelopmentMode == 'YES' :
            print 'idOutageAreas2013 '+str(idOutageAreas2013)
            print 'duration '+str(duration)
            print 'horas '+str(self.convertHours(str(row.DURATION)))
            print 'CUST: '+str(self.clientes)
            print 'CUSTH '+str(self.clientes*self.convertHours(str(row.DURATION)))
        
        borders=''
        
        rows=self.customerNode.GetSelections()
           
        for row in rows:
               #print 'self.customerNodeList[row]'+self.customerNodeList[row]
               borders+= '#'+self.customerNodeListNodeCode[row]
        if DataBase.DevelopmentMode == 'YES' :
            print borders
        
        
        cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
    
        cursorW = cnxnW.cursor()
        try:
            cursorW.execute("UPDATE OutageAreas2013 SET BORDERS=?,CUST=?,CUSTH=? WHERE ID=? ",str(borders),str(self.clientes),str(self.clientes*self.convertHours(str(duration))),str(idOutageAreas2013))
            cnxnW.commit()      
            isSuccessOutageAreas2013=True
        except:
            cnxnW.rollback()
            isSuccessOutageAreas2013=False
            


        self.cursorD.execute("SELECT NUMBER,Operator FROM LVOutageReport2013 where REGION=  ? ",str(self.userid))
        rows=self.cursorD.fetchall()
    
        idLVOutageReport2013=''
        operator=''
        for row in rows:
            idLVOutageReport2013=str(row.NUMBER)
            operator=str(row.Operator)
            
#        print 'idLVOutageReport2013= '+idLVOutageReport2013    
#        print 'operator= '+operator
        operator=operator.replace('-GDLBasic', '')
        operator+='-GDLBasic'
#        print 'operator= '+operator
        cursorW = cnxnW.cursor()
        try:
            if DataBase.DevelopmentMode == 'YES' :
                print str(int(self.clientes))
                print str(float(self.clientes*self.convertHours(str(duration))))
                print str(operator)
                print str(int(idLVOutageReport2013))
            
            cursorW.execute("UPDATE LVOutageReport2013 SET Cust=?,Custh=?,Operator=? WHERE NUMBER=? ",int(self.clientes),float(self.clientes*self.convertHours(str(duration))),str(operator),int(idLVOutageReport2013))
            cnxnW.commit()
            isSuccessLVOutageReport2013=True
        except:
            cnxnW.rollback()
            isSuccessLVOutageReport2013=False
            
        if isSuccessOutageAreas2013 and isSuccessLVOutageReport2013:
                wx.MessageBox("Los datos de incidencia han sido actualizados","Datos actualizados", wx.OK|wx.ICON_INFORMATION)
        else:
                wx.MessageBox("Problemas de conexion con la base de datos","Error", wx.OK|wx.ICON_ERROR)

    

    
    def convertHours(self,horas):
        horaMinSeg=horas.split(':')
        numHoras=float(horaMinSeg[0])
        numMin=float(horaMinSeg[1])
        numSeg=float(horaMinSeg[2])
        
#        
#        print numHoras
#        print numMin
#        print numSeg
#                
        calculo=numHoras+(numMin/60)+(numSeg/60/60)
        
        #print "calculo "+str(calculo)   
        
        return calculo
        
        
                
    def CompruebaSiExisteDistribuidora(self,MINECO):
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        self.cursorD = self.cnxnD.cursor()
                   
        sentencia = "SELECT usuario FROM gdlTrazaImportUser where usuario = 'user%s'" % str(MINECO)
        try:
            self.cursorD.execute(sentencia)
            rows=self.cursorD.fetchall()
        except:
            return
            self.cnxnD.commit()
                    
        if DataBase.DevelopmentMode == 'YES' :            
            print rows.__len__()
        if rows.__len__() == 0:
            if DataBase.DevelopmentMode == 'YES' :
                print 'la distribuidora NO existe'
            return False
                        
        else:
            if DataBase.DevelopmentMode == 'YES' :
                print 'la distribuidora EXISTE'
            return True
               
if __name__ == '__main__': 

    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
    
   
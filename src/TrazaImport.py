# -*- coding: iso-8859-15 -*-
'''
Created on 31/01/2011

@author: n1
'''
import wx
import utiDMS
import pyodbc
import ConfigParser
import trazaCreate
import LVline
import formMapsMG
import formMaps
import os
import datetime
import logging
import logging.handlers
import markup
import HTMLCreate
import cStringIO
import check0
import sys
import hashlib
import importExportExcel
#DESCOMENTAR EN PRODUCCION
# sys.setdefaultencoding('iso-8859-15')

from db import DataBase
from lib2to3.pytree import convert


"CREACION DEL LOG"
# Establecer un logger con el nivel de salida deseado
log = logging.getLogger('MiLogger')

logging.basicConfig(level=logging.DEBUG, filename='log/info.log', format="%(asctime)s - %(message)s")

MINECOList=[]
isAvanzado=False
fallos=None

def WriteLog(text):
    if DataBase.LogEnabled == 'YES' :
        log.info(text)

def WritePage():
    t2 = sorted(file('log/info.log').readlines(),reverse=True)
    page = markup.page( )
    page.p( t2 )
    return page

def WriteHTML():
    page = WritePage()
    f1=open('./HTML/header-log.html','rt')
    f3=open('./HTML/footer-bgcontent.html','rt')
    t1=f1.read()
    t3=f3.read()
    f1.close()
    f3.close()
    f2=open('log/log.html','wt')
    print >> f2,t1
    print >> f2,page
    print >> f2,t3
    f2.close()
    HTMLCreate.fileLevel1()
    HTMLCreate.fileLevel2()

class Main(wx.Frame):
    def __init__(self):
        
        wx.Frame.__init__(self, None, -1, 'Importacion de Traza', 
                size=(700, 605),style= wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX )
        self.Bind(wx.EVT_CLOSE, self.OnCloseMe)
        panel = wx.Panel(self, -1)
        
#        print self.GetBackgroundColour()
#        print panel.GetBackgroundColour()
        self.Centre()
        'Connect to DMS600'
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        
        user = os.environ.get( "USERDOMAIN" ) + "\\" + os.environ.get( "USERNAME" )
        #user = "SCADA\gedlux"
#DESHABILITAR
        #if DataBase.DevelopmentMode == 'YES' :
            #user = "CIDE-DES\gedlux"
            #user = "SCADA\gedlux"
            #user = "SCADA_DMS\gedlux"
            #user = "SCADA_DMS\user227"

#        WriteLog("Usuario Conectado " + user)
#        if user[0:4] == "user":
#            if user[4:8] == "000":
#                userid = ""
#            else:
#                userid = user[4:8]
#        else:
#            userid = ""
        'Get List of Socios'
#        cursorD.execute("SELECT L.Name, L.ID ,L.DMS\
#                 FROM gdlMineco L WHERE L.DMS IS NOT NULL AND L.ID LIKE ? AND ID in ('054','061','083','101','124','133','166','180') ORDER BY L.ID", userid + '%')
        cursorD.execute("SELECT L.Name, L.ID ,L.DMS\
                 FROM gdlMineco L WHERE L.ID IN (SELECT COMPANY FROM gdlReportCompany WHERE USERID LIKE ?)",user)
        rows=cursorD.fetchall()
        sampleList=[]
        
        if rows[0].DMS=='A':
                #print 'Avanzado'
                isAvanzado=True
        else:
                isAvanzado=False
        
        for row in rows:
            if row.DMS == None:
                row.DMS = ""
            sampleList.append(row.ID+row.DMS+"-"+row.Name)
            MINECOList.append(row.ID)
            
            
            #print MINECOList[0]
            
        self.selectedDistLabel=wx.StaticText(panel,-1,"Seleccione distribuidora",(20,2),size=(140,-1),style=wx.ALIGN_LEFT | wx.ST_NO_AUTORESIZE )

        'List of Socios'    
        self.sociosList = wx.ListBox(panel, -1, (20, 20), (250, 80), sampleList, 
                wx.LB_SINGLE)
        self.sociosList.SetSelection(-1)
        self.sociosList.Bind(wx.EVT_LISTBOX,self.OnSelect)
        
        'Close Button'
#        self.okButton = wx.Button(panel, -1, "Salir", pos=(300, 540))
#        self.okButton.Bind(wx.EVT_BUTTON, self.OnCloseMe)
        
        'Change Password Button'
        bmpCambiarContrasenha = wx.Image("CambiarContrasenha.png", wx.BITMAP_TYPE_ANY).Rescale(56.5,48.5).ConvertToBitmap()
        self.btnCngContras = wx.BitmapButton(panel, -1, bmpCambiarContrasenha, pos=(605, 425))
        self.btnCngContras.Bind(wx.EVT_BUTTON, self.OpenChgPwdFrame)
        
#        self.chgPwdButton = wx.Button(panel, -1, "Cambiar contrase�a", pos=(565, 425))
#        self.chgPwdButton.Bind(wx.EVT_BUTTON, self.OpenChgPwdFrame)
        
        'Display Selected Socio'
        self.selectedSocio="Ninguna Seleccion"
        self.selectedSocioLabel=wx.StaticText(panel,-1,self.selectedSocio,(275,20),size=(140,-1),style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE )
        self.MINECO=""
        'Display Xmax,Ymax,Xmin,Ymin'
#        self.minxLabel=wx.StaticText(panel,-1,"MinX",pos=(350,20),size=(30,-1))    
#        self.minxText=wx.TextCtrl(panel,-1,"000000",size=(70,-1),style=wx.TE_READONLY,pos=(380,17))
#        self.maxxLabel=wx.StaticText(panel,-1,"MaxX",pos=(460,20),size=(30,-1))    
#        self.maxxText=wx.TextCtrl(panel,-1,"000000",size=(70,-1),style=wx.TE_READONLY,pos=(490,17))
#        
#        self.minyLabel=wx.StaticText(panel,-1,"MinY",pos=(350,40),size=(30,-1))    
#        self.minyText=wx.TextCtrl(panel,-1,"000000",size=(70,-1),style=wx.TE_READONLY,pos=(380,37))
#        self.maxyLabel=wx.StaticText(panel,-1,"MaxY",pos=(460,40),size=(30,-1))    
#        self.maxyText=wx.TextCtrl(panel,-1,"000000",size=(70,-1),style=wx.TE_READONLY,pos=(490,37))
        

        
        'Display List of CTs'
        self.CTsText="Listado de CTs"
        self.CTsLabel=wx.StaticText(panel,-1,self.CTsText,pos=(20,110),size=(80,-1))
        self.CTsList = wx.ListBox(panel, -1, (20, 130), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
        self.importCTsButton= wx.Button(panel, -1, "4. Importar CTs", pos=(450, 35))
        self.importCTsButton.Bind(wx.EVT_BUTTON, self.OnSelectCTsButton)
        self.importCTsButton.Hide()
        self.importACTButton= wx.Button(panel, -1, "Importar CT(s) seleccionado(s)", pos=(170, 105))
        self.importACTButton.Bind(wx.EVT_BUTTON, self.OnSelectACTsButton)
        self.importACTButton.Hide()

        'Display List of Lines'
        self.LinesText="Listado de Lineas"
        self.LinesLabel=wx.StaticText(panel,-1,self.LinesText,pos=(350,110),size=(120,-1))
        self.LinesList = wx.ListBox(panel, -1, (350, 130), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
        self.importLinesButton= wx.Button(panel, -1, "5. Importar Lineas", pos=(450, 55))
        self.importLinesButton.Bind(wx.EVT_BUTTON, self.OnSelectLinesButton)
        self.importLinesButton.Hide()
        self.importALine= wx.Button(panel, -1, "Importar Linea seleccionada", pos=(515, 105))
        self.importALine.Bind(wx.EVT_BUTTON, self.OnSelectALineButton)
        self.importALine.Hide()
        
        
        'Display List of Transformers'
        self.TrafosText="Listado de Transformadores"
        self.TrafosLabel=wx.StaticText(panel,-1,self.TrafosText,pos=(20,260),size=(140,-1))
        self.TrafosList = wx.ListBox(panel, -1, (20, 280), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
        self.importTrafosButton= wx.Button(panel, -1, "3. Importar Trafos", pos=(450, 15))
        self.importTrafosButton.Bind(wx.EVT_BUTTON, self.OnSelectTrafosButton)
        self.importTrafosButton.Hide()
        
        'Display List of Customers connected'
        self.CustomersText="Listado de Clientes asociados (100 primeros)"
        self.CustomersLabel=wx.StaticText(panel,-1,self.CustomersText,pos=(450,260),size=(140,-1))
        self.CustomersList = wx.ListBox(panel, -1, (350, 280), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
        
        self.importCustomersButton= wx.Button(panel, -1, "2. Importar Clientes", pos=(450, 75))
        self.importCustomersButton.Bind(wx.EVT_BUTTON, self.OnSelectCustomersButton)
        self.importCustomersButton.Hide()
        
        'Borrar datos distribuidora'
        self.DeleteDistributorButton = wx.Button(panel, -1, "Borrar distribuidora", pos=(565, 35))
        self.DeleteDistributorButton.Bind(wx.EVT_BUTTON, self.OnDeleteDistributor)
        self.DeleteDistributorButton.Hide()
        
        
        
        'Borrar todas las distribuidoras'
        
        self.DeleteAllDistributorsButton = wx.Button(panel, -1, "Borrar todas las distribuidoras", pos=(525, 65))
        self.DeleteAllDistributorsButton.Bind(wx.EVT_BUTTON, self.OnDeleteAllDistributors)
        self.DeleteAllDistributorsButton.Hide()
        
        'CheckBox Develop mode'
        'You must comment this line if you are in production mode'

#        self.DevelopModeCheckbox = wx.CheckBox(panel, -1, " <- Develop Mode", pos=(430, 0))
#        self.DevelopModeCheckbox.Bind(wx.EVT_CHECKBOX, self.OnSelectDevelopModeCheckbox)
        print 'Check Development mode: '+DataBase.DevelopmentMode
        print 'Poner YES (todo MAYUSCULAS para entrar en Development mode)'
        if DataBase.DevelopmentMode == 'YES' :
            self.importCTsButton.Show()
            self.importLinesButton.Show()
            self.importTrafosButton.Show()
            self.importCustomersButton.Show()
            self.importACTButton.Show()
            self.importALine.Show()
            self.DeleteAllDistributorsButton.Show()
            self.DeleteDistributorButton.Show()
        
        'Import Maps Button'
        self.importMapsButton = wx.Button(panel, -1, "Importar Mapas", pos=(580, 5))
        self.importMapsButton.Bind(wx.EVT_BUTTON, self.OnSelectMapsButton)
        

#        if userid == "":

#        if userid == "":
#            self.SubestacionCheckbox = wx.CheckBox(panel, -1, "Borrar subestaci�n", pos=(525, 95))
        
        'Importar todo'
        self.ImportAllButton = wx.Button(panel, -1, "Importar", pos=(307, 40))
        self.ImportAllButton.Bind(wx.EVT_BUTTON, self.OnImportAll)
        
        'Informe TrazaImport'
        self.LogButton = wx.Button(panel, -1, "Informe TrazaImport", pos=(410, 425))
        self.LogButton.Bind(wx.EVT_BUTTON, self.Log)
        
        'Infome'
        self.InformeTrazaCheckButton = wx.Button(panel, -1, "Informe TrazaCheck", pos=(285, 425))
        self.InformeTrazaCheckButton.Bind(wx.EVT_BUTTON, self.InformeTrazaCheck)
        
        
        'TrazaCheck'
#        self.TrazaCheckButton = wx.Button(panel, -1, "TrazaCheck", pos=(200, 425))
#        self.TrazaCheckButton.Bind(wx.EVT_BUTTON, self.TrazaCheck)
        print self.InformeTrazaCheckButton.GetForegroundColour()
        print self.InformeTrazaCheckButton.GetBackgroundColour()
        
        'TrazaCheck TODAS LA LISTA DE DISTRIBUIDORAS'
        if DataBase.DevelopmentMode == 'YES' :
            self.TrazaCheckALLButton = wx.Button(panel, -1, "TrazaCheck-ALL", pos=(100, 425))
            self.TrazaCheckALLButton.Bind(wx.EVT_BUTTON, self.TrazaCheckALL)
        
        self.ImportMTCheckbox = wx.CheckBox(panel, -1, "Media Tensi�n", pos=(307, 70))
        self.ImportMTCheckbox.Value = True
        
        if isAvanzado:
            self.ImportBTCheckbox = wx.CheckBox(panel, -1, "Baja tensi�n", pos=(307, 85))
        else:
            self.ImportBTCheckbox = wx.CheckBox(panel, -1, "Clientes", pos=(307, 85))
            
        if DataBase.DevelopmentMode == 'YES' :
            'Orden de celdas'
#            self.lbBackup = wx.StaticText(panel, label="Archivo de orden de celdas: ", pos=(50,455))
#            self.tbBackup = wx.TextCtrl(panel, size=(200, -1), pos=(200,455))
#            self.btnFile = wx.Button(panel, label="Seleccionar archivo", pos=(410,455))
#            self.btnFile.Bind(wx.EVT_BUTTON, self.onOpenFile)
            self.btnOrdenCeldas = wx.Button(panel, label="Orden Celdas", pos=(200,425))
            self.btnOrdenCeldas.Bind(wx.EVT_BUTTON, self.onOrdenCeldas)
        
        'Logo'        
#        logoCide = wx.Image('logo_cide2.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
#        wx.StaticBitmap(panel, -1, logoCide, (-10, 490), (logoCide.GetWidth(), logoCide.GetHeight()))

#        logoGedlux = wx.Image('logo_gedlux.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
#        wx.StaticBitmap(panel, -1, logoGedlux, (500, 530), (logoGedlux.GetWidth(), logoGedlux.GetHeight()))

        bmpGdl = wx.Image("logo_gedlux.png", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btnLogoGdl = wx.BitmapButton(panel, -1, bmpGdl, pos=(500, 530))
        self.btnLogoGdl.Bind(wx.EVT_BUTTON, self.gotoPagWebGdl)
        
        
        bmpCIDE = wx.Image("logo_cide2.png", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btnLogoCIDE = wx.BitmapButton(panel, -1, bmpCIDE, pos=(1, 468))
        self.btnLogoCIDE.Bind(wx.EVT_BUTTON, self.gotoPagWebCIDE)
        
        'flecha'        
        #self.panelFlecha = wx.Panel(self)
        flecha = wx.Image('flecha.png', wx.BITMAP_TYPE_ANY).Rescale(50,50).ConvertToBitmap()
        
        self.flechaStaticBitmap=wx.StaticBitmap(panel, -1, flecha, (320, 450), (flecha.GetWidth(), flecha.GetHeight()))
        self.flechaStaticBitmap.Hide()
        

        
        'Import Low Voltage'
#        self.importMapsButton = wx.Button(panel, -1, "6. Importar Baja Tension", pos=(465, 300))
#        self.importMapsButton.Bind(wx.EVT_BUTTON, self.OnSelectLVButton)
        
        
#        self.gauge = wx.Gauge(panel, -1, 50, size=(500, 25),pos=(100, 540))
#        
#    def updateGauge(self, value):
#        self.gauge.SetValue(value)
    def onOpenFile(self, event):
        wildcard = "zip file (*.zip)|*.zip"
        """
        Create and show the Open FileDialog
        """

        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultFile="",
            defaultDir="C:\Backups",
#            wildcard=wildcard,
            style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR
            )
        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPaths()
            for path in paths:
                self.tbBackup.SetValue(path)
        dlg.Destroy()
        
    def Log(self,event):
        print DataBase.log
        os.system("start iexplore.exe "+DataBase.log)
        
    def InformeTrazaCheck(self,event):
        global fallos
        
        if self.MINECO!="":
            if fallos is None:
                self.TrazaCheck(event)
                
            os.system("start iexplore.exe "+DataBase.Check+self.MINECO[0:3]+"/chequeo/")
            
            if fallos>0 and DataBase.SugerenciaErrores == 'YES' :
                dlg = wx.MessageBox('�Quieres consultar la guia de TrazaCheck para saber como solucionar los problemas de importaci�n?','Se han encontrado errores!', wx.YES_NO | wx.ICON_QUESTION)
                if dlg==wx.YES:
                    print 'YES!'
                    os.system("start iexplore.exe "+DataBase.Check+"/SolucionTrazaCheck.html")
                else:
                    print 'NO!'
        else:
            dlg = wx.MessageBox('Seleccione una distribuidora', 'Seleccione una distribuidora', 
                                            wx.OK | wx.ICON_INFORMATION)
    def TrazaCheck(self,event):
        
        if self.MINECO!="":
            self.InformeTrazaCheckButton.SetBackgroundColour((240, 240, 240,255)) #Default Grey
            self.InformeTrazaCheckButton.SetForegroundColour((0,0,0,255))
            self.flechaStaticBitmap.Hide()
            
            global fallos
            fallos=check0.check(self.MINECO[0:3])
            
            
            if fallos>0:
                mensaje='NO SE PUEDE IMPORTAR TODAV�A, consulte el Informe de datos de traza creado, se han encontrado '+str(fallos)+' errores!'
            else:
                mensaje='Informe de datos de traza creado, no se han encontrado errores, puede proceder con la importaci�n.'
                
            dlg = wx.MessageBox(mensaje, 'Operacion Finalizada', 
                                            wx.OK | wx.ICON_INFORMATION)
        else:
            dlg = wx.MessageBox('Seleccione una distribuidora', 'Seleccione una distribuidora', 
                                            wx.OK | wx.ICON_INFORMATION)        
            
    def TrazaCheckALL(self,event):
         global MINECOList
         
         for distribuidora in MINECOList:
             print distribuidora
             check0.check(distribuidora)
         
         print 'TrazaCheck ALL REALIZADO CORRECTAMENTE'
        
        
    def OnChgPwd(self,event):
        if len(self.tbPassword.GetLabel()) >= 4 and len(self.tbRepPassword.GetLabel()) >= 4:
            print 'contrase�a de por lo menos 4 caracteres'
            if self.tbPassword.GetLabel() == self.tbRepPassword.GetLabel():
                self.ChgPwdInBD(self.tbPassword.GetLabel())
            else:
                print 'distintos'
                wx.MessageBox("Las contrase�as introducidas NO COINCIDEN", "Error", wx.OK | wx.ICON_WARNING)
                self.tbPassword.SetLabel("")
                self.tbRepPassword.SetLabel("")
        else:
             wx.MessageBox("La contrase�a tiene que estar compuesta por lo menos de 4 caracteres", "Error", wx.OK | wx.ICON_WARNING)
             self.tbPassword.SetLabel("")
             self.tbRepPassword.SetLabel("")
             
    def ChgPwdInBD(self,password):
        print 'cambia la contrase�a en la Base de Datos'
        'Connect to DMS600'
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        userid = os.environ.get( "USERNAME" ).lower()
#        #BORRAR EN PRODUCCION
        userid = 'gedlux'
        
        password=self.tbPassword.GetLabel()
        user_reverse = userid[::-1]
        md5introducido = hashlib.md5(password+ user_reverse).hexdigest()
        sentencia = "update gdlTrazaImportUser set password= '%s' where usuario='%s'" % (md5introducido,userid)
        print sentencia
        try:
            cursorD.execute(sentencia)
        except:
            return
        self.cnxnD.commit()
        
        if self.isPasswordEqualTo(md5introducido)== True:
            print 'contrase�a igual'
            dialog= wx.MessageBox("La contrase�a ha sido modificada con exito!", "Contrase�a modificada", wx.OK | wx.ICON_INFORMATION)
        else:
            print 'contrase�a distinta'
            dialog= wx.MessageBox("No ha sido posible modificar la contrase�a", "ERROR", wx.OK | wx.ICON_ERROR)
            
        self.frame.Close()
    
    def isPasswordEqualTo(self,md5introducido):
        'Connect to DMS600'
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        userid = os.environ.get( "USERNAME" ).lower()
#       BORRAR EN PRODUCCION
#        userid = 'gedlux'
        
        sentencia = "SELECT password FROM gdlTrazaImportUser where usuario= '%s'" % userid
        print sentencia
        try:
            cursorD.execute(sentencia)
            row=str(cursorD.fetchone())
        except:
            return
        self.cnxnD.commit()
        
        print row
        print row[3:-4]
        
        if row[3:-4]==md5introducido:
            return True
        else:
            return False
        
       
    
    def OpenChgPwdFrame(self,event):
        print 'cambia contrase�a'
        
        self.frame = wx.Frame(None, -1, 'Cambiar contrase�a')
        self.frame.SetSize((325,150))
        self.frame.SetBackgroundColour((240,240,240)) 
        self.lblPassword = wx.StaticText(self.frame, label="Contrase�a nueva :",pos=(55,25))
        self.tbPassword = wx.TextCtrl(self.frame, pos=(160,20), size=(130, -1), style = wx.TE_PASSWORD)
        self.tbPassword.SetMaxLength(20)
        
        self.lblRepPassword = wx.StaticText(self.frame, label="Confirma Contrase�a nueva :",pos=(10,55))
        self.tbRepPassword = wx.TextCtrl(self.frame, pos=(160,50), size=(130, -1), style = wx.TE_PASSWORD)
        self.tbRepPassword.SetMaxLength(20)
        
        self.AceptarButton= wx.Button(self.frame, -1, "Aceptar", pos=(215, 80))
        self.AceptarButton.Bind(wx.EVT_BUTTON, self.OnChgPwd)
        
        self.frame.Centre()
        self.frame.Show()
        
        
    def OnSelect(self,event):
        global fallos
        fallos=None
        try:
            self.selectedSocio=self.sociosList.GetString(self.sociosList.GetSelection())
        except:
            return
        self.selectedSocioLabel.SetLabel(self.selectedSocio)
        self.MINECO=self.selectedSocio[:self.selectedSocio.find("-")]
        self.InformeTrazaCheckButton.SetBackgroundColour((240, 240, 240,255)) #Default Grey
        self.InformeTrazaCheckButton.SetForegroundColour((0,0,0,255))
        self.flechaStaticBitmap.Hide()
        'Connect to Traza Database'
#        WriteLog('Abriendo base de datos distribuidor n�mero'+self.MINECO)
        

      
        self.cnxnT=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable,DataBase.TRAZAUser,DataBase.TRAZAPassword)
        cursorT = self.cnxnT.cursor()
        
       
            
       
        'Populate CTs List'
        cursorT.execute("SELECT Distinct Ins.Nombre \
                 FROM Instalaciones Ins,  CT CT   \
                 WHERE  Ins.IdInst=CT.IdInst and Ins.IdInst LIKE ?", self.MINECO[0:3] + '%')
        rows=cursorT.fetchall()
        CTs=[]
        for row in rows:
            CTs.append(row.Nombre)
        self.CTsList.Clear()
        self.CTsList.InsertItems(CTs,0)
        self.CTsLabel.SetLabel(self.CTsText+' ('+str(len(rows))+')')
#        WriteLog('CTs cargados correctamente')      
        'Populate Lines List'
        cursorT.execute("SELECT Distinct Ins.Nombre \
                 FROM Instalaciones Ins,  ATVano ATV   \
                 WHERE  Ins.IdInst=ATV.IdInst and Ins.IdInst LIKE ?", self.MINECO[0:3] + '%')
        rows=cursorT.fetchall()
        Lines=[]
        for row in rows:
            Lines.append(row.Nombre)
        self.LinesList.Clear()
        self.LinesList.InsertItems(Lines,0)
        self.LinesLabel.SetLabel(self.LinesText+' ('+str(len(rows))+')')
 #       WriteLog('Lineas cargadas correctamente')
        'Populate Trafos List'
        cursorT.execute("SELECT Ins.Nombre,\
                                    Trf.Potencia, \
                                    Trf.NumeroTransformador \
                            FROM    CTTransformadores Trf, Instalaciones Ins WHERE Trf.idInst=Ins.IdInst and Ins.IdInst LIKE ?", self.MINECO[0:3] + '%')
        rows=cursorT.fetchall()
        Trafos=[]
        for row in rows:
                Trafos.append(row.Nombre+"-"+str(row.NumeroTransformador)+"-"+str(row.Potencia))
        self.TrafosList.Clear()
        self.TrafosList.InsertItems(Trafos,0)
        self.TrafosLabel.SetLabel(self.TrafosText+' ('+str(len(rows))+')')
#        WriteLog('Transformadores cargados correctamente')
        
        'Populate Customers List'       
        cursorT.execute("  SELECT  TOP 100 Cli.CUPS, Ins.Nombre, Trf.NumeroTransformador \
                    FROM     Clientes Cli, \
                    CTTransformadores Trf, \
                    Instalaciones Ins,\
                    CT CT\
                    WHERE Ins.IdInst LIKE ? AND Ins.IdInst = Cli.IdInst and Cli.IdInst = CT.IdInst and Cli.IdTransformador=Trf.IdTransformador\
                    ORDER BY Ins.Nombre ", self.MINECO[0:3] + '%')
        rows=cursorT.fetchall()
        Customers=[]
        for row in rows:
#                Customers.append(row.Nombre)
                Customers.append(row.Nombre+"-"+str(row.NumeroTransformador)+"-"+row.CUPS)
        self.CustomersList.Clear()
        self.CustomersList.InsertItems(Customers,0)
        self.CustomersLabel.SetLabel(self.CustomersText+' ('+str(len(rows))+')')
#        WriteLog('Clientes cargados correctamente')  
#        'Populate Map Coordinates'
#        cursorT.execute("Select MIN(X) as MinX, MAX(X) as MaxX,\
#                                   MIN(Y) as MinY, MAX(Y) as MaxY\
#                                   from ATVano")
#        row=cursorT.fetchone()
#        self.minxText.Clear()
#        self.minxText.AppendText(str(row[0]))
#           
#        self.maxxText.Clear()
#        self.maxxText.AppendText(str(row[1]))
#           
#        self.minyText.Clear()
#        self.minyText.AppendText(str(row[2]))
#           
#        self.maxyText.Clear()
#        self.maxyText.AppendText(str(row[3])) 
        
    def OnCloseMe(self,event):
        dlg = wx.MessageDialog(self,
                "�Est� seguro de que quiere salir de TrazaImport?",
                "Mensaje de confirmaci�n", wx.OK|wx.CANCEL|wx.ICON_QUESTION)
        result = dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_OK:
            sys.exit()
        
#        self.Close(True)
        
    def OnSelectMapsButton (self,event):
        formMaps.open()
    
        
    def OnSelectLVButton (self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM [dbo].[LV_SECTION] \
                              WHERE LV_NETWORK LIKE ?", self.MINECO[0:3] + '%')
            linelvNumber = cursorD.fetchone()
            if linelvNumber == None:
                if isAvanzado:
                    WriteLog("Importacion lineas BT  iniciada")
                else:
                    WriteLog("Importacion clientes (Basico) iniciada")
                    
                LVline.Import(self.MINECO[0:4])
                if isAvanzado:
                    WriteLog("Importacion lineas BT  finalizada")
                else:
                    WriteLog("Importacion clientes (Basico) finalizada")
            else:
                if isAvanzado:
                    dlg = wx.MessageBox('Baja tensi�n ya importada', 'Baja tensi�n ya importada', wx.OK | wx.ICON_INFORMATION)
                else:
                    dlg = wx.MessageBox('Clientes (Basico) ya importados', 'Clientes ya importada', wx.OK | wx.ICON_INFORMATION)
        
    def OnSelectLinesButton(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM [dbo].[MV_SECTION] \
                              WHERE HIDE = 0 AND DISTRICT = ?", self.MINECO[0:3])
            lineNumber = cursorD.fetchone()
#            if lineNumber == None:
            if True: 
    
#                    print self.LinesList.GetStringSelection()
                    WriteLog("Importacion lineas MT iniciada")
                    trazaCreate.line(self.cnxnT,self.LinesList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importacion lineas MT finalizada")
                    WriteHTML()
            else:
                dlg = wx.MessageBox('L�neas ya importadas', 'L�neas ya importadas', 
                    wx.OK | wx.ICON_INFORMATION)
  
    def OnSelectALineButton(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM [dbo].[MV_SECTION] \
                              WHERE HIDE = 0 AND DISTRICT = ?", self.MINECO[0:3])
            lineNumber = cursorD.fetchone()
#            if lineNumber == None:
            if True: 
    
                    print self.LinesList.GetStringSelection()
                    WriteLog("Importacion SOLO LA LINEA MT "+self.LinesList.GetStringSelection())
                    trazaCreate.lineAlone(self.cnxnT,self.LinesList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importacion SOLO UNA LINEA MT finalizada")
                    WriteHTML()
                    
            else:
                dlg = wx.MessageBox('L�neas ya importadas', 'L�neas ya importadas', 
                    wx.OK | wx.ICON_INFORMATION)  
    
#    def OnSelectDevelopModeCheckbox(self,event):
#        if self.DevelopModeCheckbox.IsChecked():
#            self.importCTsButton.Show()
#            self.importLinesButton.Show()
#            self.importTrafosButton.Show()
#            self.importCustomersButton.Show()
#        else:
#            self.importCTsButton.Hide()
#            self.importLinesButton.Hide()
#            self.importTrafosButton.Hide()
#            self.importCustomersButton.Hide()
            

    
    def OnSelectCTsButton(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM MV_LV_SUBSTATION \
                              where CODE like ?", self.MINECO[0:3] + '%')
            ctNumber = cursorD.fetchone()
            if ctNumber == None:
                
                    WriteLog(self.CTsList.GetStringSelection())
                    WriteLog("Importacion CTs iniciada")
                    trazaCreate.CT(self.cnxnT,self.CTsList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importacion CTs finalizada")
                    WriteHTML()
            else:
                dlg = wx.MessageBox('Centros de transformaci�n ya importados', 'Centros de transformaci�n ya importados', 
                    wx.OK | wx.ICON_INFORMATION)
                
    def OnSelectACTsButton(self,event):
        if self.CTsList.GetSelection()>0:
            print self.CTsList.GetStringSelection()
        
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM MV_LV_SUBSTATION \
                              where CODE like ?", self.MINECO[0:3] + '%')
            ctNumber = cursorD.fetchone()
            if ctNumber == None:
                
                    WriteLog(self.CTsList.GetStringSelection())
                    WriteLog("Importando SOLO EL CT "+self.CTsList.GetStringSelection())
                    trazaCreate.CTAlone(self.cnxnT,self.CTsList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importacion SOLO UN CT finalizada")
            else:
                dlg = wx.MessageBox('Centros de transformaci�n ya importados', 'Centros de transformaci�n ya importados', 
                    wx.OK | wx.ICON_INFORMATION)
        

    def OnSelectTrafosButton(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM TRANSFORMER \
                              where CODE like ?", self.MINECO[0:3] + '%')
            transformerNumber = cursorD.fetchone()
            if transformerNumber == None:            
                WriteLog(self.CTsList.GetStringSelection())
                WriteLog(self.MINECO)
                WriteLog("Importacion trafos iniciada")
                trazaCreate.trafo(DataBase.TRAZATable,DataBase.DMSTable, self.MINECO)
                WriteLog("Importacion trafos finalizada")
                WriteHTML()
            else:
                dlg = wx.MessageBox('Transformadores ya importados', 'Transformadores ya importados', 
                    wx.OK | wx.ICON_INFORMATION)


    def OnSelectCustomersButton(self,event):
        cursorD = self.cnxnD.cursor()
        cursorD.execute("SELECT *\
                      FROM [dbo].[CUSTOMER]\
                      where LV_NETWORK like ?",self.MINECO[0:3] + '%')
        customersNumber = cursorD.fetchone()
        if customersNumber == None:
            if self.MINECO == "":
                dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
                wx.OK | wx.ICON_INFORMATION)
            else:
                WriteLog(self.CTsList.GetStringSelection())
                WriteLog(self.MINECO)
        #        try:
                WriteLog("Importacion clientes iniciada")
                trazaCreate.customer(self.cnxnT, self.MINECO)
                WriteLog("Importacion clientes finalizada")
                WriteHTML()
        #        except Exception:
        #            print Exception.message
        else:
            dlg = wx.MessageBox('Clientes ya importados', 'Clientes ya importados', 
                wx.OK | wx.ICON_INFORMATION)
        

    def OnDeleteDistributor(self,event):                          
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            dlg = wx.MessageBox('�Est� seguro de que quiere eliminar los datos de la distribuidora seleccionada?', 'Eliminar', 
            wx.YES_NO | wx.ICON_INFORMATION)
            if dlg == 2:
               
                self.ImportAllButton.Disable()
                self.DeleteDistributorButton.Disable()
                self.Refresh()
                
                
                WriteLog("Eliminando datos de la distribuidora")
#                if self.SubestacionCheckbox:
#                    trazaCreate.delete_distributor(self.MINECO)
#                else:
                trazaCreate.delete_distributor_without_substations(self.MINECO)
                WriteLog("Datos de la distribuidora eliminados")
                WriteHTML()
                dlg = wx.MessageBox('Datos de la distribuidora eliminados', 'Datos de la distribuidora eliminados', 
                                    wx.OK | wx.ICON_INFORMATION)
                
                self.ImportAllButton.Enable()
                self.DeleteDistributorButton.Enable()
        
    def OnDeleteAllDistributors(self,event):
        dlg = wx.MessageBox('�Est� seguro de que quiere eliminar todos los datos de todas las distribuidoras?', 'Eliminar', 
        wx.YES_NO | wx.ICON_INFORMATION)
        
        if dlg == 2:

            WriteLog("Eliminando datos de todas las distribuidoras")
            #if self.SubestacionCheckbox:
            trazaCreate.delete_all_distributors()
            #else:
            #   trazaCreate.delete_all_distributors_without_substations()
            WriteLog("Datos de todas las distribuidoras eliminados")
            WriteHTML()
            dlg = wx.MessageBox('Datos de las distribuidoras eliminados', 'Datos de las distribuidoras eliminados', 
                                    wx.OK | wx.ICON_INFORMATION)
        
    def OnImportAll(self,event):
        incidenciaImportacion=False
        
        print str(self.MINECO.replace("A", ""))
        print str(self.MINECO.replace("B", ""))
        
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            self.MINECO=self.MINECO.replace("A", "")
            self.MINECO=self.MINECO.replace("B", "")
            if  utiDMS.getValidacionTrazaCheck(self.MINECO)==False  and DataBase.SugerenciaErrores == 'YES' :
                wx.MessageBox( 'Antes de importar tiene que ejecutar TrazaCheck', 'Aviso',wx.OK | wx.ICON_INFORMATION)
                self.InformeTrazaCheckButton.SetBackgroundColour((255,0,0)) #RED
                self.flechaStaticBitmap.Show()
                return
            
            if self.ImportMTCheckbox.Value == False and self.ImportBTCheckbox.Value == False:
                self.ImportMTCheckbox.SetValue(True)
                       
            self.ImportAllButton.Disable()
            self.DeleteDistributorButton.Disable()
            
            if self.ImportMTCheckbox.Value == True:
                cursorD = self.cnxnD.cursor()
                cursorD.execute("SELECT *\
                                  FROM TRANSFORMER \
                                  where CODE like ?", self.MINECO[0:3] + '%')
                transformerNumber = cursorD.fetchone()
                if transformerNumber == None:
                    utiDMS.setValidacionTrazaCheck(self.MINECO.replace("A", ""),False)
                    WriteLog("Importando Transformadores")
                    trazaCreate.trafo(DataBase.TRAZATable,DataBase.DMSTable, self.MINECO)
                    WriteLog("Importando Centros de transformacion")
                    trazaCreate.CT(self.cnxnT,self.CTsList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importando Lineas de Alta y Media Tension")            
                    trazaCreate.line(self.cnxnT,self.LinesList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importando borderPoints")                    
                    trazaCreate.borderPoints(self.MINECO[0:3])
                    WriteLog("Importacion finalizada")
                    WriteHTML()
                    
#                    dlg = wx.MessageBox('Importaci�n realizada', 'Importaci�n realizada', 
#                                            wx.OK | wx.ICON_INFORMATION)
                else:
                    dlg = wx.MessageBox('Debe eliminar primero la media tensi�n para importarla de nuevo', 'MT ya importada', 
                                    wx.OK | wx.ICON_INFORMATION)
                    incidenciaImportacion=True
            if self.ImportBTCheckbox.Value == True:
                cursorD = self.cnxnD.cursor()
                cursorD.execute("SELECT *\
                                  FROM TRANSFORMER \
                                  where CODE like ?", self.MINECO[0:3] + '%')
                transformerNumber = cursorD.fetchone()
                if transformerNumber != None:
                    cursorD.execute("SELECT *\
                                      FROM CUSTOMER \
                                      where LV_NETWORK like ?", self.MINECO[0:3] + '%')
                    customerNumber = cursorD.fetchone()
                    if customerNumber == None:
                        WriteLog("Importando Clientes")
                        trazaCreate.customer(self.cnxnT, self.MINECO)
                        if isAvanzado:
                            WriteLog("Importando Lineas de Baja Tension")
                        LVline.Import(self.MINECO[0:4])
                        WriteLog("Importacion finalizada")
                        WriteHTML()
                    else:
                        if isAvanzado:
                            dlg = wx.MessageBox('Red de BT ya importada', 'BT ya importada',  wx.OK | wx.ICON_INFORMATION)
                        else:
                            dlg = wx.MessageBox('Red de Clientes ya importada', 'Clientes ya importada',  wx.OK | wx.ICON_INFORMATION)
                            
                        incidenciaImportacion=True
#                    dlg = wx.MessageBox('Importaci�n realizada', 'Importaci�n realizada', 
#                                            wx.OK | wx.ICON_INFORMATION)
                else:
                    dlg = wx.MessageBox('Debe importar primero la media tensi�n', 'MT no importada', 
                                    wx.OK | wx.ICON_INFORMATION)
                    incidenciaImportacion=True
            trazaCreate.separarLineas()
            
            if incidenciaImportacion!=True:
                dlg = wx.MessageBox('Importaci�n finalizada', 'Importaci�n finalizada', wx.OK | wx.ICON_INFORMATION)
            
            self.ImportAllButton.Enable()
            self.DeleteDistributorButton.Enable()
        
    def gotoPagWebGdl(self,event): 
                   os.system("start iexplore.exe www.gedlux.es")
                   
    def gotoPagWebCIDE(self,event): 
                   os.system("start iexplore.exe www.cide.net")
        
    def onOrdenCeldas(self, event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
                wx.OK | wx.ICON_INFORMATION)
        else:
            importExportExcel.openForm(self.MINECO)

#            trazaCreate.ordenCeldas(self.tbBackup.Value, self.MINECO)
#            dlg = wx.MessageBox('Finalizado', 'Orden de celdas actualizado', 
#                                                wx.OK | wx.ICON_INFORMATION)
        
            
        
#if __name__ == '__main__': 
def openForm():
    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
 

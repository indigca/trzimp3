# -*- coding: iso-8859-15 -*-
import wx
from db import DataBase
import utiDMS
import wx.grid as gridlib
import os

class Main(wx.Frame):    
    def __init__(self):
        wx.Frame.__init__(self, parent=None, title='Clientes',size=(700, 575))
        self.panel = wx.Panel(self)
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        
        # Distributor
        user = os.environ.get( "USERDOMAIN" ) + "\\" + os.environ.get( "USERNAME" )
        cursorD = self.cnxnD.cursor()
        cursorD.execute("SELECT L.Name, L.ID ,L.DMS\
                 FROM gdlMineco L WHERE L.ID IN (SELECT COMPANY FROM gdlReportCompany WHERE USERID LIKE ?)",user)
        
        rows=cursorD.fetchall()
        sampleList=[]
        for row in rows:
            sampleList.append(row.ID+row.DMS+"-"+row.Name)
        self.selectedDistLabel=wx.StaticText(self.panel,-1,"Seleccione distribuidora",(0,0),size=(140,-1),style=wx.ALIGN_LEFT | wx.ST_NO_AUTORESIZE )
        
        'List of Socios'
        self.cbDistributor = wx.ComboBox(self.panel, choices = sampleList, size=(140, -1), pos=(150, 0), style=wx.CB_READONLY)
        # Query
        queryHeight = 25
        # Column
        columnList = ["CUPS", "NOMBRE", "POTENCIA CONTRATADA", "CT"]
        self.cbColumn = wx.ComboBox(self.panel, choices = columnList, size=(140, -1), pos=(0, queryHeight), style=wx.CB_READONLY)
        self.cbColumn.SetValue("NOMBRE")
        
        # Operation
        operationList = ["MAYOR QUE", "IGUAL A", "MENOR QUE", "COMO"]
        self.cbOperation = wx.ComboBox(self.panel, choices = operationList, size=(140, -1), pos=(150, queryHeight), style=wx.CB_READONLY)
        self.cbOperation.SetValue("COMO")
        
        # Value
        self.tbValue = wx.TextCtrl(self.panel, size=(140, -1), pos=(300, queryHeight))
        
        # Button
        self.searchButton = wx.Button(self.panel, -1, "Buscar", pos=(450, queryHeight))
        self.searchButton.Bind(wx.EVT_BUTTON, self.OnSearch)
        
        # Operation Dictionary
        self.operationDict = {operationList[0]: ">", operationList[1]: "=", operationList[2]: "<", operationList[3]: "LIKE"}
        
        # Column Dictionary
        self.columnDict = {columnList[0]: "CUSTOMER.CODE", columnList[1]: "CUSTOMER.NAME", columnList[2]: "CUSTOMER.CONTRACTED_POWER", columnList[3]: "MV_SITE.NAME"}
        
        # Grid creation
        self.customersGrid = gridlib.Grid(self.panel, pos=(0, 50))
        
        self.Bind(gridlib.EVT_GRID_LABEL_LEFT_DCLICK, self.OnCellLeftDClick)

    def OnCellLeftDClick(self, evt):
        print self.customersGrid.GetCellValue(evt.GetRow(),0)
        print "OnCellLeftDClick: (%d,%d) %s\n" % (evt.GetRow(),
                                                  evt.GetCol(),
                                                  evt.GetPosition())
        evt.Skip()
        
    def OnSearch(self,event):
        
        # SQL Sentence and connection
        cursorD = self.cnxnD.cursor()
        column = self.columnDict[self.cbColumn.Value]
        operation = self.operationDict[self.cbOperation.Value]
        if operation == 'LIKE':
            value = "'%" + str(self.tbValue.Value) + "%'"
        else:
            value = "'" + str(self.tbValue.Value) + "'"
        sentence = "SELECT CUSTOMER.CODE CUPS, CUSTOMER.NAME NOMBRE, CUSTOMER.CONTRACTED_POWER POTENCIA, \
                    MV_SITE.NAME CT, RIGHT(CUSTOMER.LV_NETWORK, 1) TRANSFORMADOR\
                    FROM CUSTOMER, MV_SITE\
                    WHERE CUSTOMER.LV_NETWORK LIKE '" + self.cbDistributor.Value[0:3] + "%' AND LEFT(CUSTOMER.LV_NETWORK, \
                    LEN(CUSTOMER.LV_NETWORK) - 1) + '1' = MV_SITE.SITECODE AND \
                    " + column + " " + operation + " " + value
        sentence = "select * from [MVOutageReport2012] where Region = '045'"
        try:
            cursorD.execute(sentence)
        except:
            dlg = wx.MessageBox('Datos introducidos incorrectamente', 'Error', 
            wx.OK | wx.ICON_INFORMATION)
            return
        customers=cursorD.fetchall()
        
        # Grid definition
        numeroFilas = len(customers)
        self.customersGrid.Destroy()
        self.customersGrid = gridlib.Grid(self.panel, pos=(0, 50), size=(690, 495))
        self.customersGrid.CreateGrid(numeroFilas + 1, 5)
        
        # Grid columns definition
        self.customersGrid.SetColSize( 0, 150 )
        self.customersGrid.SetColSize( 1, 250 )
        self.customersGrid.SetColSize( 2, 150 )
        self.customersGrid.SetColSize( 3, 150 )
        self.customersGrid.SetColSize( 4, 150 )
        self.customersGrid.SetCellValue( 0, 0, "CUPS" );
        self.customersGrid.SetCellValue( 0, 1, "NOMBRE" );
        self.customersGrid.SetCellValue( 0, 2, "POTENCIA" );
        self.customersGrid.SetCellValue( 0, 3, "CT" );
        self.customersGrid.SetCellValue( 0, 4, "TRANSFORMADOR" );
        
        # Insert values into grid
        i = 0
        for customer in customers:
            i = i + 1
            columns = [customer.CUPS, customer.NOMBRE, customer.POTENCIA, customer.CT, customer.TRANSFORMADOR]
            for j in range(0,len(columns)):
                self.customersGrid.SetCellValue( i, j, str(columns[j]) );
                self.customersGrid.SetReadOnly( i, j )
        
if __name__ == '__main__': 

    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
 
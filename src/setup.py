from distutils.core import setup
import py2exe

setup(console=['formLogin.py'],
      data_files = [('', ['config.ini', 'logo_cide2.png', 'logo_gedlux.png','CambiarContrasenha.png','flecha.png'])],
      options = {
        'py2exe': {
            'includes': 'decimal',
            },
        })

#In order the program to work following dirs must be copied in /dist/:
#./log/*
#./HTML/*
#./htdocs/*

#def find_data_files(source,target,patterns):
#    if glob.has_magic(source) or glob.has_magic(target):
#        raise ValueError("Magic not allowed in src, target")
#    ret = {}
#    for pattern in patterns:
#        pattern = os.path.join(source,pattern)
#        for filename in glob.glob(pattern):
#            if os.path.isfile(filename):
#                targetpath = os.path.join(target,os.path.relpath(filename,source))
#                path = os.path.dirname(targetpath)
#                ret.setdefault(path,[]).append(filename)
#    return sorted(ret.items())
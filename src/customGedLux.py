'''
Created on Agu 07, 2013

@author: Marco Guida
'''
import utiGedLux
import wx
TRAZA='T'
DMS='D'

def getPopulate(MINECO):
    
    print str(MINECO)
    
    '''     *****QUERY POPULATEGDLCELDAS GEDLUX***** '''
    sentencia=   """DECLARE @wildcard nvarchar(4);
                 SET @wildcard = '"""+str(MINECO)+"""%';
                 WITH ct_no_celda AS
                 -- if we join to this subquery the listing should only contains CTs
                 -- that have no celdas
                 (
                 SELECT CT.IdInst
                     FROM CT
                         LEFT JOIN CTCeldas
                             ON CT.IdInst = CTCeldas.IdInst
                     WHERE CT.IdInst LIKE @wildcard AND CTCeldas.IdInst IS NULL
                 )
                  -- the first query gets the data from CTCeldas, which is where
                 -- we assume things are correct. For those that have no rows in
                 -- CTCeldas we fetch the data by deducing from the number of 
                 -- lines connected etc
                 SELECT InsCT.Codigo CodigoCT, ISNULL(InsLinea.Codigo, '1') AS CodigoLinea, CT.X, CT.Y, InsCT.Nombre, InsLinea.IdInst IdInstLinea,
                 CTCeldas.IdTipoCelda, InsCT.IdInst, CTCeldas.IdTransformador,
                 CASE WHEN DisyuntorFusible = 'Fusible' THEN 1 ELSE NULL END AS DisyuntorFusible,--OJO!!!!! ESTO ESTA BIEN?? NO ENTIENDO POR QUE SOLO DA OPCION DE FUSIBLE O NADA, CUANDO LAS OPCIONES VAN MUCHO MAS ALLA. INCLUSO HAY DOS TIPOS DE FUSIBLE. ESTO SE VE EN [traza].[dbo].[TipoManiobraProteccion]
                 --CTCeldas.DisyuntorFusible, esto es lo que pondria yo en su lugar, pero ojo! esto seria variable varchar y el codigo anterior usaba int.
                 CTCeldas.Modelo, NULL AS Transformador
                 FROM CTCeldas
                     LEFT JOIN CTDerivaciones
                         ON CTCeldas.IdDerivacion = CTDerivaciones.IdDerivacion
                     LEFT JOIN CTAlimentaciones
                         ON CTCeldas.IdAlimentacion = CTAlimentaciones.IdAlimentacion
                     LEFT JOIN CT
                         ON CTCeldas.IdInst = CT.IdInst
                     LEFT JOIN Instalaciones InsCT
                         ON CTCeldas.IdInst = InsCT.IdInst
                     LEFT JOIN Instalaciones InsLinea
                         ON (CTDerivaciones.IdInstDerivaciones = InsLinea.IdInst AND CTAlimentaciones.IdInstAlimentaciones IS NULL)
                         OR (CTAlimentaciones.IdInstAlimentaciones = InsLinea.IdInst AND CTDerivaciones.IdInstDerivaciones IS NULL)
                 WHERE CTCeldas.IdTipoCelda = 1 AND CTCeldas.Alta = 1 AND CTCeldas.IdInst LIKE @wildcard
                 UNION ALL
                 -- alimentationes (incoming lines)
                 SELECT  Instalaciones.Codigo AS CodigoCT,
                         ISNULL(Ins_ali.Codigo, '1') AS CodigoLinea,
                         CT.X,
                         CT.Y,
                         Instalaciones.Nombre,
                         CTAlimentaciones.IdInstAlimentaciones AS IdInstLinea,
                         '1' AS IdTipoCelda,
                         Instalaciones.IdInst,
                         NULL AS IdTransformador,
                         1 AS DisyuntorFusible,
                         NULL AS Modelo,
                         NULL AS Transformador
                 FROM    CT
                         JOIN ct_no_celda
                             ON ct_no_celda.IdInst = CT.IdInst
                         JOIN Instalaciones
                             ON CT.IdInst = Instalaciones.IdInst
                         JOIN CTAlimentaciones
                             ON CTAlimentaciones.IdInst = Instalaciones.IdInst
                         JOIN Instalaciones Ins_ali
                             ON Ins_ali.IdInst = CTAlimentaciones.IdInstAlimentaciones
                 WHERE Instalaciones.IdInst LIKE @wildcard
                 UNION ALL
                 -- derivaciones, or outgoing lines
                 SELECT    Instalaciones.Codigo AS CodigoCT,
                         ISNULL(Ins_der.Codigo, '1') AS CodigoLinea,
                         CT.X,
                         CT.Y,
                         Instalaciones.Nombre,
                         CTDerivaciones.IdInstDerivaciones AS IdInstLinea,
                         '1' AS IdTipoCelda,
                         Instalaciones.IdInst,
                         NULL AS IdTransformador,
                         1 AS DisyuntorFusible,
                         NULL AS Modelo,
                         NULL AS Transformador
                 FROM    CT
                         JOIN ct_no_celda
                             ON ct_no_celda.IdInst = CT.IdInst
                         JOIN Instalaciones
                             ON CT.IdInst = Instalaciones.IdInst
                         LEFT JOIN CTDerivaciones
                             ON CTDerivaciones.IdInst = Instalaciones.IdInst
                         JOIN AT
                             ON AT.IdInst = CTDerivaciones.IdInstDerivaciones
                             OR (AT.IdOrigenPunto = CT.IdInst AND IdTipoOrigen = 4)
                         JOIN Instalaciones AS Ins_der
                             ON Ins_der.IdInst = AT.IdInst
                 WHERE    Instalaciones.IdInst LIKE @wildcard
                 UNION ALL
                  -- transformer celdas
                 SELECT    Instalaciones.Codigo CodigoCT,
                         CTTransformadores.NumeroTransformador AS CodigoLinea,
                         CT.X,
                         CT.Y,
                         Instalaciones.Nombre,
                         NULL AS IdInstLinea,
                         '2' AS IdTipoCelda,
                         Instalaciones.IdInst,
                         CTTransformadores.IdTransformador,
                         NULL AS DisyuntorFusible,
                         NULL AS Modelo,
                         CTTransformadores.NumeroTransformador AS Transformador
                 FROM    CT
                         JOIN Instalaciones
                             ON CT.IdInst=Instalaciones.IdInst
                         JOIN CTTransformadores 
                             ON CTTransformadores.IdInst = Instalaciones.IdInst
                 WHERE    Instalaciones.IdInst LIKE @wildcard"""
    
    
    return utiGedLux.selectTrazaDms(sentencia,TRAZA)
    
def getExcel(MINECO):
    wx.MessageBox('Este modulo no esta disponible para esta dostribuidora','Informacion', wx.OK | wx.ICON_INFORMATION)
    return 'GedLux Excel'
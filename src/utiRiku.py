from math import sin, cos, tan, pi, sqrt

def deg2rad(deg):
    return (deg/360*2*pi)

#http://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system

def projection(lat, lon):
    e = 0.0818192
    a = 6378137 # meters
    k0 = 0.9996
    N0 = 0
    lat0 = 0
    lon0 = deg2rad(-3.0)
    E0 = 500000
    
    lat = deg2rad(lat)
    lon = deg2rad(lon)

    v = 1/sqrt(1-e**2*sin(lat)*sin(lat))
    A = (lon-lon0)*cos(lat)
    s = (1-e**2/4-3*e**4/64-5*e**6/256)*lat - (3*e**2/8+3*e**4/32+45*e**6/1024)*sin(2*lat)+(15*e**4/256+45*e**6/1024)*sin(4*lat)-35*e**6/3072*sin(6*lat)
    T = tan(lat)*tan(lat)
    C = e**2/(1-e**2)*cos(lat)*cos(lat)
    E = E0 + k0*a*v*(A+(1-T+C)*A**3/6 + (5-18*T+T**2)*A**5/120)
    N = N0+k0*a*(s+v*tan(lat)*(A**2/2+(5-T+9*C+4*C**2)*A**4/24+(61-58*T+T*T)*A**6/720))

    return [E, N]

'''
Created on 30/07/2012

@author: Marco

'''
import wx
from db import DataBase
import TrazaImport
import utiDMS
import os
import hashlib
from ImageEnhance import Color
from wx._gdi import Colour
import string
import random
import utiGedLux

class Main(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, 'Crear usuario TrazaImport', 
                size=(300, 250))
        panel = wx.Panel(self, -1)
        logoGedlux = wx.Image('logo_gedlux.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        logoAdmin = wx.Image('admin.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        wx.StaticBitmap(panel, -1, logoGedlux, (5, 110), (logoGedlux.GetWidth(), logoGedlux.GetHeight()))
        wx.StaticBitmap(panel, -1, logoAdmin, (195, 90), (logoAdmin.GetWidth()/1.5, logoAdmin.GetHeight()/1.5))
        self.Centre()
       
        #user = os.environ.get( "USERDOMAIN" ) + "\\" + os.environ.get( "USERNAME" )

#        cursorD.execute("SELECT L.Name, L.ID ,L.DMS\
#                 FROM gdlMineco L WHERE L.ID IN (SELECT COMPANY FROM gdlReportCompany WHERE USERID LIKE ?)","SCADA1\user000")
        #cursorD.execute("SELECT Name, ID\
         #        FROM gdlMineco")
      
#            sampleList.append(row.ID+row.DMS+"-"+row.Name)
         #   sampleList.append(row.ID+"-"+row.Name)
        
        self.lblUsuario = wx.StaticText(panel, label="Codigo MINECO Distribuidora :",pos=(20,20))
        self.tbUsuario = wx.TextCtrl(panel, pos=(220,20), size=(30, -1))
        self.tbUsuario.SetMaxLength(3)
        wx.EVT_KEY_DOWN(self.tbUsuario, self.OnKeyDown)
        
        self.lblPassword = wx.StaticText(panel, label="Password :",pos=(20,60))
        self.tbPassword = wx.TextCtrl(panel, pos=(80,55), size=(185, -1))
        self.tbPassword.Disable()
        #self.lblEjemplo = wx.StaticText(panel, label="(Ejemplo: SCADA\user000)",pos=(80,145))
        
        self.CreateButton= wx.Button(panel, -1, "Crear", pos=(60, 180))
        self.CreateButton.Bind(wx.EVT_BUTTON, self.Create)
        self.CreateButton.Disable()
        
        self.DeleteButton= wx.Button(panel, -1, "Borrar", pos=(160, 180))
        self.DeleteButton.Bind(wx.EVT_BUTTON, self.Delete)
        self.DeleteButton.Disable()
        
    def OnKeyDown(self,event):
        
        if DataBase.DevelopmentMode == 'YES' :
            print event.GetKeyCode()
        
        if event.GetKeyCode()==8:
            self.tbUsuario.SetLabel("")
            self.tbUsuario.SetBackgroundColour((255,255,255)) #WHITE
            self.CreateButton.Disable()
            self.DeleteButton.Disable()
            self.tbPassword.SetLabel("")
        
        if event.GetKeyCode() >= 324:
                codigoTecla=event.GetKeyCode()-276
        else:
                codigoTecla=event.GetKeyCode()
        if DataBase.DevelopmentMode == 'YES' :        
            print 'CodigoTecla= '+str(codigoTecla)
        
        if (event.GetKeyCode() >= 48 and event.GetKeyCode() <= 57)or(event.GetKeyCode()-276 >= 48 and event.GetKeyCode()-276 <= 57):
           
            if DataBase.DevelopmentMode == 'YES' :
                print len(self.tbUsuario.GetLabel())
            if len(self.tbUsuario.GetLabel()) >=0 and len(self.tbUsuario.GetLabel()) <= 2:
                self.tbUsuario.SetLabel(self.tbUsuario.GetLabel()+chr(codigoTecla))
                if len(self.tbUsuario.GetLabel()) == 3:
                    if DataBase.DevelopmentMode == 'YES' :
                        print 'son 3'
                    if self.CompruebaSiExisteDistribuidora(self.tbUsuario.GetLabel()) == True :
                       self.tbUsuario.SetBackgroundColour((255,0,0)) #RED
                       self.CreateButton.Disable()
                       self.DeleteButton.Enable()
                    else:
                        self.tbUsuario.SetBackgroundColour((127,255,0))  #GREEN 
                        self.CreateButton.Enable()
                        self.DeleteButton.Disable()
           
    def Delete(self,event):        
        userid = self.tbUsuario.Value
        mensaje="Se va a borrar el usuario 'user%s'" % userid
        
        eleccion= wx.MessageBox(mensaje, "", wx.YES_NO | wx.ICON_EXCLAMATION)
        if eleccion==wx.YES:
            if DataBase.DevelopmentMode == 'YES' :
                print 'YES'
                print userid
            sentencia = "DELETE from gdlTrazaImportUser where usuario = 'user%s' " % userid
            sentencia2 = "DELETE from gdlReportCompany where company = '%s' " % self.tbUsuario.Value
        
        try:
             print 'resultadoDelete: '+str(utiGedLux.sentenciaDeleteDms(sentencia))
             print 'resultadoDelete: '+str(utiGedLux.sentenciaDeleteDms(sentencia2))
        except:
            log.info("Error en formAddUserTrazaImport Delete")
            return      

        if self.CompruebaSiExisteDistribuidora(self.tbUsuario.GetLabel()) == False :
                self.tbUsuario.SetBackgroundColour((255,255,255))  #WHITE 
                self.CreateButton.Enable()
                self.DeleteButton.Disable()
                mensaje="El usuario 'user%s' se ha dado de baja correctamente" % userid
                eleccion= wx.MessageBox(mensaje, "", wx.OK | wx.ICON_EXCLAMATION)
                self.tbUsuario.SetLabel("")
                self.tbPassword.SetLabel("")
        else:
                self.tbUsuario.SetBackgroundColour((255,0,0)) #RED
                self.CreateButton.Disable()
                self.DeleteButton.Enable()
                mensaje="No se ha podido eliminar el usuario 'user%s'" % userid
                eleccion= wx.MessageBox(mensaje, "", wx.OK | wx.ICON_EXCLAMATION) 
        
    def CompruebaSiExisteDistribuidora(self,MINECO):         
        sentencia = "SELECT usuario FROM gdlTrazaImportUser where usuario = 'user%s'" % str(MINECO)
        #sentencia2 = "SELECT Name FROM internaMINECO where ID = '%s'" % str(MINECO)
        try:
            rows=utiGedLux.selectTrazaDms(sentencia,'D')
            #rows2=utiGedLux.sentenciaSelectBDInterna(sentencia2)
            #print str(len(rows2)) +" elemento(s)"
            
        except:
            return
            log.info("Error en formAddUserTrazaImport.CompruebaSiExisteDistribuidora")
                    
        if DataBase.DevelopmentMode == 'YES' :            
            print rows.__len__()
        if rows.__len__() == 0:
            if DataBase.DevelopmentMode == 'YES' :
                print 'la distribuidora NO existe'
            return False       
        else:
            if DataBase.DevelopmentMode == 'YES' :
                print 'la distribuidora EXISTE'
            return True
        
    def GeneratePassword(self):
        return str(self.id_generator(1, string.ascii_uppercase)+ self.id_generator(3, string.ascii_lowercase)+ '_'+self.id_generator(2, string.digits))
        
    def id_generator(self,size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for x in range(size))
        
    def Create(self,event):        
        userid = 'user'+self.tbUsuario.Value
        password=self.GeneratePassword()
        user_reverse = userid[::-1]
        md5introducido = hashlib.md5(password+ user_reverse).hexdigest()
        
        sentencia = "INSERT INTO gdlTrazaImportUser(usuario, password) VALUES ('%s','%s')" % (userid,md5introducido)
        sentencia2="insert into gdlReportCompany values('SCADA\user%s','%s')" % (self.tbUsuario.Value,self.tbUsuario.Value)
        
        if DataBase.DevelopmentMode == 'YES' :
            print sentencia
            print sentencia2
        
        try:
            print utiGedLux.sentenciaInsertDms(sentencia)
            print utiGedLux.sentenciaInsertDms(sentencia2)
            
            listadoSU=utiGedLux.getSUBDInterna()
            
            for superUser in listadoSU:
                 print utiGedLux.sentenciaInsertBDInterna("insert into gdlReportCompany values('SCADA\user%s','%s')" % (str(superUser)[3:-1],self.tbUsuario.Value))
            
        except:
            log.info("Error en formAddUserTrazaImport.Create")
            return

        if self.CompruebaSiExisteDistribuidora(self.tbUsuario.GetLabel()) == True :
            mensaje="Se ha dado de alta el usuario '%s' con password '%s'" % (userid,password)
        else:
            mensaje="No se ha podido dar de alta el usuario '%s' " % userid
        
        wx.MessageBox(mensaje, "", wx.OK | wx.ICON_INFORMATION)
        self.CreateButton.Disable()
        self.DeleteButton.Enable()
        self.tbUsuario.SetBackgroundColour((255,0,0)) #RED
        self.tbPassword.SetLabel(str(password))
        self.tbPassword.Enable()
        self.Refresh()
        
if __name__ == '__main__': 
    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
    
   
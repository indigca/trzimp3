# -*- coding: iso-8859-15 -*-
'''
Created on 07/09/2011

@author: n1
'''
import utiDMS
import ConfigParser
from db import DataBase
zone = 30
# Codigo de cliente
#minecoColletion = ['045', '054', '061', '083', '101', '124', '133', '180']
minecoColletion = ['054']
# Directorio donde guardar el csv (el directorio debe existir)
folder = 'substation/'

def toHTML(text):
    if text != None and isinstance(text,unicode):
        text = text.encode('iso-8859-1')
        #text = `text`
        text=text.replace("�","a")
        text=text.replace("�","e")
        text=text.replace("�","i")
        text=text.replace("�","o")
        text=text.replace("�","u")
        text=text.replace("�","A")
        text=text.replace("�","E")
        text=text.replace("�","I")
        text=text.replace("�","O")
        text=text.replace("�","U")
        text=text.replace("�","n")
        text=text.replace("�","N")
        text=text.replace("�","a")
        text=text.replace("�","o")
    return str(text)


cnxnT=utiDMS.connectSQL(DataBase.Address, DataBase.TRAZATable,DataBase.User,DataBase.Password)
cursorT = cnxnT.cursor()


for MINECO in minecoColletion:
    file = open(folder+MINECO+'_substation.csv','w')
    cursorT.execute("select Ins.IdInst, Ins.Nombre, Ins.Codigo, LocProvincias.Descripcion Provincia, LocMunicipios.Descripcion Municipio, \
                    TensionPrimaria.Valor TensionPrimaria, TensionSecundaria.Valor TensionSecundaria, ST.X, ST.Y\
                    from Instalaciones Ins, ST, LocProvincias, LocMunicipios, \
                    ATTensionNominal TensionPrimaria, ATTensionNominal TensionSecundaria\
                    where Ins.idinst = ST.idinst and\
                    LocProvincias.IdProvincia = Ins.IdProvincia and\
                    LocMunicipios.IdMunicipio = Ins.IdMunicipio and\
                    TensionPrimaria.IdTensionNominal = ST.TensionPrimaria_kV and\
                    TensionSecundaria.IdTensionNominal = ST.TensionSecundaria_kV and\
                    Ins.IdInst LIKE ?", MINECO[0:3] + '%')
    
    
    ST1 = cursorT.fetchall()
    
    
    print >>file, 'Datos Identificativos'
    print >>file, 'IdInst;Nombre;Codigo;Provincia;Municipio;Tension Primaria;Tension Secundaria;X;Y'
    for ST in ST1:
    #    print >>file, 'IdInst' + ';' + str(ST.IdInst)
    #    print >>file, 'Nombre' + ';' + str(ST.Nombre)
    #    print >>file, 'Provincia' + ';' + str(ST.Provincia)
    #    print >>file, 'Municipio' + ';' + str(ST.Municipio)
    #    print >>file, 'Tension Primaria' + ';' + str(ST.TensionPrimaria)
    #    print >>file, 'Tension Secundaria' + ';' + str(ST.TensionSecundaria)
        X,Y = utiDMS.convertED50(ST.X,ST.Y,zone)
        print >>file, toHTML(ST.IdInst) + ';' + toHTML(ST.Nombre) + ';' + toHTML(ST.Codigo) + ';' +toHTML(ST.Provincia) + ';' + toHTML(ST.Municipio) + ';' + toHTML(ST.TensionPrimaria) + ';' + toHTML(ST.TensionSecundaria) + ';' + str(X) + ';' + str(Y)
        
    
    cursorT.execute("SELECT Ins1.IdInst, Ins2.Nombre from Instalaciones Ins1, Instalaciones Ins2, STAlimentaciones\
                    WHERE Ins1.IdInst = STAlimentaciones.IdInst and\
                    Ins2.IdInst = STAlimentaciones.IdInstAlimentacion and\
                    Ins1.IdInst LIKE ?", MINECO[0:3] + '%')
    
    
    ST1 = cursorT.fetchall()
    
    print >>file, ' '
    print >>file, 'Alimentaciones'
    print >>file, 'IdInst;Nombre'
    for ST in ST1:
        print >>file, toHTML(ST.IdInst) + ';' + toHTML(ST.Nombre)
        
    cursorT.execute("SELECT Ins1.IdInst, STSalidas.NumeroPosicion Posicion, Ins2.Nombre from Instalaciones Ins1, Instalaciones Ins2, STSalidas\
                    WHERE Ins1.IdInst = STSalidas.IdInst and\
                    Ins2.IdInst = STSalidas.IdInstSalida and\
                    Ins1.IdInst LIKE ?\
                    order by NumeroPosicion", MINECO[0:3] + '%')
    
    
    ST1 = cursorT.fetchall()
    print >>file, ' '
    print >>file, 'Salidas'
    print >>file, 'IdInst;Posicion;Nombre'
    for ST in ST1:
        print >>file, toHTML(ST.IdInst) + ';' + toHTML(ST.Posicion) + ';' + toHTML(ST.Nombre)
        
    cursorT.execute("SELECT IdInst, IdTransformador, TensionAT, TensionBT, Potencia, CTGruposCx.Descripcion GruposCx\
                    FROM STTransformadores, CTGruposCx\
                    WHERE STTransformadores.IdGrupoCx = CTGruposCx.IdGrupoCx and\
                    IdInst LIKE ?", MINECO[0:3] + '%')
    
    
    ST1 = cursorT.fetchall()
    print >>file, ' '
    print >>file, 'Transformadores'
    print >>file, 'IdInst;IdTransformador;TensionAT;TensionBT;Potencia;GruposCx'
    for ST in ST1:
        print >>file, toHTML(ST.IdInst) + ';' + toHTML(ST.IdTransformador) + ';' + toHTML(ST.TensionAT) + ';' + toHTML(ST.TensionBT) + ';' + toHTML(ST.Potencia) + ';' + toHTML(ST.GruposCx)
    
    
    cursorT.execute("SELECT IdInst, IdSTPosicionesElementos Posicion, STTiposCeldasPE.Descripcion Elemento, STTiposPosiciones.Descripcion Tipo \
                    FROM   STCeldasPE \
                    LEFT OUTER JOIN STTiposPosiciones \
                    ON     STTiposPosiciones.IdTipoPosicion = STCeldasPE.IdTipoPosicion\
                    LEFT OUTER JOIN STTiposCeldasPE \
                    ON STCeldasPE.IdTipoCeldaPE = STTiposCeldasPE.IdTipoCeldaPE\
                    WHERE IdInst like ?\
                    ORDER BY IdSTPosicionesElementos, STTiposCeldasPE.Descripcion, STTiposPosiciones.Descripcion", MINECO[0:3] + '%')
    
    
    ST1 = cursorT.fetchall()
    print >>file, ' '
    print >>file, 'Montaje Primario'
    print >>file, 'IdInst;Posicion;Elemento;Tipo'
    for ST in ST1:
        print >>file, toHTML(ST.IdInst) + ';' + toHTML(ST.Posicion) + ';' + toHTML(ST.Elemento) + ';' + toHTML(ST.Tipo)
    
    cursorT.execute("SELECT IdInst, IdSTPosicionesElementos Posicion, STTiposCeldasSE.Descripcion Elemento, STTiposPosiciones.Descripcion Tipo\
                    FROM   STCeldasSE \
                    LEFT OUTER JOIN STTiposPosiciones \
                    ON     STTiposPosiciones.IdTipoPosicion = STCeldasSE.IdTipoPosicion\
                    LEFT OUTER JOIN STTiposCeldasSE \
                    ON STCeldasSE.IdTipoCeldaSE = STTiposCeldasSE.IdTipoCeldaSE\
                    WHERE IdInst like ?\
                    ORDER BY IdSTPosicionesElementos, STTiposCeldasSE.Descripcion, STTiposPosiciones.Descripcion", MINECO[0:3] + '%')
    
    
    ST1 = cursorT.fetchall()
    print >>file, ' '
    print >>file, 'Montaje Secundario'
    print >>file, 'IdInst;Posicion;Elemento;Tipo'
    for ST in ST1:
        print >>file, toHTML(ST.IdInst) + ';' + toHTML(ST.Posicion) + ';' + toHTML(ST.Elemento) + ';' + toHTML(ST.Tipo)
    file.close()
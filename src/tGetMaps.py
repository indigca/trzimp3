'''
Created on 25/03/2011

@author: n1
'''
import utiDMS
import utiMaps
import utiRiku
import ConfigParser
from db import DataBase

#Defining Database
#class DataBase:
#    config = ConfigParser.RawConfigParser()
#    config.read('config.ini')
#    Address = config.get('DATABASE','Address')
#    User = config.get('DATABASE','User')
#    Password = config.get('DATABASE','Password')
#    DMSTable = config.get('DATABASE','DMSTable')
#    TRAZATable = config.get('DATABASE','TRAZATable')
#    DMSLocation = config.get('DMS','DMSLocation')



def GetMaps(crd, ext, socio, zona):
    #For level 1 Centered in Madrid id 600 km /slice (socio="LAY1")
    #Level 2 Centered in Madrid 150 km /slice (socio="LAY2")
    #Level 3 Centered in the utility is 5 km /slice
    #For level 4 it is Ok 250 meters/slice
    #----------------------------------------------------------------------
    #socio="054A"
    #----------------------------------------------------------------------
    Mineco=socio[0:3]
    Tipo=socio[3]
    
    
    cnxn = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
    
    
    cursor= cnxn.cursor()
    
    if (cnxn):
        es_bounds = [600e3, 600e3, 600e3, 600e3]
    
        
#        level = 1
#        ciudad = "Madrid,Spain"
#        lat,lon,x89,y89=utiMaps.getXY_Zone(ciudad)
#        codeD='LAY'+`level`
#        utiMaps.loadMap(cnxn, level, [x89, y89], es_bounds, codeD, zona)
#       
#    
#        
#        level = 2
#        ciudad = "Madrid,Spain"
#        lat,lon,x89,y89=utiMaps.getXY_Zone(ciudad)
#        codeD='LAY'+`level`
#        
#        utiMaps.loadMap(cnxn, level, [x89, y89], es_bounds, codeD, zona)
        
        
        for level in range(3, 7): # last number is one higher than highest index
#            ciudad = "Autol, Spain"
#            lat, lon, x89, y89 = utiMaps.getXY_Zone(ciudad)
#            crd=utiRiku.projection(lat,lon) #lat, lon
            codeD = Mineco + `level`
            utiMaps.loadMap(cnxn, level, crd, ext, codeD, zona)
            print 'Level '+str(level)+' imported'
        import wx
        print 'IMPORTACION FINALIZADA!'
        wx.MessageBox("Importacion Finalizada","Importacion Finalizada", wx.OK|wx.ICON_ERROR)

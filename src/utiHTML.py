# -*- coding: iso-8859-1 -*-
'''
Created on May 4, 2011

@author: Administrator
'''
import markup
import os
def createTable(columnas,datosG):   
    #columnas=("Nombre","ApoyoSecuencial")
    #datosG=[("Hola 1","Hola 2"),("Adios 1","Adios 2")]
    page = markup.page( )
    page.table(class_='table')
    page.tr()
    for columna in columnas: 
        page.th(columna,class_='th')
    page.tr.close()
    page.tr()
    for i in range(0,len(datosG)):
        datos=datosG[i]
        for dato in datos: 
            page.td(dato,class_='td')
        page.tr.close()
    page.table.close()
    return page
def createButtonsL1(button,name):
    page = markup.page()
    for i in range(0,len(button)):
        page.p()
        page.p.close()
        page.form(method_="LINK",action_="./"+button[i])
        page.input(type_="submit",value_=button[i]+"-"+name[i])
        page.form.close()
    return page
def createButtonsL2(button):
    page = markup.page()
    for i in range(0,len(button)):
        if button[i]<> "index.html":
            page.p()
            page.p.close()
            page.form(method_="LINK",action_="./"+button[i])
            page.input(type_="submit",value_=button[i])
            page.form.close()
    return page

def refreshCIDEList(path):
    for dirnames in os.walk(path):
        if dirnames[0]==path:
            subdirs= dirnames[1]
    return subdirs
def refreshCIDEReports(path):
    for dirnames in os.walk(path):
        if dirnames[0]==path:
            filenames= dirnames[2]
    return filenames

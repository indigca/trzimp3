# -*- coding: iso-8859-15 -*-
'''
Created on 23/09/2011

@author: n1
'''
import utiDMS
from db import DataBase

cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
if cnxnW != None:
    cursorW = cnxnW.cursor()
else:
    print "Hubo un problema al intentar conectar con la base de datos. Compruebe que la configuraci�n y la conexi�n son correctas." 
    raw_input("Pulse ENTER para salir")
    exit()
files = [
         'queries/InitABB/Network_mdb.sql',
         'queries/InitABB/Opera_mdb.sql',
         'queries/InitABB/Views.sql',
         'queries/InitABB/Network_default_data.sql',
         'queries/InitABB/Opera_default_data.sql',
         'queries/InitGEDLux/gdl_ddl.sql', 
         'queries/InitGEDLux/gdlExtension.sql', 
         'queries/InitGEDLux/gdlcodemun.sql', 
         'queries/InitGEDLux/gdlcodeprov.sql', 
         'queries/InitGEDLux/gdlmineco.sql', 
         'queries/InitGEDLux/gdlmuntype.sql', 
         'queries/InitGEDLux/gdlmvcustlocation.sql', 
         'queries/InitGEDLux/gdlreportcompany.sql',
         'queries/InitGEDLux/gdlreportingservices.sql',
         'queries/InitGEDLux/init_all.sql',
         'queries/InitABB/change_zone.sql',
         'queries/InitABB/move_outagereason.sql',
         'queries/InitGEDLux/REGION.sql',
         'queries/InitGEDLux/gdllvconductor.sql'
         ]
i = 0
print "Creaci�n de tablas DMS iniciada"
for f in files:
    i = i + 1
    file = open(f,'r')
    fText = file.read()
    fTextParts = fText.split('GO')
    for fT in fTextParts:
#        print fT
        cursorW.execute(fT)
        cnxnW.commit()
cnxnW.close()
file.close()
print "Creaci�n de tablas DMS finalizada"


print "Creaci�n de tablas PIC iniciada"
cnxnW = utiDMS.connectSQL(DataBase.PICAddress, DataBase.PICTable, DataBase.PICUser, DataBase.PICPassword) #harcoded
cursorW = cnxnW.cursor()
files = [
         'queries/InitGEDLux/PIC.sql'
         ]
i = 0
for f in files:
    i = i + 1
    file = open(f,'r')
    fText = file.read()
    fTextParts = fText.split('GO')
    for fT in fTextParts:
#        print fT
        cursorW.execute(fT)
        cnxnW.commit()
cnxnW.close()
file.close()
print "Creaci�n de tablas PIC finalizada"
raw_input("Pulse ENTER para salir")

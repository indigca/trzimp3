'''
Created on 30/07/2012

@author: Eugenio and Marco
'''
import wx
from db import DataBase
import TrazaImport
import utiDMS
import os
import hashlib
import sys
import importExportExcel
import custom
import utiGedLux
import datetime
#DESCOMENTAR EN PRODUCCION
#sys.setdefaultencoding('iso-8859-15')


class Main(wx.Frame):
    def __init__(self):        
        wx.Frame.__init__(self, None, -1, 'Login TrazaImport', 
                size=(330, 160),style= wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
        panel = wx.Panel(self, -1)
        
        self.Centre()
        
        
#        custom.customPopulate('100')
#        custom.customExcel('100')
#        
#        custom.customPopulate('133')
#        custom.customExcel('133')
#        ok = utiGedLux.executeSelect('Select * from gdlOPC')
#        print datetime.datetime.now()

        logoGedlux = wx.Image('logo_gedlux.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        wx.StaticBitmap(panel, -1, logoGedlux, (30, 5), (logoGedlux.GetWidth(), logoGedlux.GetHeight()))
        
        self.lblUser = wx.StaticText(panel, label="user :",pos=(20,50))
        self.tbUser = wx.TextCtrl(panel, pos=(80,50), size=(140, -1), style = wx.NORMAL)
        self.tbUser.SetLabel(os.environ.get( "USERNAME" ).lower())
#DESHABILITAR
        #self.tbUser.SetLabel('user227')
        #self.tbUser.SetLabel('gedlux')
        self.tbUser.Disable()
        
        
        self.lblPassword = wx.StaticText(panel, label="password :",pos=(20,90))
        self.tbPassword = wx.TextCtrl(panel, pos=(80,90), size=(140, -1), style = wx.TE_PASSWORD)
#DESHABILITAR
        #self.tbPassword.SetLabel("Qtct_02")
        #self.tbPassword.SetLabel("D11382r")
        self.tbPassword.Bind(wx.EVT_TEXT_ENTER, self.OnLogin)
        self.tbPassword.Bind(wx.EVT_CHAR, self.OnPress)

        self.LoginButton= wx.Button(panel, -1, "Login", pos=(230, 90))
        self.LoginButton.Bind(wx.EVT_BUTTON, self.OnLogin)
                
#        print importExportExcel.CT3LetrasGDL("RIO LOURO")
#        print importExportExcel.CT3LetrasGDL("RUA DOS FERREIROS")
#        print importExportExcel.CT3LetrasGDL("PIAS")       
#        print importExportExcel.CT3LetrasGDL("GAVILAN") 

    def OnPress(self, event):
        if event.GetKeyCode() == 13:
            self.control.WriteText('\n>>>')
        else:
            event.Skip()
        
    def OnLogin(self,event):
        password_introducido = self.tbPassword.Value
        'Connect to DMS600'
        print DataBase.User
        print DataBase.Password
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        user = os.environ.get( "USERNAME" ).lower()
       # user = self.tbUser.GetValue()
       # print  self.tbUser.GetValue()
        print user
        #user='usuario'
        #user = "gedlux"
        sentencia = "SELECT password FROM gdlTrazaImportUser WHERE usuario = '%s'" % user
        cursorD.execute(sentencia)
        md5real = cursorD.fetchone()[0]
        user_reverse = user[::-1]
        md5introducido = hashlib.md5(password_introducido + user_reverse).hexdigest()
        print 'md5real='+str(md5real)
        print 'md5introducido='+str(md5introducido)
        
#         import base64
#         print "password a avriguar en base64-> "+str(base64.b64encode('D11382r'))
#         print "password a avriguar en base64-> "+str(base64.b64encode('D11382ms'))
        
        if md5real == md5introducido:
            self.Close(True)
            TrazaImport.openForm()
        else:
            dlg = wx.MessageBox("password incorrecto", "password incorrecto", 
                                            wx.OK | wx.ICON_INFORMATION)
        
if __name__ == '__main__': 
#def open():
    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
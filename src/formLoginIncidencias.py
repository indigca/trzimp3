# -*- coding: iso-8859-15 -*-
'''
Created on 30/07/2012

@author: Eugenio
'''
import wx
from db import DataBase
import formIncidenciasNoValidadas
import utiDMS
import os
import hashlib

class Main(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, 'Login Validación Informes', 
                size=(350, 80))
        panel = wx.Panel(self, -1)
        
        self.lblPassword = wx.StaticText(panel, label="password :",pos=(20,10))
        self.tbPassword = wx.TextCtrl(panel, pos=(80,10), size=(140, -1), style = wx.TE_PASSWORD)
        self.LoginButton= wx.Button(panel, -1, "Login", pos=(230, 10))
        self.LoginButton.Bind(wx.EVT_BUTTON, self.OnLogin)
        
    def OnLogin(self,event):
        password_introducido = self.tbPassword.Value
        
        'Connect to DMS600'
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        user = os.environ.get( "USERNAME" ).lower()
       
        #user='usuario'
        sentencia = "SELECT password FROM gdlTrazaImportUser WHERE usuario = '%s'" % user
        #user = "gedlux"
       # print user
       #print password_introducido
        cursorD.execute(sentencia)
        md5real = cursorD.fetchone()[0]
        user_reverse = user[::-1]
        md5introducido = hashlib.md5(password_introducido + user_reverse).hexdigest()
        #print 'md5real='+str(md5real)
        #print 'md5introducido='+str(md5introducido)
        if md5real == md5introducido:
            self.Close(True)
            formIncidenciasNoValidadas.openForm()
        else:
            dlg = wx.MessageBox("password incorrecto", "password incorrecto", 
                                            wx.OK | wx.ICON_INFORMATION)
        
if __name__ == '__main__': 
#def open():
    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
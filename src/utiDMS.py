# -*- coding: iso-8859-15 -*-
'''
Created on 25/03/2011

@author: n1
'''
import urllib2
import math
from math import sqrt
import pyodbc
import os
import sqlDMS
from pyproj import Proj
from pyproj import transform
import utiRiku
import TrazaImport
import unicodedata
from pygeocoder import Geocoder
import csv
import logging
import wx
from db import DataBase

def getMaps (maxCoord, minCoord, zone, slicesX, slicesY, codeD, nameDSN, fileLocation, cnxnD, level, proxyURL=''):
    
    '##########################################################################'
    '# It gets area map from idee.es and stores it on harddisk                #'
    '#   minCoord,maxCoord : Limits for the requested picture                 #'
    '#   slices: Grid      (3x3),(6x6),etc                                    #'
    '#   code: MINECO three digits                                            #'
    '#   nameDSN: Used ODBC DSN                                               #'
    '#   fileLocation: Where the file is stored                               #'
    '#   proxyURL: Http proxy (optional)                                      #'
    '#   cnxnD: Connecton to SQL                                             #'
    '##########################################################################'
    'Connect to DMS'
    cursorD = cnxnD.cursor()
    diff = (maxCoord[0] - minCoord[0], maxCoord[1] - minCoord[1])
    width = 1000
    # total number of slices
    slicesTotal = (slicesX + 1) * (slicesY + 1)
    for x in range (0, slicesX):
        minX = math.trunc(minCoord[0] + x * diff[0] / slicesX / 1.0)
        maxX = math.trunc(minCoord[0] + (x + 1) * diff[0] / slicesX / 1.0)
        for y in range (0, slicesY):
            minY = math.trunc(minCoord[1] + y * diff[1] / slicesY / 1.0)
            maxY = math.trunc(minCoord[1] + (y + 1) * diff[1] / slicesY / 1.0)
            if zone == 30 :
                picture_page = "http://www.idee.es/wms/PNOA/PNOA?REQUEST=GetMap&VERSION=1.1.1&SERVICE=WMS&SRS=EPSG:25830&BBOX=" + `minX` + "," + `minY` + "," + `maxX` + "," + `maxY` + "&WIDTH=" + `width` + "&HEIGHT=" + `width` + "&LAYERS=pnoa&STYLES=default&FORMAT=image/jpeg"
            elif zone == 29 :
                picture_page = "http://www.idee.es/wms/PNOA/PNOA?REQUEST=GetMap&VERSION=1.1.1&SERVICE=WMS&SRS=EPSG:25829&BBOX=" + `minX` + "," + `minY` + "," + `maxX` + "," + `maxY` + "&WIDTH=" + `width` + "&HEIGHT=" + `width` + "&LAYERS=pnoa&STYLES=default&FORMAT=image/jpeg"                    
            elif zone == 31 :
                picture_page = "http://www.idee.es/wms/PNOA/PNOA?REQUEST=GetMap&VERSION=1.1.1&SERVICE=WMS&SRS=EPSG:25831&BBOX=" + `minX` + "," + `minY` + "," + `maxX` + "," + `maxY` + "&WIDTH=" + `width` + "&HEIGHT=" + `width` + "&LAYERS=pnoa&STYLES=default&FORMAT=image/jpeg"                    
            
            filename = str(codeD) + "_x" + `x` + "y" + `y` + ".jpg" 
            fileHere = False
            while not fileHere:  
                fileHere = True 
                mapProxy = {"http": ""}   
                mapProxy['http'] = proxyURL
                proxy = urllib2.ProxyHandler(mapProxy)
                opener = urllib2.build_opener(proxy)
                # if proxy is not used, don't install it at urllib2
                if (proxyURL != ''):
                    urllib2.install_opener(opener)
                try:
                    fileHandler = urllib2.urlopen(picture_page, timeout=20) 
                except Exception:
                    fileHere = False
            fileObj = open(fileLocation + filename, 'wb')
            fileObj.write(fileHandler.read())
            fileObj.close()             
            texto = "c:\\ImageMagick-6.6.7-1\\" + "conv -size 1000x1000 -colors 256 " + fileLocation + filename + " " + fileLocation + filename.replace(".jpg", ".bmp")
            # number of completed slices
            slicesCompleted = (x * (slicesY + 1) + y + 1)
            # percentage of completed slices
            slicesPercentage = float(slicesCompleted) / float(slicesTotal) * 100.00
            os.system(texto)
            filename = filename.replace(".jpg", ".bmp")
            if zone == 30:
                minX2 = minX
                maxX2 = maxX
            elif zone == 29:
                minX2 = minX - 520000
                maxX2 = maxX - 520000
            elif zone == 31:
                minX2 = minX + 520000
                maxX2 = maxX + 520000
            # if recordset exists make update, if doesn't exist make insert
            rowExists = cursorD.execute("SELECT * FROM dbo.MapAdjustment WHERE name=?", filename).fetchall()

            if len(rowExists) == 0:
                cursorD.execute("insert into dbo.MapAdjustment (Name,Storing_Mode,Type,Color,Accurate,Sector,Px_Top,Py_Top,Px_Bottom,Py_Bottom,Mx_Top,My_Top,Mx_Bottom,My_Bottom)\
                                     values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)" \
                                    , filename, 'raster', 'material', 1, level, 1, minX2 + 30000, maxY - 3980000, maxX2 + 30000, minY - 3980000, minX2 + 30000, maxY - 3980000, maxX2 + 30000, minY - 3980000)           
            else:
                cursorD.execute("update dbo.MapAdjustment set Storing_Mode=?,Type=?,Color=?,Accurate=?,Sector=?,Px_Top=?,Py_Top=?,Px_Bottom=?,Py_Bottom=?,Mx_Top=?,My_Top=?,Mx_Bottom=?,My_Bottom=?\
                                     WHERE Name = ?" \
                                    , 'raster', 'material', 1, level, 1, minX2 + 30000, maxY - 3980000, maxX2 + 30000, minY - 3980000, minX2 + 30000, maxY - 3980000, maxX2 + 30000, minY - 3980000, filename)
            cnxnD.commit()
               
    
def getXY_Zone(address): #address=("Coruna,Spain")
    
    #--------Modified to migrate to Googles v3
    #--------gmaps = GoogleMaps("NotNeeded")
    #lat, lon = gmaps.address_to_latlng(address)
    results = Geocoder.geocode(address)
    lat,lon = results[0].coordinates
    
    'WGS84'
    zone = 30
    if lon <= -6:
        zone = 29
    elif lon >= 0:
        zone = 31
    p89 = Proj(proj="utm", zone=zone, ellps="GRS80", units="m", towgs84="0,0,0")
    x89, y89 = p89(lon, lat)

    return lat, lon, x89, y89, zone


def connectSQL (server, database, uid, pwd):
    cnx = None
    
    #wx.MessageBox("Comunicando con la Base de Datos","Espere")
#    dial = wx.MessageDialog(None, 'Comunicando con la Base de datos...', 'Espere', wx.OK)
#    dial.ShowModal()
#    dial.Hide()
    
    
    
    try:
        print 'DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + uid + ';PWD=' + pwd + ';autocommit=False'
        cnx = pyodbc.connect('DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + uid + ';PWD=' + pwd + ';autocommit=False')
#        cnx = pyodbc.connect('DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + database + ';Trusted_Connection=yes;autocommit=False')

    except Exception:
        TrazaImport.WriteLog("ERROR. SQL Connection failed")
        #wx.MessageBox("Problemas de conexi�n con la Base de Datos",
        #        "Error", wx.OK|wx.ICON_ERROR)
        
    
                
    return cnx


def getValidacionTrazaCheck(MINECO):
        'Connect to DMS600'
        cnxnD=connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = cnxnD.cursor()
        
        cursorD.execute("SELECT ValidacionTraza FROM gdlMINECO where ID = ?",str(MINECO))
        row=cursorD.fetchone()
        print row[0]
        
        return row[0]
    
def setValidacionTrazaCheck(MINECO,isValidado):
        'Connect to DMS600'
        cnxnD=connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = cnxnD.cursor()
        print 'isvalidado '+str(isValidado)
        print 'Mineco '+str(MINECO)
        
        
        #cursorD.execute("UPDATE gdlMINECO SET ValidacionTraza= ? WHERE ID =?",isValidado,str(MINECO))
                
        #cnxnD.commit()



def getDMSCode(xg, yg):
    # recommend changing, now we can have only nine nodes
    # in each 1 m^2 area
    xxg = str(int(xg))
    yyg = str(int(yg))
    #print 'xxg'
    #print xxg
    #print 'yyg'
    #print yyg
    
    if len(xxg) == 2:
        xxg = '0' + xxg
    if len(yyg) == 2:
        yyg = '0' + yyg
    nodecode = xxg[0:3] + yyg[0:3] + xxg[3:6] + yyg[3:6]
    return nodecode

def getCoordinates(nodecode,cursorT = None):
    """
    Get coordinates from a nodecode where nodecode follows
    the DMS naming pattern that contains the coordinates
    """
    xg = nodecode[0:3] + nodecode[6:9] + '000'
    yg = nodecode[3:6] + nodecode[9:12] + '000'  
    
#    if (str(yg).find('X') != -1) and (DataBase.DevelopmentMode == 'YES') :
#        fo = open("nodecodeCordMalas.txt", "a")
#        fo.write( "nodecode: "+str(nodecode)+"\t xg: "+str(xg)+"\t yg: "+str(yg)+" \n");
#        # Close opend file
#        fo.close()
#        
#    print 'getCoordinates xg: '+str(xg)+' yg: '+str(yg) 
#    
##    if (str(xg).find('X') != -1) and (DataBase.DevelopmentMode == 'YES') :
##        #xgMod = str(xg).split('X')
##        #xgMod=xg.replace('X','0')
##        xgMod=xg.index('X')
##        #xgMod=0
##        #wx.MessageBox('Error en utiDMS.getCoordinates. XG= '+str(xg)+' se sustituir� con '+str(xgMod[0]),'Error XG', wx.OK | wx.ICON_ERROR)
##        wx.MessageBox('Error en utiDMS.getCoordinates. XG= '+str(xg)+' se sustituir� con '+str(xgMod),'Error XG', wx.OK | wx.ICON_ERROR)
###        xg=str(xgMod[0])
##        xg=str(xgMod)
##    
##    if (str(yg).find('X') != -1) and (DataBase.DevelopmentMode == 'YES') :
##        #ygMod = str(yg).split('X')
##        #ygMod=yg.replace('X','0')
##        index=int(yg.index('X'))
##        ygMod=yg[0:index] 
##        #ygMod=0
##        #wx.MessageBox('Error en utiDMS.getCoordinates. YG= '+str(yg)+' se sustituir� con '+str(ygMod[0]),'Error YG', wx.OK | wx.ICON_ERROR)
##        wx.MessageBox('Error en utiDMS.getCoordinates. YG= '+str(yg)+' se sustituir� con '+str(ygMod),'Error YG', wx.OK | wx.ICON_ERROR)
###        yg=str(ygMod[0])
##        yg=str(ygMod)
#    
#    
#    # Open a file
#    fo = open("nodecode.txt", "a")
#    fo.write( "nodecode: "+str(nodecode)+"\t xg: "+str(xg)+"\t yg: "+str(yg)+" \n");
#
#    # Close opend file
#    fo.close()
    
    return int(xg), int(yg)

    
def convertED50 (x50, y50, zone):
    'EDS50'
    #print 'zone = '+zone
    
    p50 = Proj(proj="utm", ellps="intl", k=0.9996, x_0=500000.0, y_0=0.0 ,
    zone=zone, units="m", towgs84="-125.098545,-76.000054,-156.198703,0.0,0.0,-1.129,8.30463101")
    'WGS84'    
    p89 = Proj(proj="utm", zone=zone, ellps="GRS80", units="m", towgs84="0,0,0")
    
    '''x50,y50=p50(-3.09581,40.79182)
    x89,y89=transform (p50,p89,x50,y50)
    print x50,y50
    print x89,y89'''
    
    x89, y89 = transform(p50, p89, x50, y50)
    lon, lat = p89(x89, y89, inverse=True)
    x89, y89 = utiRiku.projection(lat, lon)
    flannaganY89 = -3980000
    flannaganX89 = +30000
#    flannaganY50 = -4000000
#    flannaganX50 = 0
    return int(x89 + flannaganX89), int(y89 + flannaganY89)

def calcDist(a1, a2, nodecode=False):
    """
    Calculate distance between two nodes. NOTE! When using
    nodecode = True, this function
    requires that the nodes use the naming convention where
    the coordinates are hashed into the node code!
    """
    if not nodecode:
        p1 = a1
        p2 = a2
    else:
        p1 = [0, 0]
        p2 = [0, 0]
        p1[0], p1[1] = getCoordinates(a1)
        p2[0], p2[1] = getCoordinates(a2)
    distance = sqrt((p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]))
    return distance

def getSitenode(cursorW, sitenode, type):
    """
    Gets a free nodecode from DMS database - function should
    actually be renamed, its not sitenode that it returns
    """

    # endings are like "X1", "X2" or "E1", "O1" etc
    endings = []
    for i in range(49,58):
        endings.append(type + chr(i))
    for i in range(65,91):
        endings.append(type + chr(i))
    for j in endings:

        chainToTest = sitenode[0:12] + j
        if type == '$':
            cursorW.execute("Select NODECODE from dbo.LV_CUSTOMER_NODE\
                         WHERE NODECODE= ?", chainToTest)
        else:
            cursorW.execute("Select CODE from dbo.MV_NODE\
                             WHERE CODE= ?", chainToTest)
        row2 = cursorW.fetchone()
        if row2 == None:
            return chainToTest
        
def getBorderPointName(cursorW, bpName):
    print bpName
    bpName = normalize(bpName).replace(' ','')[0:15] + '5046'
    print bpName
    cursorW.execute("SELECT * FROM SUBSTATION WHERE NAME LIKE ?", bpName)
    existsbpName = cursorW.fetchall()
    if len(existsbpName) > 0:
        for i in range(0,50):
            bpName2 = bpName[:-4] + `i` + '5046'
            print bpName2
            cursorW.execute("SELECT * FROM SUBSTATION WHERE NAME LIKE ?", bpName2)
            existsbpName = cursorW.fetchall()
            if len(existsbpName) == 0:
                bpName = bpName2
                break
    return bpName

#def getSitenodeDisconnector(cursorW, sitenode):
#    endings = ['E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9']
#    for j in endings:
##        print j
#        chainToTest = sitenode + j
#        cursorW.execute("Select CODE from dbo.MV_NODE\
#                         WHERE CODE= ?", chainToTest)
#        row2 = cursorW.fetchone()
#        if row2 == None:
#            return chainToTest
# 
#def getSitenodeFuse(cursorW, sitenode):
#    endings = ['O1', 'O2', 'O3', 'O4', 'O5', 'O6', 'O7', 'O8', 'O9']
#    for j in endings:
##        print j
#        chainToTest = sitenode + j
#        cursorW.execute("Select CODE from dbo.MV_NODE\
#                         WHERE CODE= ?", chainToTest)
#        row2 = cursorW.fetchone()
#        if row2 == None:
#            return chainToTest
#        
def mreplace(s, chararray, newchararray):
    s = s.encode('iso-8859-1')
    for a, b in zip(chararray, newchararray):
        s = s.replace(a, b)
    return s

def encode(s):
    chararray=    [' ','.','-','º','/', 'ñ','Ñ','ä','ë','ï','ö','ü','á','é','í','ó','ú','Ä','Ë','Ï','Ö','Ü','Á','É','Í','Ó','Ú']
    newchararray= ['', '', '', '', '',  'n','N','a','e','i','o','u','a','e','i','o','u','A','E','I','O','U','A','E','I','O','U']
#    s = s.encode('utf-8')
    for a, b in zip(chararray, newchararray):
        s = s.replace(a, b)
    return s

def normalize(s):
    return unicodedata.normalize('NFKD', unicode(s)).encode('ascii','ignore')

def getSubstationPoint(cursorW, s):
    if len(s) < 12:
        s = s + '                            '
        s = s[0:12]
    endings = []
    for i in range(49,58):
        endings.append(chr(i))
    for i in range(65,91):
        endings.append(chr(i))
    for j in endings:
        chainToTest = "%sX%s" % (s,j)
        cursorW.execute("Select CODE from dbo.MV_NODE\
                         WHERE CODE LIKE ?", chainToTest)
        row2 = cursorW.fetchone()
        if row2 == None:
            return chainToTest
        
def getCodeDisconnector(cursorW, MINECO, linName,number = 0,CTCode=None,cursorT=None):
    endings = []
    endings.append("")
    for i in range(50,58):
        endings.append(chr(i))
    for i in range(65,91):
        endings.append(chr(i))
    for i in range(65,91):
        for j in range(65,91):
            endings.append(chr(i)+chr(j))
#        print j
#        print MINECO,linName,j,number
    if CTCode != None:
        cursorW.execute("SELECT CodigoLinea FROM gdlOrdenCeldas where CodigoCT = ? and \
        Posicion = ?",CTCode,number)
        CodigoLinea = cursorW.fetchone().CodigoLinea.rstrip().lstrip()
        if CodigoLinea not in (None, ""):
            cursorT.execute("SELECT Nombre FROM Instalaciones where Codigo like ?", CodigoLinea)
            linName = cursorT.fetchone().Nombre
        else:
            linName = u"Trafo-" + linName

    for j in endings:
        if linName.isdigit():
            linName2 = normalize(linName).replace(" ","")[0:12] + linName[-1]
        else:
            linName2 = normalize(linName).replace(" ","")[0:13]
        chainToTest = "%s%s%s" % (MINECO,linName2,j)
#        print chainToTest
        cursorW.execute("Select CODE from dbo.DISCONNECTOR\
                         WHERE CODE LIKE ?", chainToTest)
        row2 = cursorW.fetchone()
        if row2 == None:
            return chainToTest

def getCodeFuse(cursorW, MINECO, linName):
    endings = []
#    print linName
    for i in range(49,58):
        endings.append('O' + chr(i))
    for i in range(65,91):
        endings.append('O' + chr(i))
    for j in endings:
#        print j
        
        chainToTest = MINECO + linName.replace(' ', '')[0:9] + j
#        print chainToTest
        cursorW.execute("Select CODE from dbo.MV_FUSE\
                         WHERE CODE LIKE ?", chainToTest)
        row2 = cursorW.fetchone()
        if row2 == None:
#            print chainToTest, '*'
            return chainToTest


def DrawLine(cnxnW, cursorW, nodeList, MINECO, VoltageLevel, line, cursorT):
    """
    Draws a group of MV_SECTIONs to represent a traza "line". nodeList contains
    a list of points, some of which are disconnector/fuse nodes and other are just
    SECTION_POINTs.
    
    nodesIn:
        [ 0 - type : POINT / CT / ...
          1 - nodecode : DMS node code, or hashed coordinates if this is section point
          2 - conductor : DMS conductor type
          3 - ct : IdInst of CT that exists in this vano ])
    """
    secc = []
    first = True
    nroVano = 0
    
    for node in nodeList:
        nroVano = nroVano + 1
        nodecode = node[1]
        X, Y = getCoordinates(nodecode)    
        
        CTCode = None
        
        if (node[0] == "CT"):
            # this middle point is a ct and warrants extra attention, we have
            # to find a free celda where to connect
            CTCode = node[3]

            nodecode = sqlDMS.getEmptyLineConnection(cursorT,
                                                     cursorW,
                                                     CTCode,
                                                     line,
                                                     MINECO)
        
            if nodecode == None:
                # TODO something went horribly wrong - no free celda was found,
                # which propably means it wasnt created like it should
                node[0] = "POINT" # lets just draw it as a section point
                nodecode = node[1]
                '''debugging Marco Guida'''
                if str(nodecode)=='312994948280':
                    print 'ok'

            print 'getCoordinates(nodecode) '+str(getCoordinates(nodecode))
        
        if first:
            # first point of linea
            first = False
            node1 = nodecode
        else:
            # normal point
            if node[0] == "POINT":
                # node type is section point, do not draw, just store
                # for later when we actually do create the MV_SECTION
                secc.append({"x":X, "y":Y})
                
            else:
                # this warrants a new MV_SECTION, its either branch, disconnector,
                # fuse or CT
                node2 = nodecode
                sectionNumber = sqlDMS.uMV_Section_ID (cnxnW, cursorW)
                sqlDMS.iMV_SectionPoints(cursorW,
                                         sectionNumber,
                                         node1,
                                         node2,
                                         secc,
                                         MINECO)
                sqlDMS.iMV_Section(cursorW,
                                   sectionNumber,
                                   node1,
                                   node2,
                                   node[2],
                                   MINECO,
                                   VoltageLevel,
                                   X,
                                   Y)
                
                if node[0] == "CT" and nroVano < (len(nodeList)):
                    # when the middle point is ct, we cannot continue from
                    # the same node but instead we have to find another free
                    # celda node from the same CT - except for last node
                    prevNode1 = node1
                    node1 = sqlDMS.getEmptyLineConnection(cursorT,
                                                          cursorW,
                                                          CTCode,
                                                          line,
                                                          MINECO)
                    if node1 == None:
                        # this CT has no more celdas for our line, which
                        # propably means it is a 'hanging' trafo, eg the
                        # line does not pass *through* the CT, it passes *by* it
                        if len(secc) == 0 or prevNode1[-2] != 'X':
                            # no section points for this last section, so we
                            # can continue drawing the trunk of the line from the last
                            # node
                            node1 = prevNode1
                        else:
                            lastSec = secc[len(secc) - 1]
                            logging.error("not implemented: line " + line
                                          + " will look funny near CT " + CTCode)
                            # TODO we should somehow go back in time and make a branch
                            #      at 'lastSec', and also connect the previously created
                            #      section to it
                            
                            # ... but before that is implemented we just jump back
                            # to the previous existing node, which may lead to pretty
                            # nice results anyways
                            node1 = prevNode1
           
                else:
                    # and in the normal case the next section will start from 
                    # where this ended
                    node1 = nodecode
                secc = []
                print 'casi acabado'
                print str(nodeList)
                print '****'
#                 '''debugging Marco Guida'''
#                 coordenadas6=open('C:\\buzon\\177\\AnalisisCoordenadas6.txt','a')
#                 coordenadas6.write('nodeList='+str(nodeList)+'\n')
#                 coordenadas6.write('***********************\n')
                cnxnW.commit()

def readCSV(path):
    reader = csv.reader(open(path, "rb"), delimiter = ";", skipinitialspace=True)
    
    listofnames = []
    
    for name in reader:
        listofnames.append(name)
    return listofnames

def filterList(list,CodigoCT):
    temp = []
    for item in list:
        if item[0].find(CodigoCT) != -1:
            temp.append(item)
    return temp




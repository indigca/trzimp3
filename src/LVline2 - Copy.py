#Module containing functions for creating elements in Traza
import utiDMS
import sqlDMS
from db import DataBase
from pyodbc import IntegrityError
import math
import TrazaImport

def getBTConductorType(cursorT, idTipoConductor):
    cursorT.execute("SELECT Descripcion FROM BTVanoTiposConductores\
                     WHERE IdTipoConductor LIKE ?", idTipoConductor)
    t = cursorT.fetchone()
    if t != None:
        conductor = t.Descripcion.strip()
            # remove trailing whitespace
        return conductor
    else:
        return "BUSBAR"


def makeAllComienzoLvNodes(cursorW, cursorT, MINECO):
    cursorT.execute("select IdBTVano, X, Y, Z \
                    from BTVano \
                    where (SELECT TOP 1 IdComienzo FROM BTVano AS b2 \
                    WHERE b2.IdComienzo = BTVano.IdBTVano) IS NOT NULL\
                    and IdBTVano like ?", MINECO[0:3] + '%')
    comNodes = cursorT.fetchall()
    for n in comNodes:
        if n.Z == -30:
            # quick hack for nodes that have their coordinates
            # translated in the database
            X = n.X
            Y = n.Y
        else:
            X, Y = utiDMS.convertED50(n.X, n.Y, 30)
        sqlDMS.makeLVNode(cursorW, X * 1000, Y * 1000, 'X', IdBTVano=n.IdBTVano, MINECO = MINECO)

def LVline(cursorT, cursorW, IdInst, MINECO, trafoboxes):
# locating the line
    cursorT.execute("select * from BT, Instalaciones\
                    where Instalaciones.IdInst = BT.IdInst\
                    and Instalaciones.IdInst LIKE ?", IdInst)
    BTline = cursorT.fetchone()
    TrazaImport.WriteLog("Importando linea de BT: " + BTline.Nombre)
    
    if BTline.IdTransformador == None:
        TrazaImport.WriteLog("Error: transformer not set for " + BTline.Nombre)
        return
    
# locating de CT

    cursorT.execute("SELECT * FROM CT, Instalaciones\
                    WHERE CT.IdInst LIKE  ?\
                    and Instalaciones.IdInst = CT.IdInst", BTline.IdInstCT)
    CT = cursorT.fetchone()
    CTcode = CT.Codigo
    CTname = CT.Nombre
    
# locating the transformer
    cursorW.execute("SELECT *\
                     FROM TRANSFORMER\
                     where CODE like ?",
                     BTline.IdTransformador + '%')
    Transformer = cursorW.fetchone()
    
# locating the transformer node

    cursorW.execute("SELECT *\
                  FROM TRANSFORMER_NODE\
                  where MV_LV_SUBSTATION = ?\
                  and ORDERNUMBER = ?", Transformer.PLACING_SITE, Transformer.ORDERNUMBER)
    NodeTransformer = cursorW.fetchone()
    Xt, Yt = utiDMS.getCoordinates(NodeTransformer.NODECODE)

    # creating the box
    # if previously created, skip
    Xbox, Ybox = Xt, Yt + 1000 + 2500 * Transformer.ORDERNUMBER
    Xswitch1, Yswitch1 = Xbox, Ybox - 300

    stationName = CTcode
    
    boxName = stationName +\
              '-' + str(Transformer.ORDERNUMBER)
    feederName = stationName \
                 + '-' + str(Transformer.ORDERNUMBER) + '-' 'IN'
    
    if not trafoboxes.has_key(NodeTransformer.NODECODE):
        NodeBox = sqlDMS.makeLVNode(cursorW, Xbox, Ybox, 'J', MINECO=MINECO)
        sqlDMS.iLV_BOX(cursorW, NodeBox, CTname, boxName)
        
        # creating the switch 1
        NodeSwitch1 = sqlDMS.makeLVNode(cursorW, Xswitch1, Yswitch1,\
                                        'W', NodeBox,\
                                        Xt = Xswitch1 - 150, Yt = Yswitch1 - 70, MINECO=MINECO)
        
        sqlDMS.iLV_SWITCH(cursorW, NodeSwitch1, \
                          boxName, \
                          feederName, 1, None, None,\
                          NAME = CTname + ' ' + str(Transformer.ORDERNUMBER))
        
        trafoboxes[NodeTransformer.NODECODE] = [NodeBox, 0]
        
        # section 1 from Transformer to Switch 1  
        LV_Section_Id = sqlDMS.iLV_Section(cursorW, NodeTransformer.NODECODE,
                                           NodeSwitch1, 'BUSBAR', stationName,
                                           Xt=Xswitch1, Yt=Yswitch1, ANGLE=90)
        
        # set connecting line section
        cursorW.execute("UPDATE TRANSFORMER SET LINE_SECTION_2 = ?\
                         WHERE ORDERNUMBER = ? AND PLACING_SITE = ?",
                        LV_Section_Id, Transformer.ORDERNUMBER,\
                        Transformer.PLACING_SITE)
        
        # section 2 from Switch 1 to Box
        sqlDMS.iLV_Section(cursorW, NodeSwitch1, NodeBox,\
                           'BUSBAR', stationName,
                           Xt=Xbox, Yt=Ybox, ANGLE=90)   
    else:
        NodeBox = trafoboxes[NodeTransformer.NODECODE][0]
        
# creating the switch 2

    # feeder number
    trafoboxes[NodeTransformer.NODECODE][1] += 1
    
    Xswitch2 = Xswitch1 + 200 * trafoboxes[NodeTransformer.NODECODE][1]
    Yswitch2 = Yswitch1
    NodeSwitch2 = sqlDMS.makeLVNode(cursorW, Xswitch2, Yswitch2, 'W', NodeBox,\
                                    Xt = Xswitch2 - 150,\
                                    Yt = Yswitch2 - 70 * \
                                    (1 + trafoboxes[NodeTransformer.NODECODE][1] % 4), MINECO=MINECO)
    feederName = boxName + '-AL' + str(trafoboxes[NodeTransformer.NODECODE][1])

    switchName = BTline.Nombre.replace(CTname + ' - ', '')
    sqlDMS.iLV_SWITCH(cursorW, NodeSwitch2, boxName,\
                      feederName,\
                      2, 16 , 'DEFAULT_gG_16',\
                      NAME = switchName) # hardcoded: fuse
    
    # section 3 from Box to Switch 2
    sqlDMS.iLV_Section(cursorW, NodeBox, NodeSwitch2, 'BUSBAR', stationName,\
                       Xt=Xswitch2, Yt=Yswitch2, ANGLE=90)
    
# creating the sections

    # getting the nodes
    
    cursorT.execute("select *,\
                    CASE \
                    WHEN (EXISTS (SELECT IdComienzo FROM BTVano AS b2 \
                    WHERE b2.IdComienzo = BTVano.IdBTVano)) \
                    THEN 1 ELSE 0 END AS MakeNode from BTVano, Instalaciones \
                    where Instalaciones.IdInst = BTVano.IdInst\
                    and Instalaciones.IdInst LIKE ?\
                    order by ApoyoSecuencial", IdInst)
    nodes = cursorT.fetchall()

    if len(nodes) == 0:
        return
    
    #conductor type
    CONDUCTOR = getBTConductorType(cursorT, nodes[0].IdTipoConductor)


    
# creating the line


    isFirst = True
    prevNode = NodeSwitch2
    prevNodeI = 0
    thisNodeI = 0
    i_node = 0
    for node in nodes:
        CONDUCTOR = getBTConductorType(cursorT, node.IdTipoConductor)
        if node.IdTipoVano == None:
            if i_node == len(nodes) - 1 and node.MakeNode != 1:
                # last of the section, must make node
                if node.Z == -30:
                    # quick hack for nodes that have their coordinates
                    # translated in the database
                    Xf = node.X
                    Yf = node.Y
                else:
                    Xf, Yf = utiDMS.convertED50(node.X, node.Y, 30)
                thisNode = sqlDMS.makeLVNode(cursorW, Xf * 1000, Yf * 1000, 'X', IdBTVano=node.IdBTVano, MINECO=MINECO)
                prevNodeI = thisNodeI
                thisNodeI = i_node
                makeNode(thisNode, prevNodeI, thisNodeI, cursorW,
                         CONDUCTOR, stationName, Xswitch2, Yswitch2, nodes, prevNode,
                         isFirst, MINECO)
                prevNode = thisNode
                if isFirst:
                    isFirst = False
                        
                        
            elif node.MakeNode == 1:
                # node was created by MakeAllComienzoLvNodes
                thisNode = getNodeByIdBTVano(cursorW, node.IdBTVano)
                prevNodeI = thisNodeI
                thisNodeI = i_node
                makeNode(thisNode, prevNodeI, thisNodeI, cursorW,
                         CONDUCTOR, stationName, Xswitch2, Yswitch2, nodes, prevNode,
                         isFirst, MINECO)
                prevNode = thisNode
                if isFirst:
                    isFirst = False

                
        elif node.IdTipoVano == '1':
            # branch
            if node.IdComienzo == None:
                if i_node == 0:
                    #this line starts with a comienzo, so we must act as if this comienzes
                    #from the trafo
                    if node.Z == -30:
                        # quick hack for nodes that have their coordinates
                        # translated in the database
                        Xf = node.X
                        Yf = node.Y
                    else:
                        Xf, Yf = utiDMS.convertED50(node.X, node.Y, 30)
                    thisNode = getNodeByIdBTVano(cursorW, node.IdBTVano)
                    prevNodeI = thisNodeI
                    thisNodeI = i_node
                    makeNode(thisNode, prevNodeI, thisNodeI, cursorW,
                             CONDUCTOR, stationName, Xswitch2, Yswitch2, nodes, prevNode,
                             isFirst, MINECO)
                    prevNode = thisNode
                    if isFirst:
                        isFirst = False
                else:
                    TrazaImport.WriteLog("Error: vano is a branch but comienzo not defined! @" + node.IdBTVano)
                
                continue
                
            if node.Z == -30:
                Xf = node.X
                Yf = node.Y
            else:
                Xf, Yf = utiDMS.convertED50(node.X, node.Y, 30)
            
            if getNodeByIdBTVano(cursorW, node.IdBTVano) == None:
                branchNode = sqlDMS.makeLVNode(cursorW, Xf * 1000, Yf * 1000, 'X', IdBTVano=node.IdBTVano, MINECO=MINECO)
            else:
                # this node was created before starting to make lines 
                branchNode = getNodeByIdBTVano(cursorW, node.IdBTVano)
            
            startingNode = getNodeByIdBTVano(cursorW, node.IdComienzo)
            if startingNode == None:
                TrazaImport.WriteLog("Node " + node.IdBTVano + " starts from undefined node " \
                      + node.IdComienzo)
                continue
            
            sqlDMS.iLV_Section(cursorW,
                               startingNode, branchNode, \
                               CONDUCTOR, stationName)
        else:
            #customer
            if node.IdBTVano == None:
                TrazaImport.WriteLog("error, customer without a node at " + node)
                continue
            custNode = getCustomerNodeByIdBTVano(cursorW, node.CodigoAcometida)
            
            if custNode != None:
                startingNode = getNodeByIdBTVano(cursorW, node.IdComienzo)
                if startingNode == None:
                    TrazaImport.WriteLog("Error: starting node of " + custNode + " not on database")
                    continue
                
                sqlDMS.iLV_Section(cursorW,
                                   startingNode, custNode, \
                                   CONDUCTOR, stationName)

                sqlDMS.uLV_CUSTOMER_NODE(cursorW, custNode, feederName, boxName, NodeSwitch2)
        
        i_node = i_node + 1
        
    
# should be renamed to makeLVSection
def makeNode(thisNode, prevNodeI, thisNodeI, cursorW,
             CONDUCTOR, stationName, Xswitch2, Yswitch2, nodes, prevNode,
             isFirst, MINECO):
    
    # creating the section
    sqlDMS.iLV_Section(cursorW,
                       prevNode, thisNode, \
                       CONDUCTOR, stationName)
    
    # adding the sectionpoints    
    
    secc = []
    numSecc = 0
    
    if isFirst:
        numSecc = numSecc + 1
        secc.append({"x":Xswitch2, "y":Yswitch2 - 1000, "numSecc":numSecc})
        
    for i in range(prevNodeI, thisNodeI):
        if nodes[i].IdTipoVano == None:
            numSecc = numSecc + 1
            if nodes[i].Z == -30:
                nX = nodes[i].X
                nY = nodes[i].Y
            else:
                nX, nY = utiDMS.convertED50(nodes[i].X, nodes[i].Y, 30)
            secc.append({"x":nX * 1000, "y":nY * 1000, "numSecc":numSecc})
    
    sqlDMS.iLV_SectionPoints (cursorW, prevNode, thisNode, secc, mineco = MINECO)


def getCustomerNodeByIdBTVano(cursorW, codigoAcometida):
    cursorW.execute("SELECT NODECODE FROM LV_CUSTOMER_NODE\
                     WHERE CUSTOMER_NODE LIKE ?", codigoAcometida)
    node = cursorW.fetchone()
    
    if node != None:
        return node.NODECODE
    else:
        if codigoAcometida != None:
            TrazaImport.WriteLog("Acometida " + codigoAcometida \
                  + " has no customer node in DMS!")
        return None
    
def getNodeByIdBTVano(cursor, idBTVano):
    cursor.execute("SELECT TOP 1 NODECODE\
                    FROM gdlLV_NODE_BTVano\
                    WHERE IdBTVano LIKE ?", idBTVano)
    nc = cursor.fetchone()
    
    if nc != None:
        return nc.NODECODE
    else:
        return None

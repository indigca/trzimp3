'''
Created on Agu 07, 2013

@author: Marco Guida
'''
import utiGedLux
TRAZA='T'
DMS='D'
def getPopulate():
    
    '''/* En esta query no se consulta CTCeldas. Se basa exclusivamente en las alimentaciones 
                     y derivaciones que tenga cada CT. A estas se le unen los transformadores del mismo.
                    Si hay un CT con mas celdas que la suma de alimentaciones y derivaciones, estas celdas
                    no apareceran. */'''
    
    sentencia= """SELECT InsCT.Codigo CodigoCT, ISNULL(InsLinea.Codigo, '1') AS CodigoLinea, CT.X, CT.Y, InsCT.Nombre, CTDerivaciones.IdInstDerivaciones IdInstLinea,\
                         '1' AS IdTipoCelda, InsCT.IdInst, NULL AS IdTransformador, NULL AS DisyuntorFusible, NULL AS Modelo, NULL AS Transformador\
                 FROM CTDerivaciones \
                     JOIN Instalaciones InsCT\
                         ON CTDerivaciones.IdInst = InsCT.IdInst AND InsCT.IdInst LIKE '100%'\
                     JOIN Instalaciones InsLinea\
                         ON CTDerivaciones.IdInstDerivaciones = InsLinea.IdInst\
                     JOIN CT\
                         ON CTDerivaciones.IdInst = CT.IdInst\
                 UNION ALL\
                 SELECT InsCT.Codigo CodigoCT, ISNULL(InsLinea.Codigo, '1') AS CodigoLinea, CT.X, CT.Y, InsCT.Nombre, CTAlimentaciones.IdInstAlimentaciones IdInstLinea,\
                         '1' AS IdTipoCelda, InsCT.IdInst, NULL AS IdTransformador, NULL AS DisyuntorFusible, NULL AS Modelo, NULL AS Transformador\
                 FROM CTAlimentaciones \
                     JOIN Instalaciones InsCT\
                         ON CTAlimentaciones.IdInst = InsCT.IdInst AND InsCT.IdInst LIKE '100%'\
                     JOIN Instalaciones InsLinea\
                         ON CTAlimentaciones.IdInstAlimentaciones = InsLinea.IdInst\
                     JOIN CT\
                         ON CTAlimentaciones.IdInst = CT.IdInst\
                 UNION ALL\
                 SELECT    Instalaciones.Codigo CodigoCT,\
                         CTTransformadores.NumeroTransformador AS CodigoLinea,\
                         CT.X,\
                         CT.Y,\
                         Instalaciones.Nombre,\
                         NULL AS IdInstLinea,\
                         '2' AS IdTipoCelda,\
                         Instalaciones.IdInst,\
                         CTTransformadores.IdTransformador,\
                         NULL AS DisyuntorFusible,\
                         NULL AS Modelo,\
                         CTTransformadores.NumeroTransformador AS Transformador \
                 FROM    CT\
                         JOIN Instalaciones\
                             ON CT.IdInst=Instalaciones.IdInst\
                         JOIN CTTransformadores \
                             ON CTTransformadores.IdInst = Instalaciones.IdInst\
                 WHERE    Instalaciones.IdInst LIKE '100%'"""
    
    
   

    return utiGedLux.selectTrazaDms(sentencia,TRAZA)
    
def getExcel():
    
  sentencia=  """SELECT InsCT.Nombre CT, InsLinea.Nombre Linea, '' AS NumeroTransformador, InsLinea.Codigo CodigoLinea, InsCT.Codigo CodigoCT, 
                    SUBSTRING(InsCT.IdInst,0,4) AS IdInst
                    FROM CTDerivaciones 
                    JOIN Instalaciones InsCT
                    ON CTDerivaciones.IdInst = InsCT.IdInst AND InsCT.IdInst LIKE '100%'
                    JOIN Instalaciones InsLinea
                    ON CTDerivaciones.IdInstDerivaciones = InsLinea.IdInst
                    JOIN CT
                    ON CTDerivaciones.IdInst = CT.IdInst
                    UNION
                    SELECT InsCT.Nombre CT, InsLinea.Nombre Linea, '' AS NumeroTransformador, InsLinea.Codigo CodigoLinea, InsCT.Codigo CodigoCT, 
                    SUBSTRING(InsCT.IdInst,0,4) AS IdInst
                    FROM CTAlimentaciones 
                    JOIN Instalaciones InsCT
                    ON CTAlimentaciones.IdInst = InsCT.IdInst AND InsCT.IdInst LIKE '100%'
                    JOIN Instalaciones InsLinea
                    ON CTAlimentaciones.IdInstAlimentaciones = InsLinea.IdInst
                    JOIN CT
                    ON CTAlimentaciones.IdInst = CT.IdInst
                    UNION
                    SELECT  Instalaciones.Nombre, 
                    Linea = 'Trafo ' + CTTrafo.NumeroTransformador + ' ' + cast(CTTrafo.Potencia as varchar(30)), CTTrafo.NumeroTransformador,
                    CodigoLinea = '', Instalaciones.Codigo CodigoCT, SUBSTRING(Instalaciones.IdInst,0,4) AS IdInst
                    FROM    CT
                    JOIN Instalaciones
                    ON CT.IdInst=Instalaciones.IdInst
                    JOIN CTTransformadores CTTrafo
                    ON CTTrafo.IdInst = Instalaciones.IdInst
                    WHERE    Instalaciones.IdInst LIKE '100%' order by CodigoCT"""
    
  return utiGedLux.selectTrazaDms(sentencia,TRAZA)
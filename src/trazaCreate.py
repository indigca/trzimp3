# -*- coding: iso-8859-15 -*-
'''
Created on May 21, 2011

@author: Administrator
'''
#Module containing functions for creating elements in Traza
import utiDMS
import sqlDMS
import sys
from db import DataBase
from pyodbc import IntegrityError
import math
import wx
import TrazaImport
import os
import csv
import xlrd
import custom

import logging
from ctypes.test.test_array_in_pointer import Value
#from elementtree.ElementTree import string
logging.basicConfig(level=logging.DEBUG)


zone = DataBase.Zone

contador= 0


def zone(MINECO):
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
    cursorW = cnxnW.cursor()
    cursorW.execute("select zone from gdlMINECO WHERE ID LIKE ?",MINECO[0:3])
    return cursorW.fetchone().zone

def populateGdlCeldas(cursorR,cnxnW, MINECO,PrgDialog):
    """
    Fill in celda data to the table gdlCeldas. When creating CT:s, this table
    is used to direct creation of the MV_LV_SUBSTATION disconnectors and transformer
    nodes.
    """
    cursorW = cnxnW.cursor()
    
    # cargar datos celdas en base de datos dms tabla gdlCeldas
    minWildcard =  MINECO[0:3] + '%'
    
    cursorW.execute("DELETE FROM gdlCeldas WHERE CodigoCT like ?", minWildcard)
    
    celdas=custom.customPopulate(str(MINECO[0:3]))
    #celdas = cursorR.fetchall()
    for c in celdas:
        if c.CodigoCT[0:3] != MINECO[0:3]:
            c.CodigoCT = MINECO[0:3] + '-' + c.CodigoCT
            
        print ''
        print 'INSERT INTO GDLCELDAS'
        
        print str(c.CodigoCT)
        print str(c.CodigoLinea[:10])
        print str(c.X)+'*'+str(c.Y)
        print str(c.Nombre)
        print str(c.IdInstLinea)
        print str(c.IdTipoCelda)
        print str(c.IdInst)
        print str(c.IdTransformador)
        print str(c.DisyuntorFusible)
        print str(c.Modelo)
        print str(c.Transformador)
           
            
        print 'correccion coordenadas: '+str(int(c.X)) + '***'+str(int(c.Y))    
        
        cursorW.execute("INSERT INTO gdlCeldas VALUES(\
                        ?,?,?,?,?,?,?,?,?,?,?,?)", \
                        c.CodigoCT, c.CodigoLinea[:10], c.X, c.Y, c.Nombre, c.IdInstLinea, \
                        c.IdTipoCelda, c.IdInst, c.IdTransformador, \
                        c.DisyuntorFusible, c.Modelo, c.Transformador)
        global contador
        contador=contador+1
        print contador
        PrgDialog.SetLabel("Faltan "+str(celdas.__len__()-contador)+" Celdas...")
    cnxnW.commit()

def populateGdlCeldasUnCT(cursorR,cnxnW, MINECO,nombreCT):
    """
    Fill in celda data to the table gdlCeldas. When creating CT:s, this table
    is used to direct creation of the MV_LV_SUBSTATION disconnectors and transformer
    nodes.
    """
    cursorW = cnxnW.cursor()
    
    # cargar datos celdas en base de datos dms tabla gdlCeldas
    minWildcard =  MINECO[0:3] + '%'
    cursorR.execute("""
                DECLARE @wildcard nvarchar(4);
                SET @wildcard = ?;
                
                WITH ct_no_celda AS
                (
                -- if we join to this subquery the listing should only contains CTs
                -- that have no celdas
                    SELECT CT.IdInst
                    FROM CT
                        LEFT JOIN CTCeldas
                            ON CT.IdInst = CTCeldas.IdInst
                    WHERE CT.IdInst LIKE @wildcard AND CTCeldas.IdInst IS NULL
                )
                -- the first query gets the data from CTCeldas, which is where
                -- we assume things are correct. For those that have no rows in
                -- CTCeldas we fetch the data by deducing from the number of 
                -- lines connected etc
                SELECT    Instalaciones.Codigo AS CodigoCT,
                        ISNULL(AT_Instalaciones.Codigo, '1') AS CodigoLinea,
                        CT.X,
                        CT.Y,
                        Instalaciones.Nombre,
                        AT.IdInst AS IdInstLinea,
                        CASE WHEN IdTransformador IS NOT NULL THEN 2 ELSE 1 END AS IdTipoCelda,
                        Instalaciones.IdInst,
                        IdTransformador,
                        CASE WHEN DisyuntorFusible = 'Fusible' THEN 1 ELSE NULL END AS DisyuntorFusible,
                        NULL AS Modelo,
                        CASE WHEN IdTransformador IS NOT NULL THEN 1 ELSE NULL END AS Transformador
                FROM CT
                    JOIN CTCeldas
                        ON CTCeldas.IdInst = CT.IdInst
                    JOIN Instalaciones
                        ON Instalaciones.IdInst = CT.IdInst
                    LEFT JOIN AT
                        ON AT.IdInst = CTCeldas.IdAlimentacion
                        OR AT.IdInst = CTCeldas.IdDerivacion
                    LEFT JOIN Instalaciones AS AT_Instalaciones
                        ON AT.IdInst = AT_Instalaciones.IdInst
                WHERE CTCeldas.IdInst LIKE @wildcard AND IdTipoCelda <> 2
                UNION ALL
                -- alimentationes (incoming lines)
                SELECT  Instalaciones.Codigo AS CodigoCT,
                        Ins_ali.Codigo AS CodigoLinea,
                        CT.X,
                        CT.Y,
                        Instalaciones.Nombre,
                        CTAlimentaciones.IdInstAlimentaciones AS IdInstLinea,
                        '1' AS IdTipoCelda,
                        Instalaciones.IdInst,
                        NULL AS IdTransformador,
                        1 AS DisyuntorFusible,
                        NULL AS Modelo,
                        NULL AS Transformador 
                FROM    CT
                        JOIN ct_no_celda
                            ON ct_no_celda.IdInst = CT.IdInst
                        JOIN Instalaciones
                            ON CT.IdInst = Instalaciones.IdInst
                        JOIN CTAlimentaciones
                            ON CTAlimentaciones.IdInst = Instalaciones.IdInst 
                        JOIN Instalaciones Ins_ali
                            ON Ins_ali.IdInst = CTAlimentaciones.IdInstAlimentaciones
                WHERE Instalaciones.IdInst LIKE @wildcard
                UNION ALL
                -- derivaciones, or outgoing lines
                SELECT    Instalaciones.Codigo AS CodigoCT,
                        Ins_der.Codigo CodigoLinea,
                        CT.X,
                        CT.Y,
                        Instalaciones.Nombre,
                        CTDerivaciones.IdInstDerivaciones AS IdInstLinea,
                        '1' AS IdTipoCelda,
                        Instalaciones.IdInst,
                        NULL AS IdTransformador,
                        1 AS DisyuntorFusible,
                        NULL AS Modelo,
                        NULL AS Transformador
                FROM    CT
                        JOIN ct_no_celda
                            ON ct_no_celda.IdInst = CT.IdInst
                        JOIN Instalaciones
                            ON CT.IdInst = Instalaciones.IdInst
                        LEFT JOIN CTDerivaciones
                            ON CTDerivaciones.IdInst = Instalaciones.IdInst
                        JOIN AT
                            ON AT.IdInst = CTDerivaciones.IdInstDerivaciones
                            -- usually derivaciones is ok, sometimes this row does
                            -- not exist and we have to actually see if there is a
                            -- line that starts from our CT
                            OR (AT.IdOrigenPunto = CT.IdInst AND IdTipoOrigen = 4)
                        JOIN Instalaciones AS Ins_der
                            ON Ins_der.IdInst = AT.IdInst
                WHERE    Instalaciones.IdInst LIKE @wildcard
                UNION ALL
                -- transformer celdas
                SELECT    Instalaciones.Codigo CodigoCT,
                        CTTransformadores.NumeroTransformador AS CodigoLinea,
                        CT.X,
                        CT.Y,
                        Instalaciones.Nombre,
                        NULL AS IdInstLinea,
                        '2' AS IdTipoCelda,
                        Instalaciones.IdInst,
                        CTTransformadores.IdTransformador,
                        NULL AS DisyuntorFusible,
                        NULL AS Modelo,
                        CTTransformadores.NumeroTransformador AS Transformador 
                FROM    CT
                        JOIN Instalaciones
                            ON CT.IdInst=Instalaciones.IdInst
                        JOIN CTTransformadores 
                            ON CTTransformadores.IdInst = Instalaciones.IdInst
                WHERE    Instalaciones.IdInst LIKE @wildcard
                    """, minWildcard)
    

    
    cursorW.execute("DELETE FROM gdlCeldas WHERE CodigoCT like ?", minWildcard)
    
    celdas = cursorR.fetchall()
    for c in celdas:
        if c.CodigoCT[0:3] != MINECO[0:3]:
            c.CodigoCT = MINECO[0:3] + '-' + c.CodigoCT
        
        print ''
        print 'INSERT INTO GDLCELDAS'
        print str(c.CodigoCT)+'-'+str(c.CodigoLinea[:10])+'-'+str(c.X)+'-'+str(c.Y)+'-'+str(c.Nombre)+'-'+str(c.IdInstLinea)+'-'+str(c.IdTipoCelda)+'-'+str(c.IdInst)+'-'+str(c.IdTransformador)+'-'+str(c.DisyuntorFusible)+'-'+str(c.Modelo)+'-'+str(c.Transformador)
        
        
        if nombreCT==c.Nombre:
            cursorW.execute("INSERT INTO gdlCeldas VALUES(\
                        ?,?,?,?,?,?,?,?,?,?,?,?)", \
                        c.CodigoCT, c.CodigoLinea[:10], c.X, c.Y, c.Nombre, c.IdInstLinea, \
                        c.IdTipoCelda, c.IdInst, c.IdTransformador, \
                        c.DisyuntorFusible, c.Modelo, c.Transformador)
           
#        else:
#            print 'skipping '+c.Nombre
        
#        global contador
#        contador=contador+1
#        print contador
    cnxnW.commit()


# Progress Dialog
def CT(cnxnR, CTName, MINECO):
    PrgDialog = wx.ProgressDialog("Importando", "Importando Centros de Transformaci�n...",
                                  maximum=1000, style=wx.PD_AUTO_HIDE)
    PrgDialog.SetSize((400, 110))
    PrgDialog.Update(1)
    progreso_actual = 0
    cursorR = cnxnR.cursor()
   
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
    cursorW = cnxnW.cursor()
    
    # Get all the CTs
    try:
    
            cursorR.execute("""
            SELECT  Distinct Ins.Nombre,
                    lp.CodigoPeco AS PROV,
                    lm.CodigoPeco,
                    Ins.Codigo,
                    CT.Particular
             FROM Instalaciones Ins JOIN CT ON Ins.IdInst=CT.IdInst 
                 LEFT JOIN LocMunicipios lm ON lm.IdMunicipio = Ins.IdMunicipio
                 LEFT JOIN LocProvincias lp ON lp.IdProvincia = Ins.IdProvincia  
             WHERE  Borrada = 0 AND Ins.IdInst LIKE  ?
             """, MINECO[0:3] + '%')
    
    except:
         wx.MessageBox("Problemas de conexi�n con la base de datos",
                "Error", wx.OK|wx.ICON_ERROR)
    CTs = cursorR.fetchall()
    
    rowsInicial= CTs.__len__()

    
    populateGdlCeldas(cursorR,cnxnW, MINECO,PrgDialog)
    
    progCT = 0 # progress counter
    for CT in CTs:
        print CT.Codigo
        try:
            progCT = progCT + 1
            importarCT(CT,cursorR,cnxnW,cursorW,MINECO,PrgDialog)
        except:
            logging.exception("failed to import CT " + CT.Codigo)
            TrazaImport.WriteLog("Error imprevisto: " + str(CT.Nombre))
            cnxnW.commit()
            
            return
        PrgDialog.Update(float(progCT) / float(len(CTs)) * 1000.0)
        PrgDialog.SetLabel("Faltan "+str(rowsInicial-progCT)+" CTs...")
    try:
        sqlDMS.createOPCCode(cursorW, MINECO[0:3], DataBase.DMSLocation)
        cnxnW.commit()
    except:
        cnxnW.commit()

def CTAlone(cnxnR, CTName, MINECO):
#    PrgDialog = wx.ProgressDialog("Importando", "Importando un Centro de Transformaci�n...",
#                                  maximum=1000, style=wx.PD_AUTO_HIDE)
#    PrgDialog.SetSize((400, 110))
#    PrgDialog.Update(1)
#    progreso_actual = 0
    cursorR = cnxnR.cursor()
   
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
    cursorW = cnxnW.cursor()
    
    # Get a CT
    cursorR.execute("""
            SELECT  Distinct Ins.Nombre,
                    lp.CodigoPeco AS PROV,
                    lm.CodigoPeco,
                    Ins.Codigo,
                    CT.Particular
             FROM Instalaciones Ins JOIN CT ON Ins.IdInst=CT.IdInst 
                 LEFT JOIN LocMunicipios lm ON lm.IdMunicipio = Ins.IdMunicipio
                 LEFT JOIN LocProvincias lp ON lp.IdProvincia = Ins.IdProvincia  
             WHERE  Borrada = 0 AND Ins.IdInst LIKE  ? AND Ins.Nombre = ?
             """, MINECO[0:3] + '%', str(CTName))
    
    CTs = cursorR.fetchall()

    populateGdlCeldasUnCT(cursorR,cnxnW, MINECO,CTName)
    
    progCT = 0 # progress counter
    for CT in CTs:
        print CT.Codigo
        try:
            progCT = progCT + 1
            importarCT(CT,cursorR,cnxnW,cursorW,MINECO)
        except:
            logging.exception("failed to import CT " + CT.Codigo)
            TrazaImport.WriteLog("Error imprevisto: " + str(CT.Nombre))
            cnxnW.commit()
        #PrgDialog.Update(float(progCT) / float(len(CTs)) * 1000.0)
    try:
        sqlDMS.createOPCCode(cursorW, MINECO[0:3], DataBase.DMSLocation)
        cnxnW.commit()
    except:
        cnxnW.commit()

def getPropios(particular):
    """
    Get 'PROPIOS' for a CT when you have 'Particular'
    """
    if not particular:
        return 1
    else:
        return 0

def getCTVoltageLevel(cursorR, cursorW, CTName, MINECO):
    """
    Get the DMS CODEINFO CODE for the voltage level of the busbars inside
    CT given in parameter CTName (note, this really in Nombre and not Codigo!)
    """
    
#     cursorR.execute("select top 1 ATTensionNominal.Valor from CTAlimentaciones, AT, ATTensionNominal, Instalaciones\
#                     where CTAlimentaciones.IdInstAlimentaciones LIKE AT.IdInst and ATTensionNominal.IdTensionNominal LIKE AT.IdTensionNominal \
#                     and Instalaciones.IdInst LIKE CTAlimentaciones.IdInst and Instalaciones.Nombre LIKE ? and Instalaciones.IdInst LIKE '?%'\
#                     union\
#                     select top 1 ATTensionNominal.Valor from CTDerivaciones, AT, ATTensionNominal, Instalaciones\
#                     where CTDerivaciones.IdInstDerivaciones LIKE AT.IdInst and ATTensionNominal.IdTensionNominal LIKE AT.IdTensionNominal \
#                     and Instalaciones.IdInst LIKE CTDerivaciones.IdInst and Instalaciones.Nombre LIKE ?", CTName, str(MINECO), CTName)

    #Modificado 27/01/14
    query="""select top 1 ATTensionNominal.Valor from CTAlimentaciones, AT, ATTensionNominal, Instalaciones
            where CTAlimentaciones.IdInstAlimentaciones LIKE AT.IdInst and ATTensionNominal.IdTensionNominal LIKE AT.IdTensionNominal 
            and Instalaciones.IdInst LIKE CTAlimentaciones.IdInst and Instalaciones.Nombre LIKE '"""+CTName+"""' 
            and Instalaciones.IdInst LIKE '""" + str(MINECO) + """%'
            union
            select top 1 ATTensionNominal.Valor from CTDerivaciones, AT, ATTensionNominal, Instalaciones
            where CTDerivaciones.IdInstDerivaciones LIKE AT.IdInst and ATTensionNominal.IdTensionNominal LIKE AT.IdTensionNominal 
            and Instalaciones.IdInst LIKE CTDerivaciones.IdInst and Instalaciones.Nombre LIKE '"""+CTName+"""' 
            and Instalaciones.IdInst LIKE '""" + str(MINECO) + """%'"""
    cursorR.execute(query)

    VoltageLevel = cursorR.fetchone()
    if VoltageLevel == None:
        # didn't find voltage level, getting the voltage level from *any* trafo we can find
        cursorR.execute("select ATTensionNominal.Valor from CTAlimentaciones, AT, ATTensionNominal, Instalaciones\
                        where CTAlimentaciones.IdInstAlimentaciones = AT.IdInst and ATTensionNominal.IdTensionNominal = AT.IdTensionNominal \
                        and Instalaciones.IdInst = CTAlimentaciones.IdInst\
                        union\
                        select ATTensionNominal.Valor from CTDerivaciones, AT, ATTensionNominal, Instalaciones\
                        where CTDerivaciones.IdInstDerivaciones = AT.IdInst and ATTensionNominal.IdTensionNominal = AT.IdTensionNominal \
                        and Instalaciones.IdInst = CTDerivaciones.IdInst")
        VoltageLevel = cursorR.fetchone()
        logging.debug("cannot find voltage level for trafo " + CTName + ", using default")
    VoltageLevel = VoltageLevel[0]
    print 'VoltageLevel: '+str(VoltageLevel)
    cursorW.execute("SELECT CODE FROM CODEINFO WHERE INFOTYPE = 'VOLTAGE_LEVEL' AND INFO = ? + ' kV'",
                    str(VoltageLevel).replace('.0', ''))   

    VoltageLevel = cursorW.fetchone()
    
    if VoltageLevel is None and DataBase.DevelopmentMode == 'YES' :
        wx.MessageBox("REVISAR CT CON PROBLEMAS: (voltage level)"+str(CTName), "DEVELOP MODE",  wx.OK | wx.ICON_ERROR)
        VoltageLevel=0   
    else:
        VoltageLevel = VoltageLevel[0]

    
    return VoltageLevel

def pad(string, length, padChar = '0'):
    """
    Pads a string with the specified character to
    be len characters long. Longer than that, and 
    it will be cut out.
    """
    if string == None:
        return padChar * length
    elif len(str(string)) > length:
        return str(string)[0:length]
    else:
        return (length - len(str(string))) * padChar + str(string)

def importarCT(CT,cursorR,cnxnW,cursorW,MINECO,PrgDialog):
    TrazaImport.WriteLog('Importando CT: ' + CT.Nombre)
    PrgDialog.Update(0,"Importando CT: "+ CT.Nombre)
    # Getting the Voltage Level from network
    VoltageLevel = getCTVoltageLevel(cursorR, cursorW, CT.Nombre,MINECO[:3])
    
    # TODO why plus uno�?�?
    code = CT.Codigo + '1'
    name = CT.Nombre
    
    if code[0:3] != MINECO:
        # in case traza code does not begin with MINECO code
        code = MINECO + '-' + code

    print 'code ='+code
    print 'name ='+name
    
    # read celda definitions from table that was previously populated,
    # taking celda ordering into account if it exists
    cursorW.execute("""
        SELECT  gdlCeldas.CodigoCT,
                gdlCeldas.CodigoLinea,
                gdlCeldas.X,
                gdlCeldas.Y,
                gdlCeldas.Nombre,
                gdlCeldas.IdInst,
                gdlCeldas.IdTipoCelda,
                gdlCeldas.IdTransformador,
                gdlCeldas.DisyuntorFusible,
                gdlCeldas.Modelo,
                gdlCeldas.Transformador,
                gdlOrdenCeldas.Posicion
        FROM gdlCeldas
            LEFT JOIN gdlOrdenCeldas
                ON gdlCeldas.CodigoCT = gdlOrdenCeldas.CodigoCT
                AND (gdlCeldas.Transformador = gdlOrdenCeldas.Transformador
                        OR
                     gdlCeldas.CodigoLinea = gdlOrdenCeldas.CodigoLinea)
        where gdlCeldas.CodigoCT = ?
        order by gdlOrdenCeldas.Posicion
        """, str(code[:-1]))
    
    print 'CT.Codigo ='+CT.Codigo
    rows = cursorW.fetchall()
    
    if rows.__len__()==0 and DataBase.DevelopmentMode == 'YES' :
            xg=0
            yg=0
            wx.MessageBox("REVISAR CT CON PROBLEMAS: "+str(CT.Codigo), "DEVELOP MODE",  wx.OK | wx.ICON_ERROR)
    
    tipo = []
    disyuntorFusible = []
    trafo = []
    for rowR in rows:
        #print 'zoneMineco + '+zone(MINECO)
        xg, yg = utiDMS.convertED50(rowR.X, rowR.Y, zone(MINECO))
        xg = int(str(xg).split('.')[0] + "000")
        yg = int(str(yg).split('.')[0] + "000")
        
#         coordenadas2=open('C:\\buzon\\177\\AnalisisCoordenadas2.txt','a')
#         coordenadas2.write('Xg '+str(xg)+' --- Yg '+str(yg)+'\n')
        
        
        print 'xg*yg'
        print str(xg)+'*'+str(yg)
        
        tipo.append(rowR.IdTipoCelda) 

        # if there is a Celda, then insert always a Disconnector
        # TODO not necessarily so!�!
        disyuntorFusible.append('Disyuntor')
        trafo.append(rowR.IdTransformador)
    num = len(tipo)

    # municipality code must be padded with zeros
    mun_code = pad(CT.CodigoPeco, 3)
    prov_code = pad(CT.PROV, 2)
    
    #Diagram
    sqlDMS.iDiagram(cursorW, code, 'M', xg, yg, num)
    #MV_LV_substation
    sqlDMS.iMV_LV_Substation(cursorW,
                             code,
                             mun = mun_code,
                             prov = prov_code,
                             mineco = MINECO,
                             propios = getPropios(CT.Particular))
    #MV_NODE
    
    #Switch for deciding type of nodes in the busbar
    values = {  1: "X",
                2: "X",
                5: "E" }
    #Insert SiteNode
    sitenode = utiDMS.getDMSCode(xg + 1000, yg + 1000) + "X1"
    sqlDMS.iMV_Node(cursorW, sitenode, xg + 1000, yg + 1000, xg + 1000, yg + 1000, sitenode, mineco=MINECO)
    #MV_Site
    sqlDMS.iMV_Site(cursorW, code, 'M', xg + 1000, yg + 1000, name=name, mineco=MINECO) 
    
    
    #Insert SiteNodes in Busbar
    existsOrdenCeldas = sqlDMS.getExistsOrdenCeldas(cursorW, CT.Codigo)
    for i in range(1, num + 1):
        addC = values.get(int(tipo[i - 1]))
        xi = xg + 1000 * i
        yi = yg + 1000
        nodecode = utiDMS.getDMSCode(xi, yi) + addC + "1"
        if tipo[i - 1] == 5: #Insert Disconnector in busbar
            if existsOrdenCeldas:
                name = utiDMS.getCodeDisconnector(cursorW, MINECO, rowR.Nombre, `i`, CTCode=code[:-1], cursorT=cursorR)
            else:
                name = utiDMS.getCodeDisconnector(cursorW, MINECO, rowR.Nombre, `i`)
            sqlDMS.iDisconnector(cursorW, nodecode, name, code, CT.Nombre) 
        if i > 1:
            sqlDMS.iMV_Node(cursorW, nodecode, xi, yi, xi, yi, sitenode, mineco=MINECO)
        
    #Insert Disconnectors
    for i in range (1, num + 1):
        xi = xg + 1000 * i
        yi = yg + 2000
        if tipo[i - 1] == 2 or tipo[i - 1] == 1 or tipo[i - 1] == 4:
            
            
#                name = MINECO + rowR.Nombre.replace(' ', '')[0:4] + rowR.Nombre[-5:] + "P" + `i`
            if existsOrdenCeldas:
                name = utiDMS.getCodeDisconnector(cursorW, MINECO, rowR.Nombre, `i`, CTCode=code[:-1], cursorT=cursorR)
            else:
                name = utiDMS.getCodeDisconnector(cursorW, MINECO, rowR.Nombre, `i`)
            if disyuntorFusible[i - 1] == 'Disyuntor':
                nodecode = utiDMS.getDMSCode(xi, yi) + "E1"
                sqlDMS.iDisconnector(cursorW, nodecode, name, code, CT.Nombre)      
            if disyuntorFusible[i - 1] == 'Fusible':
                nodecode = utiDMS.getDMSCode(xi, yi) + "O1"
                sqlDMS.iFuse(cursorW, nodecode, name, code)
            if disyuntorFusible[i - 1] == None:
                nodecode = utiDMS.getDMSCode(xi, yi) + "X1"
            sqlDMS.iMV_Node(cursorW, nodecode, xi, yi, xi, yi, sitenode, mineco=MINECO)
        
    #Insert Transformer
    j = 0
    for i in range (1, num + 1):
        xi = xg + 1000 * i
        yi = yg + 3000
        
        if tipo[i - 1] == 2:
            j = j + 1
            nodecode = utiDMS.getDMSCode(xi, yi) + "M1"
            codetrafo = trafo[i - 1]
            
            if j == 1:
                sqlDMS.pTransformer(cnxnW, cursorW, trafo[i - 1], code, nodecode, sitenode)
                sqlDMS.iMV_Node(cursorW, nodecode, xi, yi, xi, yi, sitenode, mineco=MINECO)
            else:
                sqlDMS.pTransformer(cnxnW, cursorW, trafo[i - 1], code[:-1] + str(j), nodecode, nodecode)
                sqlDMS.iMV_Node(cursorW, nodecode, xi, yi, xi, yi, nodecode, mineco=MINECO)
                sqlDMS.iMV_Site(cursorW, code[:-1] + str(j), 'T', xi, yi, name="", mineco=MINECO)
                sqlDMS.iMV_LV_Substation(cursorW, code[:-1] + str(j), mun=mun_code, prov=prov_code, mineco=MINECO)
    
    #Insert Exit Points
    for i in range (1, num + 1):
        xi = xg + 1000 * i
        yi = yg + 4000
        if tipo[i - 1] == 1 or tipo[i - 1] == 4:
            nodecode = utiDMS.getDMSCode(xi, yi) + "X1"
            sqlDMS.iMV_Node(cursorW, nodecode, xi, yi, xi, yi, sitenode, mineco=MINECO) 
                            
    #Inserting Sections
    j = 0
    for i in range (1, num + 1):
        if i < num:
        #Busbar connections
            addC1 = values.get(int(tipo[i - 1]))
            addC2 = values.get(int(tipo[i]))
            x1 = xg + 1000 * i
            y1 = yg + 1000
            x2 = xg + 1000 * (i + 1)
            y2 = y1
            node1 = utiDMS.getDMSCode(x1, y1) + addC1 + "1"
            node2 = utiDMS.getDMSCode(x2, y2) + addC2 + "1"
            mv_section_id = sqlDMS.uMV_Section_ID(cnxnW, cursorW)
            sqlDMS.iMV_Section(cursorW, mv_section_id, node1, node2, 'BUSBAR', MINECO, VoltageLevel, x1, y1, SUBSTATION=code, HIDE=1)
            sqlDMS.iMV_SectionPoints (cursorW, mv_section_id, node1, node2, [{'x':xg + 1000, 'y':yg + 1000, 'numSecc':1}], mineco=MINECO)
        
        #Disconnector connections
        if tipo[i - 1] == 2 or tipo[i - 1] == 1 or tipo[i - 1] == 4:
            x1 = xg + 1000 * i
            y1 = yg + 1000
            x2 = x1
            y2 = yg + 2000
            node1 = utiDMS.getDMSCode(x1, y1) + "X1"
            
            if disyuntorFusible[i - 1] == 'Disyuntor':
                node2 = utiDMS.getDMSCode(x2, y2) + "E1"  
            if disyuntorFusible[i - 1] == 'Fusible':
                node2 = utiDMS.getDMSCode(x2, y2) + "O1"
            if disyuntorFusible[i - 1] == None:
                node2 = utiDMS.getDMSCode(x2, y2) + "X1"
            
#                node2 = utiDMS.getDMSCode(x2, y2) + "X1"
            mv_section_id = sqlDMS.uMV_Section_ID(cnxnW, cursorW)
            sqlDMS.iMV_Section(cursorW, mv_section_id, node1, node2, 'BUSBAR', MINECO, VoltageLevel, x1, y1, SUBSTATION=code, HIDE=1)
              
        #Transformer connections (if needed)
    j = 0
    for i in range (1, num + 1):
        if tipo[i - 1] == 2:
            j = j + 1
            x1 = xg + 1000 * i
            y1 = yg + 2000
            x2 = x1
            y2 = yg + 3000
            if disyuntorFusible[i - 1] == 'Disyuntor':
                node1 = utiDMS.getDMSCode(x1, y1) + "E1"  
            if disyuntorFusible[i - 1] == 'Fusible':
                node1 = utiDMS.getDMSCode(x1, y1) + "O1"
            if disyuntorFusible[i - 1] == None:
                node1 = utiDMS.getDMSCode(x1, y1) + "X1"
            node2 = utiDMS.getDMSCode(x2, y2) + "M1"
            mv_section_id = sqlDMS.uMV_Section_ID(cnxnW, cursorW)
            codetrafo = trafo[i - 1]
            sqlDMS.iMV_Section(cursorW, mv_section_id, node1, node2, 'BUSBAR', MINECO, VoltageLevel, x1, y1, SUBSTATION=code[:-1] + str(j), HIDE=1)
            sqlDMS.sTransformer(cursorW, mv_section_id, codetrafo)
      
        #Exit connections
        if tipo[i - 1] in [1, 4]:
            x1 = xg + 1000 * i
            y1 = yg + 2000
            x2 = x1
            y2 = yg + 4000
            
            if disyuntorFusible[i - 1] == 'Disyuntor':
                node1 = utiDMS.getDMSCode(x1, y1) + "E1"  
            if disyuntorFusible[i - 1] == 'Fusible':
                node1 = utiDMS.getDMSCode(x1, y1) + "O1"
            if disyuntorFusible[i - 1] == None:
                node1 = utiDMS.getDMSCode(x1, y1) + "X1"
            
#                node1 = utiDMS.getDMSCode(x1, y1) + "X1"
            node2 = utiDMS.getDMSCode(x2, y2) + "X1"
            mv_section_id = sqlDMS.uMV_Section_ID(cnxnW, cursorW)
            sqlDMS.iMV_Section(cursorW, mv_section_id, node1, node2, 'BUSBAR', MINECO, VoltageLevel, x1, y1, SUBSTATION=code, HIDE=1)
        cnxnW.commit()

def getBTConductorType(cursorT, idTipoConductor):
    cursorT.execute("SELECT Descripcion FROM BTVanoTiposConductores\
                     WHERE IdTipoConductor = ?", idTipoConductor)
    t = cursorT.fetchone()
    if t != None:
        return t.Descripcion
    else:
        return "BUSBAR"

def makeAllComienzoLvNodes(cursorW, cursorT):
    cursorT.execute("select IdBTVano, X, Y, Z \
                    from BTVano \
                    where (SELECT TOP 1 IdComienzo FROM BTVano AS b2 \
                    WHERE b2.IdComienzo = BTVano.IdBTVano) IS NOT NULL")
    comNodes = cursorT.fetchall()
    
    for n in comNodes:
        if n.Z == -30:
            # quick hack for nodes that have their coordinates
            # translated in the database
            X = n.X
            Y = n.Y
        else:
            X, Y = utiDMS.convertED50(n.X, n.Y, zone)
        
        sqlDMS.makeLVNode(cursorW, X * 1000, Y * 1000, 'X', IdBTVano=n.IdBTVano)

def LVline(cursorT, cursorW, IdInst, MINECO, trafoboxes):
# locating the line
    cursorT.execute("select * from BT, Instalaciones\
                    where Instalaciones.IdInst = BT.IdInst\
                    and Instalaciones.IdInst = ? and Ins.IdInst LIKE ?", IdInst, MINECO[0:3] + '%')
    BTline = cursorT.fetchone()
    TrazaImport.WriteLog('Importando linea de BT: ' + BTline.Nombre)
    
    if BTline.IdTransformador == None:
        TrazaImport.WriteLog("Error: transformer not set fot " + BTline.Nombre)
        return
    
# locating de CT

    cursorT.execute("SELECT * FROM CT, Instalaciones\
                    WHERE CT.IdInst =  ?\
                    and Instalaciones.IdInst = CT.IdInst", BTline.IdInstCT)
    CT = cursorT.fetchone()
    CTName = CT.Nombre
    
# locating the transformer

    cursorW.execute("SELECT *\
                     FROM TRANSFORMER\
                     where CODE like ?",
                     BTline.IdTransformador + '%')
    Transformer = cursorW.fetchone()
    
# locating the transformer node

    cursorW.execute("SELECT *\
                  FROM TRANSFORMER_NODE\
                  where MV_LV_SUBSTATION = ?\
                  and ORDERNUMBER = ?", Transformer.PLACING_SITE, Transformer.ORDERNUMBER)
    NodeTransformer = cursorW.fetchone()
    Xt, Yt = utiDMS.getCoordinates(NodeTransformer.NODECODE)
    print 'Xt= '+Xt+' Yt= '+Yt
    
    # creating the box
    # if previously created, skip
    Xbox, Ybox = Xt, Yt + 1000 + 2500 * Transformer.ORDERNUMBER
    Xswitch1, Yswitch1 = Xbox, Ybox - 300
    
    if not trafoboxes.has_key(NodeTransformer.NODECODE):
        NodeBox = sqlDMS.makeLVNode(cursorW, Xbox, Ybox, 'J')
        sqlDMS.iLV_BOX(cursorW, NodeBox, CTName, MINECO)
        
        # creating the switch 1
        NodeSwitch1 = sqlDMS.makeLVNode(cursorW, Xswitch1, Yswitch1, 'W', NodeBox)
        
        feeder = MINECO + '-' + CTName + '-' + str(Transformer.ORDERNUMBER) + '-' 'IN'
        sqlDMS.iLV_SWITCH(cursorW, NodeSwitch1, MINECO + '-' + CTName, feeder, 1, None, None)
        
        trafoboxes[NodeTransformer.NODECODE] = [NodeBox, 0]
        
        # section 1 from Transformer to Switch 1  
        LV_Section_Id = sqlDMS.iLV_Section(cursorW, NodeTransformer.NODECODE,
                                           NodeSwitch1, 'BUSBAR', MINECO + '-' + CTName,
                                           Xt=Xswitch1, Yt=Yswitch1, ANGLE=90)
        
        # set connecting line section
        cursorW.execute("UPDATE TRANSFORMER SET LINE_SECTION_2 = ?\
                         WHERE ORDERNUMBER = ? AND PLACING_SITE = ?",
                         LV_Section_Id, Transformer.ORDERNUMBER, Transformer.PLACING_SITE)
        
        # section 2 from Switch 1 to Box
        sqlDMS.iLV_Section(cursorW, NodeSwitch1, NodeBox, 'BUSBAR', MINECO + '-' + CTName,
                           Xt=Xbox, Yt=Ybox, ANGLE=90)   
    else:
        NodeBox = trafoboxes[NodeTransformer.NODECODE][0]
        
# creating the switch 2

    # feeder number
    trafoboxes[NodeTransformer.NODECODE][1] += 1
    
    Xswitch2, Yswitch2 = Xswitch1 + 200 * trafoboxes[NodeTransformer.NODECODE][1], Yswitch1
    NodeSwitch2 = sqlDMS.makeLVNode(cursorW, Xswitch2, Yswitch2, 'W', NodeBox)
    feeder = MINECO + '-' + CTName + '-' + 'AL' + str(trafoboxes[NodeTransformer.NODECODE][1])
    sqlDMS.iLV_SWITCH(cursorW, NodeSwitch2, MINECO + '-' + CTName, feeder, 2, 16 , 'DEFAULT_gG_16') 
    
    # section 3 from Box to Switch 2
    sqlDMS.iLV_Section(cursorW, NodeBox, NodeSwitch2, 'BUSBAR', MINECO + '-' + CTName,
                       Xt=Xswitch2, Yt=Yswitch2, ANGLE=90)
    
# creating the sections

    # getting the nodes
    
    cursorT.execute("select *,\
                    CASE \
                    WHEN (EXISTS (SELECT IdComienzo FROM BTVano AS b2 \
                    WHERE b2.IdComienzo = BTVano.IdBTVano)) \
                    THEN 1 ELSE 0 END AS MakeNode from BTVano, Instalaciones \
                    where Instalaciones.IdInst = BTVano.IdInst\
                    and Instalaciones.IdInst = ?\
                    order by ApoyoSecuencial", IdInst)
    nodes = cursorT.fetchall()

    if len(nodes) == 0:
        return
    
    #conductor type
    CONDUCTOR = getBTConductorType(cursorT, nodes[0].IdTipoConductor)


    
# creating the line


    isFirst = True
    prevNode = NodeSwitch2
    prevNodeI = 0
    thisNodeI = 0
    i_node = 0
    for node in nodes:
        CONDUCTOR = getBTConductorType(cursorT, node.IdTipoConductor)
        if node.IdTipoVano == None:
            if i_node == len(nodes) - 1 and node.MakeNode != 1:
                # last of the section, must make node
                if node.Z == -30:
                    # quick hack for nodes that have their coordinates
                    # translated in the database
                    Xf = node.X
                    Yf = node.Y
                else:
                    Xf, Yf = utiDMS.convertED50(node.X, node.Y, zone(MINECO))
                thisNode = sqlDMS.makeLVNode(cursorW, Xf * 1000, Yf * 1000, 'X', IdBTVano=node.IdBTVano)
                prevNodeI = thisNodeI
                thisNodeI = i_node
                makeNode(thisNode, prevNodeI, thisNodeI, cursorW,
                         CONDUCTOR, MINECO, CTName, Xswitch2, Yswitch2, nodes, prevNode,
                         isFirst)
                prevNode = thisNode
                if isFirst:
                    isFirst = False
                        
                        
            elif node.MakeNode == 1:
                # node was created by MakeAllComienzoLvNodes
                thisNode = getNodeByIdBTVano(cursorW, node.IdBTVano)
                prevNodeI = thisNodeI
                thisNodeI = i_node
                makeNode(thisNode, prevNodeI, thisNodeI, cursorW,
                         CONDUCTOR, MINECO, CTName, Xswitch2, Yswitch2, nodes, prevNode,
                         isFirst)
                prevNode = thisNode
                if isFirst:
                    isFirst = False

                
        elif node.IdTipoVano == '1':
            # branch
            if node.IdComienzo == None:
                if i_node == 0:
                    #this line starts with a comienzo, so we must act as if this comienzes
                    #from the trafo
                    if node.Z == -30:
                        # quick hack for nodes that have their coordinates
                        # translated in the database
                        Xf = node.X
                        Yf = node.Y
                    else:
                        Xf, Yf = utiDMS.convertED50(node.X, node.Y, zone(MINECO))
                    thisNode = sqlDMS.makeLVNode(cursorW, Xf * 1000, Yf * 1000, 'X', IdBTVano=node.IdBTVano)
                    
                    prevNodeI = thisNodeI
                    thisNodeI = i_node
                    makeNode(thisNode, prevNodeI, thisNodeI, cursorW,
                             CONDUCTOR, MINECO, CTName, Xswitch2, Yswitch2, nodes, prevNode,
                             isFirst)
                    prevNode = thisNode
                    if isFirst:
                        isFirst = False
                else:
                    TrazaImport.WriteLog("Error: vano is a branch but comienzo not defined! @" + node.IdBTVano)
                
                continue
                
            if node.Z == -30:
                Xf = node.X
                Yf = node.Y
            else:
                Xf, Yf = utiDMS.convertED50(node.X, node.Y, zone(MINECO))
            
            if getNodeByIdBTVano(cursorW, node.IdBTVano) == None:
                branchNode = sqlDMS.makeLVNode(cursorW, Xf * 1000, Yf * 1000, 'X', IdBTVano=node.IdBTVano)
            else:
                # this node was created before starting to make lines 
                branchNode = getNodeByIdBTVano(cursorW, node.IdBTVano)
            
            startingNode = getNodeByIdBTVano(cursorW, node.IdComienzo)
            if startingNode == None:
                TrazaImport.WriteLog("Nodo " + node.IdBTVano + " comienza desde el nodo indefinido " \
                      + node.IdComienzo)
                continue
            
            sqlDMS.iLV_Section(cursorW,
                               startingNode, branchNode, \
                               CONDUCTOR, MINECO + '-' + CTName)
        else:
            #customer
            if node.IdBTVano == None:
                TrazaImport.WriteLog("Error, cliente sin nodo en " + node)
                continue
            custNode = getCustomerNodeByIdBTVano(cursorW, node.CodigoAcometida)
            
            if custNode != None:
                startingNode = getNodeByIdBTVano(cursorW, node.IdComienzo)
                if startingNode == None:
                    TrazaImport.WriteLog("Error: nodo incial " + custNode + " no encontrado")
                    continue
                
                sqlDMS.iLV_Section(cursorW,
                                   startingNode, custNode, \
                                   CONDUCTOR, MINECO + '-' + CTName)
        
        i_node = i_node + 1
            
# should be renamed to makeLVSection
def makeNode(thisNode, prevNodeI, thisNodeI, cursorW,
             CONDUCTOR, MINECO, CTName, Xswitch2, Yswitch2, nodes, prevNode,
             isFirst):
    
    # creating the section
    sqlDMS.iLV_Section(cursorW,
                       prevNode, thisNode, \
                       CONDUCTOR, MINECO + '-' + CTName)
    
    # adding the sectionpoints    
    
    secc = []
    numSecc = 0
    
    if isFirst:
        numSecc = numSecc + 1
        secc.append({"x":Xswitch2, "y":Yswitch2 - 1000, "numSecc":numSecc})
        
    for i in range(prevNodeI, thisNodeI):
        if nodes[i].IdTipoVano == None:
            numSecc = numSecc + 1
            if nodes[i].Z == -30:
                nX = nodes[i].X
                nY = nodes[i].Y
            else:
                nX, nY = utiDMS.convertED50(nodes[i].X, nodes[i].Y, zone(MINECO))
            secc.append({"x":nX * 1000, "y":nY * 1000, "numSecc":numSecc})
    
    sqlDMS.iLV_SectionPoints (cursorW, prevNode, thisNode, secc)

def getCustomerNodeByIdBTVano(cursorW, codigoAcometida):
    cursorW.execute("SELECT NODECODE FROM LV_CUSTOMER_NODE\
                     WHERE CUSTOMER_NODE = ?", codigoAcometida)
    node = cursorW.fetchone()
    
    if node != None:
        return node.NODECODE
    else:
        if codigoAcometida != None:
            TrazaImport.WriteLog("Acometida " + codigoAcometida \
                  + " no tiene cliente en DMS!")
        return None
    
def getNodeByIdBTVano(cursor, idBTVano):
    cursor.execute("SELECT TOP 1 NODECODE\
                    FROM gdlLV_NODE_BTVano\
                    WHERE IdBTVano = ?", idBTVano)
    nc = cursor.fetchone()
    
    if nc != None:
        return nc.NODECODE
    else:
        return None

def line(cnxnT, linName, MINECO):
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
    PrgDialog = wx.ProgressDialog("Importando", "Importando L�neas de Alta y Media Tensi�n...", maximum=1000, style=wx.PD_AUTO_HIDE)
    PrgDialog.SetSize((400, 110))
    PrgDialog.Update(1)
    cursorT = cnxnT.cursor()
    cursorT.execute("SELECT Distinct Ins.Nombre, Ins.IdInst \
                     FROM Instalaciones Ins,  ATVano ATV   \
                     WHERE  Ins.IdInst=ATV.IdInst and Ins.IdInst LIKE ?\
                     ORDER BY Ins.IdInst ", MINECO[0:3] + '%')
    lines2 = cursorT.fetchall()

    k = 0
    pd = 0
    
#    The lines are imported in order of rank and origin.
#    As for the rank, we get the ATTipoLinea Table, containing the following information:
#    IdTipoLinea    Descripcion
#    1    L�nea Principal
#    2    Derivaci�n
#    3    Subderivaci�n
#    
#    By the origin, we get the table ATTipoOrigen:
#    IdTipoOrigen    Descripcion
#    1    Punto de entronque
#    2    Subestaci�n
#    4    Centro de transformaci�n
#    5    Apoyo


    # tipo de l�nea: principal, secundaria, terciaria
    # primero se dibujan las l�neas principales
    for tipo in range(1, 4):
        # origen: CT, subestaci�n, l�nea, ...
        for origen in range(1, 6):
            cursorT.execute("SELECT Distinct Ins.Nombre, \
                    Ins.IdInst, AT.IdTipoOrigen, AT.IdTipoLinea\
                    FROM Instalaciones Ins,  ATVano ATV, AT \
                    WHERE  Ins.IdInst=ATV.IdInst and AT.IdInst=ATV.IdInst and\
                    AT.IdTipoOrigen = ? and AT.IdTipoLinea = ?\
                    and Ins.IdInst LIKE ?\
                    ORDER BY Ins.IdInst", origen, tipo, MINECO[0:3] + '%')
            lines = cursorT.fetchall()
            
            for line in lines:
                try:
                    
                    k = k + 1
                    ''' DEBUGGING MARCO GUIDA '''
                    print '**************************************************************************'+str(k)
                    if k==12:
                        print 'ahora'
                    
                    pd = importarLinea(cursorT,cnxnW,origen,tipo,MINECO,pd,line)
                    PrgDialog.Update(float(pd) / (float(len(lines2))) * 1000.0)
                except:
                    TrazaImport.WriteLog("Error imprevisto: " + str(line.Nombre))
                    logging.exception("error import line " + line.Nombre)
                    cnxnW.commit()
    try:
        PrgDialog.Update(1000)
    except:
        pass

def lineAlone(cnxnT, linName, MINECO):
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
    PrgDialog = wx.ProgressDialog("Importando", "Importando L�neas de Alta y Media Tensi�n...", maximum=1000, style=wx.PD_AUTO_HIDE)
    PrgDialog.SetSize((400, 110))
    PrgDialog.Update(1)
    cursorT = cnxnT.cursor()
    cursorT.execute("SELECT Distinct Ins.Nombre, Ins.IdInst \
                     FROM Instalaciones Ins,  ATVano ATV   \
                     WHERE  Ins.IdInst=ATV.IdInst and Ins.IdInst LIKE ?\
                     ORDER BY Ins.IdInst ", MINECO[0:3] + '%')
    lines2 = cursorT.fetchall()

    k = 0
    pd = 0
    
#    The lines are imported in order of rank and origin.
#    As for the rank, we get the ATTipoLinea Table, containing the following information:
#    IdTipoLinea    Descripcion
#    1    L�nea Principal
#    2    Derivaci�n
#    3    Subderivaci�n
#    
#    By the origin, we get the table ATTipoOrigen:
#    IdTipoOrigen    Descripcion
#    1    Punto de entronque
#    2    Subestaci�n
#    4    Centro de transformaci�n
#    5    Apoyo


    # tipo de l�nea: principal, secundaria, terciaria
    # primero se dibujan las l�neas principales
    for tipo in range(1, 4):
        # origen: CT, subestaci�n, l�nea, ...
        for origen in range(1, 6):
            cursorT.execute("SELECT Distinct Ins.Nombre, \
                    Ins.IdInst, AT.IdTipoOrigen, AT.IdTipoLinea\
                    FROM Instalaciones Ins,  ATVano ATV, AT \
                    WHERE  Ins.IdInst=ATV.IdInst and AT.IdInst=ATV.IdInst and\
                    AT.IdTipoOrigen = ? and AT.IdTipoLinea = ?\
                    and Ins.IdInst LIKE ?\
                    ORDER BY Ins.IdInst", origen, tipo, MINECO[0:3] + '%')
            lines = cursorT.fetchall()
            
            for line in lines:
                try:
                    
                    k = k + 1
                    if linName == line.Nombre:
                        pd = importarLinea(cursorT,cnxnW,origen,tipo,MINECO,pd,line)
                        PrgDialog.Update(float(pd) / (float(len(lines2))) * 1000.0)
                    
                    
                except:
                    TrazaImport.WriteLog("Error imprevisto: " + str(line.Nombre))
                    logging.exception("error import line " + line.Nombre)
                    cnxnW.commit()
    try:
        PrgDialog.Update(1000)
    except:
        pass

def getVoltageLevel(cursorT, cursorW, linName):
    """
    returns the DMS voltage level code for the specified traza linea
    """
    # Se obtiene de traza la tensi�n de la l�nea y el tipo de origen.
    #Tensi�n = TipoOrigen[1] y TipoOrigen = TipoOrigen[0]
    cursorT.execute("""
        SELECT IdTipoOrigen, ATTensionNominal.Valor
        FROM AT, Instalaciones, ATTensionNominal
        where Nombre LIKE ? and Instalaciones.IdInst LIKE AT.IdInst
        and ATTensionNominal.IdTensionNominal LIKE AT.IdTensionNominal""",
        linName)
    TipoOrigen = cursorT.fetchone()
    
    # Se obtiene el valor de dicha tensi�n en DMS
    cursorW.execute("""
        SELECT CODE FROM CODEINFO
        WHERE INFOTYPE = 'VOLTAGE_LEVEL'
        AND INFO = ? + ' kV'
        """, str(TipoOrigen[1]).replace('.0',''))        
    VoltageLevel = cursorW.fetchone()
    return VoltageLevel[0]

def getLineStartNode(cursorT, cursorW, linName, MINECO):

    print 'liname '+ linName
    
    
    cursorT.execute("""
        SELECT ATVano.IdATVano AS VANO, Inst_CT.Codigo AS CT, ST.IdInst AS ST, Nodos.IdNodo AS NODO,
            ATVano.X AS VX, ATVano.Y AS VY, ST.X AS STX, ST.Y AS STY, Nodos.X AS NX, Nodos.Y AS NY,
            ATVano_origlin.IdATVano AS VANO_L, ATVano_origlin.X AS VLX, ATVano_origlin.Y AS VLY
        FROM AT
        JOIN Instalaciones ON AT.IdInst = Instalaciones.IdInst
        LEFT JOIN ATVano ON IdOrigenPunto = ATVano.IdATVano AND IdTipoOrigen = 5
        LEFT JOIN ATVano AS ATVano_origlin ON IdOrigenLinea = ATVano_origlin.IdATVano AND IdTipoOrigen = 5
        LEFT JOIN CT ON IdOrigenPunto = CT.IdInst AND IdTipoOrigen = 4
        LEFT JOIN Instalaciones AS Inst_CT ON Inst_CT.IdInst = CT.IdInst
        LEFT JOIN ST ON IdOrigenLinea = ST.IdInst AND IdTipoOrigen = 2
        LEFT JOIN Nodos ON IdOrigenLinea = Nodos.IdNodo AND IdTipoOrigen = 1
        WHERE Instalaciones.Nombre = ? AND Instalaciones.IdInst LIKE ?
        """, linName, MINECO[0:3] + '%')
    startPoint = cursorT.fetchone()
    
    nodecode = None
    
    if startPoint.VANO != None:
        print 'entra en startPoint.VANO != None:'
        X, Y = utiDMS.convertED50(startPoint.VX, startPoint.VY, zone(MINECO))
        
#         '''DEBUGGING MARCO GUIDA'''
#         coordenadas3=open('C:\\buzon\\177\\AnalisisCoordenadas3.txt','a')
#         coordenadas3.write('X '+str(X)+' --- Y '+str(Y)+'\n')
        nodecode = utiDMS.getDMSCode(X, Y) + "X1"
        print 'nodecode TrazaCreate.getLineStartNode '+ str(nodecode)
        # TODO we have a problem here, what if there are many nodes in this
        #      location !!
    elif startPoint.CT != None:
        nodecode = sqlDMS.getEmptyLineConnection(cursorT, cursorW, startPoint.CT, linName, MINECO)
        if nodecode == None:
            wx.MessageBox('TrazaCreate.getLineStartNode, linea '+linName, 'Faltan Celdas', wx.OK | wx.ICON_ERROR)
            raise Exception("not enough celdas created for CT " + startPoint.CT + " (linea: '" + linName + "')")
    elif startPoint.ST != None:
        X, Y = utiDMS.convertED50(startPoint.STX, startPoint.STY, zone(MINECO))
        nodecode = utiDMS.getDMSCode(X, Y) + "X1"
        # TODO we have a problem here, what if there are many nodes in this
        #      location !!
    elif startPoint.NODO != None:
        X, Y = utiDMS.convertED50(startPoint.NX, startPoint.NY, zone(MINECO))
        nodecode = utiDMS.getDMSCode(X, Y) + "X1"
        
        sqlDMS.iMV_Node(cursorW, nodecode,
                        X*1000, Y*1000, X*1000, Y*1000, nodecode, mineco=MINECO)
        # TODO we have a problem here, what if there are many nodes in this
        #      location !!
    elif startPoint.VANO_L != None:
        # origen linea defined but punto is not, this has to be processed after
        # the case where origen punto IS defined, because that should be preferred
        X, Y = utiDMS.convertED50(startPoint.VLX, startPoint.VLY, zone(MINECO))
        nodecode = utiDMS.getDMSCode(X, Y) + "X1"
        # TODO we have a problem here, what if there are many nodes in this
        #      location !!
    else:
        raise Exception("not implemented?? Linea " + linName + " does not seem to have start node!")
    
    return {"type": "START", "nodecode": nodecode}

def importarLinea(cursorT,cnxnW,origen,tipo,MINECO,pd,line):
    """
    Read a medium voltage line from traza and import it to 
    DMS - this funtion creates multiple MV_SECTIONs and
    some DISCONNECTORs and MV_FUSEs
    """
    cursorW = cnxnW.cursor()
    
    # the nodes to feed to utiDMS.DrawLine, an array
    nodesIn = []
    
    start = getLineStartNode(cursorT, cursorW, line.Nombre, MINECO)
    
    nodesIn.append([start["type"], start["nodecode"], None, None])

    VoltageLevel = getVoltageLevel(cursorT, cursorW, line.Nombre)
    
    # this query will list all the destinations along the linea, except for the start
    cursorT.execute("""
        SELECT  ApoyoSecuencial,
                CASE
                    -- Fuse
                    WHEN ATVanoDerivacionesManiobras.IdATTipoManiobraProteccion IN (2,3,6)
                         OR ATVanoManiobraProteccion.IdATTipoManiobraProteccion IN (2,3,6)
                         THEN 'FUSE'
                    -- Disconnector
                    WHEN ATVanoDerivacionesManiobras.IdATTipoManiobraProteccion IN (4,5,6,8,13)
                         OR ATVanoManiobraProteccion.IdATTipoManiobraProteccion IN (4,5,6,8,13)
                         THEN 'DISCONNECTOR'
                    ELSE NULL
                END AS PROTECTION,
                CASE
                     -- another line
                     WHEN DERS.IdInst IS NOT NULL THEN 'BRANCH'
                     -- CT
                    WHEN CT_Instalaciones.IdInst IS NOT NULL  THEN 'CT'
                    -- section point
                    ELSE 'POINT'
                END AS TYPE,
                X, Y, -- a nodecode can be made from these
                ISNULL(ATVanoTiposConductores.Descripcion, 'BUSBAR') AS CONDUCTOR,
                CT_Instalaciones.Codigo AS CT_CODE
        FROM Instalaciones
        JOIN ATVano ON Instalaciones.IdInst = ATVano.IdInst
        LEFT JOIN ATVanoDerivaciones
            ON ATVano.IdATVano = ATVanoDerivaciones.IdATVano
        LEFT JOIN ATVanoDerivacionesManiobras
            ON ATVanoDerivacionesManiobras.IdDerivacion = ATVanoDerivaciones.IdDerivacion
        LEFT JOIN ATVanoManiobraProteccion
            ON ATVanoManiobraProteccion.IdATVano = ATVano.IdATVano
        JOIN AT ON ATVano.IdInst = AT.IdInst
        LEFT JOIN ATVanoTiposConductores
            ON ATVanoTiposConductores.IdTipoConductor = ATVano.IdTipoConductor
        LEFT JOIN AT AS DERS ON DERS.IdOrigenPunto = ATVano.IdATVano
        LEFT JOIN Instalaciones CT_Instalaciones
            ON CT_Instalaciones.IdInst = ATVano.IdInstCTNoSobre
                OR CT_Instalaciones.IdInst = ATVano.IdInstCTSobre
            AND CT_Instalaciones.IdTipoInst IN (3, 4)
        WHERE Instalaciones.IdInst LIKE ?
        AND Instalaciones.Nombre LIKE ?
        ORDER BY ApoyoSecuencial
            """,
            MINECO[0:3] + '%', line.Nombre)
    atvanos = cursorT.fetchall()
    prevCT = None
    
    for vano in atvanos:
#         '''DEBUGGING MARCO GUIDA'''
#         coordenadas5=open('C:\\buzon\\177\\AnalisisCoordenadas5.txt','a')
#         coordenadas5.write('Vano.Y='+str(vano.Y)+'\n')
#         if str(vano.Y)=='4079634.0':
#             print 'ahora'
        X, Y = utiDMS.convertED50(vano.X, vano.Y, zone(MINECO))
        #coordenadas=open('C:\\buzon\\177\\AnalisisCoordenadas.txt','a')
        
        
        X = X*1000
        Y = Y*1000
#         '''DEBUGGING MARCO GUIDA'''
#         if str(Y).startswith('9942'):
#                 #coordenadas.write('Vano.Y='+str(vano.Y)+'\n')
#         coordenadas.write('X '+str(X)+' --- Y '+str(Y)+'\n')
#         nodecode = utiDMS.getDMSCode(X, Y)
#         coordenadas.write('nodecode: '+nodecode+'\n')
#         vanotype = vano.TYPE;
        
        if vano.TYPE == "CT":
            # this hack is needed, because sometimes the same CT is
            # represented in both ATVano.IdInstCTNoSobre and
            # ATVano.IdInstCTSobre, while we assume they mean the
            # same thing - sometimes the CT only appears in one of
            # these!
            if vano.CT_CODE == prevCT:
                vanotype = "BRANCH"
            prevCT = vano.CT_CODE
        
        if vanotype == "BRANCH":
            nodecode = utiDMS.getSitenode(cursorW, nodecode, 'X')
            nodecode = sqlDMS.iMV_Node(cursorW, nodecode,
                                       X, Y, X, Y,
                                       nodecode, mineco=MINECO)
        elif vano.PROTECTION == "FUSE" or vano.PROTECTION == "DISCONNECTOR":
            nodetype = None
            if vano.PROTECTION == "FUSE":
                nodetype = 'O'
            elif vano.PROTECTION == "DISCONNECTOR":
                nodetype = 'E'
            
            # first check if new node is needed
            if vanotype == "POINT":
                # no branch here, can create fuse straight here
                nodecode = utiDMS.getSitenode(cursorW, nodecode, nodetype)
                nc = nodecode
                vanotype = "PROTECTION"
                nodecode = sqlDMS.iMV_Node(cursorW, nodecode,
                                           X, Y, X, Y,
                                           nodecode, mineco=MINECO)
                Xm = X
                Ym = Y
            else:
                # must create a disconnector on the line
                Xm, Ym, nc = createMiddlePoint(cursorW,
                                               nodecode,
                                               nodesIn[len(nodesIn) - 1][1],
                                               nodetype,
                                               MINECO)
                nodesIn.append(["OTHER", nc, vano.CONDUCTOR, None])
            
            # ...then create datasheets
            if vano.PROTECTION == "FUSE":
                code = utiDMS.getCodeFuse(cursorW, MINECO, line.Nombre)
                sqlDMS.iFuse(cursorW, nc, code, nc)
            elif vano.PROTECTION == "DISCONNECTOR":
                sqlDMS.iMV_Site(cursorW, nc, 'E', Xm, Ym, mineco=MINECO)
                code = utiDMS.getCodeDisconnector(cursorW, MINECO, line.Nombre)
                sqlDMS.iDisconnector(cursorW, nc, code, nc)
        
        nodesIn.append([vanotype,
                        nodecode,
                        vano.CONDUCTOR,
                        vano.CT_CODE])
    
    lastVano = nodesIn[len(nodesIn) - 1]
    if lastVano[0] == "POINT":
        # section point, but we must convert to branch because it is the end
        # of the section
        lastVano[0] = "BRANCH"
        nodecode = utiDMS.getSitenode(cursorW, lastVano[1], 'X')
        X, Y = utiDMS.getCoordinates(nodecode)
        nodecode = sqlDMS.iMV_Node(cursorW, nodecode,
                                   X, Y, X, Y,
                                   nodecode, mineco=MINECO)
        lastVano[1] = nodecode
    
    utiDMS.DrawLine(cnxnW, cursorW, nodesIn, MINECO, VoltageLevel, line.Nombre, cursorT)

    cnxnW.commit()
    return pd + 1



def createMiddlePoint(cursorW, nodeCurr, nodePrev, nodetype, MINECO):
    # Create a node in the middle of two points
    angle = getAngle(nodeCurr, nodePrev)
    X1, Y1 = utiDMS.getCoordinates(nodeCurr)
    X3 = int(X1 - 2 * math.cos(angle))
    Y3 = int(Y1 - 2 * math.sin(angle))
    nc = utiDMS.getDMSCode(X3, Y3)
    nc = utiDMS.getSitenode(cursorW, nc, nodetype)
    nc = sqlDMS.iMV_Node(cursorW, nc, X3, Y3, X3, Y3, nc, mineco=MINECO)
    
    return X3, Y3, nc

def getAngle(nodecode1, nodecode2):
    X1, Y1 = utiDMS.getCoordinates(nodecode1)
    X2, Y2 = utiDMS.getCoordinates(nodecode2)
    iX, iY = X2 - X1, Y2 - Y1
    return math.atan2(iY, iX)
        
def borderPoints(MINECO):  
    cnxnT = utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorT = cnxnT.cursor()      
    
    # Substation in border points
    
    # Get border points from Traza
    
    cursorT.execute("SELECT Instalaciones.Nombre Nombre, Nodos.X, Nodos.Y, Nodos.TensionSubestacion, Nodos.Subestacion_o_PE FROM Nodos, Instalaciones \
                     WHERE Nodos.IdNodo = Instalaciones.IdInst AND IdNodo LIKE ?", MINECO[0:3] + '%')
    BorderPoints = cursorT.fetchall()
    for bp in BorderPoints:
        try:
            cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
            cursorW = cnxnW.cursor()
            borderPointsImport(cnxnW,cursorW,bp,MINECO)
            cnxnW.close()
        except:
            cnxnW.close()
            pass
            
def borderPointsImport(cnxnW,cursorW,bp,MINECO):
    
    HighVoltageLevel = 66
    MediumVoltageLevel = bp.TensionSubestacion
    # Get values from each border point
    bpName = utiDMS.getBorderPointName(cursorW, bp.Nombre)
    bpX, bpY = utiDMS.convertED50(bp.X, bp.Y, zone(MINECO))
    bpX = int(str(bpX).split('.')[0] + "000")
    bpY = int(str(bpY).split('.')[0] + "000")
    bpnodecode = utiDMS.getDMSCode(bpX, bpY) + 'X1'
    cursorW.execute("SELECT * FROM MV_NODE WHERE CODE LIKE ?", bpnodecode)
    bpExists = cursorW.fetchall()

    if len(bpExists) > 0:
        
        if bp.Subestacion_o_PE != 'Generador':
            # Insert Substation
            sqlDMS.iSUBSTATION(cursorW, MINECO[0:3], bpName, HighVoltageLevel)
            
            # Insert Diagram
            dsX1 = bpX - 2000
            dsY1 = bpY + 1000
            dsX2 = bpX + 2000
            dsY2 = bpY + 11000
            sqlDMS.iDiagramSubstation (cursorW, MINECO[0:3] + bpName, 'A', dsX1, dsX2, dsY1, dsY2)
            
            # Insert Primary Transformer
            ptX = bpX
            ptY = bpY + 8000
            ptnodecode = utiDMS.getDMSCode(ptX, ptY) + 'P1'
            ptcode = MINECO[0:3] + bpName + 'T'
            Sn1 = 1
            Un1 = HighVoltageLevel
            Un2 = MediumVoltageLevel
            sqlDMS.iPRIMARY_TRANSFORMER(cursorW, ptnodecode, ptcode, Sn1, Un1, Un2, MINECO)
            sqlDMS.iMV_Node(cursorW, ptnodecode, ptX, ptY, ptX, ptY, ptnodecode, mineco=MINECO)
            
            # Insert Feeding Point
            fpX = bpX
            fpY = bpY + 10000 
            fpnodecode = utiDMS.getDMSCode(fpX, fpY) + 'G1'
            fpcode = MINECO[0:3] + bpName + 'G'
            Un = HighVoltageLevel
            sqlDMS.iFEEDINGPOINT(cursorW, fpnodecode, fpcode, Un)
            sqlDMS.iMV_Node(cursorW, fpnodecode, fpX, fpY, fpX, fpY, ptnodecode, mineco=MINECO)
            
            # Insert Busbar Points
            bpcode = MINECO[0:3] + bpName + 'B'
            # Busbar Point 1
            bpX1 = bpX - 1000
            bpY1 = bpY + 6000             
            sqlDMS.iBUSBARPOINT(cursorW, bpcode, bpX1, bpY1, 1)
            
            # Busbar Point 2
            bpX2 = bpX + 1000
            bpY2 = bpY + 6000 
            sqlDMS.iBUSBARPOINT(cursorW, bpcode, bpX2, bpY2, 2)

            # Insert Busbar Node
            bnX = bpX
            bnY = bpY + 6000 
            bnnodecode = utiDMS.getDMSCode(bnX, bnY) + 'A1'
            sqlDMS.iBUSBARNODE(cursorW, bnnodecode, bpcode)
            sqlDMS.iMV_Node(cursorW, bnnodecode, bnX, bnY, bnX, bnY, ptnodecode, mineco=MINECO)

            # Insert Circuit Breaker
            cbX = bpX
            cbY = bpY + 4000 
            cbnodecode = utiDMS.getDMSCode(cbX, cbY) + 'K1'
            cbcode = MINECO[0:3] + bpName + 'I'
            sqlDMS.iCIRCUIT_BREAKER(cursorW, cbnodecode, cbcode, MediumVoltageLevel)
            sqlDMS.iMV_Node(cursorW, cbnodecode, cbX, cbY, cbX, cbY, ptnodecode, mineco=MINECO)

            # Insert Feeder
            fX = bpX
            fY = bpY + 2000 
            fnodecode = utiDMS.getDMSCode(fX, fY) + 'S1'
            fcode = MINECO[0:3] + bpName + 'F'
            sqlDMS.iMV_FEEDER(cursorW, fnodecode, fcode)
            sqlDMS.iMV_Node(cursorW, fnodecode, fX, fY, fX, fY, ptnodecode, mineco=MINECO)
            
            # Insert Sections
            # Transformer - Feeding Point
            mv_section_id = sqlDMS.uMV_Section_ID(cnxnW, cursorW)
            sqlDMS.iMV_Section(cursorW, mv_section_id, ptnodecode, fpnodecode, 'BUSBAR', MINECO, HighVoltageLevel, ptX, ptY, SUBSTATION=MINECO[0:3] + bpName, HIDE=1)
            # Transformer - Busbar Node
            mv_section_id = sqlDMS.uMV_Section_ID(cnxnW, cursorW)
            sqlDMS.iMV_Section(cursorW, mv_section_id, ptnodecode, bnnodecode, 'BUSBAR', MINECO, MediumVoltageLevel, ptX, ptY, SUBSTATION=MINECO[0:3] + bpName, HIDE=1)
            # Busbar Node - Circuit Breaker
            mv_section_id = sqlDMS.uMV_Section_ID(cnxnW, cursorW)
            sqlDMS.iMV_Section(cursorW, mv_section_id, bnnodecode, cbnodecode, 'BUSBAR', MINECO, MediumVoltageLevel, bnX, bnY, SUBSTATION=MINECO[0:3] + bpName, HIDE=1)
            # Circuit Breaker - Feeder
            mv_section_id = sqlDMS.uMV_Section_ID(cnxnW, cursorW)
            sqlDMS.iMV_Section(cursorW, mv_section_id, cbnodecode, fnodecode, 'BUSBAR', MINECO, MediumVoltageLevel, cbX, cbY, SUBSTATION=MINECO[0:3] + bpName, HIDE=1)
            # Feeder - Border Point
            mv_section_id = sqlDMS.uMV_Section_ID(cnxnW, cursorW)
            sqlDMS.iMV_Section(cursorW, mv_section_id, fnodecode, bpnodecode, 'BUSBAR', MINECO, MediumVoltageLevel, fX, fY, SUBSTATION=MINECO[0:3] + bpName, HIDE=1)
        else:
            # Insert Feeding Point
            fpX = bpX
            fpY = bpY + 10000 
            fpnodecode = utiDMS.getDMSCode(fpX, fpY) + 'G1'
            fpcode = MINECO[0:3] + bpName + 'G'
            Un = HighVoltageLevel
            sqlDMS.iFEEDINGPOINT(cursorW, fpnodecode, fpcode, Un)
            sqlDMS.iMV_Node(cursorW, fpnodecode, fpX, fpY, fpX, fpY, fpnodecode, mineco=MINECO)
            # Transformer - Feeding Point
            mv_section_id = sqlDMS.uMV_Section_ID(cnxnW, cursorW)
            sqlDMS.iMV_Section(cursorW, mv_section_id, bpnodecode, fpnodecode, 'BUSBAR', MINECO, HighVoltageLevel, fpX, fpY, SUBSTATION=MINECO[0:3] + bpName, HIDE=1)
            
        cnxnW.commit()

def trafo(rDSN, wDSN, MINECO):
        PrgDialog = wx.ProgressDialog("Importando", "Importando Transformadores...", maximum=1000, style=wx.PD_AUTO_HIDE)
        PrgDialog.SetSize((400, 110))
#        main.Main.updateGauge(25)
        'Connect to Traza Database'
        cnxnR = utiDMS.connectSQL(DataBase.TRAZAAddress, rDSN, DataBase.TRAZAUser, DataBase.TRAZAPassword)
        cursorR = cnxnR.cursor()
    
        cursorR.execute("SELECT   T.Descripcion, CT.IdTransformador, CT.Potencia,\
                         CT.IdFabricante, CT.AnhoFabricacion,\
                         CT.Modelo, Ins.Nombre, CT.NumeroTransformador\
                         FROM     CTTransformadores CT, CTTensiones T, Instalaciones Ins \
                         WHERE CT.idInst=Ins.IdInst and T.IdTension=CT.IdTension  and Ins.IdInst LIKE ?", MINECO[0:3] + '%')
#for one trafo add and T.Descripcion like '045P100000006'
        rows = cursorR.fetchall()
        rowsInicial= rows.__len__()
        i = 0
        for row in rows:
            cnxnW = utiDMS.connectSQL(DataBase.Address, wDSN, DataBase.User, DataBase.Password)
            cursorW = cnxnW.cursor()
            try:
                i = i + 1
#                if row.Descripcion == '20000/13200/400/230':
#                    row.Descripcion = '20000/13200/400'
                Ux = getS(row.Descripcion)
                remarks = row.IdTransformador
                code = row.IdTransformador + "-" + row.Nombre + "-" + row.NumeroTransformador
                ordernumber = 0 #Transformer not placed
                type = row.Modelo
                year = row.AnhoFabricacion
                sn1 = row.Potencia
                #Write to DMS 
                '''DEBUGGING MARCO GUIDA'''               
                #print 'float_:'+ str(float(Ux["U1"]))+"@"+str(float(Ux["U2"]))+"@"+str(float(Ux["U3"]))+"@"+str(float(sn1))
                
                sqlDMS.iTransformer(cursorW, code[:31], 'WAREHOUSE1', ordernumber, '', \
                     float(Ux["U1"]), float(Ux["U2"]), float(Ux["U3"]), float(sn1))
                cnxnW.commit()
            except:
                TrazaImport.WriteLog("Error imprevisto")
                logging.exception("error importing transformer")
                wx.MessageBox("Problemas de conexi�n, compruebe su conexi�n y vuelva a intentarlo",
                "Error", wx.OK|wx.ICON_ERROR)
                return
                cnxnW.commit()
                
                
            PrgDialog.Update(float(i) / float(len(rows)) * 1000.0)
            PrgDialog.SetLabel("Faltan "+str(rowsInicial-i)+" transformadores...")
        print 'Num de trafos importados: '+str(rowsInicial)


def getS(Tension_V):
    pos = Tension_V.split("/")
    Sn = []
    for p in pos:
        Sn.append(int(p.replace(".", "")))
    
    if Sn.__len__()==2:
        Sn.append(int(0))
           
    Sn.sort(reverse=True)
     
    try:   
        r = {"U1":Sn[0], "U2":Sn[1], "U3":Sn[2]}
    except:
        r = {"U1":20000, "U2":400, "U3":0}
        
    return r

def customer(cnxnT, MINECO):
    PrgDialog = wx.ProgressDialog("Importando", "Importando Clientes...", maximum=1000, style=wx.PD_AUTO_HIDE)
    PrgDialog.SetSize((400, 110))
    cursorT = cnxnT.cursor()
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
    
    cursorW = cnxnW.cursor()
    
    cursorT.execute('SELECT CodigoAcometida, X, Y FROM BTVano where CodigoAcometida is not null\
                    and CodigoAcometida LIKE ?', MINECO[0:3] + '%')
    rows = cursorT.fetchall()
    crd = []
    i = 0
    for r in rows:        
        crd.append([r.CodigoAcometida, utiDMS.convertED50(r.X, r.Y, 30)])
        i = i + 1
        PrgDialog.Update(float(i) / float(len(rows)) * 125.0)
    i = 0
    for c in crd:
        cursorW.execute('INSERT INTO gdlXYDMS VALUES (?, ?, ?)', \
                   c[0], c[1][0], c[1][1])
        i = i + 1
        PrgDialog.Update(float(i) / float(len(crd)) * 125.0 + 125.0)
    cnxnW.commit()
    
#    cursorT.execute("INSERT INTO DMS.dbo.CUSTOMER\
    cursorT.execute("""
        SELECT  Clientes.CUPS AS CODE,
                Clientes.Nombre AS NAME,
                NULL AS CGROUP, 
                Tarifa AS TARIFF,
                NULL AS CURVE, 
                Clientes.EnergiaConsumida AS ENERGY1,
                0 AS ENERGY2, 
                PotenciaMaxDemandada AS REAL_PEAK,
                NULL AS REACTIVE_PEAK,
                NULL AS PHASECOUNT,
                NULL AS FUSE, 
                BTVano.CodigoAcometida AS CUSTOMER_NODE,
                Instalaciones.Codigo AS LV_NETWORK,
                CTTransformadores.NumeroTransformador AS NumeroTransformador,
                GETDATE() AS UPDATE_DATE,
                NULL AS PHASE,
                Clientes.PotenciaContratada AS CONTRACTED_POWER
        FROM    Clientes
            LEFT JOIN ClientesAcometidas ON Clientes.CUPS = ClientesAcometidas.CUPS
            LEFT JOIN BTVano ON ClientesAcometidas.IdBTVano = BTVano.IdBTVano
            LEFT JOIN Instalaciones ON Clientes.IdInst = Instalaciones.IdInst
            LEFT JOIN CTTransformadores ON Clientes.IdTransformador = CTTransformadores.IdTransformador
        WHERE Instalaciones.IdInst like ?
        """, MINECO[0:3] + '%')
    customers = cursorT.fetchall()
    i = 0
    for c in customers:
        try:
            if c.NumeroTransformador == None:
                c.NumeroTransformador = '1'
            c.LV_NETWORK = c.LV_NETWORK + c.NumeroTransformador
            
            cursorW.execute("INSERT INTO CUSTOMER (CODE,\
                        NAME,\
                        CGROUP, \
                        TARIFF,\
                        CURVE, \
                        ENERGY1,\
                        ENERGY2, \
                        REAL_PEAK,\
                        REACTIVE_PEAK,\
                        PHASECOUNT,\
                        FUSE, \
                        CUSTOMER_NODE,\
                        LV_NETWORK,\
                        UPDATE_DATE,\
                        PHASE,\
                        CONTRACTED_POWER) VALUES\
                        (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", \
                        c.CODE, \
                        c.NAME, \
                        c.CGROUP, \
                        c.TARIFF, \
                        c.CURVE, \
                        c.ENERGY1, \
                        c.ENERGY2, \
                        c.REAL_PEAK, \
                        c.REACTIVE_PEAK, \
                        c.PHASECOUNT, \
                        c.FUSE, \
                        c.CUSTOMER_NODE, \
                        c.LV_NETWORK, \
                        c.UPDATE_DATE, \
                        c.PHASE, \
                        c.CONTRACTED_POWER)
            i = i + 1
            PrgDialog.Update(float(i) / float(len(customers)) * 125.0 + 125.0 * 2)
        except:
            # En ocasiones sale el siguiente error:
            #pyodbc.IntegrityError: ('23000', "[23000] [Microsoft][ODBC SQL Server Driver][SQL Server]Violation of PRIMARY KEY constraint 'PK_CUSTOMER'. Cannot insert duplicate key in object 'dbo.CUSTOMER'. The duplicate key value is (ES0133000002001496QV0F). (2627) (SQLExecDirectW); [01000] [Microsoft][ODBC SQL Server Driver][SQL Server]The statement has been terminated. (3621)")
            pass
                 
    cursorT.execute("SELECT X,Y,\
                    CodigoAcometida AS CUSTOMER_NODE,\
                    NULL AS FUSE,\
                    0 AS MV,\
                    SUBSTRING(DireccionAcometida, 0, 40) AS NAME,\
                    1 AS IMPORTANCE,\
                    Instalaciones.Codigo + CTTransformadores.NumeroTransformador AS LV_NETWORK,\
                    NULL AS FEEDER,    \
                    NULL AS BOX, \
                    NULL AS SWITCH, \
                    GETDATE() AS CHANGETIME\
                    FROM    BTVano\
                    JOIN BT ON BTVano.IdInst = BT.IdInst\
                    JOIN Instalaciones ON BT.IdInstCT = Instalaciones.IdInst\
                    JOIN (\
                    SELECT    ROW_NUMBER() OVER (PARTITION BY X ORDER BY CodigoAcometida) AS r,\
                    CodigoAcometida AS ca\
                    FROM        BTVano WHERE CodigoAcometida IS NOT NULL AND CodigoAcometida NOT LIKE ?\
                    ) AS row_n ON BTVano.CodigoAcometida = row_n.ca\
                    JOIN CTTransformadores ON BT.IdTransformador = CTTransformadores.IdTransformador\
                    WHERE     BTVano.CodigoAcometida IS NOT NULL\
                    AND Instalaciones.IdInst LIKE ?", MINECO[0:3], MINECO[0:3] + '%')
    
    cnxnW.commit()
    customerNode = cursorT.fetchall()
    i = 0
    for c in customerNode:
        try:
            cursorW.execute("INSERT INTO LV_CUSTOMER_NODE (NODECODE, CUSTOMER_NODE, FUSE, MV, NAME,\
                        IMPORTANCE, LV_NETWORK, FEEDER, BOX, SWITCH, CHANGETIME) VALUES\
                        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                        utiDMS.getSitenode(cursorW, utiDMS.getDMSCode(c.X, c.Y), '$'),
                        c.CUSTOMER_NODE, c.FUSE, c.MV, c.NAME, \
                        c.IMPORTANCE, c.LV_NETWORK, c.FEEDER, c.BOX, c.SWITCH, c.CHANGETIME)
        except:
            TrazaImport.WriteLog("Error imprevisto")
            logging.exception("Error when inserting customer node to DMS, " + c.CUSTOMER_NODE)
        cnxnW.commit()
        i = i + 1
        PrgDialog.Update(float(i) / float(len(customerNode)) * 125.0 + 125.0 * 3)
    cursorT.execute("SELECT    BTVano.CodigoAcometida AS CUSTOMER_NODE,\
                            ROW_NUMBER() OVER\
                                (PARTITION BY BTVano.CodigoAcometida ORDER BY Clientes.Nombre)\
                                AS NUMBER,\
                            Clientes.PotenciaContratada AS P,\
                            (SELECT \
                                CASE\
                                    WHEN (Clientes.EnergiaConsumida < 0.1 OR Clientes.EnergiaReactiva < 0.1)\
                                        THEN 1.0\
                                    ELSE Clientes.EnergiaConsumida / \
                                            SQRT((CAST(Clientes.EnergiaConsumida AS bigint) * \
                                                  CAST(Clientes.EnergiaConsumida AS bigint) + \
                                                  CAST(Clientes.EnergiaReactiva AS bigint) * \
                                                  CAST(Clientes.EnergiaReactiva AS bigint))) \
                                END\
                            ) AS COSFII,\
                            Clientes.EnergiaConsumida AS ENERGY,\
                            NULL AS COMMENT,\
                            NULL AS PHASE\
                    FROM    Clientes\
                                JOIN ClientesAcometidas\
                                    ON Clientes.CUPS = ClientesAcometidas.CUPS\
                                LEFT JOIN BTVano\
                                    ON ClientesAcometidas.IdBTVano = BTVano.IdBTVano\
                    WHERE    BTVano.CodigoAcometida IS NOT NULL AND BTVano.CodigoAcometida <> (Clientes.IdDistribuidora + '-')\
                    AND BTVano.CodigoAcometida LIKE ?", MINECO[0:3] + '%')
    
    customerNodeLoad = cursorT.fetchall()
    i = 0
    for c in customerNodeLoad:
        
        cursorW.execute("INSERT INTO CUSTOMER_NODE_LOAD (CUSTOMER_NODE, NUMBER, P, COSFII,\
                        ENERGY, COMMENT, PHASE) VALUES\
                        (?, ?, ?, ?, ?, ?, ?)", c.CUSTOMER_NODE, c.NUMBER, c.P, c.COSFII, \
                        c.ENERGY, c.COMMENT, c.PHASE)
        i = i + 1
        PrgDialog.Update(float(i) / float(len(customerNodeLoad)) * 125.0 + 125.0 * 4)
        
    cursorW.execute("SELECT *\
                    FROM       gdlXYDMS")
    prueba = cursorW.fetchall()
    cursorW.execute("SELECT XDMS*1000 AS X,\
                            YDMS*1000 AS Y,\
                            XDMS*1000 AS XCODE,\
                            YDMS*1000 AS YCODE,\
                            CodigoAcometida\
                    FROM        LV_CUSTOMER_NODE\
                                    JOIN gdlXYDMS ON LV_CUSTOMER_NODE.CUSTOMER_NODE = gdlXYDMS.CodigoAcometida\
                    WHERE LV_CUSTOMER_NODE.CUSTOMER_NODE LIKE ?", MINECO[0:3] + '%')
    lvLoad = cursorW.fetchall()
    
    cursorW.execute("SELECT CUSTOMER_NODE, NODECODE FROM LV_CUSTOMER_NODE\
                     WHERE CUSTOMER_NODE LIKE ?", MINECO[0:3] + '%')
    lvLoad2 = cursorW.fetchall()
    i = 0
    for c in lvLoad:
        for d in lvLoad2:
            if c.CodigoAcometida == d.CUSTOMER_NODE:
                try:
                    cursorW.execute("INSERT INTO LV_NODE(CODE, X, Y, XCODE, YCODE, SITENODE, SymbolAngle, gdlMINECO)\
                                    VALUES (?, ?, ?, ?, ?, ?, ?,?)", \
                                    d.NODECODE, c.X, c.Y, c.XCODE, c.YCODE, d.NODECODE, 0, MINECO[0:3])
                    break
                except:
                    TrazaImport.WriteLog("Error imprevisto")
                    logging.exception("Error when inserting into LV_NODE")
        i = i + 1
        PrgDialog.Update(float(i) / float(len(lvLoad)) * 125.0 + 125.0 * 5)
    cursorT.execute("SELECT Siglas FROM DatosDistribuidoras WHERE Siglas = ?", MINECO[0:3])
    Siglas = cursorT.fetchone()
    Sigla = Siglas[0]
    cursorW.execute("SELECT    LV_NETWORK,\
                            ROW_NUMBER() OVER (PARTITION BY LV_NETWORK ORDER BY ENERGY) AS NUMBER,\
                            P,\
                            COSFII,\
                            ENERGY,\
                            COMMENT\
                    FROM LV_CUSTOMER_NODE\
                            JOIN CUSTOMER_NODE_LOAD\
                                ON CUSTOMER_NODE_LOAD.CUSTOMER_NODE = LV_CUSTOMER_NODE.CUSTOMER_NODE\
                    WHERE LV_NETWORK LIKE ?", Sigla + '%')
    
    mvLvLoad = cursorW.fetchall()
    i = 0
    for c in mvLvLoad:
        cursorW.execute("INSERT INTO MV_LV_LOAD (MV_LV_SUBSTATION, NUMBER, P, COSFII, ENERGY, COMMENT)\
                        VALUES (?, ?, ?, ?, ?, ?)", \
                        c.LV_NETWORK, c.NUMBER, c.P, c.COSFII, c.ENERGY, c.COMMENT)
        i = i + 1
        PrgDialog.Update(float(i) / float(len(mvLvLoad)) * 125.0 + 125.0 * 6)
                
    cnxnW.commit()
    
    cursorW.execute("select LV_NETWORK AS MV_LV_SUBSTATION, CONTRACTED_POWER AS P, 1 AS COSFII, ENERGY1 AS ENERGY \
                    from customer WHERE customer_node is null and (ENERGY1 != 0 OR CONTRACTED_POWER != 0)\
                    AND LV_NETWORK LIKE ?", MINECO[0:3] + '%')
    mvLvLoad2 = cursorW.fetchall()
    j = 0
    for c in mvLvLoad2:
        j = j + 1
        for i in range(1, 500000):
            cursorW.execute("SELECT * FROM MV_LV_LOAD WHERE MV_LV_SUBSTATION LIKE ? AND NUMBER LIKE ?", \
                            c.MV_LV_SUBSTATION, i)
            NumberExists = cursorW.fetchall()
            if len(NumberExists) == 0:
                cursorW.execute("INSERT INTO MV_LV_LOAD (MV_LV_SUBSTATION, NUMBER, P, COSFII, ENERGY)\
                            VALUES (?, ?, ?, ?, ?)", \
                            c.MV_LV_SUBSTATION, i, c.P, c.COSFII, c.ENERGY)
                cnxnW.commit()
                PrgDialog.Update(float(j) / float(len(mvLvLoad2)) * 125.0 + 125.0 * 7)
                break
    TrazaImport.WriteLog('Clientes importados')
    PrgDialog.Update(1000)

def delete_distributor(MINECO):
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
    cursorW = cnxnW.cursor()
    cursorW.execute("DELETE FROM MV_NODE WHERE CODE IN (\
                    SELECT CODE FROM gdlExtension, MV_NODE WHERE gdlMINECO IS NULL AND X BETWEEN X1*1000 AND X2*1000 \
                    AND Y BETWEEN Y1*1000 AND Y2*1000 AND MINECO LIKE ?)", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_NODE WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_SECTIONPOINT WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM DISCONNECTOR WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM CUSTOMER WHERE LV_NETWORK LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM CUSTOMER_NODE_LOAD WHERE CUSTOMER_NODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM DIAGRAM WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM CAPACITOR WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM BUSBARNODE WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM BUSBARPOINT WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM CIRCUIT_BREAKER WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_BOX WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_NODE WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_SECTION WHERE LV_NETWORK LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_SECTIONPOINT WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_CUSTOMER_NODE WHERE CUSTOMER_NODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_FEEDER WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_LV_SUBSTATION WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_LV_LOAD WHERE MV_LV_SUBSTATION LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_SECTION WHERE DISTRICT LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_SITE WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM NODE_REGIONS WHERE REGION_CODES LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM NODE_REGIONS2 WHERE REGION_CODES LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM SUBSTATION WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM TRANSFORMER WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM TRANSFORMER_NODE WHERE MV_LV_SUBSTATION LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM TRANSFORMER_PLACING WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM gdlCTConnections WHERE LI_IdInst LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM FEEDINGPOINT WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM SwitchingComponent WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM NODE_REGIONS WHERE REGION_CODES LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM PRIMARY_TRANSFORMER WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_FUSE WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM gdlOPC WHERE MINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_SWITCH WHERE BOX LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM gdlLV_NODE_BTVano WHERE IdBTVano LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM gdlXYDMS WHERE CodigoAcometida LIKE ?", MINECO[0:3] + '%')  
    cursorW.execute("DELETE FROM gdlCeldas WHERE CodigoCT like ?",  MINECO[0:3] + '%') 
    cnxnW.commit()
        
def delete_all_distributors():
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
    cursorW = cnxnW.cursor()
    cursorW.execute("DELETE FROM MV_NODE")
    cursorW.execute("DELETE FROM MV_SECTIONPOINT")
    cursorW.execute("DELETE FROM CUSTOMER")
    cursorW.execute("DELETE FROM DISCONNECTOR")
    cursorW.execute("DELETE FROM CUSTOMER_NODE_LOAD")
    cursorW.execute("DELETE FROM DIAGRAM")
    cursorW.execute("DELETE FROM CAPACITOR")
    cursorW.execute("DELETE FROM BUSBARNODE")
    cursorW.execute("DELETE FROM BUSBARPOINT")
    cursorW.execute("DELETE FROM CIRCUIT_BREAKER")
    cursorW.execute("DELETE FROM LV_BOX")
    cursorW.execute("DELETE FROM LV_NODE")
    cursorW.execute("DELETE FROM LV_SECTION")
    cursorW.execute("DELETE FROM LV_SECTIONPOINT")
    cursorW.execute("DELETE FROM LV_CUSTOMER_NODE")
    cursorW.execute("DELETE FROM MV_FEEDER")
    cursorW.execute("DELETE FROM MV_LV_SUBSTATION")
    cursorW.execute("DELETE FROM MV_LV_LOAD")
    cursorW.execute("DELETE FROM MV_SECTION")
    cursorW.execute("DELETE FROM MV_SITE")
    cursorW.execute("DELETE FROM NODE_REGIONS")
    cursorW.execute("DELETE FROM NODE_REGIONS2")
    cursorW.execute("DELETE FROM SUBSTATION")
    cursorW.execute("DELETE FROM TRANSFORMER")
    cursorW.execute("DELETE FROM TRANSFORMER_NODE")
    cursorW.execute("DELETE FROM TRANSFORMER_PLACING")
    cursorW.execute("DELETE FROM gdlCTConnections")
    cursorW.execute("DELETE FROM FEEDINGPOINT")
    cursorW.execute("DELETE FROM SwitchingComponent")
    cursorW.execute("DELETE FROM NODE_REGIONS")
    cursorW.execute("DELETE FROM PRIMARY_TRANSFORMER")
    cursorW.execute("DELETE FROM MV_FUSE")
    cursorW.execute("DELETE FROM Measure")
    cursorW.execute("DELETE FROM MeasureNodes")
    cursorW.execute("DELETE FROM MeasureValues")
    cursorW.execute("DELETE FROM gdlOPC")
    cursorW.execute("DELETE FROM LV_SWITCH")
    cursorW.execute("DELETE FROM gdlLV_NODE_BTVano")
    cursorW.execute("DELETE FROM gdlXYDMS")
    cnxnW.commit()
    
def delete_distributor_without_substations(MINECO):
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
    cursorW = cnxnW.cursor()
    cursorW.execute("DELETE FROM MV_NODE WHERE CODE IN (\
                    SELECT CODE FROM gdlExtension, MV_NODE WHERE gdlMINECO IS NULL AND X BETWEEN X1*1000 AND X2*1000 \
                    AND Y BETWEEN Y1*1000 AND Y2*1000 AND MINECO LIKE ?\
                    AND SITENODE NOT IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_NODE WHERE gdlMINECO LIKE ?\
                    AND SITENODE NOT IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER)", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_SECTIONPOINT WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM DISCONNECTOR WHERE CODE LIKE ?\
                    AND NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM CUSTOMER WHERE LV_NETWORK LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM CUSTOMER_NODE_LOAD WHERE CUSTOMER_NODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM DIAGRAM WHERE CODE LIKE ?\
                    AND CODE NOT IN (SELECT CODE FROM SUBSTATION)", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM CAPACITOR WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM CIRCUIT_BREAKER WHERE CODE LIKE ?\
                    AND NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_BOX WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_NODE WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_SECTION WHERE LV_NETWORK LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_SECTIONPOINT WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_CUSTOMER_NODE WHERE CUSTOMER_NODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_FEEDER WHERE CODE LIKE ?\
                    AND NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_LV_SUBSTATION WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_LV_LOAD WHERE MV_LV_SUBSTATION LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_SECTION WHERE DISTRICT LIKE ?\
                    AND NODE1 NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER)) AND NODE2 NOT IN \
                    (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_SITE WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM NODE_REGIONS WHERE REGION_CODES LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM NODE_REGIONS2 WHERE REGION_CODES LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM TRANSFORMER WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM TRANSFORMER_NODE WHERE MV_LV_SUBSTATION LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM TRANSFORMER_PLACING WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM gdlCTConnections WHERE LI_IdInst LIKE ?", MINECO[0:3] + '%')
    # delete gdlCTConnections references to non-existent nodes
    cursorW.execute("DELETE FROM gdlCTConnections WHERE CT_NODECODE NOT IN (SELECT CODE FROM MV_NODE)")
#    cursorW.execute("DELETE FROM FEEDINGPOINT WHERE CODE LIKE ?",MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM SwitchingComponent WHERE gdlMINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM NODE_REGIONS WHERE REGION_CODES LIKE ?", MINECO[0:3] + '%')
#    cursorW.execute("DELETE FROM PRIMARY_TRANSFORMER WHERE CODE LIKE ?",MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM MV_FUSE WHERE CODE LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM gdlOPC WHERE MINECO LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM LV_SWITCH WHERE BOX LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM gdlLV_NODE_BTVano WHERE IdBTVano LIKE ?", MINECO[0:3] + '%')
    cursorW.execute("DELETE FROM gdlXYDMS WHERE CodigoAcometida LIKE ?", MINECO[0:3] + '%')   
#    DELETE SUBSTATIONS FROM BORDER POINTS
    cursorW.execute("DELETE from SUBSTATION  WHERE CODE LIKE ?", MINECO[0:3] + '%5046')
    cursorW.execute("DELETE from PRIMARY_TRANSFORMER   WHERE CODE LIKE ?", MINECO[0:3] + '%5046T')
    cursorW.execute("DELETE from DIAGRAM WHERE CODE LIKE ?", MINECO[0:3] + '%5046')
    cursorW.execute("DELETE from FEEDINGPOINT WHERE CODE LIKE ?", MINECO[0:3] + '%5046G')
    cursorW.execute("DELETE from BUSBARNODE WHERE CODE LIKE ?", MINECO[0:3] + '%5046B')
    cursorW.execute("DELETE from BUSBARPOINT WHERE CODE LIKE ?", MINECO[0:3] + '%5046B')
    cursorW.execute("DELETE from CIRCUIT_BREAKER WHERE CODE LIKE ?", MINECO[0:3] + '%5046I')
    cursorW.execute("DELETE from MV_FEEDER WHERE CODE LIKE ?", MINECO[0:3] + '%5046F')
    cursorW.execute("DELETE from MV_NODE WHERE CODE IN (\
                  SELECT NODE1 from MV_SECTION WHERE SUBSTATION LIKE ? UNION\
                  SELECT NODE2 from MV_SECTION WHERE SUBSTATION LIKE ?)", \
                  MINECO[0:3] + '%5046', MINECO[0:3] + '%5046')
    cursorW.execute("DELETE from MV_SECTION WHERE SUBSTATION LIKE ?", MINECO[0:3] + '%5046')
    cursorW.execute("DELETE FROM gdlCeldas WHERE CodigoCT like ?",  MINECO[0:3] + '%')
    cnxnW.commit()
        
def delete_all_distributors_without_substations():
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
    cursorW = cnxnW.cursor()
    cursorW.execute("DELETE FROM MV_NODE WHERE SITENODE NOT IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER UNION SELECT NODECODE FROM BUSBARNODE)")
    cursorW.execute("DELETE FROM MV_SECTIONPOINT")
    cursorW.execute("DELETE FROM CUSTOMER")
    cursorW.execute("DELETE FROM DISCONNECTOR WHERE NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))")
    cursorW.execute("DELETE FROM CUSTOMER_NODE_LOAD")
    cursorW.execute("DELETE FROM DIAGRAM WHERE CODE NOT IN (SELECT CODE FROM SUBSTATION)")
    cursorW.execute("DELETE FROM CAPACITOR")
    cursorW.execute("DELETE FROM CIRCUIT_BREAKER WHERE NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))")
    cursorW.execute("DELETE FROM LV_BOX")
    cursorW.execute("DELETE FROM LV_NODE")
    cursorW.execute("DELETE FROM LV_SECTION")
    cursorW.execute("DELETE FROM LV_SECTIONPOINT")
    cursorW.execute("DELETE FROM LV_CUSTOMER_NODE")
    cursorW.execute("DELETE FROM MV_FEEDER WHERE NODECODE NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))")
    cursorW.execute("DELETE FROM MV_LV_SUBSTATION")
    cursorW.execute("DELETE FROM MV_LV_LOAD")
    cursorW.execute("DELETE FROM MV_SECTION WHERE NODE1 NOT IN (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER)) AND NODE2 NOT IN\
                     (SELECT CODE FROM MV_NODE WHERE SITENODE IN (SELECT NODECODE FROM PRIMARY_TRANSFORMER))")
    cursorW.execute("DELETE FROM MV_SITE")
    cursorW.execute("DELETE FROM NODE_REGIONS")
    cursorW.execute("DELETE FROM NODE_REGIONS2")
    cursorW.execute("DELETE FROM TRANSFORMER")
    cursorW.execute("DELETE FROM TRANSFORMER_NODE")
    cursorW.execute("DELETE FROM TRANSFORMER_PLACING")
    cursorW.execute("DELETE FROM gdlCTConnections")
#    cursorW.execute("DELETE FROM FEEDINGPOINT")
    cursorW.execute("DELETE FROM SwitchingComponent")
    cursorW.execute("DELETE FROM NODE_REGIONS")
    cursorW.execute("DELETE FROM PRIMARY_TRANSFORMER")
    cursorW.execute("DELETE FROM MV_FUSE")
    cursorW.execute("DELETE FROM Measure")
    cursorW.execute("DELETE FROM MeasureNodes")
    cursorW.execute("DELETE FROM MeasureValues")
    cursorW.execute("DELETE FROM gdlOPC")
    cursorW.execute("DELETE FROM LV_SWITCH")
    cursorW.execute("DELETE FROM gdlLV_NODE_BTVano")
    cursorW.execute("DELETE FROM gdlXYDMS")
    cnxnW.commit()
    
def separarLineas():
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
    cursorW = cnxnW.cursor()
    cursorW.execute("WHILE EXISTS (\
                            SELECT TOP 1 sp1.MV_Section_Id\
                            FROM MV_SECTIONPOINT AS sp1 JOIN MV_SECTIONPOINT AS sp2\
                                            ON sp1.X = sp2.X AND\
                                                 sp1.Y = sp2.Y AND\
                                                 sp1.MV_Section_Id < sp2.MV_Section_Id\
                            )\
                    BEGIN\
                        UPDATE MV_SECTIONPOINT SET \
                        X = X+1000\
                        WHERE MV_Section_Id IN\
                        (\
                        SELECT TOP 1 sp1.MV_Section_Id\
                        FROM MV_SECTIONPOINT AS sp1 JOIN MV_SECTIONPOINT AS sp2\
                                        ON sp1.X = sp2.X AND\
                                             sp1.Y = sp2.Y AND\
                                             sp1.MV_Section_Id < sp2.MV_Section_Id\
                        )\
                    END")
    cnxnW.commit()


def ordenCeldas(filename):
    """
    Not used anymore - this function was meant to ensure that 
    the order of celdas in a CT remain static throughout importss
    """
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
    cursorW = cnxnW.cursor()
    book = xlrd.open_workbook(filename)
    sheet = book.sheet_by_name('orden_celdas')
    textDict = {}
    for rownum in range(sheet.nrows):
        rowValues = sheet.row_values(rownum)
        CodigoCT = rowValues[0]
        Posicion = rowValues[1]
        CodigoLinea = rowValues[2]
        Transformador = rowValues[3]
        sentencia = 'INSERT INTO gdlOrdenCeldas VALUES(%s,%s,%s,%s)' % (CodigoCT, Posicion, CodigoLinea, Transformador)
        cursorW.execute(sentencia)
    cnxnW.commit()


# -*- coding: iso-8859-15 -*-
'''
Created on 07/09/2011

@author: n1
'''
import utiDMS
import ConfigParser
import LVline2
import TrazaImport
import wx
from db import DataBase


def Import(MINECO):
    PrgDialog = wx.ProgressDialog("Importando", "Importando L�neas de Baja Tensi�n...", maximum=1000)
    PrgDialog.SetSize((400,110))
    PrgDialog.Update(0.001)
    cnxnT=utiDMS.connectSQL(DataBase.TRAZAAddress,\
                            DataBase.TRAZATable,\
                            DataBase.TRAZAUser,\
                            DataBase.TRAZAPassword)
    
    
    cursorT = cnxnT.cursor()
    cursorT.execute("SELECT IdInst FROM Instalaciones WHERE IdTipoInst = 2 AND IdInst LIKE ?", MINECO[0:3] + '%')
    insts = cursorT.fetchall()
    cnxnW = utiDMS.connectSQL(DataBase.Address,\
                              DataBase.DMSTable,\
                              DataBase.User,\
                              DataBase.Password)
    cursorW = cnxnW.cursor()
    # for holding a node code lookup
    cursorW.execute("IF NOT EXISTS\
                    (SELECT * FROM INFORMATION_SCHEMA.TABLES\
                    WHERE TABLE_NAME = 'gdlLV_NODE_BTVano')\
                    CREATE TABLE gdlLV_NODE_BTVano (\
                        NODECODE nvarchar(14),\
                        IdBTVano nvarchar(50)    )")
    trafoboxes = dict()
    LVline2.makeAllComienzoLvNodes(cursorW, cursorT, MINECO)
    cnxnW.commit() # must commit these before continuing
    
    i=0
    for inst in insts:
        i=i+1
        try:
            LVline2.LVline(cursorT,cursorW,inst,MINECO[0:3], trafoboxes)
            cnxnW.commit()
        except:
            TrazaImport.WriteLog("Error imprevisto")
            cnxnW.commit()
        PrgDialog.Update(float(i)/float(len(insts))*1000.0)

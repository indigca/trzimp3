# -*- coding: iso-8859-15 -*-
import wx
from db import DataBase
import utiDMS
import wx.grid as gridlib
import os

class Main(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, parent=None, title='Incidencias Validadas',size=(553, 600))
        self.panel = wx.Panel(self)
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        self.Dist = ''
        self.Arch = ''
        self.Tipo = ''
        self.DictTipo = {'Media':'MVOutageReport','Baja':'LVOutageReport'}
        # Distributor
        domain = os.environ.get( "USERDOMAIN" )
        user = os.environ.get( "USERNAME" )
        userDomain = domain + "\\" + user
        userDomain = "SCADA1\user000"
        cursorD = self.cnxnD.cursor()
        cursorD.execute("SELECT L.Name, L.ID ,L.DMS\
                 FROM gdlMineco L WHERE L.ID IN (SELECT COMPANY FROM gdlReportCompany WHERE USERID LIKE ?)",userDomain)
        rows=cursorD.fetchall()
        sampleList=[]
        for row in rows:
            sampleList.append(row.ID+row.DMS+"-"+row.Name)
        self.selectedDistLabel=wx.StaticText(self.panel,-1,"Seleccione distribuidora",(0,0),size=(-1,-1),style=wx.ALIGN_LEFT | wx.ST_NO_AUTORESIZE )
        
        'List of Socios'
        self.cbDistributor = wx.ComboBox(self.panel,  choices = sampleList, size=(140, -1), pos=(150, 0), style=wx.CB_READONLY)
        
        'Archivos'
        cursorD = self.cnxnD.cursor()
        cursorD.execute("SELECT NAME\
                 FROM FaultArchive")
        rows=cursorD.fetchall()
        sampleList=[]
        for row in rows:
            sampleList.append(row.NAME)
        self.selectedArchLabel=wx.StaticText(self.panel,-1,"Seleccione Archivo",(0,25),size=(-1,-1),style=wx.ALIGN_LEFT | wx.ST_NO_AUTORESIZE )
        self.cbArchive = wx.ComboBox(self.panel,  choices = sampleList, size=(140, -1), pos=(150, 25), style=wx.CB_READONLY)
        
        'Tipo de Informe'
        sampleList=['Media', 'Baja']
        self.selectedTipoLabel=wx.StaticText(self.panel,-1,"Tipo de incidencia",(0,50),size=(-1,-1),style=wx.ALIGN_LEFT | wx.ST_NO_AUTORESIZE )
        self.cbTipo = wx.ComboBox(self.panel,  choices = sampleList, size=(140, -1), pos=(150, 50), style=wx.CB_READONLY)
        
        # Button
        self.searchButton = wx.Button(self.panel, -1, "Ver", pos=(215, 75))
        self.searchButton.Bind(wx.EVT_BUTTON, self.OnSearch)
        
        # Operator
        self.selectedDistLabel=wx.StaticText(self.panel,-1,"Operador",(0,100),size=(140,-1),style=wx.ALIGN_LEFT | wx.ST_NO_AUTORESIZE )
        self.tbOperator = wx.TextCtrl(self.panel, size=(140, -1), pos=(150, 100))
        self.tbOperator.SetValue(user)
        
      
        # Grid creation
        self.informesGrid = gridlib.Grid(self.panel, pos=(0, 125))
        
#        self.Bind(gridlib.EVT_GRID_LABEL_LEFT_DCLICK, self.OnCellLeftDClick)
        


        
    def OnSearch(self,event):
        
        # SQL Sentence and connection
        cursorD = self.cnxnD.cursor()
        self.Arch = self.cbArchive.Value
        self.Dist = self.cbDistributor.Value[0:3]
        self.Tipo = self.DictTipo[self.cbTipo.Value]
#        sentence = "select VALIDATED TOP 1 FROM MVOutageReport%s" % self.Arch
#        try:
#            cursorD.execute(sentence)
#        except:
#            sentence = """ALTER TABLE MVOutageReport%s
#            ADD VALIDATED Bit NOT NULL
#            constraint df_MVOutageReport%s_Validated default 0
#            """ % (self.Arch,self.Arch)
#            try:
#                cursorD.execute(sentence)
#                self.cnxnD.commit()
#            except:
#                pass
#        sentence = "select * from [%s%s] where Region = '%s' and VALIDATED = 0" % (self.Tipo,self.Arch,self.Dist)     
        sentence = "select * from [%s%s] where Region = '%s' AND NUMBER IN (SELECT id FROM gdlValidatedReports WHERE archivo = '%s')"  % (self.Tipo,self.Arch,self.Dist,self.Arch)     

        try:
            cursorD.execute(sentence)
        except:
            dlg = wx.MessageBox('Datos introducidos incorrectamente', 'Error', 
            wx.OK | wx.ICON_INFORMATION)
            return
        informes=cursorD.fetchall()
        
        # Grid definition
        numeroFilas = len(informes)
        self.informesGrid.Destroy()
        self.informesGrid = gridlib.Grid(self.panel, pos=(0, 125), size=(533, 495))
        self.informesGrid.CreateGrid(numeroFilas + 1, 4)
        
        # Grid columns definition
        self.informesGrid.SetColSize( 0, 50 )
        self.informesGrid.SetColSize( 1, 150 )
        self.informesGrid.SetColSize( 2, 150 )
        self.informesGrid.SetColSize( 3, 100 )
        self.informesGrid.SetCellValue( 0, 0, "NUMERO" );
        self.informesGrid.SetCellValue( 0, 1, "FECHA" );
        self.informesGrid.SetCellValue( 0, 2, "OPERADOR" );
        self.informesGrid.SetCellValue( 0, 3, "DURACION" );
        
        # Insert values into grid
        i = 0
        for informe in informes:
            i = i + 1
            columns = [informe.NUMBER, informe.Time, informe.Operator, informe.Duration]
            for j in range(0,len(columns)):
                self.informesGrid.SetCellValue( i, j, str(columns[j]) );
                self.informesGrid.SetReadOnly( i, j )
                
    def OnCellLeftDClick(self, evt):
        NUMBER = self.informesGrid.GetCellValue(evt.GetRow(),0)
        dlg = wx.MessageBox('Confirmar', 'Confirmar', 
                    wx.YES_NO | wx.ICON_INFORMATION)
        if dlg == 2:
            cursorD = self.cnxnD.cursor()
            user = os.environ.get( "USERDOMAIN" ) + "\\" + os.environ.get( "USERNAME" )
            user = os.environ.get( "USERNAME" )
            user = self.tbOperator.Value
            sentence = "UPDATE %s%s SET Operator = '%s-Aprobada' WHERE NUMBER = %s" % (self.Tipo,self.Arch,user,NUMBER)
            cursorD.execute(sentence)
            sentence = "INSERT INTO gdlValidatedReports VALUES (%s,'%s')" % (NUMBER,self.Arch)
            cursorD.execute(sentence)
            self.cnxnD.commit()
        evt.Skip()
        self.OnSearch(evt)
        
def openForm():
    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
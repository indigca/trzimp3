


def line(cnxnT, linName, MINECO):
    cursorT = cnxnT.cursor()
#    cursorT.execute("SELECT Distinct Ins.Nombre \
#                     FROM Instalaciones Ins,  ATVano ATV   \
#                     WHERE  Ins.IdInst=ATV.IdInst")
#    lines = cursorT.fetchall()
#    for line in lines:   
    cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
    cursorW = cnxnW.cursor()        
#        cursorT.execute("SELECT  Ins.Nombre , ATV.X , ATV.Y, ATV.ApoyoSecuencial,\
#                                Tipo.Descripcion \
#                                FROM     ATVano ATV, Instalaciones Ins, dbo.ATVanoTiposConductores Tipo\
#                                WHERE       ATV.IdInst = Ins.IdInst and ATV.IdTipoConductor=Tipo.IdTipoConductor\
#                                AND Ins.Nombre = ? ORDER BY ATV.ApoyoSecuencial",line.Nombre)
#    print line.Nombre
    cursorT.execute("SELECT  Ins.Nombre, ST.X, ST.Y, ApoyoSecuencial = 0, Descripcion = ''\
                     FROM     Instalaciones Ins, dbo.AT, dbo.ST ST\
                     WHERE       AT.IdInst = Ins.IdInst and ST.IdInst = AT.IdOrigenLinea\
                     and Ins.Nombre = ? and AT.IdTipoOrigen = 2\
                     UNION\
                     SELECT  Ins.Nombre, CT.X, CT.Y, ApoyoSecuencial = 0, Descripcion = ''\
                     FROM     Instalaciones Ins, dbo.AT, dbo.CT CT\
                     WHERE       AT.IdInst = Ins.IdInst and CT.IdInst = AT.IdOrigenPunto\
                     and Ins.Nombre = ? and AT.IdTipoOrigen = 4\
                     UNION\
                     SELECT  Ins.Nombre, ATV.X, ATV.Y, ApoyoSecuencial = 0, Descripcion = ''\
                     FROM     Instalaciones Ins, dbo.AT, dbo.ATVano ATV\
                     WHERE       AT.IdInst = Ins.IdInst and ATV.IdATVano = AT.IdOrigenPunto\
                     and Ins.Nombre = ? and AT.IdTipoOrigen = 5\
                     UNION\
                     SELECT  Ins.Nombre , ATV.X , ATV.Y, ATV.ApoyoSecuencial,\
                     Tipo.Descripcion\
                     FROM     ATVano ATV, Instalaciones Ins, dbo.ATVanoTiposConductores Tipo\
                     WHERE       ATV.IdInst = Ins.IdInst and ATV.IdTipoConductor=Tipo.IdTipoConductor\
                     AND Ins.Nombre = ?\
                     ORDER BY ApoyoSecuencial", linName, linName, linName, linName)
    rows = cursorT.fetchall()
#        print rows
    secc = []
    numSecc = 0
    #Finding connecting CTs
    CTs = sqlDMS.getLineEndings (cursorT, linName)
    extremo = []
    for CT in CTs:
        #Finding Connection NodeCodes
        extremo.append(sqlDMS.getEmptyLineConnection(cursorT, cursorW, MINECO + "-" + CT, linName))
    # First and Last points of section
    sn1X, sn1Y = utiDMS.convertED50(rows[0].X, rows[0].Y, 30)
    sn2X, sn2Y = utiDMS.convertED50(rows[len(rows) - 1].X, rows[len(rows) - 1].Y, 30)
    sn1 = utiDMS.getDMSCode(sn1X, sn1Y)
    sn2 = utiDMS.getDMSCode(sn2X, sn2Y)
    FirstLastPts = [1, len(rows)]
    for row in rows:
        row.X, row.Y = utiDMS.convertED50(row.X, row.Y, 30)
        row.X = int(str(row.X).split('.')[0] + "000")
        row.Y = int(str(row.Y).split('.')[0] + "000")      
        numSecc = numSecc + 1
        print numSecc
        if numSecc in FirstLastPts: #First  or last point is a node inside the CT
            if numSecc == 1:
                sn11 = sn1
                sn21 = sn2
            if numSecc == len(rows):
                sn11 = sn2
                sn21 = sn1
            # Case 1 CT in each sitenodes
            if len(extremo) == 2:
                d01 = utiDMS.calcDist(extremo[0], sn11, nodecode=True)
                d11 = utiDMS.calcDist(extremo[1], sn11, nodecode=True)
                if d01 < d11:
                    sitenode = extremo[0]
                else:
                    sitenode = extremo[1]
            # Case only 1 CT in 1 of 2 sitenodes
            if len(extremo) == 1:
                # Calculate distance between CT and sitenodes
                d01 = utiDMS.calcDist(extremo[0], sn11, nodecode=True)
                d11 = utiDMS.calcDist(extremo[0], sn21, nodecode=True)
                # Compare distance to get the correct sitenode
                if d01 < d11:
                    sitenode = extremo[0]
                else:
                    # the other one has not CT, then add node
                    sitenode = utiDMS.getSitenode(cursorW, sn11)
                    sqlDMS.iMV_Node(cursorW, sitenode, row.X, row.Y, row.X, row.Y, sitenode)
            # Case no CT in section        
            if len(extremo) == 0:
                # add node
                sitenode = utiDMS.getSitenode(cursorW, sn11)
                sqlDMS.iMV_Node(cursorW, sitenode, row.X, row.Y, row.X, row.Y, sitenode)    
            if numSecc == 1:
                sitenode1 = sitenode
            if numSecc == len(rows):
                sitenode2 = sitenode
        else:
            secc.append({"x":row.X, "y":row.Y, "numSecc":numSecc})
    print 'rowX', row.X, 'rowY', row.Y, 'sitenode1', sitenode1, 'sitenode2', sitenode2, 'secc', secc
    sectionNumber = sqlDMS.uMV_Section_ID (cnxnW, cursorW)
    sqlDMS.iMV_SectionPoints (cursorW, sectionNumber, sitenode1, sitenode2, secc)
    sqlDMS.iMV_Section(cursorW, sectionNumber, sitenode1, sitenode2, row.Descripcion, MINECO, 13, row.X, row.Y)
    cnxnW.commit()
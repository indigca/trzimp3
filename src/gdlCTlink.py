'''
Created on 07/09/2011

@author: n1
'''
import utiDMS
import ConfigParser
import trazaCreate

class DataBase:
    config = ConfigParser.RawConfigParser()
    config.read('config.ini')
    Address = config.get('DATABASE','Address')
    User = config.get('DATABASE','User')
    Password = config.get('DATABASE','Password')
    DMSTable = config.get('DATABASE','DMSTable')
    TRAZATable = config.get('DATABASE','TRAZATable')
    DMSLocation = config.get('DMS','DMSLocation')
    
MINECO = '061A'
cnxnT=utiDMS.connectSQL(DataBase.Address, DataBase.TRAZATable+MINECO,DataBase.User,DataBase.Password)
cursorT = cnxnT.cursor()
cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
cursorW = cnxnW.cursor() 
cursorT.execute("select Instalaciones.IdInst, Nombre from CT, Instalaciones\
                where CT.IdInst = Instalaciones.IdInst")
CTs = cursorT.fetchall()
for CT in CTs:
    print CT.Nombre
    cursorW.execute("insert into gdlCTLink VALUES (?,?)", CT.IdInst, CT.Nombre)
cnxnW.commit()
# -*- coding: iso-8859-15 -*-
import wx
import utiDMS
import pyodbc
import ConfigParser
import trazaCreate
import LVline
import formMaps
import os
import datetime
import logging
import logging.handlers
import markup
import HTMLCreate
import cStringIO
import check0
import sys
#sys.setdefaultencoding('iso-8859-15')

from db import DataBase


"CREACION DEL LOG"
# Establecer un logger con el nivel de salida deseado
log = logging.getLogger('MiLogger')

logging.basicConfig(level=logging.DEBUG, filename='log/info.log', format="%(asctime)s - %(message)s")

def WriteLog(text):
    log.warning(text)

def WritePage():
    t2 = sorted(file('log/info.log').readlines(),reverse=True)
    page = markup.page( )
    page.p( t2 )
    return page

def WriteHTML():
    page = WritePage()
    f1=open('./HTML/header-log.html','rt')
    f3=open('./HTML/footer-bgcontent.html','rt')
    t1=f1.read()
    t3=f3.read()
    f1.close()
    f3.close()
    f2=open('log/log.html','wt')
    print >> f2,t1
    print >> f2,page
    print >> f2,t3
    f2.close()
    HTMLCreate.fileLevel1()
    HTMLCreate.fileLevel2()

class Main(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, 'Importacion de Excel', 
                size=(700, 575))
        panel = wx.Panel(self, -1)
        'Connect to DMS600'
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        user = os.environ.get( "USERDOMAIN" ) + "\\" + os.environ.get( "USERNAME" )
        WriteLog("Usuario Conectado " + user)
#        if user[0:4] == "user":
#            if user[4:8] == "000":
#                userid = ""
#            else:
#                userid = user[4:8]
#        else:
#            userid = ""
        'Get List of Socios'
#        cursorD.execute("SELECT L.Name, L.ID ,L.DMS\
#                 FROM gdlMineco L WHERE L.DMS IS NOT NULL AND L.ID LIKE ? AND ID in ('054','061','083','101','124','133','166','180') ORDER BY L.ID", userid + '%')
        cursorD.execute("SELECT L.Name, L.ID ,L.DMS\
                 FROM gdlMineco L WHERE L.ID IN (SELECT COMPANY FROM gdlReportCompany WHERE USERID LIKE ?)",user)
        rows=cursorD.fetchall()
        sampleList=[]
        for row in rows:
            sampleList.append(row.ID+row.DMS+"-"+row.Name)
        self.selectedDistLabel=wx.StaticText(panel,-1,"Seleccione distribuidora",(20,2),size=(140,-1),style=wx.ALIGN_LEFT | wx.ST_NO_AUTORESIZE )
        
        'List of Socios'    
        self.sociosList = wx.ListBox(panel, -1, (20, 20), (250, 80), sampleList, 
                wx.LB_SINGLE)
        self.sociosList.SetSelection(-1)
        self.sociosList.Bind(wx.EVT_LISTBOX,self.OnSelect)
        
        'Close Button'
        self.okButton = wx.Button(panel, -1, "Salir", pos=(300, 490))
        self.okButton.Bind(wx.EVT_BUTTON, self.OnCloseMe)
        
        'Display Selected Socio'
        self.selectedSocio="Ninguna Seleccion"
        self.selectedSocioLabel=wx.StaticText(panel,-1,self.selectedSocio,(275,20),size=(140,-1),style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE )
        self.MINECO=""
        'Display Xmax,Ymax,Xmin,Ymin'
#        self.minxLabel=wx.StaticText(panel,-1,"MinX",pos=(350,20),size=(30,-1))    
#        self.minxText=wx.TextCtrl(panel,-1,"000000",size=(70,-1),style=wx.TE_READONLY,pos=(380,17))
#        self.maxxLabel=wx.StaticText(panel,-1,"MaxX",pos=(460,20),size=(30,-1))    
#        self.maxxText=wx.TextCtrl(panel,-1,"000000",size=(70,-1),style=wx.TE_READONLY,pos=(490,17))
#        
#        self.minyLabel=wx.StaticText(panel,-1,"MinY",pos=(350,40),size=(30,-1))    
#        self.minyText=wx.TextCtrl(panel,-1,"000000",size=(70,-1),style=wx.TE_READONLY,pos=(380,37))
#        self.maxyLabel=wx.StaticText(panel,-1,"MaxY",pos=(460,40),size=(30,-1))    
#        self.maxyText=wx.TextCtrl(panel,-1,"000000",size=(70,-1),style=wx.TE_READONLY,pos=(490,37))
        
        'Display List of CTs'
        self.CTsText="Listado de CTs"
        self.CTsLabel=wx.StaticText(panel,-1,self.CTsText,pos=(20,110),size=(80,-1))
        self.CTsList = wx.ListBox(panel, -1, (20, 130), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
#        self.importCTsButton= wx.Button(panel, -1, "4. Importar CTs", pos=(150, 270))
#        self.importCTsButton.Bind(wx.EVT_BUTTON, self.OnSelectCTsButton)
        
        'Display List of Lines'
        self.LinesText="Listado de Lineas"
        self.LinesLabel=wx.StaticText(panel,-1,self.LinesText,pos=(350,110),size=(120,-1))
        self.LinesList = wx.ListBox(panel, -1, (350, 130), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
#        self.importLinesButton= wx.Button(panel, -1, "5. Importar Lineas", pos=(480, 270))
#        self.importLinesButton.Bind(wx.EVT_BUTTON, self.OnSelectLinesButton)
        
        'Display List of Transformers'
        self.TrafosText="Listado de Transformadores"
        self.TrafosLabel=wx.StaticText(panel,-1,self.TrafosText,pos=(20,260),size=(140,-1))
        self.TrafosList = wx.ListBox(panel, -1, (20, 280), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
#        self.importTrafosButton= wx.Button(panel, -1, "3. Importar Trafos", pos=(150, 490))
#        self.importTrafosButton.Bind(wx.EVT_BUTTON, self.OnSelectTrafosButton)
        
        'Display List of Customers connected'
        self.CustomersText="Listado de Clientes asociados (100 primeros)"
        self.CustomersLabel=wx.StaticText(panel,-1,self.CustomersText,pos=(350,260),size=(140,-1))
        self.CustomersList = wx.ListBox(panel, -1, (350, 280), (320, 120), ("Ninguna Seleccion",), 
                wx.LB_SINGLE)
        
#        self.importCustomersButton= wx.Button(panel, -1, "2. Importar Clientes", pos=(480, 490))
#        self.importCustomersButton.Bind(wx.EVT_BUTTON, self.OnSelectCustomersButton)
        
        'Import Maps Button'
        self.importMapsButton = wx.Button(panel, -1, "Importar Mapas", pos=(590, 5))
        self.importMapsButton.Bind(wx.EVT_BUTTON, self.OnSelectMapsButton)
        
        'Logo'        
        logoCide = wx.Image('logo/logo_cide2.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        wx.StaticBitmap(panel, -1, logoCide, (125, 455), (logoCide.GetWidth(), logoCide.GetHeight()))

        logoGedlux = wx.Image('logo/logo_gedlux.png', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        wx.StaticBitmap(panel, -1, logoGedlux, (425, 480), (logoGedlux.GetWidth(), logoGedlux.GetHeight()))
        
        'Borrar datos distribuidora'
        self.DeleteDistributorButton = wx.Button(panel, -1, "Borrar distribuidora", pos=(575, 35))
        self.DeleteDistributorButton.Bind(wx.EVT_BUTTON, self.OnDeleteDistributor)
#        if userid == "":
#            'Borrar todas las distribuidoras'
#            self.DeleteAllDistributorsButton = wx.Button(panel, -1, "Borrar todas las distribuidoras", pos=(525, 65))
#            self.DeleteAllDistributorsButton.Bind(wx.EVT_BUTTON, self.OnDeleteAllDistributors)
#        if userid == "":
#            self.SubestacionCheckbox = wx.CheckBox(panel, -1, "Borrar subestaci�n", pos=(525, 95))
        
        'Importar todo'
        self.ImportAllButton = wx.Button(panel, -1, "Importar", pos=(307, 40))
        self.ImportAllButton.Bind(wx.EVT_BUTTON, self.OnImportAll)
        
        'Informe ExcelImport'
        self.LogButton = wx.Button(panel, -1, "Informe ExcelImport", pos=(285, 425))
        self.LogButton.Bind(wx.EVT_BUTTON, self.Log)
        
        self.ImportMTCheckbox = wx.CheckBox(panel, -1, "Media Tensi�n", pos=(307, 70))
        self.ImportMTCheckbox.Value = True
        self.ImportBTCheckbox = wx.CheckBox(panel, -1, "Baja Tensi�n", pos=(307, 85))
        
    def Log(self,event):
        os.system("start iexplore.exe http://"+os.environ.get( "USERDOMAIN" )+"/log/Log.html")
        
    def InformeTrazaCheck(self,event):
        if self.MINECO!="":
            os.system("start iexplore.exe http://"+os.environ.get( "USERDOMAIN" )+"/TrazaCheck/"+self.MINECO+"/")
        else:
            dlg = wx.MessageBox('Seleccione una distribuidora', 'Seleccione una distribuidora', 
                                            wx.OK | wx.ICON_INFORMATION)
    def TrazaCheck(self,event):
        check0.check()
        dlg = wx.MessageBox('Informe de datos de traza creado', 'Operacion Finalizada', 
                                            wx.OK | wx.ICON_INFORMATION)
        
    def OnSelect(self,event):
        try:
            self.selectedSocio=self.sociosList.GetString(self.sociosList.GetSelection())
        except:
            return
        self.selectedSocioLabel.SetLabel(self.selectedSocio)
        self.MINECO=self.selectedSocio[:self.selectedSocio.find("-")]
       
        'Connect to Traza Database'
#        WriteLog('Abriendo base de datos distribuidor n�mero'+self.MINECO)
        self.cnxnT=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable,DataBase.TRAZAUser,DataBase.TRAZAPassword)
        cursorT = self.cnxnT.cursor()
       
        'Populate CTs List'
        cursorT.execute("SELECT Distinct Ins.Nombre \
                 FROM Instalaciones Ins,  CT CT   \
                 WHERE  Ins.IdInst=CT.IdInst and Ins.IdInst LIKE ?", self.MINECO[0:3] + '%')
        rows=cursorT.fetchall()
        CTs=[]
        for row in rows:
            CTs.append(row.Nombre)
        self.CTsList.Clear()
        self.CTsList.InsertItems(CTs,0)
        self.CTsLabel.SetLabel(self.CTsText+' ('+str(len(rows))+')')
        WriteLog('CTs cargados correctamente')      
        'Populate Lines List'
        cursorT.execute("SELECT Distinct Ins.Nombre \
                 FROM Instalaciones Ins,  ATVano ATV   \
                 WHERE  Ins.IdInst=ATV.IdInst and Ins.IdInst LIKE ?", self.MINECO[0:3] + '%')
        rows=cursorT.fetchall()
        Lines=[]
        for row in rows:
            Lines.append(row.Nombre)
        self.LinesList.Clear()
        self.LinesList.InsertItems(Lines,0)
        self.LinesLabel.SetLabel(self.LinesText+' ('+str(len(rows))+')')
        WriteLog('Lineas cargadas correctamente')
        'Populate Trafos List'
        cursorT.execute("SELECT Ins.Nombre,\
                                    Trf.Potencia, \
                                    Trf.NumeroTransformador \
                            FROM    CTTransformadores Trf, Instalaciones Ins WHERE Trf.idInst=Ins.IdInst and Ins.IdInst LIKE ?", self.MINECO[0:3] + '%')
        rows=cursorT.fetchall()
        Trafos=[]
        for row in rows:
                Trafos.append(row.Nombre+"-"+str(row.NumeroTransformador)+"-"+str(row.Potencia))
        self.TrafosList.Clear()
        self.TrafosList.InsertItems(Trafos,0)
        self.TrafosLabel.SetLabel(self.TrafosText+' ('+str(len(rows))+')')
        WriteLog('Transformadores cargados correctamente')
        
        'Populate Customers List'       
        cursorT.execute("  SELECT  TOP 100 Cli.CUPS, Ins.Nombre, Trf.NumeroTransformador \
                    FROM     Clientes Cli, \
                    CTTransformadores Trf, \
                    Instalaciones Ins,\
                    CT CT\
                    WHERE Ins.IdInst LIKE ? AND Ins.IdInst = Cli.IdInst and Cli.IdInst = CT.IdInst and Cli.IdTransformador=Trf.IdTransformador\
                    ORDER BY Ins.Nombre ", self.MINECO[0:3] + '%')
        rows=cursorT.fetchall()
        Customers=[]
        for row in rows:
#                Customers.append(row.Nombre)
                Customers.append(row.Nombre+"-"+str(row.NumeroTransformador)+"-"+row.CUPS)
        self.CustomersList.Clear()
        self.CustomersList.InsertItems(Customers,0)
        self.CustomersLabel.SetLabel(self.CustomersText+' ('+str(len(rows))+')')
        WriteLog('Clientes cargados correctamente')  
        
    def OnCloseMe(self,event):
        self.Close(True)
        
    def OnSelectMapsButton (self,event):
        formMaps.open()
        
    def OnSelectLVButton (self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM [dbo].[LV_SECTION] \
                              WHERE LV_NETWORK LIKE ?", self.MINECO[0:3] + '%')
            linelvNumber = cursorD.fetchone()
            if linelvNumber == None:
                WriteLog("Importacion lineas BT  iniciada")
                LVline.Import(self.MINECO[0:4])
                WriteLog("Importacion lineas BT  finalizada")
            else:
                dlg = wx.MessageBox('Baja tensi�n ya importada', 'Baja tensi�n ya importada', 
                    wx.OK | wx.ICON_INFORMATION)
        
    def OnSelectLinesButton(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM [dbo].[MV_SECTION] \
                              WHERE HIDE = 0 AND DISTRICT = ?", self.MINECO[0:3])
            lineNumber = cursorD.fetchone()
#            if lineNumber == None:
            if True:
    
#                    print self.LinesList.GetStringSelection()
                    WriteLog("Importacion lineas MT iniciada")
                    trazaCreate.line(self.cnxnT,self.LinesList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importacion lineas MT finalizada")
                    WriteHTML()
            else:
                dlg = wx.MessageBox('L�neas ya importadas', 'L�neas ya importadas', 
                    wx.OK | wx.ICON_INFORMATION)
    
    def OnSelectCTsButton(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM MV_LV_SUBSTATION \
                              where CODE like ?", self.MINECO[0:3] + '%')
            ctNumber = cursorD.fetchone()
            if ctNumber == None:
                
                    WriteLog(self.CTsList.GetStringSelection())
                    WriteLog("Importacion CTs iniciada")
                    trazaCreate.CT(self.cnxnT,self.CTsList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importacion CTs finalizada")
                    WriteHTML()
            else:
                dlg = wx.MessageBox('Centros de transformaci�n ya importados', 'Centros de transformaci�n ya importados', 
                    wx.OK | wx.ICON_INFORMATION)

    def OnSelectTrafosButton(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            cursorD = self.cnxnD.cursor()
            cursorD.execute("SELECT *\
                              FROM TRANSFORMER \
                              where CODE like ?", self.MINECO[0:3] + '%')
            transformerNumber = cursorD.fetchone()
            if transformerNumber == None:            
                WriteLog(self.CTsList.GetStringSelection())
                WriteLog(self.MINECO)
                WriteLog("Importacion trafos iniciada")
                trazaCreate.trafo(DataBase.TRAZATable,DataBase.DMSTable, self.MINECO)
                WriteLog("Importacion trafos finalizada")
                WriteHTML()
            else:
                dlg = wx.MessageBox('Transformadores ya importados', 'Transformadores ya importados', 
                    wx.OK | wx.ICON_INFORMATION)


    def OnSelectCustomersButton(self,event):
        cursorD = self.cnxnD.cursor()
        cursorD.execute("SELECT *\
                      FROM [dbo].[CUSTOMER]\
                      where LV_NETWORK like ?",self.MINECO[0:3] + '%')
        customersNumber = cursorD.fetchone()
        if customersNumber == None:
            if self.MINECO == "":
                dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
                wx.OK | wx.ICON_INFORMATION)
            else:
                WriteLog(self.CTsList.GetStringSelection())
                WriteLog(self.MINECO)
        #        try:
                WriteLog("Importacion clientes iniciada")
                trazaCreate.customer(self.cnxnT, self.MINECO)
                WriteLog("Importacion clientes finalizada")
                WriteHTML()
        #        except Exception:
        #            print Exception.message
        else:
            dlg = wx.MessageBox('Clientes ya importados', 'Clientes ya importados', 
                wx.OK | wx.ICON_INFORMATION)
        

    def OnDeleteDistributor(self,event):                          
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:
            dlg = wx.MessageBox('�Est� seguro de que quiere eliminar los datos de la distribuidora seleccionada?', 'Eliminar', 
            wx.YES_NO | wx.ICON_INFORMATION)
            if dlg == 2:
                WriteLog("Eliminando datos de la distribuidora")
#                if self.SubestacionCheckbox:
#                    trazaCreate.delete_distributor(self.MINECO)
#                else:
                trazaCreate.delete_distributor_without_substations(self.MINECO)
                WriteLog("Datos de la distribuidora eliminados")
                WriteHTML()
                dlg = wx.MessageBox('Datos de la distribuidora eliminados', 'Datos de la distribuidora eliminados', 
                                    wx.OK | wx.ICON_INFORMATION)
        
    def OnDeleteAllDistributors(self,event):
        dlg = wx.MessageBox('�Est� seguro de que quiere eliminar todos los datos de todas las distribuidoras?', 'Eliminar', 
        wx.YES_NO | wx.ICON_INFORMATION)
        
        if dlg == 2:

            WriteLog("Eliminando datos de todas las distribuidoras")
            if self.SubestacionCheckbox:
                trazaCreate.delete_all_distributors()
            else:
                trazaCreate.delete_all_distributors_without_substations()
            WriteLog("Datos de todas las distribuidoras eliminados")
            WriteHTML()
            dlg = wx.MessageBox('Datos de las distribuidoras eliminados', 'Datos de las distribuidoras eliminados', 
                                    wx.OK | wx.ICON_INFORMATION)
        
    def OnImportAll(self,event):
        if self.MINECO == "":
            dlg = wx.MessageBox('Debe seleccionar una distribuidora', 'Seleccione una distribuidora', 
            wx.OK | wx.ICON_INFORMATION)
        else:           
            if self.ImportMTCheckbox.Value == True:
                cursorD = self.cnxnD.cursor()
                cursorD.execute("SELECT *\
                                  FROM TRANSFORMER \
                                  where CODE like ?", self.MINECO[0:3] + '%')
                transformerNumber = cursorD.fetchone()
                if transformerNumber == None:
                    WriteLog("Importando Transformadores")
                    trazaCreate.trafo(DataBase.TRAZATable,DataBase.DMSTable, self.MINECO)
                    WriteLog("Importando Centros de transformacion")
                    trazaCreate.CT(self.cnxnT,self.CTsList.GetStringSelection(),self.MINECO[0:3])
                    WriteLog("Importando Lineas de Alta y Media Tension")            
                    trazaCreate.line(self.cnxnT,self.LinesList.GetStringSelection(),self.MINECO[0:3])
                    trazaCreate.borderPoints(self.cnxnT, self.MINECO[0:3])
                    WriteLog("Importacion finalizada")
                    WriteHTML()
                    dlg = wx.MessageBox('Importaci�n realizada', 'Importaci�n realizada', 
                                            wx.OK | wx.ICON_INFORMATION)
                else:
                    dlg = wx.MessageBox('Debe eliminar primero la media tensi�n para importarla de nuevo', 'MT ya importada', 
                                    wx.OK | wx.ICON_INFORMATION)
            if self.ImportBTCheckbox.Value == True:
                cursorD = self.cnxnD.cursor()
                cursorD.execute("SELECT *\
                                  FROM TRANSFORMER \
                                  where CODE like ?", self.MINECO[0:3] + '%')
                transformerNumber = cursorD.fetchone()
                if transformerNumber != None:
                    WriteLog("Importando Clientes")
                    trazaCreate.customer(self.cnxnT, self.MINECO)
                    WriteLog("Importando Lineas de Baja Tension")
                    LVline.Import(self.MINECO[0:4])
                    WriteLog("Importacion finalizada")
                    WriteHTML()
                    dlg = wx.MessageBox('Importaci�n realizada', 'Importaci�n realizada', 
                                            wx.OK | wx.ICON_INFORMATION)
                else:
                    dlg = wx.MessageBox('Debe importar primero la media tensi�n', 'MT no importada', 
                                    wx.OK | wx.ICON_INFORMATION)
            trazaCreate.separarLineas()

        
if __name__ == '__main__': 

    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
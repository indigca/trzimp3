# Create measurement nodes based on OPC code table

import ConfigParser
import utiDMS
from db import DataBase

import sqlDMS


cnxn = utiDMS.connectSQL(DataBase.Address,\
                         DataBase.DMSTable,\
                         DataBase.User,\
                         DataBase.Password)
cursor = cnxn.cursor()

cursor.execute("SELECT MINECO, TYPE, OPC_CODE\
                FROM gdlOPC")

for meas in cursor.fetchall():
    cursor.execute("SELECT NODECODE, X, Y FROM DISCONNECTOR\
                    JOIN MV_NODE ON DISCONNECTOR.NODECODE\
                    = MV_NODE.CODE\
                    WHERE DISCONNECTOR.CODE = ?",\
                   meas.MINECO + '-' + meas.OPC_CODE.replace('.', ''))
    disc = cursor.fetchone()

    opc_code = "\\\\APL\\1\\OPC\\" + meas.MINECO\
                + '.MEAS.' + meas.TYPE + '.'\
                + meas.OPC_CODE.upper()

    unit = None
    type = 1
    yoffset = 0
    if meas.TYPE == 'I':
        unit = 'A'
        type = 3
        yoffset = 500
    elif meas.TYPE == 'P':
        unit = 'kW'
        type = 4
        yoffset = 0
    elif meas.TYPE == 'V':
        unit = 'kV'
        type = 2
        yoffset = -500

    sqlDMS.makeMeasurement(cursor, disc.X+100, disc.Y + yoffset,\
                           opc_code,\
                           ConnectedNode = disc.NODECODE,\
                           Unit = unit, MeasureType = type)


cnxn.commit()

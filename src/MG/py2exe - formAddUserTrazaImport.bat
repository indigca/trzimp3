cd C:\git\TrzImp3\src
rmdir dist /s
rmdir MG\Output /s
python setup_formAddUserTrazaImport.py py2exe
cd dist
copy C:\git\TrzImp3\src\config.ini
copy C:\git\TrzImp3\src\logo\logo_gedlux.png
copy C:\git\TrzImp3\src\admin.png
copy C:\git\TrzImp3\src\db.pyc
copy C:\git\TrzImp3\src\ConfigParser.pyc
rename formAddUserTrazaImport.exe AdminTool.exe
cd ..\MG
"script innoAdminTool.iss"
echo By Marco Guida (GEDLUX)
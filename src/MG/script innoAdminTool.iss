; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "AdminTool"
#define MyAppVersion "1.0"
#define MyAppPublisher "GEDLux"
#define MyAppURL "http://www.gedlux.es"
#define MyAppExeName "AdminTool.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{162DF1DB-8200-4188-B97C-BF1627142697}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
OutputBaseFilename=AdminTool
SetupIconFile=C:\git\TrzImp3\src\admin.ico
Password=Skso_48
Compression=lzma
SolidCompression=yes

[Languages]
Name: "spanish"; MessagesFile: "compiler:Languages\Spanish.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 0,6.1

[Files]
Source: "C:\git\TrzImp3\src\dist\AdminTool.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\_ctypes.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\_elementtree.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\_hashlib.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\_imaging.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\_socket.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\_ssl.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\admin.png"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\bz2.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\config.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\ConfigParser.pyc"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\db.pyc"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\library.zip"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\logo_gedlux.png"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\lxml.etree.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\pyexpat.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\pyodbc.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\pyproj._geod.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\pyproj._proj.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\python26.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\select.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\unicodedata.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\w9xpopen.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\wx._controls_.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\wx._core_.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\wx._gdi_.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\wx._misc_.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\wx._windows_.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\wxbase28uh_net_vc.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\wxbase28uh_vc.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\wxmsw28uh_adv_vc.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\wxmsw28uh_core_vc.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\git\TrzImp3\src\dist\wxmsw28uh_html_vc.dll"; DestDir: "{app}"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent


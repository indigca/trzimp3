# -*- coding: iso-8859-1 -*-
'''
Created on May 4, 2011

@author: Administrator
'''
import utiDMS
import utiHTML
import HTMLCreate
import markup
from db import DataBase
import unicodedata
import os

from time import strftime
from ConfigParser import ConfigParser

totalFallos=0

def toHTML(text):
    text = text.encode('iso-8859-1')
    text = `text`
    text=text.replace("�","a")
    text=text.replace("�","e")
    text=text.replace("�","i")
    text=text.replace("�","o")
    text=text.replace("�","u")
    text=text.replace("�","A")
    text=text.replace("�","E")
    text=text.replace("�","I")
    text=text.replace("�","O")
    text=text.replace("�","U")
    text=text.replace("�","n")
    text=text.replace("�","N")
    text=text.replace("�","a")
    text=text.replace("�","o")
    return text

def normalize(s):
    try:
     return unicodedata.normalize('NFKD', unicode(s)).encode('ascii','ignore')
    except:
        return s

def checkLines(MINECO):
    global totalFallos
    cnxnD=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorD = cnxnD.cursor()

    cursorD.execute('Select Inst.Nombre,ATV.IdTipoConductor,ATV.ApoyoSecuencial \
                     from dbo.ATVano ATV , dbo.Instalaciones Inst \
                     where IdTipoConductor is NULL and Inst.IdInst=ATV.IdInst AND Inst.IdInst LIKE ? Order By ATV.IdInst,ATV.ApoyoSecuencial', MINECO[0:3] + '%')
    rows=cursorD.fetchall()
    
    if rows.__len__()==0:
        return None
   
    totalFallos+=rows.__len__()
    print 'checkLines '+str(rows.__len__())
    print 'totalFallos '+str(totalFallos)
    
    tableHeader=("Nombre","ApoyoSecuencial")
    tableData=[]
    for row in rows:
        tableData.append((normalize(row.Nombre),row.ApoyoSecuencial))
    page=utiHTML.createTable(tableHeader, tableData)
    return page
       
def checkCTs(MINECO):
    cnxnD=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorD = cnxnD.cursor()

    cursorD.execute('Select  Inst.Nombre, Tipos.Descripcion, CTC.IdCelda  \
                     from dbo.CTCeldas CTC, dbo.Instalaciones Inst , dbo.CTTiposCeldas Tipos \
                     where (CTC.IdTipoCelda = 1 and (CTC.IdAlimentacion is null and CTC.IdDerivacion is null) and CTC.IdInst=Inst.IdInst and CTC.IdTipoCelda = Tipos.IdTipoCelda AND Inst.IdInst LIKE ?) \
                     or (CTC.IdTipoCelda = 2 and CTC.IdTransformador is null and CTC.IdInst=Inst.IdInst and CTC.IdTipoCelda=Tipos.IdTipoCelda AND Inst.IdInst LIKE ?) \
                     order by Inst.Nombre,Tipos.Descripcion', MINECO[0:3] + '%', MINECO[0:3] + '%')
    rows=cursorD.fetchall()
    tableHeader=("Nombre","Tipo","Identificador","Conexion")
    tableData=[]
    for row in rows:
        tableData.append((normalize(row.Nombre),normalize(row.Descripcion),row.IdCelda,"No Definida"))
    page=utiHTML.createTable(tableHeader, tableData) 
    return page

def checkCTs2(MINECO):
    cnxnD=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorD = cnxnD.cursor()

    cursorD.execute('WITH Errors AS (select * from \
                        (select Y.Nombre, sum(Y.NumeroConexiones) NumeroConexiones, sum(Y.NumeroCeldas) NumeroCeldas, (sum\
                        (Y.NumeroConexiones)-sum(Y.NumeroCeldas)) Resta from\
                        (select X.Nombre, count(X.Nombre) NumeroConexiones, NumeroCeldas = 0 from\
                        (select CTDerivaciones.IdDerivacion id, Instalaciones.Nombre\
                        from CTDerivaciones, Instalaciones \
                        where CTDerivaciones.IdInst = Instalaciones.IdInst AND Instalaciones.IdInst LIKE ?\
                        union\
                        select CTAlimentaciones.IdAlimentacion id, Instalaciones.Nombre\
                        from CTAlimentaciones, Instalaciones \
                        where CTAlimentaciones.IdInst = Instalaciones.IdInst AND Instalaciones.IdInst LIKE ?) as X\
                        group by X.Nombre\
                        union\
                        Select  Inst.Nombre, NumeroConexiones = 0, count(Inst.Nombre) NumeroCeldas\
                        from dbo.CTCeldas CTC, dbo.Instalaciones Inst , dbo.CTTiposCeldas Tipos \
                        where (CTC.IdTipoCelda = 1 and CTC.IdInst=Inst.IdInst and CTC.IdTipoCelda = Tipos.IdTipoCelda AND \
                        Inst.IdInst LIKE ?) \
                        group by Inst.Nombre ) as Y \
                        group by Y.Nombre) as Z \
                        where Z.Resta > 0),\
                        prueba as (SELECT CTAlimentacionesProtecciones.IdAlimentacion, CTAlimentaciones.IdInst\
                        FROM CTAlimentacionesProtecciones join CTAlimentaciones on CTAlimentacionesProtecciones.IdAlimentacion = CTAlimentaciones.IdAlimentacion),\
                        prueba2 as\
                        (select IdAlimentacion, prueba.IdInst, IdTipoInst from prueba join Instalaciones ON prueba.IdInst= Instalaciones.IdInst where  prueba.IdInst like ?),\
                        Errors2 as (select Instalaciones.IdInst, Errors.Nombre,NumeroConexiones, NumeroCeldas,Resta from Errors join Instalaciones on Errors.Nombre = Instalaciones.Nombre),\
                        definitivo1 as (select Errors2.IdInst, Nombre,NumeroConexiones,NumeroCeldas,Resta,IdAlimentacion,IdTipoInst from Errors2 join prueba2 on Errors2.IdInst= prueba2.IdInst),\
                        definitivo2 as(select definitivo1.Nombre, COUNT(*) as Elementos from definitivo1 group by Nombre)\
                        select Errors.Nombre, Errors.NumeroConexiones,Errors.NumeroCeldas,(COALESCE(definitivo2.Elementos,0)) AS Elementos, \
                        (COALESCE(Errors.Resta-(COALESCE(definitivo2.Elementos,0)),0))  as RestaFinal from Errors left join definitivo2 on Errors.Nombre=definitivo2.Nombre \
                        where Errors.Resta-definitivo2.Elementos is null or Errors.Resta-definitivo2.Elementos > 0', MINECO[0:3] + '%', MINECO[0:3] + '%', MINECO[0:3] + '%', MINECO[0:3] + '%')
    rows=cursorD.fetchall()
    
    if rows.__len__()==0:
        return None
    
    global totalFallos
    totalFallos+=rows.__len__()
    print 'checkCTs2  '+str(rows.__len__())
    print 'totalFallos '+str(totalFallos)
    
    tableHeader=("Nombre","Numero Conexiones","NumeroCeldas","Elementos","Diferencia")
    tableData=[]
    for row in rows:
       # if row.Resta > 0:
       tableData.append((normalize(row.Nombre),row.NumeroConexiones,row.NumeroCeldas,row.Elementos,row.RestaFinal))
        
    page=utiHTML.createTable(tableHeader, tableData) 
    return page




def checkTrafos(MINECO):
    cnxnD=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorD = cnxnD.cursor()

    cursorD.execute('Select Inst.Nombre,Trf.IdTransformador \
                     from dbo.CTTransformadores Trf , dbo.Instalaciones Inst \
                     where Trf.Potencia is NULL and Inst.IdInst=Trf.IdInst  AND Inst.IdInst LIKE ? Order By Trf.IdInst', MINECO[0:3])
    rows=cursorD.fetchall()
    
    if rows.__len__()==0:
        return None
    
    global totalFallos
    totalFallos+=rows.__len__()
    print 'checkTrafos  '+str(rows.__len__())
    print 'totalFallos '+str(totalFallos)
    
    tableHeader=("CT","IdTransformador","Potencia")
    tableData=[]
    for row in rows:
        tableData.append((normalize(row.Nombre),row.IdTransformador),"No Definida")
    page=utiHTML.createTable(tableHeader, tableData)
    return page

def checkClientes1(MINECO):
    cnxnD=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorD = cnxnD.cursor()

    cursorD.execute('Select Cli.CUPS,Cli.Nombre \
                     from dbo.Clientes Cli \
                     where Cli.IdInst is NULL and IdDistribuidora LIKE ? and Estado=\'A\'', MINECO[0:3] + '%')
    rows=cursorD.fetchall()
    
    if rows.__len__()==0:
        return None
    
    global totalFallos
    totalFallos+=rows.__len__()
    print 'checkClientes1  '+str(rows.__len__())
    print 'totalFallos '+str(totalFallos)
    
    tableHeader=("CUPS","Nombre")
    tableData=[]
    for row in rows:
        if row.CUPS is not None:
            row.CUPS=normalize(row.CUPS)
        if row.Nombre is not None:
            row.Nombre=normalize(row.Nombre)
        tableData.append((row.CUPS,row.Nombre))
    page=utiHTML.createTable(tableHeader, tableData)
    return page

# check lines with more than 2 CTs associated
def checkLinesCTs(MINECO):
    cnxnD=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorD = cnxnD.cursor()

    cursorD.execute('select x.Nombre, count(x.Nombre) Total, x.IdTipoOrigen\
                    from (\
                    select Lineas2.Nombre, AT.IdTipoOrigen\
                    from CTAlimentaciones, Instalaciones Lineas2, Instalaciones CTs, AT\
                    where CTAlimentaciones.IdInst = CTs.IdInst and CTAlimentaciones.IdInstAlimentaciones = Lineas2.IdInst\
                    and Lineas2.IdInst = AT.IdInst AND Lineas2.IdInst LIKE ?\
                    union all\
                    select Lineas3.Nombre, AT.IdTipoOrigen\
                    from CTDerivaciones, Instalaciones Lineas3, Instalaciones CTs, AT\
                    where CTDerivaciones.IdInst = CTs.IdInst and CTDerivaciones.IdInstDerivaciones = Lineas3.IdInst\
                    and Lineas3.IdInst = AT.IdInst AND Lineas3.IdInst LIKE ?\
                    ) as x\
                    group by x.Nombre, x.IdTipoOrigen\
                    having count(x.Nombre) > 2 OR (count(x.Nombre) = 2 AND x.IdTipoOrigen != 4)\
                    order by x.Nombre', MINECO[0:3] + '%', MINECO[0:3] + '%')
    rows=cursorD.fetchall()
    
    if rows.__len__()==0:
        return None
    
    global totalFallos
    totalFallos+=rows.__len__()
    print 'checkLinesCTs  '+str(rows.__len__())
    print 'totalFallos '+str(totalFallos)
    
    tableHeader=("Nombre Linea","Numero CTs")
    tableData=[]
    for row in rows:
        if row.Nombre is not None:
            row.Nombre=normalize(row.Nombre)
        if row.Total is not None:
            row.Total=row.Total
        tableData.append((row.Nombre,row.Total))
    page=utiHTML.createTable(tableHeader, tableData)
    return page
   
# check lines with more than 2 CTs associated
def checkLinesWithoutCT(MINECO):

    cnxnD=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorD = cnxnD.cursor()

    cursorD.execute('select AT.IdInst, Nombre, IdTipoOrigen, Borrada from AT, Instalaciones\
                    where AT.IdInst = Instalaciones.IdInst and Instalaciones.Borrada = 0 and IdTipoOrigen = 4 AND Instalaciones.IdInst LIKE ?\
                    except\
                    (\
                    select Lineas2.IdInst, Lineas2.Nombre, AT.IdTipoOrigen, Lineas2.Borrada\
                    from CTAlimentaciones, Instalaciones Lineas2, Instalaciones CTs, AT\
                    where CTAlimentaciones.IdInst = CTs.IdInst and CTAlimentaciones.IdInstAlimentaciones = Lineas2.IdInst\
                    and Lineas2.IdInst = AT.IdInst AND Lineas2.IdInst LIKE ?\
                    union all\
                    select Lineas3.IdInst, Lineas3.Nombre, AT.IdTipoOrigen, Lineas3.Borrada\
                    from CTDerivaciones, Instalaciones Lineas3, Instalaciones CTs, AT\
                    where CTDerivaciones.IdInst = CTs.IdInst and CTDerivaciones.IdInstDerivaciones = Lineas3.IdInst\
                    and Lineas3.IdInst = AT.IdInst AND Lineas3.IdInst LIKE ?\
                    ) ', MINECO[0:3] + '%', MINECO[0:3] + '%', MINECO[0:3] + '%')
    rows=cursorD.fetchall()
    
    if rows.__len__()==0:
        return None
    
    global totalFallos
    totalFallos+=rows.__len__()
    print 'checkLinesWithoutCT  '+str(rows.__len__())
    print 'totalFallos '+str(totalFallos)
    
    tableHeader=("Identificador","Nombre Linea")
    tableData=[]
    for row in rows:
        if row.Nombre is not None:
            row.Nombre=normalize(row.Nombre)
        tableData.append((row.IdInst,row.Nombre))
    page=utiHTML.createTable(tableHeader, tableData)
    return page   

# check LV lines without transformer
def checkLVLinesWithoutTransformer(MINECO):

    cnxnD=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorD = cnxnD.cursor()

    cursorD.execute('select * from BT, Instalaciones\
                    where Instalaciones.IdInst = BT.IdInst\
                    and IdTransformador is NULL\
                    and Instalaciones.IdInst like ?', MINECO[0:3] + '%')
    rows=cursorD.fetchall()
    
    if rows.__len__()==0:
        return None
    
    global totalFallos
    totalFallos+=rows.__len__()
    print 'checkLVLinesWithoutTransformer  '+str(rows.__len__())
    print 'totalFallos '+str(totalFallos)
    
    tableHeader=("Identificador","Nombre Linea")
    tableData=[]
    for row in rows:
        if row.Nombre is not None:
            row.Nombre=normalize(row.Nombre)
        tableData.append((row.IdInst,row.Nombre))
    page=utiHTML.createTable(tableHeader, tableData)
    return page   

# check LV lines without transformer
def checkLVLinesTransformerNotFound(MINECO):

    cnxnD=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorD = cnxnD.cursor()

    cursorD.execute('select * from BT, Instalaciones\
                    where Instalaciones.IdInst = BT.IdInst \
                    and Instalaciones.IdInst like ?\
                    and IdTransformador NOT IN\
                    (SELECT IdTransformador from CTTransformadores)', MINECO[0:3] + '%')
    rows=cursorD.fetchall()
    
    if rows.__len__()==0:
        return None
    
    global totalFallos
    totalFallos+=rows.__len__()
    print 'checkLVLinesTransformerNotFound  '+str(rows.__len__())
    print 'totalFallos '+str(totalFallos)
    
    tableHeader=("Identificador","Nombre Linea")
    tableData=[]
    for row in rows:
        if row.Nombre is not None:
            row.Nombre=normalize(row.Nombre)
        tableData.append((row.IdInst,row.Nombre))
    page=utiHTML.createTable(tableHeader, tableData)
    return page  
   

#--------------------------------------------------------------------------------

def check(MINECO):
#    reportList=utiHTML.refreshCIDEList ('htdocs/')
#    for socio in reportList:
    
#    Mineco=socio[0:3]
#    Tipo=socio[3]
#    Path=('c:/buzon/'+MINECO+'/chequeo/')
    global totalFallos
    totalFallos=0
    
    print DataBase.Webserver+MINECO+'/chequeo/'
    Path=(DataBase.Webserver+MINECO+'/chequeo/')
    pageLines=checkLines(MINECO)
    pageCTs=checkCTs2(MINECO)
    pageTrafos=checkTrafos(MINECO)
    pageClientes1=checkClientes1(MINECO)
    pageLinesCTs=checkLinesCTs(MINECO)
    pageLinesWithoutCT=checkLinesWithoutCT(MINECO)
    pageLVLinesWithoutTransformer=checkLVLinesWithoutTransformer(MINECO)
    pageLVLinesTransformerNotFound=checkLVLinesTransformerNotFound(MINECO)
               
#    print '*********************************'
#    print str(pageLines)
#    print str(pageCTs)
#    print str(pageTrafos)
#    print str(pageClientes1)
#    print str(pageLinesCTs)
#    print str(pageLinesWithoutCT)
#    print str(pageLVLinesWithoutTransformer)
#    print str(pageLVLinesTransformerNotFound)
#    print '*********************************'
    
               
    f1=open('./HTML/header-L3.html','rt')
    f3=open('./HTML/footer-bgcontent.html','rt')
    
    t1=f1.read()
    t3=f3.read()
    f1.close()
    f3.close()
    
    try:

        if not os.path.isdir(DataBase.Webserver):
            os.mkdir(DataBase.Webserver)
            print DataBase.Webserver
        if not os.path.isdir(DataBase.Webserver+MINECO):
            os.mkdir(DataBase.Webserver+MINECO)
            print DataBase.Webserver+MINECO
        if not os.path.isdir(Path):
            os.mkdir(Path)
            print Path
        
    except IOError:    
        print 'No ha sido posible crear la ruta destino'
        
    f2=open(Path+MINECO+"-"+strftime("%Y-%m-%d_%H-%M-%S")+'.html','wt')
    print >> f2,t1
    page = markup.page( )
    page.p ("")
    page.h1("Distribuidora "+ MINECO)
    page.p(strftime("%Y-%m-%d_%H-%M-%S"))
    page.p()
    page.h3 ("Chequeo de Lineas")
    page.p ("Para las lineas se comprueba que todos los vanos tienen definido el tipo de conductor")
    page.p ("La tabla detalla los vanos erroneos")
    
    if not pageLines is None:
        print >> f2,page
        print >> f2,pageLines
    else:
        page.p ("<center><b style=\"color:#00FF00; background:#000000\">     NO SE HA ENCONTRADO ESTE TIPO DE ERROR     </b></center>")
        page.p ("")
        print >> f2,page
        
    page = markup.page( )
    page.p ("")
    page.h3 ("Chequeo de CTs")
    page.p ("Para los CTs se comprueba que todas las posiciones tipo linea tienen linea asociada. \
             Muestra la diferencia entre las conexiones reales en la red y las que aparecen en las celdas.")
    if not pageCTs is None:
        print >> f2,page
        print >> f2,pageCTs
    else:
        page.p ("<center><b style=\"color:#00FF00; background:#000000\">     NO SE HA ENCONTRADO ESTE TIPO DE ERROR     </b></center>")
        page.p ("")
        print >> f2,page
    
    page = markup.page( )
    page.p ("")
    page.h3 ("Chequeo de Transformadores")
    page.p ("Para los Trafos se comprueba que tienen potencia definida y CT de instalacion")
    
    if not pageTrafos is None:
        print >> f2,page
        print >> f2,pageTrafos
    else:
        page.p ("<center><b style=\"color:#00FF00; background:#000000\">     NO SE HA ENCONTRADO ESTE TIPO DE ERROR     </b></center>")
        page.p ("")
        print >> f2,page
    
    page = markup.page( )
    page.p ("")
    page.h3 ("Chequeo de Numero de CTs asociados por linea")
    page.p ("Una linea no puede tener mas de dos CTs asociados (principio y final) o mas de uno si su origen no es un CT (final)")
    
    if not pageLinesCTs is None:
        print >> f2,page
        print >> f2,pageLinesCTs
    else:
        page.p ("<center><b style=\"color:#00FF00; background:#000000\">     NO SE HA ENCONTRADO ESTE TIPO DE ERROR     </b></center>")
        page.p ("")
        print >> f2,page
    
    page = markup.page( )
    page.p ("")
    page.h3 ("Chequeo de lineas sin CT")
    page.p ("Una linea cuyo origen es un CT debe tener un CT asociado")
    
    
    if not pageLinesWithoutCT is None:
        print >> f2,page
        print >> f2,pageLinesWithoutCT
    else:
        page.p ("<center><b style=\"color:#00FF00; background:#000000\">     NO SE HA ENCONTRADO ESTE TIPO DE ERROR     </b></center>")
        page.p ("")
        print >> f2,page
    
    page = markup.page( )
    page.p ("")
    page.h3 ("Chequeo de lineas de baja sin transformador")
    page.p ("Una linea de baja tensi�n debe tener un transformador asociado")
    
    if not pageLVLinesWithoutTransformer is None:
        print >> f2,page
        print >> f2,pageLVLinesWithoutTransformer
    else:
        page.p ("<center><b style=\"color:#00FF00; background:#000000\">     NO SE HA ENCONTRADO ESTE TIPO DE ERROR     </b></center>")
        page.p ("")
        print >> f2,page  
    
    page = markup.page( )
    page.p ("")
    page.h3 ("Chequeo de transformador de las lineas de baja tension")
    page.p ("Se comprueba que el transformador existe")
    
    
    if not pageLVLinesTransformerNotFound is None:
        print >> f2,page
        print >> f2,pageLVLinesTransformerNotFound 
    else:
        page.p ("<center><b style=\"color:#00FF00; background:#000000\">     NO SE HA ENCONTRADO ESTE TIPO DE ERROR     </b></center>")
        print >> f2,page               
    
    page = markup.page( )
    page.p ("")
    page.h3 ("Chequeo de Clientes (1)")
    page.p ("Deben de tener Ct y Trafo")
    
    print >> f2,page
    page = markup.page( )
    page.p ("")
    if not pageClientes1 is None:
        print >> f2,pageClientes1
    else:
        page.p ("<center><b style=\"color:#00FF00; background:#000000\">     NO SE HA ENCONTRADO ESTE TIPO DE ERROR     </b></center>")
        page.p ("")
        #print >> f2,page
    
    page.p ("<center><b style=color:#FF0000>Total errores encontrados: "+str(totalFallos)+"</b></center>")
    #page.p ("Total errores encontrados: "+str(totalFallos))
    
    print >> f2,page
    print >> f2,t3
    
    f2.close()
  
    
    if totalFallos==0 or DataBase.ImportBlock == 'NO' :
        utiDMS.setValidacionTrazaCheck(MINECO,True)
    else:
        utiDMS.setValidacionTrazaCheck(MINECO,False)
    

    


#    HTMLCreate.fileLevel1()
#    HTMLCreate.fileLevel2()
    HTMLCreate.fileLevel3(MINECO)
    
    return totalFallos

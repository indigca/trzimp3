'''
Created on 30/07/2012

@author: Eugenio
DEPRECATED ON 09/AGU/2013 <-----------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
-----------------------------------------------
'''
import wx
from db import DataBase
import TrazaImport
import utiDMS
import os
import hashlib

class Main(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, 'Crear usuario', 
                size=(300, 250))
        panel = wx.Panel(self, -1)
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        user = os.environ.get( "USERDOMAIN" ) + "\\" + os.environ.get( "USERNAME" )

#        cursorD.execute("SELECT L.Name, L.ID ,L.DMS\
#                 FROM gdlMineco L WHERE L.ID IN (SELECT COMPANY FROM gdlReportCompany WHERE USERID LIKE ?)","SCADA1\user000")
        cursorD.execute("SELECT Name, ID\
                 FROM gdlMineco")
        rows=cursorD.fetchall()
        sampleList=[]
        for row in rows:
#            sampleList.append(row.ID+row.DMS+"-"+row.Name)
            sampleList.append(row.ID+"-"+row.Name)
        self.selectedDistLabel=wx.StaticText(panel,-1,"Seleccione distribuidora",(20,2),size=(140,-1),style=wx.ALIGN_LEFT | wx.ST_NO_AUTORESIZE )
        'List of Socios'    
        self.sociosList = wx.ListBox(panel, -1, (20, 20), (250, 80), sampleList, 
                wx.LB_SINGLE)
        self.sociosList.SetSelection(-1)
        self.sociosList.Disable()
#        self.sociosList.Bind(wx.EVT_LISTBOX,self.OnSelect)
        self.lblUsuario = wx.StaticText(panel, label="usuario :",pos=(20,120))
        self.tbUsuario = wx.TextCtrl(panel, pos=(80,120), size=(185, -1))
        
        self.lblPassword = wx.StaticText(panel, label="Password :",pos=(20,160))
        self.tbPassword = wx.TextCtrl(panel, pos=(80,160), size=(185, -1))
        
        self.lblEjemplo = wx.StaticText(panel, label="(Ejemplo: SCADA\user000)",pos=(80,145))
        
        self.CreateButton= wx.Button(panel, -1, "Crear", pos=(60, 180))
        self.CreateButton.Bind(wx.EVT_BUTTON, self.Create)
        
        self.DeleteButton= wx.Button(panel, -1, "Borrar", pos=(160, 180))
        self.DeleteButton.Bind(wx.EVT_BUTTON, self.Delete)
        
    def Delete(self,event):        
        'Connect to DMS600'
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        userid = self.tbUsuario.Value
        password= self.tbPassword.Value
        #try:
            #self.selectedSocio=self.sociosList.GetString(self.sociosList.GetSelection())
        #except:
           # return
        #company=self.selectedSocio[:self.selectedSocio.find("-")]
        #sentencia = "INSERT INTO gdlReportCompany(USERID, COMPANY) VALUES ('%s','%s')" % (userid,company[0:3])
        
        user_reverse = userid[::-1]
        md5introducido = hashlib.md5(password+ user_reverse).hexdigest()
        
        sentencia = "DELETE from gdlTrazaImportUser where usuario = '%s' AND password = '%s'" % (userid,md5introducido)
        
        
        try:
            cursorD.execute(sentencia)
        except:
            return
        self.cnxnD.commit()       
        
    def Create(self,event):        
        'Connect to DMS600'
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        userid = self.tbUsuario.Value
        password= self.tbPassword.Value
        #try:
            #self.selectedSocio=self.sociosList.GetString(self.sociosList.GetSelection())
        #except:
           # return
        #company=self.selectedSocio[:self.selectedSocio.find("-")]
        #sentencia = "INSERT INTO gdlReportCompany(USERID, COMPANY) VALUES ('%s','%s')" % (userid,company[0:3])
        
        user_reverse = userid[::-1]
        md5introducido = hashlib.md5(password+ user_reverse).hexdigest()
        
        sentencia = "INSERT INTO gdlTrazaImportUser(usuario, password) VALUES ('%s','%s')" % (userid,md5introducido)
        
        
        try:
            cursorD.execute(sentencia)
        except:
            return
        self.cnxnD.commit()
        
if __name__ == '__main__': 
#def open():
    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
    
   
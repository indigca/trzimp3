CREATE TABLE [dbo].[FaultReport2014](
	[NUMBER] [decimal](9, 0) NOT NULL DEFAULT((0)),
	[Time] [datetime] NULL,
	[Company] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Substation] [nvarchar](50) NULL,
	[Feeder] [nvarchar](50) NULL,
	[OutageReason] [nvarchar](50) NULL,
	[FaultReason] [nvarchar](50) NULL,
	[FaultLocation] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Operator] [nvarchar](50) NULL,
	[Operation] [decimal](9, 0) NULL DEFAULT((0)),
	[Isolation] [decimal](9, 0) NULL DEFAULT((0)),
	[FaultType] [decimal](9, 0) NULL DEFAULT((0)),
	[OtherFault] [nvarchar](50) NULL,
	[Ik] [decimal](9, 0) NULL DEFAULT((0)),
	[PhaseCnt] [decimal](9, 0) NULL DEFAULT((0)),
	[SearchTimeMen] [float] NULL DEFAULT((0)),
	[SearchTimeFault] [float] NULL DEFAULT((0)),
	[RepairingTime] [float] NULL DEFAULT((0)),
	[WorkingTime] [float] NULL DEFAULT((0)),
	[Temperature] [float] NULL DEFAULT((0)),
	[Wind] [nvarchar](50) NULL,
	[Humidity] [nvarchar](50) NULL,
	[StartingTime] [datetime] NULL,
	[EndingTime] [datetime] NULL,
	[Duration] [nvarchar](50) NULL,
	[LVNet] [decimal](9, 0) NULL DEFAULT((0)),
	[LVNeth] [float] NULL DEFAULT((0)),
	[Cust] [decimal](9, 0) NULL DEFAULT((0)),
	[Custh] [float] NULL DEFAULT((0)),
	[NDE] [float] NULL DEFAULT((0)),
	[AdditionalData] [nvarchar](1024) NULL,
	[CABLINGRATE] [decimal](9, 0) NULL DEFAULT((0)),
	[AutoCalculation] [decimal](9, 0) NOT NULL DEFAULT((0)),
	[VALIDATED] [bit] NOT NULL DEFAULT((0)),
 CONSTRAINT [PK_FaultReport2014] PRIMARY KEY CLUSTERED 
(
	[NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[MVOutageReport2014](
	[NUMBER] [decimal](9, 0) NOT NULL DEFAULT((0)),
	[Type] [decimal](9, 0) NULL DEFAULT((0)),
	[State] [decimal](9, 0) NULL DEFAULT((0)),
	[Time] [datetime] NULL,
	[Company] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Substation] [nvarchar](50) NULL,
	[Feeder] [nvarchar](50) NULL,
	[OutageReason] [nvarchar](50) NULL,
	[FaultReason] [nvarchar](50) NULL,
	[FaultLocation] [nvarchar](50) NULL,
	[MaintOutageReason] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Operator] [nvarchar](50) NULL,
	[Operation] [decimal](9, 0) NULL DEFAULT((0)),
	[Isolation] [decimal](9, 0) NULL DEFAULT((0)),
	[FaultType] [decimal](9, 0) NULL DEFAULT((0)),
	[OtherFault] [nvarchar](50) NULL,
	[Ik] [decimal](9, 0) NULL,
	[PhaseCnt] [decimal](9, 0) NULL,
	[SearchTimeMen] [float] NULL,
	[SearchTimeFault] [float] NULL,
	[RepairingTime] [float] NULL,
	[WorkingTime] [float] NULL,
	[Temperature] [float] NULL,
	[Wind] [nvarchar](50) NULL,
	[Humidity] [nvarchar](50) NULL,
	[StartingTime] [datetime] NULL,
	[EndingTime] [datetime] NULL,
	[Duration] [nvarchar](50) NULL,
	[LVNet] [decimal](9, 0) NULL,
	[LVNeth] [float] NULL,
	[Cust] [decimal](9, 0) NULL,
	[Custh] [float] NULL,
	[NDE] [float] NULL,
	[AdditionalData] [nvarchar](1024) NULL,
	[AutoCalculation] [decimal](9, 0) NOT NULL,
	[VALIDATED] [bit] NOT NULL DEFAULT((0)),
 CONSTRAINT [PK_MVOutageReport2014] PRIMARY KEY CLUSTERED 
(
	[NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[LVOutageReport2014](
	[NUMBER] [decimal](9, 0) NOT NULL DEFAULT ((0)),
	[State] [decimal](9, 0) NULL DEFAULT ((0)),
	[Type] [decimal](9, 0) NULL  DEFAULT ((0)),
	[Time] [datetime] NULL,
	[Company] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[OutageReason] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Operator] [nvarchar](50) NULL,
	[StartingTime] [datetime] NULL,
	[EndingTime] [datetime] NULL,
	[Duration] [nvarchar](50) NULL,
	[LVNet] [decimal](9, 0) NULL DEFAULT ((0)),
	[LVNeth] [float] NULL DEFAULT ((0)),
	[Cust] [decimal](9, 0) NULL DEFAULT ((0)),
	[Custh] [float] NULL DEFAULT ((0)),
	[NDE] [float] NULL DEFAULT ((0)),
	[AdditionalData] [nvarchar](1024) NULL,
	[LVOutageReason] [nvarchar](3) NULL,
	[LVFaultReason] [nvarchar](3) NULL,
	[LVFaultLocation] [nvarchar](3) NULL,
	[LVFaultType] [nvarchar](3) NULL,
	[LVProtection] [nvarchar](3) NULL,
	[VALIDATED] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_LVOutageReport2014] PRIMARY KEY CLUSTERED 
(
	[NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[LVNetworkOutages2014](
	[CODE] [nvarchar](32) NOT NULL,
	[OUTAGEAREA] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_LVNetworkOutages2014] PRIMARY KEY CLUSTERED 
(
	[OUTAGEAREA] ASC,
	[CODE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[MaintenanceOutageReport2014](
	[NUMBER] [numeric](18, 0) NOT NULL,
	[TIME] [datetime] NULL,
	[COMPANY] [nvarchar](50) NULL,
	[REGION] [nvarchar](50) NULL,
	[SUBSTATION] [nvarchar](50) NULL,
	[FEEDER] [nvarchar](50) NULL,
	[OUTAGEREASON] [nvarchar](50) NULL,
	[MAINTOUTAGEREASON] [nvarchar](3) NULL,
	[FAULTLOCATION] [nvarchar](50) NULL,
	[ADDRESS] [nvarchar](50) NULL,
	[OPERATOR] [nvarchar](50) NULL,
	[STARTINGTIME] [datetime] NULL,
	[ENDINGTIME] [datetime] NULL,
	[DURATION] [nvarchar](50) NULL,
	[LVNET] [numeric](18, 0) NULL,
	[LVNETH] [float] NULL,
	[CUST] [numeric](18, 0) NULL,
	[CUSTH] [float] NULL,
	[NDE] [float] NULL,
	[ADDITIONALDATA] [nvarchar](1024) NULL,
	[CABLINGRATE] [numeric](18, 0) NULL,
	[AUTOCALCULATION] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_MaintenanceOutageReport2014] PRIMARY KEY CLUSTERED 
(
	[NUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[OutageAreas2014](
	[ID] [numeric](18, 0) NOT NULL,
	[NUMBER] [numeric](18, 0) NULL,
	[TYPE] [numeric](18, 0) NULL,
	[FEEDINGPOINT] [nvarchar](50) NULL,
	[BORDERS] [nvarchar](MAX) NULL,
	[STARTINGTIME] [datetime] NULL,
	[ENDINGTIME] [datetime] NULL,
	[DURATION] [nvarchar](50) NULL,
	[LVNET] [numeric](18, 0) NULL,
	[LVNETH] [float] NULL,
	[CUST] [numeric](18, 0) NULL,
	[CUSTH] [float] NULL,
	[NDE] [float] NULL,
	[BREAKS] [numeric](18, 0) NULL,
	[TRANSFERDATE] [datetime] NULL,
 CONSTRAINT [PK_OutageAreas2014] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO FaultArchive VALUES (2014,	1,	1,	1,	0)

	
CREATE FUNCTION [dbo].[TYPE2WDS]
(
	@type nvarchar(80),
	@lang nvarchar(35)
)
RETURNS nvarchar(80)
AS
BEGIN

	DECLARE @ret nvarchar(max);
	SET @ret = (SELECT TRANSLATION FROM TRANSLATION WHERE LANG = @lang AND STRING = 'OUTAGE_TYPE_' + CAST(@type AS nvarchar(4)))

	RETURN @ret
	
END

GO

CREATE FUNCTION [dbo].[GETNOTNULL] 
(
	@s1 nvarchar(50),
	@s2 nvarchar(50)
)
RETURNS nvarchar(50)
AS
BEGIN
	IF @s1 IS NULL RETURN @s2
	RETURN @s1
	
END

GO

CREATE FUNCTION GETCODEINFO
(
	@infotype nvarchar(50),
	@code nvarchar(3)
)
RETURNS nvarchar(50)
AS
BEGIN
	DECLARE @explanation nvarchar(50);

	SELECT @explanation = INFO FROM CODEINFO WHERE INFOTYPE = @infotype AND CODE = @code;
	
	RETURN @explanation

END

GO

CREATE FUNCTION [dbo].[DDIFFHOUR] 
(
	@start datetime,
	@end datetime
)
RETURNS float
AS
BEGIN
	IF @start > @end OR @start IS NULL OR @end IS NULL
		RETURN NULL;

	DECLARE @ret float;
	SET @ret = CAST(DATEDIFF(second, @start,@end) AS FLOAT)/60/60;
	RETURN @ret;
END

GO

CREATE FUNCTION FORMAT_TIME 
(
	@hours float
)
RETURNS nvarchar(9)
AS
BEGIN
	DECLARE @h int
	SET @h = FLOOR(@hours)
	DECLARE @m int
	SET @m = FLOOR((@hours-@h)*60)
	DECLARE @s int
	SET @s = FLOOR(((@hours-@h)*60-@m)*60)

	DECLARE @hs nvarchar(3)
	DECLARE @ms nvarchar(2)
	DECLARE @ss nvarchar(2)

	IF (@h < 10)
	BEGIN
		SET @hs = '0' + CAST(@h AS nvarchar(3))
	END
	ELSE
	BEGIN
		SET @hs = CAST(@h AS nvarchar(3))
	END
	
	IF (@m < 10) SET @ms = '0' + CAST(@m AS nvarchar(2))
	ELSE SET @ms = CAST(@m AS nvarchar(2))

	IF (@s < 10) SET @ss = '0' + CAST(@s AS nvarchar(2))
	ELSE SET @ss = CAST(@s AS nvarchar(2))

	DECLARE @Result nvarchar(9)

	SELECT @Result = @hs + ':' + @ms + ':' + @ss

	RETURN @Result
END
GO

  CREATE FUNCTION [dbo].[SplitStrings]
(
    @List       NVARCHAR(MAX),
    @Delimiter  NVARCHAR(255)
)
RETURNS TABLE
AS
    RETURN (SELECT Number = ROW_NUMBER() OVER (ORDER BY Number),
        Value FROM (SELECT Number, Value = LTRIM(RTRIM(SUBSTRING(@List, Number, 
        CHARINDEX(@Delimiter, @List + @Delimiter, Number) - Number)))
    FROM (SELECT ROW_NUMBER() OVER (ORDER BY s1.[object_id])
        FROM sys.all_objects AS s1 CROSS APPLY sys.all_objects) AS n(Number)
    WHERE Number <= CONVERT(INT, LEN(@List))
        AND SUBSTRING(@Delimiter + @List, Number, 1) = @Delimiter
    ) AS y);
GO


CREATE VIEW [dbo].[MUNI_POWER_SP]
AS
SELECT	MUNI, PROVINCE, UTILITY, COUNT(NAME) AS CUST, SUM(CONTRACTED_POWER) AS POWER, 0 AS PROPIOS
FROM	CUSTOMER
			JOIN gdlMvCustLocation ON CUSTOMER.CUSTOMER_NODE = gdlMvCustLocation.CUSTOMER_NODE
GROUP BY MUNI, PROVINCE, UTILITY
UNION
SELECT     dbo.MV_LV_SUBSTATION.MUN, dbo.MV_LV_SUBSTATION.PROVINCE, dbo.MV_LV_SUBSTATION.COMPANY, 
                      COUNT(NAME) AS CUST, SUM(CONTRACTED_POWER) AS POWER, 1 AS PROPIOS
FROM         dbo.CUSTOMER INNER JOIN
                      dbo.MV_LV_SUBSTATION ON dbo.CUSTOMER.LV_NETWORK = dbo.MV_LV_SUBSTATION.CODE
GROUP BY dbo.MV_LV_SUBSTATION.MUN, dbo.MV_LV_SUBSTATION.PROVINCE, dbo.MV_LV_SUBSTATION.COMPANY

GO

CREATE VIEW [dbo].[COMPANY_POWER_SP] AS
SELECT	MV_LV_SUBSTATION.COMPANY,
				SUM(CONTRACTED_POWER) AS POWER
FROM		CUSTOMER
				JOIN MV_LV_SUBSTATION
					ON CUSTOMER.LV_NETWORK = MV_LV_SUBSTATION.CODE
GROUP BY MV_LV_SUBSTATION.COMPANY

GO

CREATE VIEW [dbo].[MUNICIPALITIES_SP] AS
SELECT	NameProv AS REGION, IDMun AS MUN, [TYPE], NameMun
FROM gdlCodeProv 
	JOIN gdlCodeMun
		ON gdlCodeProv.IDProv = gdlCodeMun.IDProv
	JOIN gdlMunType
		ON gdlCodeMun.IDProv = gdlMunType.PROVINCE AND gdlCodeMun.IDMun = gdlMunType.MUNI

GO

CREATE VIEW [dbo].[MV_LV_SUBSTATION_POWER]
AS
SELECT MV_LV_SUBSTATION.CODE,
		SUM(SN1) AS POWER
FROM MV_LV_SUBSTATION
	LEFT JOIN TRANSFORMER ON MV_LV_SUBSTATION.CODE = TRANSFORMER.PLACING_SITE
WHERE MV_LV_SUBSTATION.PROPIOS = 0
GROUP BY MV_LV_SUBSTATION.CODE
UNION
SELECT MV_LV_SUBSTATION.CODE,
		SUM(CONTRACTED_POWER) AS POWER
FROM MV_LV_SUBSTATION
	LEFT JOIN CUSTOMER ON MV_LV_SUBSTATION.CODE = CUSTOMER.LV_NETWORK
WHERE MV_LV_SUBSTATION.PROPIOS = 1
GROUP BY MV_LV_SUBSTATION.CODE

GO

INSERT INTO FaultNumber (ID) Values (1)

GO

INSERT INTO REGION SELECT ID, SUBSTRING(Name, 1, 40) FROM gdlMINECO
GO

delete from dbo.SectionNumber
insert into dbo.SectionNumber (ID,MV_Section_Id,LV_Section_Id) values (1,0,0)
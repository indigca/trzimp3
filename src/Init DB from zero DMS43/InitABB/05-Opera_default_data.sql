--Default data to Opera-database tables

INSERT INTO DBO.SMS_CODES (Code, Info)
SELECT 'Date', '%#d.%#m.%Y';

INSERT INTO DBO.SMS_CODES (Code, Info)
SELECT 'End_Note', 'Company';

INSERT INTO DBO.SMS_CODES (Code, Info)
SELECT 'Fault_text', 'Failure at distribution of Electricity at %s.';

INSERT INTO DBO.SMS_CODES (Code, Info)
SELECT 'Maint_out_text', 'Maintenance outage at %s starting: %s %s.';

INSERT INTO DBO.SMS_CODES (Code, Info)
SELECT 'No_time', 'Fault clearing has been started.';

INSERT INTO DBO.SMS_CODES (Code, Info)
SELECT 'Repaired', 'Distribution will be back %s about %s.';

INSERT INTO DBO.SMS_CODES (Code, Info)
SELECT 'Time', '%#H:%M';

INSERT INTO DBO.SMS_CODES (Code, Info)
SELECT 'Today', 'today';

INSERT INTO DBO.SMS_CODES (Code, Info)
SELECT 'Tomorrow', 'tomorrow';

GO

--Needed FlexData definitions to the UNIT table *****************
INSERT INTO UNIT 
(
	ID,
	DESCRIPTION,
	EDITABLE
)
VALUES 
(
	'0deb9257-08b5-4a9f-b424-8caa528ea2a8',
	'Call Data',	
	1
)
;

GO

INSERT INTO UNIT_TYPE 
(
	ID,
	UNIT_ID,
	DATA_TYPE,
	DESCRIPTION,
	EDITABLE,
	UI_ORDER
)
VALUES 
(
	'4be709ee-219f-4ac1-8605-c5e6344f9b8b',
	'0deb9257-08b5-4a9f-b424-8caa528ea2a8',
	0,
	'Call Class',
	1,
	1
)
;

GO

INSERT INTO [dbo].[UNIT]
(
	[ID]
	,[DESCRIPTION]
	,[EDITABLE]
)
VALUES
(
	'19207b1b-4bd0-46e1-a3df-3cc137e29a2a',
	'Work Group Free Data',
	1
);
GO

INSERT INTO [dbo].[UNIT]
(
	[ID]
	,[DESCRIPTION]
	,[EDITABLE]
)
VALUES
(
	'16c3eb8b-bca7-4cd9-a24c-701102336d55',
	'Persons Free Data',
	1
);
GO

INSERT INTO [dbo].[UNIT]
(
	[ID]
	,[DESCRIPTION]
	,[EDITABLE]
)
VALUES
(
	'6060f1c4-2e8f-4889-9e85-f6381608276b',
	'Phone Free Data',
	1
);
GO

INSERT INTO UNIT
(
	ID,
	DESCRIPTION,
	EDITABLE
)
VALUES
(
	'a970bc3c-5210-4483-9849-4fd0238294c0',
	'AREA',
	1
)
GO

INSERT INTO UNIT_TYPE
( 
	ID,
	UNIT_ID,
	DATA_TYPE,
	DESCRIPTION,
	EDITABLE
)
VALUES
(
	'41be706c-2f4c-4557-ba90-1651e800ea98',
	'a970bc3c-5210-4483-9849-4fd0238294c0',
	0,
	'AREA_TYPE',
	1
)
GO

INSERT INTO UNIT_TYPE_FEATURES
( 
	ID,
	UNIT_TYPE_ID,
	DESCRIPTION, 
	EDITABLE 
)
VALUES
(
	'55db70c0-2282-433f-be79-3b45a7973fa7',
	'41be706c-2f4c-4557-ba90-1651e800ea98',
    'MAINTENANCE_AREA',
    0
)
GO

INSERT INTO UNIT_TYPE_FEATURES
( 
	ID, 
	UNIT_TYPE_ID, 
	DESCRIPTION, 
	EDITABLE 
)
VALUES
(
	'2f6d9791-5c2f-4601-a6f9-928460ec181e',      
	'41be706c-2f4c-4557-ba90-1651e800ea98',
    'OUTAGE_INFO',
    0
)
GO

INSERT INTO UNIT_TYPE_FEATURES
( 
	ID, 
	UNIT_TYPE_ID, 
	DESCRIPTION, 
	EDITABLE 
)
VALUES
(
	'0fc43835-5230-441a-bd6e-dbcc446bd11c',       
	'41be706c-2f4c-4557-ba90-1651e800ea98',
    'DISTRIBUTION_AREA',
    0
)
GO

delete [dbo].[LVNetworkOutages]
delete [dbo].[LVNetworkOutages2013]
delete [dbo].[MVOutageReport]
delete [dbo].[MVOutageReport2013]
delete [dbo].[OutageAreas]
delete [dbo].[OutageAreas2013]
delete [dbo].[OUTAGEDATA]
delete [dbo].[gdlValidatedReports]
delete [dbo].[LVOutageReport]
delete [dbo].[LVOutageReport2013]
delete [dbo].[MaintenanceOutageReport2013]
delete [dbo].[MaintenanceOutageReport]
update [dbo].[FaultNumber] set FaultNumber=0,MaintenanceOutageNumber=0,
      LVOutageNumber=0,
      ReClosingNumber=0,
      OutageAreaNumber=0,
      ExactFaultLocationNumber=0,
      MVOutageNumber=0 where id = 1
      
update [dbo].[FaultArchive] set FaultNumber=1
      ,OutageZones=1
      ,Queries=1
      ,Locking=0 where NAME=2013 OR NAME = 2014

delete [dbo].[gdlValidatedReports]

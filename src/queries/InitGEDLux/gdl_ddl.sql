--------------------------------------------------
-- Create Table dbo.gdlCodeMun
--------------------------------------------------
Create table dbo.gdlCodeMun (
    IDProv                         NVARCHAR(2)                    not null,
    IDMun                          NVARCHAR(4)                    not null,
    NameMun                        NVARCHAR(50)                   not null,
    CONSTRAINT [PK__gdlCodeM__941AE34A3A379A64] PRIMARY KEY CLUSTERED 
(
	[IDProv] ASC,
	[IDMun] ASC
)) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlCodeProv
--------------------------------------------------
Create table dbo.gdlCodeProv (
    IDProv                         NVARCHAR(2)                    PRIMARY KEY,
    NameProv                       NVARCHAR(32)                   null) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlMINECO
--------------------------------------------------
Create table dbo.gdlMINECO (
    ID                             NVARCHAR(3)                    PRIMARY KEY,
    Name                           NVARCHAR(64)                   not null,
    Region                         NVARCHAR(32)                   not null,
    DMS                            NVARCHAR(1)                    null,
    City                           VARCHAR(32)                    null,
    Zone                           NVARCHAR(2)                    null) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlMunType
--------------------------------------------------
Create table dbo.gdlMunType (
    UTILITY                        NVARCHAR(3)                    PRIMARY KEY,
    MUNI                           NVARCHAR(4)                    not null,
    TYPE                           NVARCHAR(1)                    null,
    PROVINCE                       NVARCHAR(2)                    not null) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlReportCompany
--------------------------------------------------
Create table dbo.gdlReportCompany (
	NUMBERID					   int IDENTITY (1, 1)              PRIMARY KEY,
    USERID                         NVARCHAR(4000)                   not null,
    COMPANY                        NVARCHAR(3)                    not null) 
on [PRIMARY] ;


CREATE TABLE [dbo].[gdlMvCustLocation](
	[CUSTOMER_NODE] [nvarchar](32) PRIMARY KEY,
	[UTILITY] [nvarchar](3) NOT NULL,
	[MUNI] [nvarchar](4) NOT NULL,
	[PROVINCE] [nvarchar](2) NOT NULL
)

CREATE TABLE [dbo].[gdlCTConnections](
	[CT_NODECODE] [nvarchar](14) NOT NULL,
	[LI_IdInst] [nvarchar](50) NULL
)

CREATE TABLE [dbo].[gdlXYDMS](
	[CodigoAcometida] [nvarchar](50) NOT NULL,
	[XDMS] [int] NULL,
	[YDMS] [int] NULL
)	
CREATE TABLE [dbo].[gdlOPC](
	[MINECO] [varchar](3) NULL,
	[TYPE] [varchar](2) NULL,
	[OPC_CODE] [varchar](30) NOT NULL
)
CREATE TABLE [dbo].[gdlCT](
	[MINECO] [varchar](3) NULL,
	[DMS_Name] [varchar](50) NULL,
	[SCADA_Name] [varchar](50) NULL,
	[CT_Name] [varchar](50) NULL
) 


CREATE TABLE [dbo].[gdlExtension](
	[MINECO] [nchar](3) NULL,
	[X1] [int] NULL,
	[Y1] [int] NULL,
	[X2] [int] NULL,
	[Y2] [int] NULL
) ON [PRIMARY]

CREATE TABLE gdlLV_NODE_BTVano (
                        NODECODE nvarchar(14),
                        IdBTVano nvarchar(50)    )
                        
GO

CREATE TABLE [dbo].[gdlCeldas](
	[CodigoCT] [nchar](10) NULL,
	[CodigoLinea] [nchar](10) NULL,
	[X] [int] NULL,
	[Y] [int] NULL,
	[Nombre] [nvarchar](50) NULL,
	[IdInstLinea] [nvarchar](50) NULL,
	[IdTipoCelda] [int] NULL,
	[IdInst] [nvarchar](50) NULL,
	[IdTransformador] [nvarchar](50) NULL,
	[DisyuntorFusible] [int] NULL,
	[Modelo] [int] NULL,
	[Transformador] [int] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[gdlLVConductor](
	[CodeTraza] [nchar](32) NULL,
	[CodeDMS] [nchar](32) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[gdlOrdenCeldas](
	[CodigoCT] [nchar](10) NOT NULL,
	[Posicion] [int] NOT NULL,
	[CodigoLinea] [nchar](10) NULL,
	[Transformador] [int] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[gdlTrazaImportUser](
	[usuario] [nchar](7) NOT NULL,
	[password] [nchar](32) NOT NULL
) ON [PRIMARY]

GO

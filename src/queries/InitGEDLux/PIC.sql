CREATE TABLE [dbo].[SCADA_CalidadIndividual](
	[SCCI_IdCalidadIndividual] [int] IDENTITY(1,1) NOT NULL,
	[SCCI_CUPS] [varchar](32) NOT NULL,
	[SCCI_MinEco] [varchar](3) NOT NULL,
	[SCCI_IdInterrupcion] [uniqueidentifier] NULL,
	[SCCI_FechaInterrupcion] [smalldatetime] NULL,
	[SCCI_Duracion] [int] NULL,
	[SCCI_Origen] [char](1) NULL,
	[SCCI_Zona] [int] NULL,
	[SCCI_Provincia] [varchar](2) NULL,
	[SCCI_Municipio] [varchar](3) NULL,
	[SCCI_Tipo] [int] NULL,
 CONSTRAINT [PK_SCADA_TB] PRIMARY KEY CLUSTERED 
(
	[SCCI_IdCalidadIndividual] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Historico](
	[SCCI_IdCalidadIndividual] [int] IDENTITY(1,1) NOT NULL,
	[SCCI_CUPS] [varchar](32) NOT NULL,
	[SCCI_MinEco] [varchar](3) NOT NULL,
	[SCCI_IdInterrupcion] [uniqueidentifier] NULL,
	[SCCI_FechaInterrupcion] [smalldatetime] NULL,
	[SCCI_Duracion] [int] NULL,
	[SCCI_Origen] [char](1) NULL,
	[SCCI_Zona] [int] NULL,
	[SCCI_Provincia] [varchar](2) NULL,
	[SCCI_Municipio] [varchar](3) NULL,
	[SCCI_Tipo] [int] NULL,
	[SCCI_IdModificada] [uniqueidentifier] NULL,
 CONSTRAINT [PK_SCADA_Historico] PRIMARY KEY CLUSTERED 
(
	[SCCI_IdCalidadIndividual] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[PIC_Calidadindividual](
	[SCCI_IdCalidadIndividual] [int] IDENTITY(1,1) NOT NULL,
	[SCCI_CUPS] [varchar](22) NOT NULL,
	[SCCI_MinEco] [varchar](3) NOT NULL,
	[SCCI_IdInterrupcion] [uniqueidentifier] NULL,
	[SCCI_FechaInterrupcion] [smalldatetime] NULL,
	[SCCI_Duracion] [int] NULL,
	[SCCI_Origen] [char](1) NULL,
	[SCCI_Zona] [int] NULL,
	[SCCI_Provincia] [varchar](2) NULL,
	[SCCI_Municipio] [varchar](3) NULL,
	[SCCI_Tipo] [int] NULL,
	[SCCI_IdModificada] [uniqueidentifier] NULL,
 CONSTRAINT [PK_SCADA_PIC_Calidad_Individual] PRIMARY KEY CLUSTERED 
(
	[SCCI_IdCalidadIndividual] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
'''
Created on May 10, 2011

@author: Administrator
'''

import utiDMS
import sqlDMS
import utiRiku
from db import DataBase

def init1(mineco):
    cnxnW=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable,DataBase.User,DataBase.Password)
    
    cursorW=cnxnW.cursor()   
    
    #Get socio details
    
    name=sqlDMS.getSocioName(cursorW,mineco)
    city=sqlDMS.getSocioCity(cursorW,mineco)
    dmsType=sqlDMS.getSocioDMS(cursorW,mineco)
    zone=sqlDMS.getSocioZone(cursorW,mineco)
    if zone is None:
        zone=utiDMS.getZone(city+',Spain')
        print zone
        sqlDMS.setSocioZone(cursorW,mineco,zone)
    cnxnW.commit()
    
    
    cnxnT=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable, DataBase.TRAZAUser, DataBase.TRAZAPassword)
    cursorT=cnxnT.cursor()  
     

    return cnxnW,cnxnT


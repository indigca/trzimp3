# -*- coding: iso-8859-1 -*-
'''
Created on May 8, 2011

@author: Administrator
'''
import utiHTML
import initV
import sqlDMS
import markup

def fileLevel1():
    reportList=utiHTML.refreshCIDEList ('htdocs/')
     
    
    cnxnW,cnxnT=initV.init1("045")
    cursor=cnxnW.cursor()
    names=[]
    for i in range(0,len(reportList)):
        dmy=sqlDMS.getSocioName(cursor,reportList[i][0:3])
        names.append(dmy)
    
    text=utiHTML.createButtonsL1(reportList, names)
    #print text
    
    f=open('./HTML/tmp.html','wt')
    print >> f,text
    f.close()
    
    f1=open('./HTML/header-L1.html','rt')
    f2=open('./HTML/tmp.html','rt')
    f3=open('./HTML/footer-bgcontent.html','rt')
    
    t1=f1.read()
    t2=f2.read()
    t3=f3.read()
    
    f4=open('htdocs/index.html','wt')
    
    print >> f4,t1+t2+t3
    
    f1.close()
    f2.close()
    f3.close()
    f4.close()
    
def fileLevel2():
    CIDEList=utiHTML.refreshCIDEList ('htdocs/')
    
    for subdir in CIDEList:
        reportList=utiHTML.refreshCIDEReports ('htdocs/'+subdir)
        text=utiHTML.createButtonsL2(reportList)
            
        f=open('./HTML/tmp.html','wt')
        print >> f,text
        f.close()
        
        f1=open('./HTML/header-L2.html','rt')
        f2=open('./HTML/tmp.html','rt')
        f3=open('./HTML/footer-bgcontent.html','rt')
        
        t1=f1.read()
        t2=f2.read()
        t3=f3.read()
        
        f4=open('htdocs/'+subdir+'/index.html','wt')
        
        print >> f4,t1
        page = markup.page( )
        page.h1("Distribuidora "+ subdir)
        print >> f4,page
        print >>f4,t2+t3
        
        f1.close()
        f2.close()
        f3.close()
        f4.close()


def fileLevel3(MINECO):

        reportList=utiHTML.refreshCIDEReports ('c:/buzon/'+MINECO+'/chequeo')
        text=utiHTML.createButtonsL2(reportList)
            
        f=open('./HTML/tmp.html','wt')
        print >> f,text
        f.close()
        
        f1=open('./HTML/header-L2.html','rt')
        f2=open('./HTML/tmp.html','rt')
        f3=open('./HTML/footer-bgcontent.html','rt')
        
        t1=f1.read()
        t2=f2.read()
        t3=f3.read()
        
        try:
            f4=open('c:/buzon/'+MINECO+'/chequeo/index.html','wt')
            print >> f4,t1
            page = markup.page( )
            page.h1("Distribuidora "+ MINECO)
            print >> f4,page
            print >>f4,t2+t3
            f4.close()
        except IOError:
            print IOError.message
        

        
        
        f1.close()
        f2.close()
        f3.close()
        

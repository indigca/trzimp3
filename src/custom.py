'''
Created on Agu 07, 2013

@author: Marco Guida
'''

import wx
import custom100
import customGedLux
import customCIDE

distribuidorasCIDE=['022','036','039','044','045','046','047','054','061','096','121','136','168','177','199','210','214','262','309','999']

def customPopulate(MINECO):
    if MINECO=='100':
        #wx.MessageBox(custom100.getPopulate(), 'Populate', wx.OK | wx.ICON_INFORMATION) 
        print '100'
        return custom100.getPopulate()
    elif str(MINECO) in distribuidorasCIDE:
        print 'CIDE'
        return customCIDE.getPopulate(MINECO)
    else:
        print 'GedLux'
        return customGedLux.getPopulate(MINECO)
        #wx.MessageBox('Distribuidora desconocida', 'Populate', wx.OK | wx.ICON_INFORMATION)
    
def customExcel(MINECO):
    if MINECO=='100':
        print '100'
        return custom100.getExcel()
    elif str(MINECO) in distribuidorasCIDE:
        print 'CIDE'
        return customCIDE.getExcel(MINECO)
    else:
        print 'GedLux'
        return customGedLux.getExcel(MINECO)
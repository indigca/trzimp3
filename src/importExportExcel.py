# -*- coding: iso-8859-15 -*-
import wx
import os
import pyodbc
from xlrd import open_workbook
from tempfile import TemporaryFile
import xlwt
import utiDMS
from db import DataBase
import TrazaImport
from TrazaImport import *
import sys
import CT_line_export
import datetime

fileName='/orden_celdas.xls'
pathXLS=''

class ButtonFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, 'Orden Celdas', 
                size=(300, 225))
        panel = wx.Panel(self, -1)
        separacionBotones = 23
        self.button = wx.Button(panel, -1, "Importar datos de hoja de c�lculo", pos=(50, separacionBotones*3))
        #self.button2 = wx.Button(panel, -1, "Crear hoja de c�lculo", pos=(50, separacionBotones*2))
        self.Bind(wx.EVT_BUTTON, self.ReadExcel, self.button)
        #self.Bind(wx.EVT_BUTTON, self.WriteExcel, self.button2)
        self.button3 = wx.Button(panel, -1, "Traza -> XLS", pos=(50, separacionBotones*1))
        self.Bind(wx.EVT_BUTTON, self.TrzImport, self.button3)
        
        self.btnGeneraITMOPC = wx.Button(panel, -1, "Genera OPC e ITM", pos=(50, separacionBotones*2))
        self.Bind(wx.EVT_BUTTON, self.generaOPCITM, self.btnGeneraITMOPC)
#        self.button4 = wx.Button(panel, -1, "2. Abrir hoja de c�lculo", pos=(50, separacionBotones*3))
#        self.Bind(wx.EVT_BUTTON, self.OpenExcel, self.button4)
        #self.button.SetDefault()
        #self.button2.SetDefault()
        #self.button3.SetDefault()
#        self.button4.SetDefault()
        
    def generaOPCITM(self, event):
        
        dlg = wx.DirDialog(self, "Selecciona un directorio:")
        if dlg.ShowModal() == wx.ID_OK:
            print "You chose %s" % dlg.GetPath()
        dlg.Destroy()
        
        try:
            book = open_workbook(dlg.GetPath()+'\orden_celdas.xls')
        except:
            dlg = wx.MessageBox('No existe archivo o el formato no es correcto', 'Error', 
                                wx.OK | wx.ICON_INFORMATION)
            return
        
        sheet = book.sheet_by_index(0)
        opc=open(dlg.GetPath()+'\Opera.opc','w')
        itm=open(dlg.GetPath()+'\Opera.itm','w')
        itm.write('[POSITIONITEMS]\n')
        numLineaArchivoITM=1
        numLineaArchivoOPC=1
        numItemOpc=1
        esTrafo=False
        anteriorCT=''
        try:
            conn=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
            cursor=conn.cursor()
            cursor.execute("delete from SwitchingComponent where Switch like '"+str(self.MINECO)+"%'")
            conn.commit()
        except:
            wx.MessageBox('No se ha podido resetear la tabla de enlace OPC', 'Error',  wx.OK | wx.ICON_ERROR)
        
        for row_index in range(sheet.nrows):
            if (sheet.cell(row_index,7).value=='Si' or sheet.cell(row_index,7).value=='SI' or sheet.cell(row_index,7).value=='si') and sheet.cell(row_index,6).value==0:
                now=datetime.datetime.now()
                print  str(now)
                
                print 'row_index '+str(row_index)
                Codigo3Letras=sheet.cell(row_index,2).value
                Posicion=sheet.cell(row_index,3).value
                
                try:
                    #conn=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
                    cursor=conn.cursor()
                    
                    print  sheet.cell(row_index,0).value.replace(' ','')+"1"
                    print  str(int(Posicion))
                    
                    if int(Posicion) ==1:
                        cursor.execute("SELECT CODE FROM DISCONNECTOR where MV_SITE = '"+sheet.cell(row_index,0).value.replace(' ','')+"1'")
                    else:
                        cursor.execute("SELECT CODE FROM DISCONNECTOR where MV_SITE = '"+sheet.cell(row_index,0).value.replace(' ','')+"1' and CODE like '%"+str(int(Posicion))+"'")
                    
                    result = cursor.fetchone()
                    conn.commit()
                    
                    for switch in result:
                        print switch
                        print str(now)[:-3]
                        print str('\\\\APL\\1\\P\\'+str(self.MINECO)+'A'+str(Codigo3Letras)+'_P'+str(int(Posicion))+'INT\\10')
                        cursor.execute("insert into SwitchingComponent (Switch,State,Changed,OPCCode) values  ('"+switch+"','1','"+str(now)[:-3]+"','"+str('\\\\APL\\1\\P\\'+str(self.MINECO)+'A'+str(Codigo3Letras)+'_P'+str(int(Posicion))+'INT\\10')+"')")
                        conn.commit()
                except:
                    wx.MessageBox('No se ha podido crear el enlace OPC �La distribuidora est� importada?', 'Error',  wx.OK | wx.ICON_ERROR)
                    return

               

                itm.write(str(numLineaArchivoITM)+'=\\\\APL\\1\\P\\'+str(self.MINECO)+'A'+str(Codigo3Letras)+'_P'+str(int(Posicion))+'INT\\10'+'\n')
                numLineaArchivoITM+=1
                
                if anteriorCT!=sheet.cell(row_index,0).value:
                    anteriorCT=sheet.cell(row_index,0).value
                    numLineaArchivoOPC=1
                    opc.write('[POSITIONINDICATION_'+str(numItemOpc)+']\n')
                    numItemOpc+=1
                    opc.write('Name='+Codigo3Letras+'_Pos\n')
                    opc.write('UpdateRate=0\n')
                    opc.write('Alarms=1\n')
                #print 'Leyendo xls: '+str(CodigoCT)
                opc.write(str(numLineaArchivoOPC)+'=\\\\APL\\1\\P\\'+str(self.MINECO)+'A'+str(Codigo3Letras)+'_P'+str(int(Posicion))+'INT\\10'+'\n')
                numLineaArchivoOPC+=1

        opc.close()  
        itm.close() 
        
        
    def TrzImport(self, event):
        CT_line_export.imp(self.MINECO)
        self.WriteExcel(event)
        dlg = wx.MessageBox('Importaci�n Finalizada', 'Finalizado', 
                                                wx.OK | wx.ICON_INFORMATION)
    
    def OpenExcel(self, event):
        pass
    
    def ReadExcel(self, event):
        dlg = wx.MessageBox('Es necesario borrar la distribuidora y volver a importar', 'Aviso', wx.YES_NO | wx.ICON_WARNING)
        wx.MessageBox('NO BORRA TODAVIA', 'DEVELOP', wx.YES_NO | wx.ICON_WARNING)
        print dlg
#        dialog=wx.FileDialog(None,"Choose a File",os.getcwd(),"","*.*",wx.OPEN)
#        if dialog.ShowModal() == wx.ID_OK:
#            self.filePath= dialog.GetPath()
#        dialog.Destroy()
        
        if dlg==wx.YES:

            #TrazaImport.OnDeleteDistributor(self,event)
            conn=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
            cursor=conn.cursor()
    #        cursor.execute("drop table gdlOrdenCeldas")
    #        cursor.execute("create table gdlOrdenCeldas (CodigoCT NVARCHAR(32), NombreCT NVARCHAR(32), Posicion NUMERIC, CodigoLinea TEXT, NombreLinea TEXT, Transformador NUMERIC)")
            try:
                book = open_workbook(self.path)
            except:
                dlg = wx.MessageBox('No existe archivo o el formato no es correcto', 'Error', 
                                    wx.OK | wx.ICON_INFORMATION)
                return
            
            sheet = book.sheet_by_index(0)
            sentencia = "DELETE FROM gdlOrdenCeldas WHERE CodigoCT LIKE '"+self.MINECO+"%';"
            sentencia2 = "DELETE FROM gdlSCADACT WHERE CTCodeDMS LIKE '"+self.MINECO+"%';"
            cursor.execute(sentencia)
            conn.commit()
            cursor.execute(sentencia2)
            conn.commit()
            i = 0
            for row_index in range(sheet.nrows):
                i = i + 1
                if i != 1:
                    CodigoCT=sheet.cell(row_index,0).value
                    NombreCT=sheet.cell(row_index,1).value
                    Posicion=int(float(sheet.cell(row_index,3).value))
                    CodigoLinea=sheet.cell(row_index,4).value
                    NombreLinea=sheet.cell(row_index,5).value
                    if sheet.cell(row_index,6).value != "":
                        Transformador=int(float(sheet.cell(row_index,6).value)) 
                    else:
                        Transformador = None
                    if CodigoCT[0:3] == self.MINECO:
                        cursor.execute("insert into gdlOrdenCeldas (CodigoCT,Posicion,CodigoLinea,Transformador)\
                                    values (?,?,?,?)", \
                                    CodigoCT,Posicion,CodigoLinea,Transformador)
            conn.commit()
            dlg = wx.MessageBox('Orden de celdas actualizado', 'Finalizado', 
                                                    wx.OK | wx.ICON_INFORMATION)
        
    def WriteExcel(self, event):
        conn=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursor=conn.cursor()
        cnxnT=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable,DataBase.TRAZAUser,DataBase.TRAZAPassword)
        cursorT = cnxnT.cursor()
        sentencia = "SELECT * FROM gdlOrdenCeldas WHERE CodigoCT LIKE '"+self.MINECO+"%'  order by CodigoCT ;" 
        sentencia2 = "delete FROM gdlSCADACT where CTCodeDMS like '"+self.MINECO+"%';"
        try:            
            cursor.execute(sentencia)
            result = cursor.fetchall()
            conn.commit()
            cursor.execute(sentencia2)
            conn.commit()
        except:
            print "Table not Found!"
            wx.MessageBox('Error en la estructura de la Base de Datos, falta la tabla gdlSCADACT', 'Error', wx.OK | wx.ICON_ERROR)
            sys.exit()


        style1 = xlwt.easyxf('pattern: pattern solid, pattern_fore_colour light_orange')
        style2 = xlwt.easyxf('pattern: pattern solid, pattern_fore_colour light_yellow')
        style3 = xlwt.easyxf('pattern: pattern solid, pattern_fore_colour red')
        
        book = xlwt.Workbook(encoding='iso-8859-15')
        #book = xlwt.Workbook(encoding="utf-8")
        sheet1 = book.add_sheet('Sheet 1')
        sheet1.write(0,0,'CodigoCT')
        sheet1.write(0,1,'NombreCT')
        sheet1.write(0,2,'Codigo 3 letras')
        sheet1.write(0,3,'Posicion')
        sheet1.write(0,4,'CodigoLinea')
        sheet1.write(0,5,'NombreLinea')
        sheet1.write(0,6,'Transformador')
        sheet1.write(0,7,'Telemandado Si/No')
        sheet1.write(0,9,'Distribuidora:')
        sheet1.write(0,10,self.MINECO)
        sheet1.insert_bitmap('logo_gedlux.bmp', 2, 9)
        
        
        cnxnT=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable,DataBase.TRAZAUser,DataBase.TRAZAPassword)
        cursorT = cnxnT.cursor()
        cont=1
        codigoCTAnterior=''
        fallothreeCharCode=False
        ultimothreeCharCode=''
        estilo=False
        style=style1
        for i in result:
            print str(codigoCTAnterior)
            print str(i.CodigoCT)
            if codigoCTAnterior!=i.CodigoCT:
                if estilo:
                    style=style2
                else:
                    style=style1
                estilo=not estilo
                    
            print i.CodigoCT.rstrip()
            if i.CodigoCT.rstrip() != "":
                sentencia = "SELECT Nombre FROM Instalaciones WHERE Codigo like '%s'" % i.CodigoCT.rstrip()
                cursorT.execute(sentencia)
                print sentencia
                NombreCTlist = cursorT.fetchall()
                if len(NombreCTlist) != 0:
                    NombreCT = NombreCTlist[0][0]
                else:                
                    i.CodigoCT = i.CodigoCT[4:]
                    sentencia = "SELECT Nombre FROM Instalaciones WHERE Codigo like '%s' AND IdInst LIKE '%s'" % (i.CodigoCT.rstrip(),self.MINECO + '%')
                    
                    cursorT.execute(sentencia)
                    NombreCTlist = cursorT.fetchall()
                    if NombreCTlist != 0:
                        NombreCT = NombreCTlist[0][0]
                    else:
                        NombreCT = ""
            else:
                NombreCT = ""
            print NombreCT
            if i.CodigoLinea.rstrip() != "":
                sentencia = "SELECT Nombre FROM Instalaciones WHERE Codigo = '%s'" % i.CodigoLinea.rstrip()
                cursorT.execute(sentencia)
                NombreLinea = cursorT.fetchall()[0][0]
            else:
                NombreLinea = ""
            if i.Transformador == None:
                i.Transformador = ""
            
            
            
            sheet1.write(cont,0,i.CodigoCT,style)            
            sheet1.write(cont,1,NombreCT,style)
            threeCharCode=CT3LetrasGDL(NombreCT)
            
            
            if threeCharCode.__len__()==3:
                
                if i.CodigoCT.startswith(str(self.MINECO)):
                    codigoCT=str(i.CodigoCT)
                else:
                    codigoCT=str(self.MINECO+'-'+i.CodigoCT)
                    
                    if codigoCT=='022-ESI184':
                        print 'ahora'
                    
                if codigoCTAnterior!=codigoCT:
                    
                    contadorCodigo3Letras=1
                    try:
                        cursor.execute("SELECT * FROM gdlSCADACT where TresLetrasScada = '"+threeCharCode+"'")
                        result = cursor.fetchall()
                        conn.commit()
                        if len(result) == 0:
                            cursor.execute("insert into gdlSCADACT (CTCodeDMS,TresLetrasSCADA) values (?,?)",codigoCT,str(threeCharCode))
                            conn.commit()
                            fallothreeCharCode=False
                            ultimothreeCharCode=threeCharCode
                        else:
                            fallothreeCharCode=True
                            
                           #wx.MessageBox('El codigo '+threeCharCode+' ya existe en la base de datos para la distribuidora '+str(self.MINECO)+' sustituir', 'Error', wx.OK | wx.ICON_INFORMATION)
                            while fallothreeCharCode:
                                if contadorCodigo3Letras>=10:
                                     wx.MessageBox('Fallo range codigo 3 caracteres, este tipo de error hay que resolverlo manualmente', 'Error', wx.OK | wx.ICON_INFORMATION)
                                     return
                                threeCharCode=threeCharCode[:-1]+str(contadorCodigo3Letras)
                                contadorCodigo3Letras+=1
                               
                                cursor.execute("SELECT * FROM gdlSCADACT where TresLetrasScada = '"+threeCharCode+"'")
                                result = cursor.fetchall()
                                conn.commit()
                                if len(result) == 0:
                                    fallothreeCharCode=False
                                    cursor.execute("insert into gdlSCADACT (CTCodeDMS,TresLetrasSCADA) values (?,?)",codigoCT,str(threeCharCode))
                                    conn.commit()
                                    ultimothreeCharCode=threeCharCode
                                else:
                                    fallothreeCharCode=True
                                
                            #threeCharCode='###'
#                            cursor.execute("insert into gdlSCADACT (CTCodeDMS,TresLetrasSCADA) values (?,?)",codigoCT,str(threeCharCode))
#                            conn.commit()
                            
                        
                        codigoCTAnterior=codigoCT
                    except pyodbc.IntegrityError:
                        print 'duplicidad'
                        wx.MessageBox('El codigo '+threeCharCode+' ya existe en la base de datos para la distribuidora '+str(self.MINECO), 'Error', wx.OK | wx.ICON_INFORMATION)
                
            else:
                wx.MessageBox('El codigo de 3 letras de '+i.CodigoCT+' no es correcto y no se importar� ('+threeCharCode+')', 'Error', wx.OK | wx.ICON_ERROR)
                fallothreeCharCode=True
                threeCharCode='###'
                try:
                    cursor.execute("insert into gdlSCADACT (CTCodeDMS,TresLetrasSCADA) values (?,?)",codigoCT,str(threeCharCode))
                except:
                    print 'la primary key no se puede repetir'
                conn.commit()
            #print 'DEVUELVO '+str(CT3LetrasGDL(NombreCT))

            if not fallothreeCharCode:
                sheet1.write(cont,2,ultimothreeCharCode,style)
            else:
                sheet1.write(cont,2,'###',style3)
                
            if i.Posicion < 10:
                sheet1.write(cont,3,'0'+str(i.Posicion),style)
                #print 'posicion '+str(int('0')+i.Posicion)
            else:       
                sheet1.write(cont,3,str(i.Posicion),style)
                #print 'posicion '+str(i.Posicion)
            
            sheet1.write(cont,4,i.CodigoLinea,style)
            sheet1.write(cont,5,NombreLinea,style)
            sheet1.write(cont,6,i.Transformador,style)
            if i.Transformador==1:
                sheet1.write(cont,7,'No se aplica',style)
            else:
                sheet1.write(cont,7,'No',style)
            cont=cont+1
            
        try:
            global pathXLS
            print 'self.path: '+self.path
            
            if not os.path.isdir(pathXLS):
               os.mkdir(pathXLS)
        except IOError:    
            print 'No ha sido posible crear la ruta destino'
        
        
        #book.save(self.path+ "/orden_celdas.xls")
        print 'ButtonFrame.path: '+str(ButtonFrame.path)
        print 'pathXLS: '+str(pathXLS)
        print str(self.path)
        book.save(self.path)
        book.save(TemporaryFile())
        dlg = wx.MessageBox('Archivo creado', 'Finalizado', wx.OK | wx.ICON_INFORMATION)
        
        if DataBase.DevelopmentMode == 'YES' :
            os.system("start \"C:\Program Files (x86)\OpenOffice 4\program\swriter.exe\" "+self.path)
        
#if __name__ == '__main__':
#    MINECO = '061'

def CT3LetrasGDL(NombreCT):
#    print 'CT3LetrasGDL '+str(NombreCT)
    #print 'index inicio: '+str(NombreCT.index("("))
    #print 'index fin: '+str(NombreCT.index(")"))
    try:
        inicioIarentesis=NombreCT.index("(")
        finParentesis=NombreCT.index(")")
        print 'Hay parentesis'
        print NombreCT[inicioIarentesis:finParentesis+2]
        NombreCT=NombreCT.replace(str(NombreCT[inicioIarentesis:finParentesis+2]),'')
    except:
        print 'No hay parentesis'
           
    print NombreCT
    NombreCT=NombreCT.replace('�','N')
    NombreCT=NombreCT.replace('�','N')
    NombreCT=NombreCT.replace('-',' ')
    NombreCT=NombreCT.replace('_',' ')
    NombreCT=NombreCT.replace(',',' ')
    NombreCT=NombreCT.replace('.','')
    nombreCTSplit=str(NombreCT).split(' ')
    nombreCTSplitDepurado=[]
    codigo3Letras=''
    consonantes='BCDFGHJKLMNPQRSTVWXYZ'
    vocales='AEIOU'
    numeros='0123456789'
    print '********************'
    
    repetirControl=True
    
    while repetirControl:
        repetirControl=False
        for palabra in nombreCTSplit:
            print str(palabra.__len__())
            if palabra.__len__() <3 or palabra.startswith("CT") or palabra=='ISF':
                nombreCTSplit.remove(palabra)
                repetirControl=True
    
    for palabra in nombreCTSplit:
       #palabra=palabra.strip('.')
       contieneNumero=False
       for letra in palabra:
           if letra in numeros:
               contieneNumero=True
               break
       
       #insertar aqui el archivo con el listado de las palabras a ignorar
       if not "(" in palabra and not ")" in palabra and not contieneNumero and palabra!= "III" and palabra!='LOS' and palabra!='SAN':
            if palabra.__len__()==3 and nombreCTSplit.__len__()==1:
                return palabra
            elif palabra.__len__()>3:
                nombreCTSplitDepurado.append(palabra)
            
        
    print nombreCTSplitDepurado
    
#    print 'primera condicion '+str(nombreCTSplitDepurado.__len__()==1)
#    print 'segunda condicion '+str(nombreCTSplitDepurado[0].__len__()==3)
#    if nombreCTSplitDepurado.__len__()==1 and nombreCTSplitDepurado[0].__len__()==3:
#            return nombreCTSplitDepurado[0].upper()
    
    for nameOriginal in nombreCTSplitDepurado:
        name=nameOriginal.upper()
        print name
        posicionLetra=0
        ultimaLetraCapturada=-1
#        if not name.__len__()==3:
        for letra in name:
            if letra !=' ':
                posicionLetra+=1
                #print 'posicionLetra '+str(posicionLetra)
                if letra in consonantes:
                    codigo3Letras+=letra
                    ultimaLetraCapturada=posicionLetra
                    #print 'codigo3Letras '+codigo3Letras
                    if codigo3Letras.__len__()==2 and nombreCTSplitDepurado.__len__()>1 and (nombreCTSplitDepurado.index(nameOriginal)+1!=nombreCTSplitDepurado.__len__()):
#                        print 'COMPUESTO'
#                        print 'ultimo '+nombreCTSplitDepurado[-1]
#                        print 'primera letra '+(nombreCTSplitDepurado[-1])[:1]
                        codigo3Letras+=(nombreCTSplitDepurado[-1])[:1]
                    
                    if codigo3Letras.__len__()==3:
                            return codigo3Letras
                    #print nombreCTSplitDepurado.index(name)
                    print str(posicionLetra)
                    print str(name.__len__())
                    print nameOriginal
                    if nombreCTSplitDepurado.index(nameOriginal)+1<nombreCTSplitDepurado.__len__() and posicionLetra==name.__len__():
                        codigo3Letras=''
        
        if codigo3Letras!='':
            print '--> '+str(name.__len__())+'---'+str(ultimaLetraCapturada)
            print name
            while name.__len__()>ultimaLetraCapturada:
                print 'HAY MAS LETRAS'
                codigo3Letras+=name[ultimaLetraCapturada]
                if codigo3Letras.__len__()==3:
                            return codigo3Letras
                ultimaLetraCapturada+=1
        
            for letra in name:
                if letra in vocales:
                    codigo3Letras=str(letra)+codigo3Letras
                    if codigo3Letras.__len__()==3:
                         return codigo3Letras
    return 'MANUAL'        


            


def openForm(MINECO):
    global fileName,pathXLS
    pathXLS='C:/buzon/'+MINECO[0:3]+'/orden_celdas'

    ButtonFrame.MINECO = MINECO[0:3]
    #/orden_celdas.xls'
    #ButtonFrame.path = 'C:/buzon/'+MINECO[0:3]+'/orden_celdas'
    print 'C:/buzon/'+MINECO[0:3]+'/orden_celdas/orden_celdas.xls'
    ButtonFrame.path = 'C:/buzon/'+MINECO[0:3]+'/orden_celdas'+fileName
    app = wx.PySimpleApp()
    frame = ButtonFrame()
    frame.Show()
    app.MainLoop()

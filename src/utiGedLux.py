'''
Created on 08/08/2013
@author: Marco Guida
'''
from db import DataBase
import utiDMS
import logging
log = logging.getLogger('utiGedLux')

def getSUBDInterna():
    return sentenciaSelectBDInterna("SELECT ID FROM internaMINECO where isSU='1'")

def selectTrazaDms(sentencia,BD):
   TRAZA='T'
   DMS='D'
   INTERNA='I'
   
   try:
     print str(BD)
     print str(DMS)
     print str(BD==DMS)
     if BD==DMS:
         cnxn=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
     elif BD==TRAZA:
         cnxn=utiDMS.connectSQL(DataBase.TRAZAAddress, DataBase.TRAZATable,DataBase.TRAZAUser,DataBase.TRAZAPassword)
     elif BD==INTERNA:
         cnxn=utiDMS.connectSQL("192.168.1.15\DMSEXPRESS", "DBINTERNAMG", "sa", "D11382r")
     else:
         raise 
     
     cursor = cnxn.cursor()
     cursor.execute(sentencia)
     if "select" in sentencia.lower():
     #if sentencia.lower().startswith('select'):
         rows=cursor.fetchall()
         return rows
     else:
         cnxn.commit()
         return 'commit'
     
   except:
     log.info("Error en selectDMS")
     return None

def sentenciaDeleteDms(sentencia):
     return selectTrazaDms(sentencia,'D')
     
def sentenciaInsertDms(sentencia):
     return selectTrazaDms(sentencia,'D')
     
def sentenciaDeleteBDInterna(sentencia):
     return selectTrazaDms(sentencia,'I')
     
def sentenciaInsertBDInterna(sentencia):
     return selectTrazaDms(sentencia,'I')
    
def sentenciaSelectBDInterna(sentencia):
     return selectTrazaDms(sentencia,'I')
import urllib2
import math

import os
from pyproj import Proj
from pyproj import transform
from googlemaps import GoogleMaps #http://py-googlemaps.sourceforge.net/#geocoding
import utiRiku
import Image
import wx
import ConfigParser
from pyproj import Proj
from db import DataBase
from pygeocoder import Geocoder

#Defining Database
#class DataBase:
#    config = ConfigParser.RawConfigParser()
#    config.read('config.ini')
#    Address = config.get('DATABASE','Address')
#    User = config.get('DATABASE','User')
#    Password = config.get('DATABASE','Password')
#    DMSTable = config.get('DATABASE','DMSTable')
#    TRAZATable = config.get('DATABASE','TRAZATable')
#    DMSLocation = config.get('DMS','DMSLocation')

def getXY_Zone(address): #address=("Coruna,Spain")
    #Migrated to API v3
    #gmaps = GoogleMaps("NotNeeded")   
    #lat, lon = gmaps.address_to_latlng(address)
    results = Geocoder.geocode(address)
    lat,lon = results[0].coordinates
    '''WGS84'
    zone=30
    if lon <=  -6:
        zone=29
    elif lon >= 0:
        zone=31    
    p89=Proj(proj="utm",zone=zone,ellps="GRS80",units="m",towgs84="0,0,0")
    x89,y89=p89(lon,lat)
    'print x89,y89,zone'''
    return getXY_Zone_from_latlon(lat, lon) #lat,lon,x89,y89,zone

def getXY_Zone_from_latlon(lat, lon):
    crd = utiRiku.projection(lat, lon)
    x89 = crd[0]
    y89 = crd[1]
    return lat, lon, x89, y89

def getMaps (maxCoord, minCoord, slicesX, slicesY, codeD, fileLocation, cnxnD, level, zona, proxyURL=''):
    '##########################################################################'
    '# It gets area map from idee.es and stores it on harddisk                #'
    '#   minCoord,maxCoord : Limits for the requested picture                 #'
    '#   slices: Grid      (3x3),(6x6),etc                                    #'
    '#   code: MINECO three digits                                            #'
    '#   nameDSN: Used ODBC DSN                                               #'
    '#   fileLocation: Where the file is stored                               #'
    '#   proxyURL: Http proxy (optional)                                      #'
    '#   cnxnD: Connection to SQL                                             #'
    '##########################################################################'
    'Connect to DMS'
    cursorD = cnxnD.cursor()
    diff = (maxCoord[0] - minCoord[0], maxCoord[1] - minCoord[1])
    width = 1000
    slicesTotal = (slicesX) * (slicesY)
    
    # Progress Dialog
    PrgDialog = wx.ProgressDialog("Nivel " + `level`, "Renderizando...", maximum=1000)
    
    for x in range (0, slicesX):
        minX = math.trunc(minCoord[0] + x * diff[0] / slicesX / 1.0)
        maxX = math.trunc(minCoord[0] + (x + 1) * diff[0] / slicesX / 1.0)
        for y in range (0, slicesY):
            minY = math.trunc(minCoord[1] + y * diff[1] / slicesY / 1.0)
            maxY = math.trunc(minCoord[1] + (y + 1) * diff[1] / slicesY / 1.0)

            #print 'Getting ', minX, minY, maxX, maxY

            picture_page = "http://www.idee.es/wms/PNOA/PNOA?REQUEST=GetMap&VERSION=1.1.1&SERVICE=WMS&SRS=EPSG:25830&BBOX=" + `minX` + "," + `minY` + "," + `maxX` + "," + `maxY` + "&WIDTH=" + `width` + "&HEIGHT=" + `width` + "&LAYERS=pnoa&STYLES=default&FORMAT=image/jpeg"                    
            #print picture_page
            #filename = str(codeD) + "_" + zona + "x" + `x` + "y" + `y` + ".jpg" 
            filename = str(codeD) + "_" + zona + "c" + `x` + "d" + `y` + ".jpg" 
            print filename+'------------------------'
            filename2=filename
            filename2=filename2.replace(".jpg",".bmp")
            tile = cursorD.execute("select * from dbo.MapAdjustment where Name LIKE ?" \
            , filename2) 
            existingTile =tile.fetchall()
            if len(existingTile) > 0:
                continue
            #filename = str(codeD) + "_" + zona + "x" + `x` + "y" + `y` + ".jpg"
            filename = str(codeD) + "_" + zona + "c" + `x` + "d" + `y` + ".jpg" 
            fileHere = False
            cont=1
            while not fileHere:  
                fileHere = True 
                cont+=1
                mapProxy = {"http": ""}   
                mapProxy['http'] = proxyURL
                proxy = urllib2.ProxyHandler(mapProxy)
#                print proxy.proxies          
#                opener = urllib2.build_opener(proxy) 
#                urllib2.install_opener(opener)
                try:
                    print picture_page
                    fileHandler = urllib2.urlopen(picture_page, timeout=20) 
                    fileObj = open(fileLocation + filename, 'wb')
                    fileObj.write(fileHandler.read())
                    fileObj.close()
                    im = Image.open(fileLocation + filename)
                except Exception:
                    #wx.MessageBox("Problemas al importar los mapas","Error", wx.OK|wx.ICON_ERROR)
                    print "Trying again, try number " +str(cont)
                    #return
                    fileHere = False
#            fileObj = open(fileLocation + filename, 'wb')
#            fileObj.write(fileHandler.read())
#            fileObj.close()
##            texto="c:\\ImageMagick-6.6.7-1\\"+"conv -size 1000x1000 -colors 256 " + fileLocation+filename + " "  + fileLocation+filename.replace(".jpg",".bmp")
##            print texto
#
#            
##            os.system(texto)
#            
#
#            im = Image.open(fileLocation + filename)
            im = im.convert('RGB').convert('P', palette=Image.ADAPTIVE, colors=255)
            im.save(fileLocation + filename.replace(".jpg", ".bmp"))
            
            os.remove(fileLocation + filename)
            filename=filename.replace(".jpg",".bmp")
            #print filename

            p89 = Proj(proj="utm", zone=30, ellps="GRS80", units="m", towgs84="0,0,0")
            flannaganY89 = -3980000
            flannaganX89 = +30000
            lon, lat = p89(minX, minY, inverse=True)
            minX2, minY2 = utiRiku.projection(lat, lon)
            minX2, minY2 = minX2 + flannaganX89, minY2 + flannaganY89
            lon, lat = p89(maxX, maxY, inverse=True)
            maxX2, maxY2 = utiRiku.projection(lat, lon)
            maxX2, maxY2 = maxX2 + flannaganX89, maxY2 + flannaganY89
            minX2 = minX
            maxX2 = maxX
            #minX2, minY2 = convertED50(minX, minY, 30)
            #maxX2, maxY2 = convertED50(maxX, maxY, 30)
#            print "insert into dbo.MapAdjustment (Name,Storing_Mode,Type,Color,Accurate,Sector,Px_Top,Py_Top,Px_Bottom,Py_Bottom,Mx_Top,My_Top,Mx_Bottom,My_Bottom) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)" , filename, 'raster', 'material', 1, level, 1, minX2 + 30000, maxY - 3980000, maxX2 + 30000, minY - 3980000, minX2 + 30000, maxY - 3980000, maxX2 + 30000, minY - 3980000
#            cursorD.execute("insert into dbo.MapAdjustment (Name,Storing_Mode,Type,Color,Accurate,Sector,Px_Top,Py_Top,Px_Bottom,Py_Bottom,Mx_Top,My_Top,Mx_Bottom,My_Bottom)\
#                                 values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)" \
#                                , filename, 'raster', 'material', 1, level, 1, minX2, maxY2, maxX2, minY2, minX2, maxY2, maxX2, minY2)    
                 
            
            cursorD.execute("insert into dbo.MapAdjustment (Name,Storing_Mode,Type,Color,Accurate,Sector,Px_Top,Py_Top,Px_Bottom,Py_Bottom,Mx_Top,My_Top,Mx_Bottom,My_Bottom)\
                     values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)" \
                    , filename, 'raster', 'material', 1, level, 1, minX2 + 30000, maxY - 3980000, maxX2 + 30000, minY - 3980000, minX2 + 30000, maxY - 3980000, maxX2 + 30000, minY - 3980000)                  
            cnxnD.commit()
            
            # number of completed slices
            slicesCompleted = (x * slicesY + y + 1)
            # percentage of completed slices
            slicesCompletedTotal = 0
            slicesTotal = 0
            res = {1: 600e3,
                   2: 200e3,
                   3:  10e3,
                   4:   2e3,
                   5:   1e3,
                   6:   0.25e3 }
            
            for i in range(3, 7):
                res2 = res[i]
                slicesX2 = int(float(maxCoord[0] - minCoord[0]) / res2)
                if slicesX2 < 1:
                    slicesX2 = 1
                slicesY2 = int(float(maxCoord[1] - minCoord[1]) / res2)
                if slicesY2 < 1:
                    slicesY2 = 1
                slicesTotal = slicesTotal + slicesX2 * slicesY2
                
            for i in range(3, level):
                res2 = res[i]
                slicesX2 = int(float(maxCoord[0] - minCoord[0]) / res2)
                if slicesX2 < 1:
                    slicesX2 = 1
                slicesY2 = int(float(maxCoord[1] - minCoord[1]) / res2)
                if slicesY2 < 1:
                    slicesY2 = 1
                
                slicesCompletedTotal = slicesCompletedTotal + slicesX2 * slicesY2
#            print slicesX2, slicesY2
            slicesCompletedTotal = slicesCompletedTotal + slicesCompleted
    
    
#            print slicesCompleted, slicesCompletedTotal, slicesTotal
            
            slicesPercentage = float(slicesCompletedTotal) / float(slicesTotal) * 100.00
#            print slicesX, slicesY, x, y
#            print "Progreso del nivel " + `level` + ": " + `slicesPercentage` + "%"
            PrgDialog.Update(slicesPercentage * 10)
    PrgDialog.Update(1000)

def loadMap(cnxn, level, center, bounds, codeD, zona):
    
    res = {1: 600e3,
           2: 200e3,
           3:  10e3,
           4:   2e3,
           5:   1e3,
           6:   0.25e3 }
    
    res = res[level]

    minCoord = (center[0] - bounds[0], center[1] - bounds[2])
    maxCoord = (center[0] + bounds[1], center[1] + bounds[3])

    slicesX = int(float(maxCoord[0] - minCoord[0]) / res)
    if slicesX < 1:
        slicesX = 1
    
    slicesY = int(float(maxCoord[1] - minCoord[1]) / res)
    if slicesY < 1:
        slicesY = 1
    
    fileLocation = DataBase.DMSLocation + 'Map/mater' + `level` + '/'
    print fileLocation + 'Map/mater' + `level` + '/'
    proxyURL = ""

    getMaps(maxCoord, minCoord, slicesX, slicesY, codeD, fileLocation, cnxn, level, zona, proxyURL)


                
def convertED50 (x50, y50, zone):
    'EDS50'
    p50 = Proj(proj="utm", ellps="intl", k=0.9996, x_0=500000.0, y_0=0.0 ,
    zone=zone, units="m", towgs84="-125.098545,-76.000054,-156.198703,0.0,0.0,-1.129,8.30463101")
    'WGS84'    
    p89 = Proj(proj="utm", zone=zone, ellps="GRS80", units="m", towgs84="0,0,0")
    
    '''x50,y50=p50(-3.09581,40.79182)
    x89,y89=transform (p50,p89,x50,y50)
    print x50,y50
    print x89,y89'''
    
    x89, y89 = transform(p50, p89, x50, y50)
    lon, lat = p89(x89, y89, inverse=True)
    x89, y89 = utiRiku.projection(lat, lon)
    flannaganY89 = -3980000
    flannaganX89 = +30000
    return int(x89 + flannaganX89), int(y89 + flannaganY89)



# -*- coding: iso-8859-15 -*-
'''
Created on 28/01/2011

@author: n1
'''
import utiDMS
import sys
import TrazaImport


# Measurements
##############
# make a measurement: only need to call this, this calls all the relevant
# iSomething-functions
    # returns the node code
def makeMeasurement(cursor, X, Y, OPCCode, ConnectedNode=None, \
                    Prefix=None, Unit=None, DecimalPlaces=1, MeasureType=0):
    NODECODE = None
    for i in range(1, 9):
        NODECODE = utiDMS.getDMSCode(X, Y) + 'F' + str(i)
        cursor.execute("SELECT NODECODE FROM Component\
                        WHERE NODECODE = ?", NODECODE)
        r = cursor.fetchone()
        if r == None:
            break
        if i == 9:
            raise ValueError("Too many measurements in one spot")

    iComponent(cursor, NODECODE, X, Y)
    ScadaCode = iMeasure(cursor, NODECODE, MeasureType=MeasureType, \
                         Prefix=Prefix, Unit=Unit, \
                         DecimalPlaces=DecimalPlaces)
    if ConnectedNode != None:
        iMeasureNodes(cursor, NODECODE, ConnectedNode)
    iMeasureValues(cursor, ScadaCode, OPCCode)

    return NODECODE
    


def iComponent(cursor, NODECODE, X, Y, NETLEVEL=0, \
               ComponentClassId=1, ComponentCode=None, \
               XCODE=None, YCODE=None, \
               SymbolAngle=0, LabelAngle=0, HIDE=0, \
               HIDE_COMPONENT=0):
    if XCODE == None:
        XCODE = X
    if YCODE == None:
        YCODE = Y
    if ComponentCode == None:
        ComponentCode = NODECODE
    
    cursor.execute("INSERT INTO Component \
                    (NODECODE, NETLEVEL, ComponentClassId,\
                     ComponentCode, X, Y, XCODE, YCODE,\
                     SymbolAngle, LabelAngle, HIDE, HIDE_COMPONENT)\
                     VALUES (?, ?, ?,\
                             ?, ?, ?, ?, ?,\
                             ?, ?, ?, ?)",
                   NODECODE, NETLEVEL, ComponentClassId, \
                   ComponentCode, X, Y, XCODE, YCODE, \
                   SymbolAngle, LabelAngle, HIDE, HIDE_COMPONENT)


# returns the ScadaCode that was inserted
def iMeasure(cursor, NODECODE, ScadaCode=None, MeasureType=0, \
             Operation=0, Prefix=None, Unit=None, \
             Factor=1, Value=0, DecimalPlaces=1):
    if ScadaCode == None:
        ScadaCode = NODECODE

    cursor.execute("INSERT INTO Measure\
                    (NODECODE, ScadaCode, MeasureType,\
                     Operation, Prefix, Unit, Factor,\
                     Value, DecimalPlaces) VALUES\
                     (?, ?, ?,\
                     ?, ?, ?, ?,\
                     ?, ?)",
                   NODECODE, ScadaCode, MeasureType, \
                   Operation, Prefix, Unit, Factor, \
                   Value, DecimalPlaces)

    return ScadaCode



def iMeasureNodes(cursor, NODECODE, ConnectedNode):
    cursor.execute("INSERT INTO MeasureNodes (NODECODE, ConnectedNode)\
                    VALUES (?, ?)", NODECODE, ConnectedNode)



def iMeasureValues(cursor, ScadaCode, OPCCode, Value=0, \
                   Time=None, Status=0, Changed=None):
    cursor.execute("INSERT INTO MeasureValues\
                    (ScadaCode, Value, Time, Status, Changed, OPCCode)\
                    VALUES (?, ?, ?, ?, ?, ?)",
                   ScadaCode, Value, Time, Status, Changed, OPCCode)



# MV stuff
##########
def iDiagram (cursor, code, type, xg, yg, num):

    x1 = xg
    y1 = yg
    xs = 1000 * (1 + num)
    ys = 4000
    
    x2 = x1 + xs
    y2 = y1 + ys

    

    cursor.execute("insert into dbo.DIAGRAM (code,type,x1,x2,y1,y2,hide_diagram)\
                                      values (?,?,?,?,?,?,?)", \
                                              code, type, x1, x2, y1, y2, 0)
#    cursor.execute("insert into dbo.diagram (code) select ? where ? not in (select code from dbo.diagram)\
#                    update dbo.diagram\
#                    set type=?,x1=?, x2=?,y1=?,y2=?,hide_diagram=?\
#                    where code = ?", code,code,type,x1,x2,y1,y2,0,code)

def iFuse (cursor, nodecode, code, mv_site):
    cursor.execute("insert into dbo.MV_FUSE (nodecode,code) select nodecode=?, code=? where ? not in (select nodecode from dbo.disconnector)\
                    update dbo.MV_FUSE\
                    set code=?,type=?,status=?,statusctrl=?\
                    where nodecode = ?", nodecode, code, nodecode, code, "DEFAULT_200", 1, 0, nodecode)


    '''cursor.execute("insert into dbo.DISCONNECTOR (nodecode,code,mv_site,disconnector_type,status,control,subsymbolcode)\
                    values (?,?,?,?,?,?,?)", \
                    nodecode,code,mv_site,"L",1,0,0)'''

def iDisconnector (cursor, nodecode, code, mv_site, CTName=None):
    cursor.execute("insert into dbo.disconnector (nodecode,code) select nodecode=?, code=? where ? not in (select nodecode from dbo.disconnector)\
                    update dbo.disconnector\
                    set code=?,mv_site=?, disconnector_type=?,status=?,control=?,subsymbolcode=?,direction=' '\
                    where nodecode = ?", nodecode, code, nodecode, code, mv_site, "L", 1, 0, 0, nodecode)
    opccode = None
    '''cursor.execute("insert into dbo.DISCONNECTOR (nodecode,code,mv_site,disconnector_type,status,control,subsymbolcode)\
                    values (?,?,?,?,?,?,?)", \
                    nodecode,code,mv_site,"L",1,0,0)'''
    if CTName != None:
        cursor.execute("insert into gdlOPC values (?,'I',?)", code[0:3], code[3:])
    cursor.execute("select * from gdlCT where DMS_Name LIKE ?", code[3:-2])
    codes = cursor.fetchall()
    if len(codes) == 0 and CTName != None:
        cursor.execute("insert into gdlCT values (?,?,NULL,?)", code[0:3], code[3:-2], CTName)

def createOPCCode(cursor, MINECO, DMSLocation):
# This function creates and fill the opera.itm and opera.opc files and SwitchingComponent table
    itmfile = open(DMSLocation + 'data/Opera.itm', 'w')
    opcfile = open(DMSLocation + 'data/Opera.opc', 'w')
    print >> itmfile, '[POSITIONITEMS]'    
# In gdlCT we have the join between dms and scada names. We need Scada name to make the opc code
    cursor.execute("select * from gdlCT WHERE MINECO = ?", MINECO)
    gdlCTs = cursor.fetchall()
    k = 0
    j = 0
    for gdlCT in gdlCTs:
        cursor.execute("select * from gdlOPC WHERE MINECO = ? AND OPC_CODE LIKE ?", MINECO, gdlCT.DMS_Name + 'P%')
        CTs = cursor.fetchall()
        SCADA_Name = gdlCT.SCADA_Name
        if SCADA_Name != None:
            j = j + 1
            print >> opcfile, '[POSITIONINDICATION_%s]' % j
            print >> opcfile, 'Name=%s_POS' % SCADA_Name
            print >> opcfile, 'UpdateRate=0'
            print >> opcfile, 'Alarms=0'
        i = 0
        for CT in CTs:    
            if SCADA_Name != None:
                k = k + 1
                i = i + 1
                opccode = "\\\\APL1\\OPC\\%s.%s.POS.P0%s" % (MINECO,SCADA_Name,CT.OPC_CODE[-1])
                print >> itmfile, "%s=%s" % (k,opccode)
                print >> opcfile, "%s=%s" % (i,opccode)               
            else:
                opccode = None
            cursor.execute("insert into dbo.SwitchingComponent\
                        (Switch, Type, State, ScadaCode, ScadaControl, ScadaCurrent, Status, Time, Changed, OPCCode, QUALITY, forced,gdlMINECO)\
                        VALUES (?,'E', 1, NULL, NULL, NULL,0,NULL,NULL,?,0,0,?)\
                        ", CT.OPC_CODE, opccode,MINECO[0:3])
    k = 0
    j = 0
    print >> itmfile, '[MEASUREMENTITEMS]'
    for gdlCT in gdlCTs:
        cursor.execute("select * from gdlOPC WHERE MINECO = ? AND OPC_CODE LIKE ?", MINECO, gdlCT.DMS_Name + 'P%')
        CTs = cursor.fetchall()
        SCADA_Name = gdlCT.SCADA_Name    
        if SCADA_Name != None:
            j = j + 1
            print >> opcfile, '[MEASUREMENT_%s]' % j
            print >> opcfile, 'Name=%s_MED' % SCADA_Name
            print >> opcfile, 'UpdateRate=30'
            print >> opcfile, 'Alarms=0'
        i = 0
        for CT in CTs:        
            cursor.execute("select * from gdlCT WHERE MINECO = ? AND DMS_Name = ?", MINECO, CT.OPC_CODE[:-2])
            gdlCT = cursor.fetchone()
            SCADA_Name = gdlCT.SCADA_Name
            if SCADA_Name != None:
                k = k + 1
                i = i + 1
                opccode = "\\\\APL1\\OPC\\%s.MEAS.I.%s.%s" % (MINECO,CT.OPC_CODE[:-2],CT.OPC_CODE[-2:])
                print >> itmfile, "%s=%s" % (k,opccode)
                print >> opcfile, "%s=%s" % (i,opccode)
    itmfile.close()
    opcfile.close()

def iMV_Node (cursor, code, x, y, xcode, ycode, sitenode, symbolangle=0, hide=0, hide_node=0, mineco=None, desc=None, line=None):
    if desc == 'ST':
        code = utiDMS.getSubstationPoint(cursor, utiDMS.encode(line)[0:12])
        sitenode = code
    try:
        cursor.execute("insert into dbo.mv_node (code,sitenode, gdlMINECO) select code=?, sitenode=?, gdlMINECO=?\
                        update dbo.mv_node\
                        set x=?,y=?, xcode=?,ycode=?,sitenode=?,symbolangle=?,hide=?,hide_node=?\
                        where code LIKE ?", code, sitenode, mineco[0:3], x, y, xcode, ycode, sitenode, symbolangle, hide, hide_node, code)
    except:
        aaa=None
    # fill in region data to ensure the node shows even without electricity
    if mineco != None:
        cursor.execute("MERGE NODE_REGIONS\
                        USING (SELECT ?, ?) AS src (code, mineco)\
                        ON (NODE_REGIONS.NODECODE LIKE src.code)\
                        WHEN MATCHED THEN UPDATE SET REGION_CODES = src.mineco\
                        WHEN NOT MATCHED THEN INSERT (NODECODE, REGION_CODES)\
                        VALUES (src.code, src.mineco);", \
                       code, mineco)
    return code


def iMV_LV_Substation(cursor, code, mun=None, prov=None, mineco=None):
    cursor.execute("insert into dbo.MV_LV_SUBSTATION\
                    (CODE, MUN, PROVINCE, COMPANY)\
                    select ?, ?, ?, ? where ? not in\
                    (select code from dbo.mv_lv_substation)",
                   code, mun, prov, mineco, code)
    
def iMV_Section(cursor, MV_Section_Id, NODE1, NODE2, CONDUCTOR, nMINECO, VOLTAGE_LEVEL, Xt, Yt, \
                        PARALLEL_NUMBER=1, \
                        LENGTH=None, \
                        SUBSTATION=None, \
                        MANUFACTURE_YEAR=0, \
                        INSPECTION_YEAR=0, \
                        POLING_YEAR=0, \
                        WIRING_YEAR=0, \
                        AREA=None, \
                        OWNER=None, \
                        ANGLE=0, \
                        HIDE=0, \
                        HIDE_SECTION=0):
    cursor.execute("insert into dbo.MV_SECTION (MV_Section_Id,\
                                    NODE1,\
                                    NODE2,\
                                    PARALLEL_NUMBER,\
                                    VOLTAGE_LEVEL,\
                                    LENGTH,\
                                    SUBSTATION,\
                                    CONDUCTOR,\
                                    MANUFACTURE_YEAR,\
                                    INSPECTION_YEAR,\
                                    POLING_YEAR,\
                                    WIRING_YEAR,\
                                    AREA,\
                                    OWNER,\
                                    DISTRICT,\
                                    Xt,\
                                    Yt,\
                                    ANGLE,\
                                    HIDE,\
                                    HIDE_SECTION)\
                    values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", \
                                    MV_Section_Id, \
                                    NODE1, \
                                    NODE2, \
                                    PARALLEL_NUMBER, \
                                    VOLTAGE_LEVEL, \
                                    LENGTH, \
                                    SUBSTATION, \
                                    CONDUCTOR, \
                                    MANUFACTURE_YEAR, \
                                    INSPECTION_YEAR, \
                                    POLING_YEAR, \
                                    WIRING_YEAR, \
                                    AREA, \
                                    OWNER, \
                                    nMINECO, \
                                    Xt, \
                                    Yt, \
                                    ANGLE, \
                                    HIDE, \
                                    HIDE_SECTION)

def iLV_Section(cursor, NODE1, NODE2, CONDUCTOR, LV_NETWORK, \
                        PARALLEL_NUMBER=1, \
                        LENGTH=None, \
                        MANUFACTURE_YEAR=0, \
                        INSPECTION_YEAR=0, \
                        POLING_YEAR=0, \
                        WIRING_YEAR=0, \
                        OWNER=None, \
                        ANGLE=0, \
                        Xt=None, \
                        Yt=None, \
                        HIDE=0):
    
    # get free section id
    cursor.execute("SELECT MAX(LV_Section_Id) + 1 AS max FROM LV_SECTION")
    LV_Section_Id = cursor.fetchone().max
    if LV_Section_Id == None:
        LV_Section_Id = 1
    if NODE1 != None and NODE2 != None:
        cursor.execute("insert into dbo.LV_SECTION (LV_Section_Id,\
                                        NODE1,\
                                        NODE2,\
                                        PARALLEL_NUMBER,\
                                        LV_NETWORK,\
                                        LENGTH,\
                                        CONDUCTOR,\
                                        MANUFACTURE_YEAR,\
                                        INSPECTION_YEAR,\
                                        POLING_YEAR,\
                                        WIRING_YEAR,\
                                        OWNER,\
                                        DOWNSTREAM_LINE,\
                                        Xt,\
                                        Yt,\
                                        ANGLE,\
                                        HIDE)\
                        values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", \
                                        LV_Section_Id, \
                                        NODE1, \
                                        NODE2, \
                                        PARALLEL_NUMBER, \
                                        LV_NETWORK, \
                                        LENGTH, \
                                        CONDUCTOR, \
                                        MANUFACTURE_YEAR, \
                                        INSPECTION_YEAR, \
                                        POLING_YEAR, \
                                        WIRING_YEAR, \
                                        OWNER, \
                                        0, \
                                        Xt, \
                                        Yt, \
                                        ANGLE, \
                                        HIDE)
    
    return LV_Section_Id
    
def iMV_SectionPoints (cursor, mv_section_id, node1, node2, secc, mineco = None):
         
    for i in range(0, len(secc)):
        seccN = secc[i]
        numSecc = seccN["numSecc"]
        x = seccN["x"]
        y = seccN["y"]
        cursor.execute("insert into dbo.MV_SECTIONPOINT (mv_section_id,\
                                                     node1,\
                                                     node2,\
                                                     parallel_number,\
                                                     number,\
                                                     x,\
                                                     y,\
                                                     gdlMINECO)\
                                    values(?,?,?,?,?,?,?,?)", \
                                                    mv_section_id, \
                                                    node1, \
                                                    node2, \
                                                    1, \
                                                    numSecc, \
                                                    x, \
                                                    y,
                                                    mineco[0:3])      

def iLV_SectionPoints (cursor, node1, node2, secc, mineco=None):
         
    for i in range(0, len(secc)):
        seccN = secc[i]
        numSecc = seccN["numSecc"]
        x = seccN["x"]
        y = seccN["y"]
        if node1 != None and node2 != None:
            cursor.execute("insert into dbo.LV_SECTIONPOINT (node1,\
                                                         node2,\
                                                         parallel_number,\
                                                         number,\
                                                         x,\
                                                         y,\
                                                         gdlMINECO)\
                                        values(?,?,?,?,?,?,?)", \
                                                        node1, \
                                                        node2, \
                                                        1, \
                                                        numSecc, \
                                                        x, \
                                                        y,\
                                                        mineco[0:3])   

def iLV_BOX(cursor, node, CTName, code):
         

    cursor.execute("insert into dbo.LV_BOX (NODECODE,\
                                            CODE,\
                                            ADDRESS,\
                                            INSTALLATION_DATE,\
                                            TYPE,\
                                            MANUFACTURER,\
                                            MANUFACTURE_DATE,\
                                            INSPECTION_DATE,\
                                            CHANGE_DATE\
                                            )\
                                values(?,?,?,?,?,?,?,?,?)", \
                                                node, \
                                                code, \
                                                CTName, \
                                                None, \
                                                '', \
                                                '', \
                                                None, \
                                                None, \
                                                None\
                                                )   

def uLV_CUSTOMER_NODE(cursor, node, feeder, box, switch):
    cursor.execute("UPDATE LV_CUSTOMER_NODE \
                    SET FEEDER = ?, \
                        BOX = ?, \
                        SWITCH = ? \
                    WHERE NODECODE LIKE ?",
                   feeder, box, switch, node)


def iLV_SWITCH(cursor, node, box, feeder, DEVICE, FUSE, FUSE_TYPE, NAME=None):
    cursor.execute("insert into dbo.LV_SWITCH (NODECODE,\
                                            TYPE,\
                                            BOX,\
                                            FEEDER,\
                                            NAME,\
                                            STATUS,\
                                            DEVICE,\
                                            FUSE_BOX,\
                                            FUSE,\
                                            FUSE_TYPE\
                                            )\
                                values(?,?,?,?,?,?,?,?,?,?)", \
                                                node, \
                                                'J', \
                                                box, \
                                                feeder, \
                                                NAME[0:39], \
                                                1, \
                                                DEVICE, \
                                                None,
                                                FUSE,
                                                FUSE_TYPE
                                                )

def makeLVNode(cursor, X, Y, type, SITENODE=None, \
               IdBTVano=None, Xt=None, Yt=None,MINECO=None):
    for i in range(1, 9):
        code = utiDMS.getDMSCode(X, Y) + type + str(i)
        
        cursor.execute("SELECT CODE FROM MV_NODE WHERE CODE = ?\
                        UNION\
                        SELECT CODE FROM LV_NODE WHERE CODE = ?", code, code)
        if cursor.fetchone() == None:
            break
        elif i == 9:
            TrazaImport.WriteLog("M�s de 10 nodos en la misma localizaci�n")
    
    if IdBTVano != None:
        cursor.execute("INSERT INTO gdlLV_NODE_BTVano (NODECODE, IdBTVano) VALUES (?, ?)", code, IdBTVano)
    
    if SITENODE == None:
        SITENODE = code

    if (Xt == None or Yt == None):
        iLV_NODE(cursor, code, X, Y, X, Y, SITENODE,mineco=MINECO)
    else:
        iLV_NODE(cursor, code, X, Y, Xt, Yt, SITENODE,mineco=MINECO)
    
    return code

def iLV_NODE(cursor, CODE, X, Y, XCODE, YCODE, SITENODE,mineco=None):
    cursor.execute("insert into dbo.LV_NODE (CODE,\
                                            X,\
                                            Y,\
                                            XCODE,\
                                            YCODE,\
                                            SITENODE,\
                                            gdlMINECO)\
                                values(?,?,?,?,?,?,?)", \
                                                CODE, \
                                                X, \
                                                Y, \
                                                XCODE, \
                                                YCODE, \
                                                SITENODE,\
                                                mineco[0:3])

def iMV_Site(cursor, code, type, xg, yg, name=None, mineco=None):
    
    if type == 'M':
        nodecode = utiDMS.getDMSCode(xg, yg) + 'X1'
    if type == 'E':
        nodecode = utiDMS.getDMSCode(xg, yg) + 'E1'
#    type = "M"
    sitecode = code
#    name=sitecode

    if name == None:
        name = sitecode
    cursor.execute("SELECT NODECODE FROM MV_SITE WHERE NODECODE = ?",nodecode)
    nodecodeExists = cursor.fetchall()
    if len(nodecodeExists) == 0:
        cursor.execute("insert into dbo.mv_site (nodecode,type,gdlMINECO) select  nodecode= ?,\
                        type=?, gdlMINECO=? where ? not in (select nodecode from dbo.mv_site)\
                        update dbo.mv_site\
                        set type=?, sitecode=?, name=?,\
                        protective_earthing=?, system_earthing=?, combined_earthing=? \
                        where nodecode = ?", nodecode, type, mineco[0:3], nodecode, type, sitecode, name, 0, 0, 0, nodecode)

def iTransformer(cursor, \
                 CODE, PLACING_SITE, ORDERNUMBER, MANUFACTURER, \
                 U1, U2, U3, SN1):
        cursor.execute("insert into dbo.TRANSFORMER(CODE,PLACING_SITE,ORDERNUMBER,MANUFACTURER,\
                 U1,U2,U3,SN1)\
                 values (?,?,?,?,?,?,?,?)", \
                 CODE, PLACING_SITE, ORDERNUMBER, MANUFACTURER, \
                 U1, U2, U3, SN1)
  
    
    
def uMV_Section_ID(cnxn, cursor):
    cursor.execute("UPDATE dbo.SECTIONNUMBER SET mv_section_id = mv_section_id + 1 WHERE ID=1")
#    cnxn.commit()
    cursor.execute("SELECT mv_section_id \
                 FROM dbo.SECTIONNUMBER \
                 WHERE  ID=1")
    row = cursor.fetchone()
    return int(row[0])
    
def uLV_Section_ID(cnxn, cursor):
    cursor.execute("UPDATE dbo.SECTIONNUMBER SET lv_section_id = lv_section_id + 1 WHERE ID=1")
    #cnxn.commit()
    
def gTransformerCode(cnxn, cursor, code):
    cursor.execute("Select CODE from dbo.TRANSFORMER where CODE like ?", code + "%")
    try:
        row = cursor.fetchone()
        return row[0]
    except Exception as e:
        sys.exit("Transformer Not Found")

def gSiteOrderNumber(cnxn, cursor, sitename):
    cursor.execute("Select max(ORDERNUMBER) from dbo.TRANSFORMER where PLACING_SITE = ?", sitename)
    row = cursor.fetchone()
    if row[0] >= 0: #Checking for null
        return int(row[0])
    else:
        return 0
    
    
    
def pTransformer(cnxn, cursor, code, sitename, nodecode, sitenode):#Transformer Placement
    tCode = gTransformerCode(cnxn, cursor, code)
    tOrderNumber = gSiteOrderNumber(cnxn, cursor, sitename) + 1
     
    cursor.execute("UPDATE dbo.TRANSFORMER SET PLACING_SITE=?,ORDERNUMBER=? WHERE CODE LIKE ?", \
                    sitename, tOrderNumber, tCode)
    cursor.execute("insert into dbo.transformer_node (nodecode, sitenode) select nodecode=? , sitenode= ? where ? not in (select nodecode from dbo.transformer_node)\
                    update dbo.transformer_node\
                    set mv_lv_substation=?,ordernumber=?, sitenode=?\
                    where nodecode = ?", nodecode, sitenode, nodecode, sitename, tOrderNumber, sitenode, nodecode)
       
def sTransformer(cursorW, mv_section_id, codetrafo):#Insert Section Number for Trafo
     
    cursorW.execute("UPDATE dbo.TRANSFORMER SET LINE_SECTION_1=? where CODE like ?", mv_section_id, codetrafo + "%")
    
def getSocioName(cursor, mineco):
    cursor.execute("Select Name from dbo.gdlMINECO where ID = ?", mineco)
    row = cursor.fetchone()
    if row is None:
        valor = "Codigo no encontrado"
    else:
        valor = row[0]
    return valor
   
def getSocioCity(cursor, mineco):
    cursor.execute("Select City from dbo.gdlMINECO where ID = ?", mineco)
    row = cursor.fetchone()
    if row is None:
        valor = "Codigo no encontrado"
    else:
        valor = row[0]
    return valor

def getSocioZone(cursor, mineco):
    cursor.execute("Select Zone from dbo.gdlMINECO where ID = ?", mineco)
    row = cursor.fetchone()
    if row is None:
        valor = -1
    else:
        valor = row[0]
    return valor

def getSocioDMS(cursor, mineco):
    cursor.execute("Select DMS from dbo.gdlMINECO where ID = ?", mineco)
    row = cursor.fetchone()
    if row is None:
        valor = "Codigo no encontrado"
    else:
        valor = row[0]
    return valor

def setSocioZone(cursor, mineco, zone):
    cursor.execute("update dbo.gdlMINECO SET Zone=? where ID=?", zone, mineco)
    
def correctCoordinates(cursorT):
    
    'ATVano'
    cursorT.execute("SELECT ATV.IdATVano ,ATV.X , ATV.Y, ATV.Z FROM ATVano ATV  WHERE ATV.Z >= 0")
    rows = cursorT.fetchall()
    for row in rows:
        row.X, row.Y = utiDMS.convertED50(row.X, row.Y, zone)
        row.Z = -30
        cursorT.execute("update ATVano SET X=?,Y=?,Z=? WHERE idATVano=?", row.X, row.Y, row.Z, row.IdATVano)   
        cnxnT.commit()
        
    'BTVano'
    cursorT.execute("SELECT BTV.IdBTVano ,BTV.X , BTV.Y, BTV.Z FROM BTVano BTV  WHERE BTV.Z >= 0")
    rows = cursorT.fetchall()
    for row in rows:
        row.X, row.Y = utiDMS.convertED50(row.X, row.Y, zone)
        row.Z = -30
        cursorT.execute("update BTVano SET X=?,Y=?,Z=? WHERE idBTVano=?", row.X, row.Y, row.Z, row.IdBTVano)   
        cnxnT.commit() 
        
    'CT'
    cursorT.execute("SELECT CT.IdInst ,CT.X , CT.Y, CT.Z FROM CT  WHERE CT.Z >= 0")
    rows = cursorT.fetchall()
    for row in rows:
        row.X, row.Y = utiDMS.convertED50(row.X, row.Y, zone)
        row.Z = -30
        cursorT.execute("update CT SET X=?,Y=?,Z=? WHERE idInst=?", row.X, row.Y, row.Z, row.IdInst)   
        cnxnT.commit()

    'ST'
    cursorT.execute("SELECT ST.IdInst ,ST.X , ST.Y, ST.Z FROM ST  WHERE ST.Z >= 0")
    rows = cursorT.fetchall()
    for row in rows:
        row.X, row.Y = utiDMS.convertED50(row.X, row.Y, zone)
        row.Z = -30
        cursorT.execute("update ST SET X=?,Y=?,Z=? WHERE idInst=?", row.X, row.Y, row.Z, row.IdInst)   
        cnxnT.commit()
        
def getLineEndings(curT, Linea):

#    curT.execute("Select Inst.Nombre as NombreLinea,  Inst2.Nombre as CTExtremos from dbo.AT AT, dbo.CTCeldas CTC, dbo.Instalaciones Inst, dbo.Instalaciones Inst2\
#    where CTC.IdInstLinea = AT.IdInst and AT.IdInst=Inst.IdInst and Inst2.IdInst = CTC.IdInst and Inst.Nombre=?",Linea)
    curT.execute("select Lineas.Nombre NombreLinea, CTs.Codigo CTExtremos\
    from CTAlimentaciones, Instalaciones Lineas, Instalaciones CTs\
    where CTAlimentaciones.IdInst LIKE CTs.IdInst and CTAlimentaciones.IdInstAlimentaciones LIKE Lineas.IdInst\
    and Lineas.Nombre LIKE ?\
    union\
    select Lineas.Nombre, CTs.Codigo\
    from CTDerivaciones, Instalaciones Lineas, Instalaciones CTs\
    where CTDerivaciones.IdInst LIKE CTs.IdInst and CTDerivaciones.IdInstDerivaciones LIKE Lineas.IdInst\
    and Lineas.Nombre LIKE ?", Linea, Linea)
    
    endings = curT.fetchall()
    extremos = []
    for ending in endings:
        extremos.append(ending.CTExtremos)
    return extremos
   

def getEmptyLineConnection(cursorT, cursorW, CTName, linName, MINECO, celda=0):
    if celda == 0:
        return getEmptyLineConnection2(cursorT, cursorW, CTName, linName, MINECO)
    else:
        cursorW.execute("Select NODECODE from dbo.MV_SITE where sitecode = ? AND gdlMINECO = ?", CTName, MINECO[0:3])
        CT = cursorW.fetchone()
        #Checking for connections
        xg, yg = utiDMS.getCoordinates(CT [0])
        cursorT.execute("SELECT  CT.X , CT.Y, Ins.Nombre , CTAli.IdInstAlimentaciones IdInstLinea, IdTipoCelda = '1', Ins.IdInst , IdTransformador = NULL, IdCelda\
                    FROM  CT , Instalaciones Ins, CTAlimentaciones CTAli, CTCeldas\
                    WHERE CTAli.IdInst = Ins.IdInst  and CT.IdInst=Ins.IdInst \
                    AND Ins.Codigo  = ? AND CT.IdInst like ?\
                    AND CTCeldas.IdInstLinea = CTAli.IdInstAlimentaciones\
                    AND Ins.IdInst = CTCeldas.IdInst\
                    UNION\
                    SELECT  CT.X , CT.Y, Ins.Nombre , CTDer.IdInstDerivaciones IdInstLinea, IdTipoCelda = '1', Ins.IdInst , IdTransformador = NULL, IdCelda\
                    FROM  CT , Instalaciones Ins, CTDerivaciones CTDer, CTCeldas\
                    WHERE CTDer.IdInst = Ins.IdInst  and CT.IdInst=Ins.IdInst \
                    AND Ins.Codigo  = ? AND CT.IdInst like ?\
                    AND CTCeldas.IdInstLinea = CTDer.IdInstDerivaciones\
                    AND Ins.IdInst = CTCeldas.IdInst\
                    UNION\
                    SELECT  CT.X , CT.Y, Ins.Nombre , IdInstLinea = NULL, IdTipoCelda = '2', Ins.IdInst , CTTrafo.IdTransformador, IdCelda\
                    FROM  CT , Instalaciones Ins,CTTransformadores CTTrafo, CTCeldas\
                    WHERE CTTrafo.IdInst = Ins.IdInst  and CT.IdInst=Ins.IdInst \
                    AND Ins.Codigo  = ? AND CT.IdInst like ?\
                    AND CTCeldas.IdTransformador = CTTrafo.IdTransformador\
                    AND Ins.IdInst = CTCeldas.IdInst\
                    order by IdCelda", CTName, MINECO[0:3] + '%', CTName, MINECO[0:3] + '%', CTName, MINECO[0:3] + '%')
    
        rows = cursorT.fetchall()
        
        if len(rows) > 0:
            cursorT.execute("SELECT Ins2.IdInst\
              FROM CTCeldas, Instalaciones Ins1, Instalaciones Ins2\
              where Ins1.IdInst = CTCeldas.IdInst and Ins2.IdInst = CTCeldas.IdInstLinea\
              and Ins1.Codigo = ? and Ins2.Nombre = ? and Ins1.IdInst like ?", CTName, linName, MINECO[0:3] + '%')
        
            emptyConnection = cursorT.fetchone()
            if emptyConnection == None:
                emptyLine = getEmptyLineConnection2(cursorT, cursorW, CTName, linName, MINECO)
                return emptyLine
            i = 0
            for row in rows:
                i = i + 1
                if row.IdInstLinea == emptyConnection[0]:
                    
                    break
              
            nodecode = str(utiDMS.getDMSCode(xg + 1000 * (i - 1), yg=yg + 3000)) + "X1"
            return nodecode
        else:
            return getEmptyLineConnection2(cursorT, cursorW, CTName, linName, MINECO)
        
#        cursorW.execute("Select CODE from dbo.MV_NODE where code = ?",nodecode)
#        row=cursorW.fetchone()
#        if row is not None: #it means feeder free on the CT
#            cursorW.execute("Select Node.CODE,cnx2.CT_NODECODE from dbo.MV_NODE Node \
#                             inner join dbo.gdlCTConnections cnx2 on cnx2.CT_NODECODE=Node.CODE where cnx2.CT_NODECODE = ?",nodecode)
#            row2=cursorW.fetchone()
#            if row2 is None: #nodecode not used, I mean not connected to Line
#                #store lineId and nodecode on gdlCTConnection table
#                cursorT.execute("Select IdInst from dbo.Instalaciones where Nombre=? and IdTipoInst < 3",linName)
#                lineId=cursorT.fetchone().IdInst
#                cursorW.execute("insert into dbo.gdlCtConnections(CT_Nodecode,LI_Idinst) values (?,?)",nodecode,lineId)              
#                return nodecode
#        else:

def getEmptyLineConnection2(cursorT, cursorW, CTName, linName, MINECO):
        cursorW.execute("Select NODECODE from dbo.MV_SITE where sitecode = ? AND gdlMINECO = ?", CTName, MINECO[0:3])
        CT = cursorW.fetchone()
        #Checking for connections
        xg, yg = utiDMS.getCoordinates(CT [0])
        for i in range (1, 8):
            nodecode = str(utiDMS.getDMSCode(xg + 1000 * (i - 1), yg=yg + 3000)) + "X1"
            cursorW.execute("Select CODE from dbo.MV_NODE where code = ?", nodecode)
            row = cursorW.fetchone()
            if row is not None: #it means feeder free on the CT
                cursorW.execute("Select Node.CODE,cnx2.CT_NODECODE from dbo.MV_NODE Node \
                                 inner join dbo.gdlCTConnections cnx2 on cnx2.CT_NODECODE=Node.CODE where cnx2.CT_NODECODE = ?", nodecode)
                row2 = cursorW.fetchone()
                if row2 is None: #nodecode not used, I mean not connected to Line
                    #store lineId and nodecode on gdlCTConnection table
                    cursorT.execute("Select IdInst from dbo.Instalaciones where Nombre LIKE ? and IdTipoInst < 3", linName)
                    lineId = cursorT.fetchone().IdInst
                    cursorW.execute("insert into dbo.gdlCtConnections(CT_Nodecode,LI_Idinst) values (?,?)", nodecode, lineId)              
                    return nodecode
#            else:
#                print "NOT Found feeder" + `i`

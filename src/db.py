import ConfigParser
import base64
class DataBase:
    config = ConfigParser.RawConfigParser()
#    config.read('config/config.ini')
    config.read('config.ini')
    DevelopmentMode=config.get('DVLOP','Check')
    Address = config.get('DMS_DATABASE','Address')
    User = config.get('DMS_DATABASE','User')
    Password = base64.b64decode(config.get('DMS_DATABASE','Password'))
#    Password = config.get('DMS_DATABASE','Password')
    DMSTable = config.get('DMS_DATABASE','DMSTable')
    PICAddress = config.get('PIC_DATABASE','Address')
    PICUser = config.get('PIC_DATABASE','User')
    PICPassword = base64.b64decode(config.get('PIC_DATABASE','Password'))
#    PICPassword = config.get('PIC_DATABASE','Password')
    PICTable = config.get('PIC_DATABASE','PICTable')
   
    if DevelopmentMode == 'YES' :
        TRAZAAddress = config.get('TRAZA_DATABASE','AddressDevelop')
        Check = config.get('TRAZA','CheckDevelop')
    else:
        TRAZAAddress = config.get('TRAZA_DATABASE','Address')
        Check = config.get('TRAZA','Check')
    #print base64.b64encode('cidereader6')
    TRAZAUser = config.get('TRAZA_DATABASE','User')
    TRAZAPassword = base64.b64decode(config.get('TRAZA_DATABASE','Password'))
#    TRAZAPassword = config.get('TRAZA_DATABASE','Password')
    TRAZATable = config.get('TRAZA_DATABASE','TRAZATable')

    
    DMSLocation = config.get('DMS','DMSLocation')
    if DevelopmentMode == 'YES' :
        log = config.get('DMS','logDevelop')
    else: 
        log = config.get('DMS','log')
    Zone = config.get('TRAZA','Zone')
#    WebServer = config.get('WEBSERVER','Path')
    Webserver = config.get('WEBSERVER','Path')
    LogEnabled = config.get('DVLOP','LogEnabled')
    ImportBlock=config.get('DVLOP','ImportBlock')
    SugerenciaErrores=config.get('DVLOP','Sugerencias')
   
    
    
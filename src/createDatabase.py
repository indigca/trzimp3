'''
Created on 23/09/2011

@author: n1
'''
import utiDMS
from db import DataBase
print "Creacion de tablas DMS iniciada"
cnxnW = utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password) #harcoded
cursorW = cnxnW.cursor()
files = [
         'queries/Network_mdb.sql',
         'queries/Opera_mdb.sql',
         'queries/Views.sql',
         'queries/Network_default_data.sql',
         'queries/Opera_default_data.sql',
         'queries/InitGEDLux/gdl_ddl.sql', 
         'queries/InitGEDLux/gdlExtension.sql', 
         'queries/InitGEDLux/gdlcodemun.sql', 
         'queries/InitGEDLux/gdlcodeprov.sql', 
         'queries/InitGEDLux/gdlmineco.sql', 
         'queries/InitGEDLux/gdlmuntype.sql', 
         'queries/InitGEDLux/gdlmvcustlocation.sql', 
         'queries/InitGEDLux/gdlreportcompany.sql',
         'queries/InitGEDLux/gdlreportingservices.sql',
         'queries/InitGEDLux/init_all.sql',
         'queries/change_zone2.sql',
         'queries/move_outagereason2.sql',
         'queries/InitGEDLux/REGION.sql'
         ]
i = 0
for f in files:
    i = i + 1
    file = open(f,'r')
    fText = file.read()
    fTextParts = fText.split('GO')
    for fT in fTextParts:
        cursorW.execute(fT)
        cnxnW.commit()
file.close()
print "Creacion de tablas DMS finalizada"

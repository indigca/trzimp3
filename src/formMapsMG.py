# -*- coding: iso-8859-15 -*-
'''
Created on 10/08/2011

@author: n1
'''
import wx
import utiMaps
import utiRiku
import tGetMaps
import ConfigParser
import utiDMS
import os
from db import DataBase
from wx._core import CENTRE

class Panel(wx.Panel):
    def __init__(self, parent):
        
        'Connect to DMS600'
#        print DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password
        self.cnxnD=utiDMS.connectSQL(DataBase.Address, DataBase.DMSTable, DataBase.User, DataBase.Password)
        cursorD = self.cnxnD.cursor()
        user = os.environ.get( "USERDOMAIN" ) + "\\" + os.environ.get( "USERNAME" )
 
        'Get List of Socios'
        cursorD.execute("SELECT L.Name, L.ID ,L.DMS\
                 FROM gdlMineco L WHERE L.ID IN (SELECT COMPANY FROM gdlReportCompany WHERE USERID LIKE ?)",user) 
        
        rows=cursorD.fetchall()
        sampleList=[]
        for row in rows:
            sampleList.append(row.ID+row.DMS+"-"+row.Name)
            
        wx.Panel.__init__(self, parent, size=(700,700))

        # create some sizers
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        grid = wx.GridBagSizer(hgap=1, vgap=1)

        hSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.quote = wx.StaticText(self, label="Importación de mapas: ")
        grid.Add(self.quote, pos=(0, 0))

        # A button
        self.btnImportar = wx.Button(self, label="Importar")
        self.Bind(wx.EVT_BUTTON, self.evtImportar, self.btnImportar)
#        self.ExitButton = wx.Button(self, -1, "Salir", pos=(300, 530))
#        self.ExitButton.Bind(wx.EVT_BUTTON, self.OnCloseMe)
        self.lblExtension = wx.StaticText(self, label="Cliente: ")
        grid.Add(self.lblExtension, pos=(1, 0))
        self.cbCliente = wx.ComboBox(self, choices = sampleList, size=(140, -1))
        self.cbCliente.Select(0)
#        self.tbCliente = wx.TextCtrl(self, value = "045A", size=(140, -1))
        grid.Add(self.cbCliente, pos=(1, 2))
        # the edit control - one line version.
        self.btnLocalizar = wx.Button(self, label="Localizar")
        grid.Add(self.btnLocalizar, pos=(1, 3))
        self.lblZona = wx.StaticText(self, label="Zona :")
        grid.Add(self.lblZona, pos=(2, 0))
        #self.tbZona = wx.TextCtrl(self, size=(140, -1))
        #grid.Add(self.tbZona, pos=(2, 1))   
        
        
        self.panelRb = wx.Panel(self)
        self.rbZona29 = wx.RadioButton(self.panelRb, -1, "29",pos=(0, 0))
        self.rbZona30 = wx.RadioButton(self.panelRb, -1, "30",pos=(40, 0))
        self.rbZona30.SetValue(True)
        self.rbZona31 = wx.RadioButton(self.panelRb, -1, "31",pos=(80, 0))
        grid.Add(self.panelRb, pos=(2, 1)) 
        
        self.lblUbicacion = wx.StaticText(self, label="Ubicación :")
        grid.Add(self.lblUbicacion, pos=(3, 0))
        self.tbUbicacion = wx.TextCtrl(self, size=(140, -1))
        grid.Add(self.tbUbicacion, pos=(3, 1))        
        self.btnLocalizar2 = wx.Button(self, label="Localizar")
        grid.Add(self.btnLocalizar2, pos=(3, 2))
        
        self.Bind(wx.EVT_BUTTON, self.evtLocalizar2, self.btnLocalizar2)
        self.Bind(wx.EVT_BUTTON, self.evtLocalizar, self.btnLocalizar)
        
        # coordenadas de la ubicación seleccionada
        self.lblCoordX = wx.StaticText(self, label="Coordenada X:")
        grid.Add(self.lblCoordX, pos=(4, 0))
        self.tbCoordX = wx.TextCtrl(self, size=(140, -1))
        grid.Add(self.tbCoordX, pos=(4, 1))
        self.lblCoordY = wx.StaticText(self, label="Coordenada Y:")
        grid.Add(self.lblCoordY, pos=(5, 0))
        self.tbCoordY = wx.TextCtrl(self, size=(140, -1))
        grid.Add(self.tbCoordY, pos=(5, 1))      
        



        # Extention from central point
        self.lblExtension = wx.StaticText(self, label="Extensión en km.:")
        grid.Add(self.lblExtension, pos=(6, 0))
        
#       # Values for extension N, W, E, S
        self.tbExtN = wx.TextCtrl(self, value = "3", size=(14, -1))
        grid.Add(self.tbExtN, pos=(7, 1))
        self.tbExtW = wx.TextCtrl(self, value = "3", size=(14, -1))
        grid.Add(self.tbExtW, pos=(8, 0))
        self.tbExtE = wx.TextCtrl(self, value = "3", size=(14, -1))
        grid.Add(self.tbExtE, pos=(8, 2))
        self.tbExtS = wx.TextCtrl(self, value = "3", size=(14, -1))
        grid.Add(self.tbExtS, pos=(9, 1)) 



        hSizer.Add(grid, 0, wx.ALL, 5)
        mainSizer.Add(hSizer, 0, wx.ALL, 5)
#       mainSizer.Add(hSizerRb, 0, wx.ALL, 5)
        mainSizer.Add(self.btnImportar, 0, wx.CENTER)
#        mainSizer.Add(self.ExitButton, 0, wx.CENTER)
        self.SetSizerAndFit(mainSizer)
        
    def OnCloseMe(self,event):
        self.Close(True)
        
    def evtLocalizar(self, event):
        cliente = self.cbCliente.Value[0:3]
        cursorD = self.cnxnD.cursor()
        'Get List of Socios'
        cursorD.execute("SELECT L.ID, L.City\
                 FROM gdlMineco L WHERE L.DMS IS NOT NULL and L.ID = ? ORDER BY L.ID", cliente )
        row=cursorD.fetchone()
        print row
        self.tbUbicacion.SetValue(row[1])
        lat, lon, x89, y89 = utiMaps.getXY_Zone(self.tbUbicacion.Value)
        crd = utiRiku.projection(lat, lon) #lat, lon
        self.tbCoordX.SetValue(`crd[0]`)
        self.tbCoordY.SetValue(`crd[1]`)
    
    def evtLocalizar2(self, event):
        lat, lon, x89, y89 = utiMaps.getXY_Zone(self.tbUbicacion.Value)
        crd = utiRiku.projection(lat, lon) #lat, lon
        self.tbCoordX.SetValue(`crd[0]`)
        self.tbCoordY.SetValue(`crd[1]`)

    def evtImportar(self, event):
        X = float(self.tbCoordX.Value)
        Y = float(self.tbCoordY.Value)
        N = float(self.tbExtN.Value) * 1000
        W = float(self.tbExtW.Value) * 1000
        E = float(self.tbExtE.Value) * 1000
        S = float(self.tbExtS.Value) * 1000
        crd = [X, Y]
        ext = [W, E, S, N]
        cliente = self.cbCliente.Value[0:4]
        print self.rbZona29.Value
        print self.rbZona30.Value
        print self.rbZona31.Value
        
        if self.rbZona30.Value:
            zona='30'
        elif self.rbZona29.Value:
            zona='29'
        else:
            zona='31'
        
        print zona
        #zona = self.tbZona.Value
        print cliente
        tGetMaps.GetMaps(crd, ext, cliente, zona)
        
#class DataBase:
#    config = ConfigParser.RawConfigParser()
#    config.read('config.ini')
#    Address = config.get('DATABASE','Address')
#    User = config.get('DATABASE','User')
#    Password = config.get('DATABASE','Password')
#    DMSTable = config.get('DATABASE','DMSTable')
#    TRAZATable = config.get('DATABASE','TRAZATable')
#    DMSLocation = config.get('DMS','DMSLocation')
    
def open():    
    app = wx.App(False)
    frame = wx.Frame(None, -1, 'Importación de mapas', size=(475,350),style= wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
    panel = Panel(frame)
    frame.Centre()
    frame.Show()
    app.MainLoop()

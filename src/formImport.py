# -*- coding: utf-8 -*-
import wx
from db import DataBase
import utiDMS
import wx.grid as gridlib
import os

class Main(wx.Frame):    
    def __init__(self):
        wx.Frame.__init__(self, parent=None, title='Importacion',size=(510, 570))
        self.panel = wx.Panel(self)
        self.Map = wx.Image('map/0613_x0y0.bmp', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.Map2 = wx.StaticBitmap(self.panel, -1, self.Map, (0, 0), (500, 500))
        self.Map2.Bind(wx.EVT_LEFT_UP, self.onClick)
        status = self.CreateStatusBar()
        menubar = wx.MenuBar()
        first = wx.Menu()
        second = wx.Menu()
        first.Append(wx.NewId(),"Nuevo Mapa","Abrir nuevo mapa")
        first.Append(wx.NewId(),"Cambiar configuracion","Cambiar configuracion")
        menubar.Append(first,"Archivo")
        menubar.Append(second,"Edicion")
        self.SetMenuBar(menubar)
        
    def onClick(self,event):
        print event.GetPosition()
        box = wx.TextEntryDialog(None,"Nombre","Nombre","Introduzca Nombre de CT")
        if box.ShowModal()==wx.ID_OK:
            answer=box.GetValue()
            print answer
if __name__ == '__main__': 

    app = wx.PySimpleApp()
    Main().Show()
    app.MainLoop()
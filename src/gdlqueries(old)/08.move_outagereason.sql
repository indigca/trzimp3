UPDATE CODEINFO SET INFOTYPE = 'FaultReason_move' WHERE INFOTYPE = 'FaultReason'
UPDATE CODEINFO SET INFOTYPE = 'FaultReason' WHERE INFOTYPE = 'OutageReason'
UPDATE CODEINFO SET INFOTYPE = 'OutageReason' WHERE INFOTYPE = 'FaultReason_move'
UPDATE CODEINFO SET INFOTYPE = 'LVFaultReason' WHERE INFOTYPE = 'LVOutageReason'

UPDATE	FaultReport
SET		FaultReason = REPLACE(OutageReason, 'S', 'I'),
		OutageReason = FaultReason
UPDATE	FaultReport2013
SET		FaultReason = REPLACE(OutageReason, 'S', 'I'),
		OutageReason = FaultReason
		
UPDATE	MVOutageReport
SET		FaultReason = REPLACE(OutageReason, 'S', 'I'),
		OutageReason = FaultReason,
		MaintOutageReason = REPLACE(MaintOutageReason, 'S', 'P')
UPDATE	MVOutageReport2013
SET		FaultReason = REPLACE(OutageReason, 'S', 'I'),
		OutageReason = FaultReason,
		MaintOutageReason = REPLACE(MaintOutageReason, 'S', 'P')
UPDATE	MVOutageReport2012
SET		FaultReason = REPLACE(OutageReason, 'S', 'I'),
		OutageReason = FaultReason,
		MaintOutageReason = REPLACE(MaintOutageReason, 'S', 'P')		
UPDATE	LVOutageReport
SET		LVFaultReason = REPLACE(LVOutageReason, 'S', 'I'),
		LVOutageReason = LVFaultReason
UPDATE	LVOutageReport2013
SET		LVFaultReason = REPLACE(LVOutageReason, 'S', 'I'),
		LVOutageReason = LVFaultReason
UPDATE	LVOutageReport2012
SET		LVFaultReason = REPLACE(LVOutageReason, 'S', 'I'),
		LVOutageReason = LVFaultReason

UPDATE CODEINFO SET CODE = REPLACE(CODE, 'S', 'I') WHERE INFOTYPE = 'FaultReason'
UPDATE CODEINFO SET CODE = REPLACE(CODE, 'S', 'I') WHERE INFOTYPE = 'LVFaultReason'
UPDATE CODEINFO SET CODE = REPLACE(CODE, 'S', 'P') WHERE INFOTYPE = 'MaintOutageReason'
UPDATE CODEINFO SET CODE = REPLACE(CODE, 'S', 'P') WHERE INFOTYPE = 'LVMaintOutageReason'

SELECT * FROM CODEINFO WHERE INFOTYPE LIKE '%Reason'
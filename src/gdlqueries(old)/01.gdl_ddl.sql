-------------------------20130626 -------------------------------------------------

--------------------------------------------------
-- Create Table dbo.gdlCodeMun
--------------------------------------------------
Create table dbo.gdlCodeMun (
    IDProv                         NVARCHAR(2)                    not null,
    IDMun                          NVARCHAR(3)                    not null,
    NameMun                        NVARCHAR(50)                   not null,
    CONSTRAINT [PK__gdlCodeM__941AE34A3A379A64] PRIMARY KEY CLUSTERED 
(
	[IDProv] ASC,
	[IDMun] ASC
)) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlCodeProv
--------------------------------------------------
Create table dbo.gdlCodeProv (
    IDProv                         NVARCHAR(2)                    PRIMARY KEY,
    NameProv                       NVARCHAR(32)                   null) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlMINECO
--------------------------------------------------
Create table dbo.gdlMINECO (
    ID                             NVARCHAR(3)                    PRIMARY KEY,
    Name                           NVARCHAR(64)                   not null,
    Region                         NVARCHAR(32)                   not null,
    DMS                            NVARCHAR(1)                    null,
    City                           VARCHAR(32)                    null,
    Zone                           NVARCHAR(2)                    null) 
on [PRIMARY] ;

--------------------------------------------------
-- Create Table dbo.gdlMunType
--------------------------------------------------
Create table dbo.gdlMunType (
    UTILITY                        NVARCHAR(3)                    not null,
    MUNI                           NVARCHAR(3)                    not null,
    TYPE                           NVARCHAR(1)                    null,
    PROVINCE                       NVARCHAR(2)                    not null) 

--------------------------------------------------
-- Create Table dbo.gdlReportCompany
--------------------------------------------------
Create table dbo.gdlReportCompany (
	NUMBERID					   int IDENTITY (1, 1)              PRIMARY KEY,
    USERID                         NVARCHAR(4000)                   not null,
    COMPANY                        NVARCHAR(3)                    not null) 
on [PRIMARY] ;


CREATE TABLE [dbo].[gdlMvCustLocation](
	[CUSTOMER_NODE] [nvarchar](32) PRIMARY KEY,
	[UTILITY] [nvarchar](3) NOT NULL,
	[MUNI] [nvarchar](3) NOT NULL,
	[PROVINCE] [nvarchar](2) NOT NULL
)

CREATE TABLE [dbo].[gdlCTConnections](
	[CT_NODECODE] [nvarchar](14) NOT NULL,
	[LI_IdInst] [nvarchar](50) NULL
)

CREATE TABLE [dbo].[gdlXYDMS](
	[CodigoAcometida] [nvarchar](50) NOT NULL,
	[XDMS] [int] NULL,
	[YDMS] [int] NULL
)	
CREATE TABLE [dbo].[gdlOPC](
	[MINECO] [varchar](3) NULL,
	[TYPE] [varchar](2) NULL,
	[OPC_CODE] [varchar](30) NOT NULL
)
CREATE TABLE [dbo].[gdlCT](
	[MINECO] [varchar](3) NULL,
	[DMS_Name] [varchar](50) NULL,
	[SCADA_Name] [varchar](50) NULL,
	[CT_Name] [varchar](50) NULL
) 


CREATE TABLE [dbo].[gdlExtension](
	[MINECO] [nchar](3) NULL,
	[X1] [int] NULL,
	[Y1] [int] NULL,
	[X2] [int] NULL,
	[Y2] [int] NULL
) ON [PRIMARY]

CREATE TABLE gdlLV_NODE_BTVano (
                        NODECODE nvarchar(14),
                        IdBTVano nvarchar(50)    )
                        
GO

CREATE TABLE [dbo].[gdlCeldas](
	[CodigoCT] [nchar](30) NOT NULL,
	[CodigoLinea] [nchar](10) NULL,
	[X] [int] NULL,
	[Y] [int] NULL,
	[Nombre] [nvarchar](50) NULL,
	[IdInstLinea] [nvarchar](50) NULL,
	[IdTipoCelda] [int] NULL,
	[IdInst] [nvarchar](50) NULL,
	[IdTransformador] [nvarchar](50) NULL,
	[DisyuntorFusible] [int] NULL,
	[Modelo] [int] NULL,
	[Transformador] [int] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[gdlLVConductor](
	[CodeTraza] [nchar](32) NULL,
	[CodeDMS] [nchar](32) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[gdlOrdenCeldas](
	[CodigoCT] [nchar](10) NOT NULL,
	[Posicion] [int] NOT NULL,
	[CodigoLinea] [nchar](10) NULL,
	[Transformador] [int] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[gdlTrazaImportUser](
	[usuario] [nchar](7) NOT NULL,
	[password] [nchar](32) NOT NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[gdlValidatedReports](
	[id] [int] NOT NULL,
	[archivo] [nchar](20) NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MV_LV_SUBSTATION]
	ADD [MUN] [nvarchar](3) NULL,
	[COMPANY] [nvarchar](3) NULL,
	[PROVINCE] [nvarchar](3) NULL,
	 [PROPIOS] [bit] NULL 
	 
GO

ALTER TABLE CUSTOMER ADD CONTRACTED_POWER [float] NULL

GO


CREATE TABLE [dbo].[FaultReport2012](
	[NUMBER] [decimal](9, 0) NOT NULL,
	[Time] [datetime] NULL,
	[Company] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Substation] [nvarchar](50) NULL,
	[Feeder] [nvarchar](50) NULL,
	[OutageReason] [nvarchar](50) NULL,
	[FaultReason] [nvarchar](50) NULL,
	[FaultLocation] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Operator] [nvarchar](50) NULL,
	[Operation] [decimal](9, 0) NULL,
	[Isolation] [decimal](9, 0) NULL,
	[FaultType] [decimal](9, 0) NULL,
	[OtherFault] [nvarchar](50) NULL,
	[Ik] [decimal](9, 0) NULL,
	[PhaseCnt] [decimal](9, 0) NULL,
	[SearchTimeMen] [float] NULL,
	[SearchTimeFault] [float] NULL,
	[RepairingTime] [float] NULL,
	[WorkingTime] [float] NULL,
	[Temperature] [float] NULL,
	[Wind] [nvarchar](50) NULL,
	[Humidity] [nvarchar](50) NULL,
	[StartingTime] [datetime] NULL,
	[EndingTime] [datetime] NULL,
	[Duration] [nvarchar](50) NULL,
	[LVNet] [decimal](9, 0) NULL,
	[LVNeth] [float] NULL,
	[Cust] [decimal](9, 0) NULL,
	[Custh] [float] NULL,
	[NDE] [float] NULL,
	[AdditionalData] [nvarchar](1024) NULL,
	[CABLINGRATE] [decimal](9, 0) NULL,
	[AutoCalculation] [decimal](9, 0) NOT NULL,
	[VALIDATED] [bit] NOT NULL,
 CONSTRAINT [PK_FaultReport2012] PRIMARY KEY CLUSTERED 
(
	[NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [NUMBER]
GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [Operation]
GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [Isolation]
GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [FaultType]
GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [Ik]
GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [PhaseCnt]
GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [SearchTimeMen]
GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [SearchTimeFault]
GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [RepairingTime]
GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [WorkingTime]
GO

ALTER TABLE [dbo].[FaultReport2012] ADD  DEFAULT ((0)) FOR [Temperature]
GO

CREATE TABLE [dbo].[FaultReport2013](
	[NUMBER] [decimal](9, 0) NOT NULL,
	[Time] [datetime] NULL,
	[Company] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Substation] [nvarchar](50) NULL,
	[Feeder] [nvarchar](50) NULL,
	[OutageReason] [nvarchar](50) NULL,
	[FaultReason] [nvarchar](50) NULL,
	[FaultLocation] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Operator] [nvarchar](50) NULL,
	[Operation] [decimal](9, 0) NULL,
	[Isolation] [decimal](9, 0) NULL,
	[FaultType] [decimal](9, 0) NULL,
	[OtherFault] [nvarchar](50) NULL,
	[Ik] [decimal](9, 0) NULL,
	[PhaseCnt] [decimal](9, 0) NULL,
	[SearchTimeMen] [float] NULL,
	[SearchTimeFault] [float] NULL,
	[RepairingTime] [float] NULL,
	[WorkingTime] [float] NULL,
	[Temperature] [float] NULL,
	[Wind] [nvarchar](50) NULL,
	[Humidity] [nvarchar](50) NULL,
	[StartingTime] [datetime] NULL,
	[EndingTime] [datetime] NULL,
	[Duration] [nvarchar](50) NULL,
	[LVNet] [decimal](9, 0) NULL,
	[LVNeth] [float] NULL,
	[Cust] [decimal](9, 0) NULL,
	[Custh] [float] NULL,
	[NDE] [float] NULL,
	[AdditionalData] [nvarchar](1024) NULL,
	[CABLINGRATE] [decimal](9, 0) NULL,
	[AutoCalculation] [decimal](9, 0) NOT NULL,
	[VALIDATED] [bit] NOT NULL,
 CONSTRAINT [PK_FaultReport2013] PRIMARY KEY CLUSTERED 
(
	[NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [NUMBER]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [Operation]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [Isolation]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [FaultType]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [Ik]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [PhaseCnt]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [SearchTimeMen]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [SearchTimeFault]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [RepairingTime]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [WorkingTime]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [Temperature]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [LVNet]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [LVNeth]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [Cust]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [Custh]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [NDE]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [CABLINGRATE]
GO

ALTER TABLE [dbo].[FaultReport2013] ADD  DEFAULT ((0)) FOR [AutoCalculation]
GO

CREATE TABLE [dbo].[MVOutageReport2012](
	[NUMBER] [decimal](9, 0) NOT NULL,
	[Type] [decimal](9, 0) NULL,
	[State] [decimal](9, 0) NULL,
	[Time] [datetime] NULL,
	[Company] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Substation] [nvarchar](50) NULL,
	[Feeder] [nvarchar](50) NULL,
	[OutageReason] [nvarchar](50) NULL,
	[FaultReason] [nvarchar](50) NULL,
	[FaultLocation] [nvarchar](50) NULL,
	[MaintOutageReason] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Operator] [nvarchar](50) NULL,
	[Operation] [decimal](9, 0) NULL,
	[Isolation] [decimal](9, 0) NULL,
	[FaultType] [decimal](9, 0) NULL,
	[OtherFault] [nvarchar](50) NULL,
	[Ik] [decimal](9, 0) NULL,
	[PhaseCnt] [decimal](9, 0) NULL,
	[SearchTimeMen] [float] NULL,
	[SearchTimeFault] [float] NULL,
	[RepairingTime] [float] NULL,
	[WorkingTime] [float] NULL,
	[Temperature] [float] NULL,
	[Wind] [nvarchar](50) NULL,
	[Humidity] [nvarchar](50) NULL,
	[StartingTime] [datetime] NULL,
	[EndingTime] [datetime] NULL,
	[Duration] [nvarchar](50) NULL,
	[LVNet] [decimal](9, 0) NULL,
	[LVNeth] [float] NULL,
	[Cust] [decimal](9, 0) NULL,
	[Custh] [float] NULL,
	[NDE] [float] NULL,
	[AdditionalData] [nvarchar](1024) NULL,
	[AutoCalculation] [decimal](9, 0) NOT NULL,
	[VALIDATED] [bit] NOT NULL,
 CONSTRAINT [PK_MVOutageReport2012] PRIMARY KEY CLUSTERED 
(
	[NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MVOutageReport2012] ADD  DEFAULT ((0)) FOR [NUMBER]
GO

ALTER TABLE [dbo].[MVOutageReport2012] ADD  DEFAULT ((0)) FOR [Type]
GO

ALTER TABLE [dbo].[MVOutageReport2012] ADD  DEFAULT ((0)) FOR [State]
GO

ALTER TABLE [dbo].[MVOutageReport2012] ADD  DEFAULT ((0)) FOR [Operation]
GO

ALTER TABLE [dbo].[MVOutageReport2012] ADD  DEFAULT ((0)) FOR [Isolation]
GO

ALTER TABLE [dbo].[MVOutageReport2012] ADD  DEFAULT ((0)) FOR [FaultType]
GO

CREATE TABLE [dbo].[MVOutageReport2013](
	[NUMBER] [decimal](9, 0) NOT NULL,
	[Type] [decimal](9, 0) NULL,
	[State] [decimal](9, 0) NULL,
	[Time] [datetime] NULL,
	[Company] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Substation] [nvarchar](50) NULL,
	[Feeder] [nvarchar](50) NULL,
	[OutageReason] [nvarchar](50) NULL,
	[FaultReason] [nvarchar](50) NULL,
	[FaultLocation] [nvarchar](50) NULL,
	[MaintOutageReason] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Operator] [nvarchar](50) NULL,
	[Operation] [decimal](9, 0) NULL,
	[Isolation] [decimal](9, 0) NULL,
	[FaultType] [decimal](9, 0) NULL,
	[OtherFault] [nvarchar](50) NULL,
	[Ik] [decimal](9, 0) NULL,
	[PhaseCnt] [decimal](9, 0) NULL,
	[SearchTimeMen] [float] NULL,
	[SearchTimeFault] [float] NULL,
	[RepairingTime] [float] NULL,
	[WorkingTime] [float] NULL,
	[Temperature] [float] NULL,
	[Wind] [nvarchar](50) NULL,
	[Humidity] [nvarchar](50) NULL,
	[StartingTime] [datetime] NULL,
	[EndingTime] [datetime] NULL,
	[Duration] [nvarchar](50) NULL,
	[LVNet] [decimal](9, 0) NULL,
	[LVNeth] [float] NULL,
	[Cust] [decimal](9, 0) NULL,
	[Custh] [float] NULL,
	[NDE] [float] NULL,
	[AdditionalData] [nvarchar](1024) NULL,
	[AutoCalculation] [decimal](9, 0) NOT NULL,
	[VALIDATED] [bit] NOT NULL,
 CONSTRAINT [PK_MVOutageReport2013] PRIMARY KEY CLUSTERED 
(
	[NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MVOutageReport2013] ADD  DEFAULT ((0)) FOR [NUMBER]
GO

ALTER TABLE [dbo].[MVOutageReport2013] ADD  DEFAULT ((0)) FOR [Type]
GO

ALTER TABLE [dbo].[MVOutageReport2013] ADD  DEFAULT ((0)) FOR [State]
GO

ALTER TABLE [dbo].[MVOutageReport2013] ADD  DEFAULT ((0)) FOR [Operation]
GO

ALTER TABLE [dbo].[MVOutageReport2013] ADD  DEFAULT ((0)) FOR [Isolation]
GO

ALTER TABLE [dbo].[MVOutageReport2013] ADD  DEFAULT ((0)) FOR [FaultType]
GO

CREATE TABLE [dbo].[LVOutageReport2012](
	[NUMBER] [decimal](9, 0) NOT NULL,
	[State] [decimal](9, 0) NULL,
	[Type] [decimal](9, 0) NULL,
	[Time] [datetime] NULL,
	[Company] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[OutageReason] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Operator] [nvarchar](50) NULL,
	[StartingTime] [datetime] NULL,
	[EndingTime] [datetime] NULL,
	[Duration] [nvarchar](50) NULL,
	[LVNet] [decimal](9, 0) NULL,
	[LVNeth] [float] NULL,
	[Cust] [decimal](9, 0) NULL,
	[Custh] [float] NULL,
	[NDE] [float] NULL,
	[AdditionalData] [nvarchar](1024) NULL,
	[LVOutageReason] [nvarchar](3) NULL,
	[LVFaultReason] [nvarchar](3) NULL,
	[LVFaultLocation] [nvarchar](3) NULL,
	[LVFaultType] [nvarchar](3) NULL,
	[LVProtection] [nvarchar](3) NULL,
	[VALIDATED] [bit] NOT NULL,
 CONSTRAINT [PK_LVOutageReport2012] PRIMARY KEY CLUSTERED 
(
	[NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LVOutageReport2012] ADD  DEFAULT ((0)) FOR [NUMBER]
GO

ALTER TABLE [dbo].[LVOutageReport2012] ADD  DEFAULT ((0)) FOR [State]
GO

ALTER TABLE [dbo].[LVOutageReport2012] ADD  DEFAULT ((0)) FOR [Type]
GO

ALTER TABLE [dbo].[LVOutageReport2012] ADD  DEFAULT ((0)) FOR [LVNet]
GO

ALTER TABLE [dbo].[LVOutageReport2012] ADD  DEFAULT ((0)) FOR [LVNeth]
GO

ALTER TABLE [dbo].[LVOutageReport2012] ADD  DEFAULT ((0)) FOR [Cust]
GO

ALTER TABLE [dbo].[LVOutageReport2012] ADD  DEFAULT ((0)) FOR [Custh]
GO

ALTER TABLE [dbo].[LVOutageReport2012] ADD  DEFAULT ((0)) FOR [NDE]
GO

ALTER TABLE [dbo].[LVOutageReport2012] ADD  CONSTRAINT [df_LVOutageReport2012_Validated]  DEFAULT ((0)) FOR [VALIDATED]
GO


CREATE TABLE [dbo].[LVOutageReport2013](
	[NUMBER] [decimal](9, 0) NOT NULL,
	[State] [decimal](9, 0) NULL,
	[Type] [decimal](9, 0) NULL,
	[Time] [datetime] NULL,
	[Company] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[OutageReason] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Operator] [nvarchar](50) NULL,
	[StartingTime] [datetime] NULL,
	[EndingTime] [datetime] NULL,
	[Duration] [nvarchar](50) NULL,
	[LVNet] [decimal](9, 0) NULL,
	[LVNeth] [float] NULL,
	[Cust] [decimal](9, 0) NULL,
	[Custh] [float] NULL,
	[NDE] [float] NULL,
	[AdditionalData] [nvarchar](1024) NULL,
	[LVOutageReason] [nvarchar](3) NULL,
	[LVFaultReason] [nvarchar](3) NULL,
	[LVFaultLocation] [nvarchar](3) NULL,
	[LVFaultType] [nvarchar](3) NULL,
	[LVProtection] [nvarchar](3) NULL,
	[VALIDATED] [bit] NOT NULL,
 CONSTRAINT [PK_LVOutageReport2013] PRIMARY KEY CLUSTERED 
(
	[NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LVOutageReport2013] ADD  DEFAULT ((0)) FOR [NUMBER]
GO

ALTER TABLE [dbo].[LVOutageReport2013] ADD  DEFAULT ((0)) FOR [State]
GO

ALTER TABLE [dbo].[LVOutageReport2013] ADD  DEFAULT ((0)) FOR [Type]
GO

ALTER TABLE [dbo].[LVOutageReport2013] ADD  DEFAULT ((0)) FOR [LVNet]
GO

ALTER TABLE [dbo].[LVOutageReport2013] ADD  DEFAULT ((0)) FOR [LVNeth]
GO

ALTER TABLE [dbo].[LVOutageReport2013] ADD  DEFAULT ((0)) FOR [Cust]
GO

ALTER TABLE [dbo].[LVOutageReport2013] ADD  DEFAULT ((0)) FOR [Custh]
GO

ALTER TABLE [dbo].[LVOutageReport2013] ADD  DEFAULT ((0)) FOR [NDE]
GO

ALTER TABLE [dbo].[LVOutageReport2013] ADD  CONSTRAINT [df_LVOutageReport2013_Validated]  DEFAULT ((0)) FOR [VALIDATED]
GO



